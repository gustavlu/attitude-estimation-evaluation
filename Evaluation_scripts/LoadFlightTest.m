function [ EKF, IMU, AUX, px4, optitrack, control, testdir, isel, i_std_acc, i_mag_ref,...
    tstart, tstop] = LoadFlightTest( testcase, px4_in, evaldir, EKF)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
px4 = px4_in;
control = px4_in;
optitrack = px4_in;
isel = 1:numel(px4_in.time);
i_std_acc = isel;
i_mag_ref = isel;
tstart = 0;
tstop = px4_in.time(end);
testdir = cd;

switch testcase
    case -1
        isel = 1:numel(px4.time);
        %         isel = 4800:5200;
        i_std_acc = 4800:5200;
        testdir = fullfile(evaldir,'');
    case 0
        isel = 1:numel(px4.time);
        %         isel = 4800:5200;
        i_std_acc = 4800:5200;
        testdir = fullfile(evaldir,'calib_mag');
    case 20 % Cross, no disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_3_sans_perturbations.txt';
        [ IMU, AUX ] = mk7_pretreat_csv( fullfile(evaldir, evalfolder), evalfile, 2 );
        i_std_acc = 4000:5000;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [-4; -7.35; 8.55]/64.5*pi/180;
        tstart = 27; % Log time
        tstop = 49; 
    case 21 % Cross, no disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_4.txt';
        [ IMU, AUX ] = mk7_pretreat_csv(fullfile(evaldir,  evalfolder), evalfile, 2 );
        i_std_acc = 5000:6000;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [-4; -7.35; 8.55]/64.5*pi/180;
        tstart = 27; % Log time
        tstop = 49; 
    case 22 % Cross, no disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_5.txt';
        [ IMU, AUX ] = mk7_pretreat_csv( fullfile(evaldir, evalfolder), evalfile, 2 );
        i_std_acc = 3500:5000;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [-4; -7.35; 8.55]/64.5*pi/180;
        tstart = 27; % Log time
        tstop = 49; 
    case 23% Cross, 15A disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_6_avec_perturbations.txt';
        fullfile(evaldir, evalfolder)
        [ IMU, AUX ] = mk7_pretreat_csv( fullfile(evaldir, evalfolder), evalfile, 2 );
        i_std_acc = 4900:5300;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [-4; -7.35; 8.55]/64.5*pi/180;
        tstart = 27; % Log time
        tstop = 49;  %59
    case 24 % Cross, 15A disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_7.txt';
        [ IMU, AUX ] = mk7_pretreat_csv( fullfile(evaldir, evalfolder), evalfile, 2 );
        i_std_acc = 3500:5000;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [-4; -7.35; 8.55]/64.5*pi/180;
        tstart = 27; % Log time
        tstop = 49;  %59
    case 25 % Cross, 15A disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_8.txt';
        [ IMU, AUX ] = mk7_pretreat_csv( fullfile(evaldir, evalfolder), evalfile, 2 );
        i_std_acc = 3500:5000;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [-4; -7.35; 8.55]/64.5*pi/180;
        tstart = 27; % Log time 15
        tstop = 49; 
    case 26 % Slow, 15A disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_9_lent.txt';
        [ IMU, AUX ] = mk7_pretreat_csv( fullfile(evaldir, evalfolder), evalfile, 2 );
        i_std_acc = 8000:9000;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [15; -8; 24.05]/193*pi/180;
        tstart = 50; % Log time 38
        tstop = 175; 
    case 27 % Slow, 15A disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_10.txt';
        [ IMU, AUX ] = mk7_pretreat_csv(fullfile(evaldir, evalfolder), evalfile, 2 );
        i_std_acc = 8200:8600;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [-29; -16.3; 24.05]/196*pi/180;
        tstart = 50; % Log time 50
        tstop = 175; 
    case 28 % Slow, vertical 15A disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_11_lent_vertical.txt';
        [ IMU, AUX ] = mk7_pretreat_csv( fullfile(evaldir, evalfolder), evalfile, 2 );
        i_std_acc = 8200:8600;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [10; -8.9; 24.05]/196*pi/180;
        tstart = 20; % Log time
        tstop = 175; 
    case 29 % Fast, vertical 15A disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_12_rapide_vertical.txt';
        [ IMU, AUX ] = mk7_pretreat_csv( fullfile(evaldir, evalfolder), evalfile, 2 );
        i_std_acc = 3500:5000;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [58; -16.3; 24.05]/196*pi/180;
        tstart = 32; % Log time
        tstop = 55; 
    case 30 % Fast, vertical 15A disturbance
        evalfolder = 'Logs';
        evalfile = 'log_2018-08-31_13.txt';
        [ IMU, AUX ] = mk7_pretreat_csv(fullfile(evaldir, evalfolder), evalfile, 2 );
        i_std_acc = 3500:5000;
        i_mag_ref = 1:2000;
        EKF.x0(5:7) = [58; -16.3; 24.05]/196*pi/180;
        tstart = 23; % Log time
        tstop = 51; 
end

end


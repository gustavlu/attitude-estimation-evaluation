% clc;
% clear all;
% close all;

simu = 1;
% universal constants
g = 9.81;           % [m.s^-2] gravition constant
% mag_ref = [-16.9; -28.55; -35.3];   % [uT] Toulouse
mag_ref = [-0.169; -0.2855; 0.353];   % [G] Toulouse
h0 = 150; % [m] Toulouse
deg2rad = pi/180;
rad2deg = 180/pi;
DISA = 0;

% structure multiRotor (Uav)
rotorFault = [1 1 1 1 1 1]'; %  [1 0 1 1 0 1]';

% Uav:
% theta0 = 23 deg, angle of attack at the rotor shaft
% thetaT = 5.7 deg, angle of attack at the blade tip

% Uav.nbRotor    = 6;        % [-]       Number of rotor of the multirotor
% Uav.m          = 0.36;     % [kg]      Mass of the multirotor
% Uav.hG         = 0.00;     % [m]       orthogonal distance between rotors' plane and the center of gravity
% Uav.l          = 0.0;      % [m]       half span of the central frame
% Uav.L          = 0.1;      % [m]       span of an arm,
% Uav.hM         = 0.02;     % [m]       height of a motor,
% Uav.alpha      = 0;        % [deg]     tilt angle of an arm
% Uav.gamma      = 0;        % [deg]     tilt angle (absolute value) of the motor due to the wedge (for rotor 1)
% Uav.beta1      = -45;      % [deg]     yaw angle of the arm 1 in the body fixed frame [rad]
% Uav.rotorDirection = [-1, 1, -1, 1, -1, 1];    % [-1, 1]   direction of rotation of the rotor 1, ( -1: ClockWise (CW), 1: CounterClockWise (CCW) )
% Uav.Kdrag      = [ 0.05 , 0.181, 0;...
%     -0.157, 0.183, 0;...
%     0  ,   0  , .0];   % Kzz replaced by rotor cT loss deduced from vertical speed
% Uav.I          = diag([5.5e-4+4.67e-4, 5.5e-4+4.67e-4, 9.5e-4+9.9e-4]);
% 
% Uav.propellers.cT      = 2.55e-7*g;% [N.s^-2.rad^2]   thrust coefficient (T = cT*omega_r^2)
% Uav.propellers.cQ      = 1.14e-8*g;% [N.s^-2.rad^2]   drag coefficient   (D = cQ*omega_r^2)
% 
% Uav.nbRotor    = 6;        % [-]       Number of rotor of the multirotor
% Uav.m          = 0.36;     % [kg]      Mass of the multirotor
% Uav.hG         = 0.00;     % [m]       orthogonal distance between rotors' plane and the center of gravity 
% Uav.l          = 0.0;      % [m]       half span of the central frame
% Uav.L          = 0.1;      % [m]       span of an arm,
% Uav.hM         = 0.02*0;   % [m]       height of a motor,
% Uav.alpha      = -9;       % [deg]     tilt angle of an arm
% Uav.gamma      = 1;        % [deg]     tilt angle (absolute value) of the motor due to the wedge (for rotor 1)
% Uav.beta1      = -30;      % [deg]     yaw angle of the arm 1 in the body fixed frame [rad]
% Uav.rotorDirection = [-1, 1, -1, 1, -1, 1]';    % [-1, 1]   direction of rotation of the rotor 1, ( -1: ClockWise (CW), 1: CounterClockWise (CCW) )
% Uav.Kdrag      = [ 0.05 ,  0  , 0;...
%                      0  , 0.05, 0;...
%                      0  ,  0  , 0];   % Kzz replaced by rotor cT loss deduced from vertical speed 
% Uav.I          = diag([5.5e-4+4.67e-4, 5.5e-4+4.67e-4, 9.5e-4+9.9e-4]);
% 
% Uav.propellers.cTm      = 2.55e-7;% [N.s^-2.rad^2]   thrust coefficient (T = cT*omega_r^2)
% Uav.propellers.cQm      = 1.14e-8;% [N.s^-2.rad^2]   drag coefficient   (D = cQ*omega_r^2)
% Uav.propellers.theta0  = 24;       % [deg]             angle of attack of the propellers at the rotor shaft
% Uav.propellers.theta1  = -10;      % [deg]             equivalent twist angle
% Uav.propellers.c       = 0.016;    % [m]               mean chord of the propeller
% Uav.propellers.r       = 0.06;     % [m]               radius of the propeller
% Uav.propellers.a       = 6;        %                   lift slop coefficient
% Uav.propellers.I       = slabInertia(Uav.propellers.r, Uav.propellers.c, 0.002, 0.017);  % inertia matrix of the propeller
% 
% Uav.motors.tau         = 108e-3/4.8; %
% Uav.motors.omega_max   = 2*pi*120;   % [rad.s^-1]    maximum pulsation speed of motors (2*pi*fmax)
% Uav.motors.omega_min   = 2*pi*40;    % [rad.s^-1]    minimum pulsation speed of motors (2*pi*fmax)
% Uav.motors.m           = 10e-3;      % [kg]          motor mass
% Uav.motors.r           = 0.01;       % [m]           motor radius
% Uav.motors.I           = cylinderInertia(Uav.motors.r, Uav.hM, Uav.motors.m);

% Uav = preprocessMultiRotor(Uav);
% Uav = preprocessMultiRotor(Uav, rotorFault);

% structure Sensors
%   IMU
Sensors.IMU.acc.bias = [0;0;0];                     % [m/s^2] bias
Sensors.IMU.acc.sigma =  g/2*[1/3;1/3;1/3];        % [m/s^2] noise std
Sensors.IMU.acc.noise = 1;                          % [0, 1] Noise activated?
Sensors.IMU.acc.fs = 200;                           % [Hz] sampling frequency
Sensors.IMU.gyr.bias = 0*[0.2;0.1;-0.3]*pi/180;       % [rad/s] bias
Sensors.IMU.gyr.sigma = [20/3;20/3;20/3]*pi/180;    % [rad/s] noise std
Sensors.IMU.gyr.noise = 1;                          % [0, 1] Noise activated?
Sensors.IMU.gyr.fs = 200;                           % [Hz] sampling frequency
Sensors.IMU.mag.bias = [0;0;0];                     % [uT] bias
Sensors.IMU.mag.sigma = 3*[0.1;0.1;0.1]*norm(mag_ref);              % [uT] noise std
Sensors.IMU.mag.noise = 1;                          % [0, 1] Noise activated?
Sensors.IMU.mag.fs = 50;                            % [Hz] sampling frequency
Sensors.baro.bias = 0;                              % [Pa] bias
Sensors.baro.sigma = 3.6/3;                           % [uT] noise std
Sensors.baro.noise = 1;                             % [0, 1] Noise activated?
Sensors.baro.fs = 50;                               % [Hz] sampling frequency

%   Tachymeters
Sensors.tachys.sigma = 2/3;                         % [Hz]
Sensors.tachys.fs = 400;                            % [Hz] sampling frequency
Sensors.tachys.noise = 1;
%   GPS
Sensors.GPS.sigmaP = 2/3;                           % [m]
Sensors.GPS.sigmaS = 3/3;                           % [m/s]
Sensors.GPS.fs = 10;                                % [Hz] sampling frequency
Sensors.GPS.noise = 1;
%   MotionCapture
Sensors.MotionCapture.sigmaP = 2e-3/3;              % [m]
Sensors.MotionCapture.sigmaS = 2e-3/3*100;          % [m/s]
Sensors.MotionCapture.sigmaEuler = 0.5/3*deg2rad;   % [rad]
Sensors.MotionCapture.fs = 100;                     % [Hz] sampling frequency
Sensors.MotionCapture.noise = 1;
% 
% % create the corresponding Simulink bus object
% BusInfo = struct2BusObject(Uav, 'UavBus');
% BusInfo = struct2BusObject(Sensors, 'SensorsBus');

%% saturations and control parameters

% Trep_rotors = Uav.motors.tau; % trep attitude loop
% Trep_Omega  = 0.2;
% Trep_Euler  = 0.5;
% Trep_speed  = 1.5;
% Trep_position = 2;
% 
% Thrust_sat = 0.9*Uav.Thrust_sat;
% Torques_sat = 0.9*Uav.Torques_sat;
% 
% EulerMax = [30, 30, 360*10]*deg2rad;
% EulerMin = [-30, -30, -360*10]*deg2rad;
% OmegaMax = [(EulerMax(1)-EulerMin(1))/(Trep_Euler/3);...
%             (EulerMax(2)-EulerMin(2))/(Trep_Euler/3);...
%             200*deg2rad];
% OmegaMin = [-OmegaMax(1); -OmegaMax(2); -OmegaMax(3)];
% dOmegaMax = [OmegaMax(1)/(Trep_Omega/3); OmegaMax(2)/(Trep_Omega/3); OmegaMax(3)/(Trep_Omega/3)];
% dOmegaMin = -[OmegaMax(1)/(Trep_Omega/3); OmegaMax(2)/(Trep_Omega/3); OmegaMax(3)/(Trep_Omega/3)];
% 
% Xmax = [50;50;5];
% Xmin = [-50;-50;0];
% Vmax = [4;4;3];
% Vmin = [-4;-4;-3];
% minAccAngle = min( abs([EulerMax(1) EulerMin(1) EulerMax(2) EulerMin(2)]) );
% AccMax = [g*tan(minAccAngle); g*tan(minAccAngle); max(Thrust_sat)/Uav.m*cos(EulerMax(1))*cos(EulerMax(2))-g];
% AccMin = [-g*tan(minAccAngle); -g*tan(minAccAngle); min(Thrust_sat)/Uav.m-g];
% JerkMax = [AccMax(1)/(Trep_Euler/3);...
%            AccMax(2)/(Trep_Euler/3);...
%            AccMax(3)/(Trep_rotors/3)];
% JerkMin = [AccMin(1)/(Trep_Euler/3);...
%            AccMin(2)/(Trep_Euler/3);...
%            AccMin(3)/(Trep_rotors/3)];
% SnapMax = [JerkMax(1)/(Trep_Omega/3);...
%            JerkMax(2)/(Trep_Omega/3);...
%            JerkMax(3)/(Trep_rotors/3)];
% SnapMin = [JerkMin(1)/(Trep_Omega/3);...
%            JerkMin(2)/(Trep_Omega/3);...
%            JerkMin(3)/(Trep_rotors/3)];
%        
% phi_hat_0 = 0;
% theta_hat_0 = 0;
% psi_hat_0 = 0;
% 
% landingHeight = 0.05;   % [m] to reset integrators, psi_offset, etc. when landed..
% 
% trep_speedLoop = 1.5;
% trep_positionLoop = 5;
% trep_OmegaLoop = 0.2;
% trep_attitudeLoop = 0.5;
% 
% Position_0 = [0;0;0];
% Velocity_0 = [0;0;0];
% Attitude_0 = [0;0;0]*pi/180;
% 
% RCB_sampleTime = 1/400;
% Ts_positionLoop = 1/100;
Ts_attitudeLoop = 1/400;
Ts_attitude_loop = Ts_attitudeLoop;

%% Compute control laws
% attitudeLoop_CTL;

%% EKF-IMU init - References and covariance modulation 
EKF.mag_ref = mag_ref;
EKF.gvec = [0;0;-g];
k_cov = 1;
EKF.kcov_perf = 1;
EKF.kcov_mag_perf = 1;
EKF.kcov = 1;
EKF.kcov_mag = 1;

%% EKF-IMU init - Init covariance and state
sigma_acc = Sensors.IMU.acc.sigma;
sigma_mag = Sensors.IMU.mag.sigma;
sigma_gyr = Sensors.IMU.gyr.sigma; sp = sigma_gyr(1); sq = sigma_gyr(2); sr = sigma_gyr(3);
sigma_vgps = ones(3,1)*Sensors.GPS.sigmaS;
sigma_pgps = ones(3,1)*Sensors.GPS.sigmaP;
sigma_baro = Sensors.baro.sigma;
bGyr_max = 0.01*deg2rad; % Maximum angular variation over T
T_bGyr = 1; % [s]
bAcc_max = 0.5;
T_bAcc = 60; % [s]

EKF.cov.gyro = k_cov*sigma_gyr.^2;
EKF.cov.acc = k_cov*sigma_acc.^2;
EKF.cov.bgyro = (bGyr_max*[1;1;1]/3/sqrt(T_bGyr)).^2;
EKF.cov.bacc = (bAcc_max*[1;1;1]/3/sqrt(T_bAcc)).^2;

EKF.Ra = k_cov*diag(sigma_acc.^2);
EKF.Rm = k_cov*diag(sigma_mag.^2);
EKF.Rv = k_cov*diag(sigma_vgps.^2);
EKF.Rp = k_cov*diag(sigma_pgps.^2);
EKF.Rb = k_cov*diag(sigma_baro.^2);

% euler0 = [90;90;180]*pi/180; % Extreme init
euler0 = [0;0;0]*pi/180;
euler0_cov = [90/3 90/3 90/3]*pi/180;
EKF.P0 = blkdiag(EulerCov2QuaternionCov(euler0, (diag(euler0_cov)).^2),...
    10*diag(EKF.cov.bgyro), diag(sigma_vgps).^2, diag(sigma_pgps).^2, diag(EKF.cov.gyro));
% [qhat0; bwhat0; Vhat0, Phat0; b]
EKF.x0 = [Euler2Quaternion(euler0(1),euler0(2),euler0(3));zeros(3,1);zeros(3,1);zeros(3,1);zeros(3,1)];

EKF.sigmax0 = [euler0_cov'; EKF.cov.bgyro; sigma_vgps; sigma_pgps; EKF.cov.gyro];
% Barometric model
p0 = 101325; % ISA SL pressure
L = 0.0065; % Temperature lapse rate
R0 = 8.31447; % Universal gas constant
g = 9.81; % g, just g
M = 0.0289644; % Molar mass, dry air
T0 = 288.15 + DISA; % ISA SL temperature
EKF.atmos.K1 = L/T0;
EKF.atmos.K2 = g*M/(R0*L);
EKF.atmos.p0 = p0;

nx = numel(EKF.x0);
nx_att = 7; % Attitude state (Q; b_gyro)
%% EKF Performance model parameters
% First order perf. model
tau_ff = 1; 
EKF.accest.k_ff = 1/tau_ff;
tau_fb_acc = 5;
EKF.accest.tau = tau_fb_acc;
EKF.accest.k_fb = 1/tau_fb_acc;

nx_att_acc = 10;

tau_fb_mag = 5;
EKF.magest.tau = tau_fb_mag;
EKF.magest.k_fb = 1/tau_fb_mag;

% Lateral acceleration test
p_fa = 1-0.9; % ~2 sigma
sd = Confidence_Set(1-p_fa,2);
l_det_acc_2 = sd^2;
sd = Confidence_Set(1-p_fa,3);
l_det_acc_3 = sd^2;
sd = Confidence_Set(1-p_fa,20);
l_det_acc_20 = sd^2;
sd = Confidence_Set(1-p_fa,30);
l_det_acc_30 = sd^2;
p_fa = 10e-6;
sd = Confidence_Set(1-p_fa,3);
l_det_acc_3_10em6 = sd^2;
sd = Confidence_Set(1-p_fa,2);
l_det_acc_2_10em6 = sd^2;

EKF.accest.l_det_2 = l_det_acc_2;
EKF.accest.l_det_3 = l_det_acc_3;
EKF.accest.l_det_20 = l_det_acc_20;
EKF.accest.l_det_30 = l_det_acc_30;
EKF.accest.l_det_2_10em6 = l_det_acc_2_10em6;
EKF.accest.l_det_3_10em6 = l_det_acc_3_10em6;

% Chi2 threshold for dim = 1
p_fa = 10e-6;
sd = Confidence_Set(1-p_fa,1);
EKF.stats.l_det_1_10em6 = sd^2;
p_fa = 10e-3;
sd = Confidence_Set(1-p_fa,1);
EKF.stats.l_det_1_10em3 = sd^2;
p_fa = 1-0.9967;
sd = Confidence_Set(1-p_fa,1);
EKF.stats.l_det_1_3sig = sd^2;

% Chi2 threshold for dim = 2
p_fa = 10e-6;
sd = Confidence_Set(1-p_fa,2);
EKF.stats.l_det_2_10em6 = sd^2;
p_fa = 10e-3;
sd = Confidence_Set(1-p_fa,2);
EKF.stats.l_det_2_10em3 = sd^2;
p_fa = 1-0.9967;
sd = Confidence_Set(1-p_fa,2);
EKF.stats.l_det_2_3sig = sd^2;

% Chi2 threshold for dim = 3
p_fa = 10e-6;
sd = Confidence_Set(1-p_fa,3);
EKF.stats.l_det_3_10em6 = sd^2;
p_fa = 10e-3;
sd = Confidence_Set(1-p_fa,3);
EKF.stats.l_det_3_10em3 = sd^2;
p_fa = 10e-2;
sd = Confidence_Set(1-p_fa,3);
EKF.stats.l_det_3_10em2 = sd^2;
p_fa = 10e-1;
sd = Confidence_Set(1-p_fa,3);
EKF.stats.l_det_3_10em1 = sd^2;
p_fa = 1-0.999534;
sd = Confidence_Set(1-p_fa,3);
EKF.stats.l_det_3_4sig = sd^2;
p_fa = 1-0.9967;
sd = Confidence_Set(1-p_fa,3);
EKF.stats.l_det_3_3sig = sd^2;
p_fa = 1-0.954499;
sd = Confidence_Set(1-p_fa,3);
EKF.stats.l_det_3_2sig = sd^2;
p_fa = 1-0.682689;
sd = Confidence_Set(1-p_fa,3);
EKF.stats.l_det_3_1sig = sd^2;

% Chi2 threshold for dim = 9
p_fa = 10e-6;
sd = Confidence_Set(1-p_fa,9);
EKF.stats.l_det_9_10em6 = sd^2;
p_fa = 10e-3;
sd = Confidence_Set(1-p_fa,9);
EKF.stats.l_det_9_10em3 = sd^2;

% Chi2 threshold for dim = 30
p_fa = 10e-6;
sd = Confidence_Set(1-p_fa,30);
EKF.stats.l_det_30_10em6 = sd^2;
p_fa = 10e-3;
sd = Confidence_Set(1-p_fa,30);
EKF.stats.l_det_30_10em3 = sd^2;

% Magnetic deviation test
p_fa = 1-0.9967;
% p_fa = 1-0.9; % ~2 sigma
sd = Confidence_Set(1-p_fa,2);
l_det_mag_2 = sd^2;
sd = Confidence_Set(1-p_fa,3);
l_det_mag_3 = sd^2;
EKF.magest.l_det_2 = l_det_mag_2;
EKF.magest.l_det_3 = l_det_mag_3;

p_fa = 1-0.9967; % 3 sigma
sd = Confidence_Set(1-p_fa,1);
l_det_mag_iso = sd^2;
% l_det = 5e-6;
EKF.magest.l_det_iso = l_det_mag_iso;

% Second order perf. model
% p1 = 0.5*(-1-1i);
% p2 = 0.5*(-1+1i);

% p1 = -0.1;
% p2 = -0.1;
N = 250;
w0 = 1/(N*Ts_attitudeLoop);

p1 = -w0;
p2 = p1;

if imag(p1) == 0
    EKF.accest.zeta = 1;
else
    EKF.accest.zeta = cos(atan(real(p1)/imag(p1)));
end
EKF.accest.pulse = sqrt(p1*p2);

%% Fault tolerant EKF - health status params
EKF.health.nmax = 5; % Load factor acceleration cutoff 
EKF.health.eta_acc = 0.4; % Fraction of g for acceleration cutoff
EKF.health.eta_mag = 0.2; % Fraction of norm(mag_ref) for mag cutoff
EKF.health.eta_sigma = 0.5; % Fraction of 3 sigma of reference norm for cutoff

EKF.health.t_conf_low = 1;

%% EKF Saturated bias model parameters
Psat = 1 - 0.68; % Noise saturation probability
Qv = diag(EKF.cov.bgyro);

% [ F, kaw, Qd, bmax ] = SatBiasDiscreteF3( Psat, T, Qv, Ts_attitudeLoop)
nsig = 5; % Sigma for setting bmax
bmax = 3*pi/180;%nsig*sqrt(EKF.cov.bgyro(1)); % maximum 0.2 rad/s for each axis


rho = 10;   % Saturation level parameter
T = Ts_attitudeLoop; % Bias anti-windup time constant
%[ F, kaw, Qd ] = SatBiasDiscrete_bmax_rho( bmax, rho, Qv, Ts_attitudeLoop);
tleak = 1;
% [ F, kaw, Qd ] = SatBiasDiscrete_bmax_rho_tleak( bmax, rho, tleak, Qv, Ts_attitudeLoop)
%EKF.bsat.F = eye(3)*F;
EKF.bsat.tau = T;
EKF.bsat.kaw = 1-exp(-Ts_attitudeLoop/T);
EKF.bsat.kaw_sto = EKF.bsat.kaw;
EKF.bsat.Qd_l = Ts_attitudeLoop*Qv;
EKF.bsat.Qd = Qv;
EKF.bsat.bmax = bmax;

%% Decoupled EKF parameters
EKF.partq.taupsi = 10/3;
EKF.partq.taualpha = 10/3;
EKF.partq.taupsi_bias = 10/3*200/3;
EKF.partq.taualpha_bias = 10/3*200/3;
EKF.partq.dt_gain_mag = 1/Sensors.IMU.mag.fs;
EKF.partq.dt_gain_acc = 1/Sensors.IMU.acc.fs;

% BusInfo = struct2BusObject(EKF, 'EKFBus');
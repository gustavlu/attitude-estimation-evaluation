%  Plot Gyro, Acc, and Mag curves
indplot = find(t<tstop);
tplot = t(indplot);

adata = accm_out(:,indplot)';
gbody = acc_out(:,indplot)';
mdata = magm_out(:,indplot)';
mbody = mag_out(:,indplot)';
accdeltanorm = abs(norm_acc_out(indplot)-norm(EKF.gvec))';
magdeltanorm = abs(norm_mag_out(indplot)-norm(EKF.mag_ref))';
fonty_imu = 20;
n_down = 1;
Gplot = tf([1],[1 1]);
ssg = ss(Gplot);

acc_rejrate = sum(corr_status_out(1,acc_ok(indplot)>0)==0)/numel(corr_status_out(1,acc_ok(indplot)>0));
mag_rejrate = sum(corr_status_out(2,mag_ok(indplot)>0)==0)/numel(corr_status_out(1,mag_ok(indplot)>0));

if ~exist('tsim','var') && exist('tstop','var')
    tsim = tstop;
end

%%
figimuacc = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
sfh1 = subplot(511);
hold on, grid on, axis([0 tsim -15 15]),
plot(downsample(tplot,n_down), downsample(adata(:,1),n_down), 'Linewidth',1),
plot(downsample(tplot,n_down), downsample(gbody(:,1),n_down),'--', 'Linewidth',1),
%     plot(tplot, adata_diff(:,1), 'Linewidth',1),
ylabel('$a_{B,x}$','interpreter','latex','fontsize',fonty_imu)
h1 = legend({'$a_{m,x}$','$a_{ref,B,x}$'},'interpreter','latex','fontsize',fonty_imu,'location','Northeast');
sfh1.Position  = sfh1.Position + [0 0.05 -0.05 0];
set(gca,'xticklabel',[])
funFigureProperty(h1);

sfh2 = subplot(512); hold on, grid on, axis([0 tsim -15 15]),
plot(downsample(tplot,n_down), downsample(adata(:,2),n_down), 'Linewidth',1),
plot(downsample(tplot,n_down), downsample(gbody(:,2),n_down),'--', 'Linewidth',1),
%     plot(tplot, adata_diff(:,2), 'Linewidth',1),
ylabel('$a_{B,y}$','interpreter','latex','fontsize',fonty_imu)
h2 = legend({'$a_{m,y}$','$a_{ref,B,y}$'},'interpreter','latex','fontsize',fonty_imu,'location','Northeast');
sfh2.Position  = sfh2.Position + [0 0.05 -0.05 0];
set(gca,'xticklabel',[])
funFigureProperty(h2);

sfh3 = subplot(513); hold on, grid on, axis([0 tsim -25 5]),
plot(downsample(tplot,n_down), downsample(adata(:,3),n_down), 'Linewidth',1)
plot(downsample(tplot,n_down), downsample(gbody(:,3),n_down),'--', 'Linewidth',1)
%     plot(tplot, adata_diff(:,3), 'Linewidth',1)
ylabel('$a_{B,z}$','interpreter','latex','fontsize',fonty_imu)
h3 = legend({'$a_{m}$ $[m/s^2]$','$a_{ref,B}$ $[m/s^2]$'},'interpreter','latex','fontsize',fonty_imu,'location','Best');
sfh3.Position  = sfh3.Position + [0 0.05 -0.05 0];
set(gca,'xticklabel',[])
funFigureProperty(h3);

sfh4 = subplot(514); hold on, grid on, axis([0 tsim 0 20])
plot(downsample(tplot,n_down), downsample(accdeltanorm,n_down), 'Linewidth',1)
ylabel('$\epsilon_{|a|}$','interpreter','latex','fontsize',fonty_imu)
%xlabel('time [s]','interpreter','latex','fontsize',fonty_imu)
h4 = legend({'$||a_{m}|-|a_{ref}||$'},'interpreter','latex','fontsize',fonty_imu,'location','Best');
sfh4.Position  = sfh4.Position + [0 0.05 -0.05 0];
set(gca,'xticklabel',[])
funFigureProperty(h4);

sfh5 = subplot(515); hold on, grid on, axis([0 tsim -0.05 1.05])
plot(downsample(tplot,n_down), downsample(corr_status_out(1,indplot),n_down),'*', 'Linewidth',1)
plot(downsample(tplot,n_down), lsim(ss(Gplot),downsample(corr_status_out(1,indplot),n_down),0:tplot(end)/(numel(tplot)-1):tplot(end),0.5),'Linewidth',1)
text(mean(tplot),0.5,['Acc. rejection rate = ',num2str(acc_rejrate,'%3.2f')],'fontsize',fonty_imu)
ylabel('$a\_status$','interpreter','latex','fontsize',fonty_imu)
xlabel('time [s]','interpreter','latex','fontsize',fonty_imu)
h5 = legend({'$a\_status$','$a\_status,\,{LP-filtered}$'},'interpreter','latex','fontsize',fonty_imu,'location','southwest');
sfh5.Position  = sfh5.Position + [0 0.05 -0.05 0];
funFigureProperty(h5);


%%

fonty_imu = 20;
figimugyro = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
sfh1 = subplot(311);
hold on, grid on, axis tight, %axis([0 tsim -1.5 1.5]),
plot(downsample(tplot,n_down), downsample(gyro_out(1,indplot),n_down)*r2d, 'Linewidth',1),
%plot(downsample(tplot,n_down), downsample(mbody(:,1),n_down),'--', 'Linewidth',1),
%     plot(tplot, adata_diff(:,1), 'Linewidth',1),
ylabel('$\omega_{x}\,[^{\circ}/s]$','interpreter','latex','fontsize',fonty_imu)
% h1 = legend({'$a_{m,x}$','$m_{ref,B,x}$'},'interpreter','latex','fontsize',fonty_imu,'location','Northeast');
sfh1.Position  = sfh1.Position + [0 0.05 -0.05 0];
set(gca,'xticklabel',[])
funFigureProperty;

sfh2 = subplot(312); hold on, grid on, axis tight, %axis([0 tsim -1.5 1.5]),
plot(downsample(tplot,n_down), downsample(gyro_out(2,indplot),n_down)*r2d, 'Linewidth',1),
%     plot(tplot, adata_diff(:,2), 'Linewidth',1),
ylabel('$\omega_{y}\,[^{\circ}/s]$','interpreter','latex','fontsize',fonty_imu)
% h2 = legend({'$m_{m,y}$','$m_{ref,B,y}$'},'interpreter','latex','fontsize',fonty_imu,'location','Northeast');
sfh2.Position  = sfh2.Position + [0 0.1 -0.05 0];
set(gca,'xticklabel',[])
funFigureProperty;

sfh3 = subplot(313); hold on, grid on, axis tight, %axis([0 tsim -1.5 1.5]),
plot(downsample(tplot,n_down), downsample(gyro_out(3,indplot),n_down)*r2d, 'Linewidth',1)
%     plot(tplot, adata_diff(:,3), 'Linewidth',1)
ylabel('$\omega_{z}\,[^{\circ}/s]$','interpreter','latex','fontsize',fonty_imu)
%h3 = legend({'$\omega_{m}\,[^{\circ}]$','$m_{ref,B}$ [G]'},'interpreter','latex','fontsize',fonty_imu,'location','Best');
sfh3.Position  = sfh3.Position + [0 0.15 -0.05 0];
% set(gca,'xticklabel',[])
xlabel('time [s]','interpreter','latex','fontsize',fonty_imu)
funFigureProperty;
linkaxes([sfh1, sfh2, sfh3],'x')

%% Magnetometer plot
fonty_imu = 20;
figimumag = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
sfh1 = subplot(511);
hold on, grid on, axis tight, %axis([0 tsim -1.5 1.5]),
plot(downsample(tplot,n_down), downsample(mdata(:,1),n_down), 'Linewidth',1),
plot(downsample(tplot,n_down), downsample(mbody(:,1),n_down),'--', 'Linewidth',1),
%     plot(tplot, adata_diff(:,1), 'Linewidth',1),
ylabel('$m_{B,x}$','interpreter','latex','fontsize',fonty_imu)
h1 = legend({'$a_{m,x}$','$m_{ref,B,x}$'},'interpreter','latex','fontsize',fonty_imu,'location','Northeast');
sfh1.Position  = sfh1.Position + [0 0.05 -0.05 0];
set(gca,'xticklabel',[])
funFigureProperty(h1);

sfh2 = subplot(512); hold on, grid on, axis tight, %axis([0 tsim -1.5 1.5]),
plot(downsample(tplot,n_down), downsample(mdata(:,2),n_down), 'Linewidth',1),
plot(downsample(tplot,n_down), downsample(mbody(:,2),n_down),'--', 'Linewidth',1),
%     plot(tplot, adata_diff(:,2), 'Linewidth',1),
ylabel('$m_{B,y}$','interpreter','latex','fontsize',fonty_imu)
h2 = legend({'$m_{m,y}$','$m_{ref,B,y}$'},'interpreter','latex','fontsize',fonty_imu,'location','Northeast');
sfh2.Position  = sfh2.Position + [0 0.05 -0.05 0];
set(gca,'xticklabel',[])
funFigureProperty(h2);

sfh3 = subplot(513); hold on, grid on, axis tight, %axis([0 tsim -1.5 1.5]),
plot(downsample(tplot,n_down), downsample(mdata(:,3),n_down), 'Linewidth',1)
plot(downsample(tplot,n_down), downsample(mbody(:,3),n_down),'--', 'Linewidth',1)
%     plot(tplot, adata_diff(:,3), 'Linewidth',1)
ylabel('$m_{B,z}$','interpreter','latex','fontsize',fonty_imu)
h3 = legend({'$m_{m}$ [G]','$m_{ref,B}$ [G]'},'interpreter','latex','fontsize',fonty_imu,'location','Best');
sfh3.Position  = sfh3.Position + [0 0.05 -0.05 0];
set(gca,'xticklabel',[])
funFigureProperty(h3);

sfh4 = subplot(514); hold on, grid on, axis tight, %axis([0 tsim 0 2.5])
plot(downsample(tplot,n_down), downsample(magdeltanorm,n_down), 'Linewidth',1)
ylabel('$$\epsilon_{|m|}$$','interpreter','latex','fontsize',fonty_imu)
%xlabel('time [s]','interpreter','latex','fontsize',fonty_imu)
h4 = legend({'$||m_{m}|-|m_{ref}||$'},'interpreter','latex','fontsize',fonty_imu,'location','Best');
sfh4.Position  = sfh4.Position + [0 0.05 -0.05 0];
set(gca,'xticklabel',[])
funFigureProperty(h4);

sfh5 = subplot(515); hold on, grid on, axis([0 tsim -0.05 1.05])
plot(downsample(tplot,n_down), downsample(corr_status_out(2,indplot),n_down),'*', 'Linewidth',1)
plot(downsample(tplot(mag_ok(indplot)>0),n_down), lsim(ss(Gplot),downsample(corr_status_out(2,mag_ok(indplot)>0),n_down),0:tplot(end)/(numel(tplot(mag_ok(indplot)>0))-1):tplot(end),0.5),'Linewidth',1)
text(mean(tplot),0.5,['Mag. rejection rate = ',num2str(mag_rejrate,'%3.2f')],'fontsize',fonty_imu)
ylabel('$m\_status$','interpreter','latex','fontsize',fonty_imu)
xlabel('time [s]','interpreter','latex','fontsize',fonty_imu)
h5 = legend({'$m\_status$','$m\_status,\,{LP-filtered}$'},'interpreter','latex','fontsize',fonty_imu,'location','Best');
sfh5.Position  = sfh5.Position + [0 0.05 -0.05 0];
funFigureProperty(h5);


%%
% pathname_save = 'D:\goman\Documents\Docs\these_gustav\Articles\SysTol_2019\figures\results_replay';
% pathname_save = 'F:\Hem_190429\SysTol_2019\figures\results_replay';
% pathname_save = 'E:\Hem_190503\Thesis\Figures\';
% 
% resname = num2str(testcase);
% savefig(figimuacc,fullfile(pathname_save,[resname,'_imu_acc']));
% export_fig(fullfile(pathname_save,[resname,'_imu_acc']),'-pdf',figimuacc);
% savefig(figimumag,fullfile(pathname_save,[resname,'_imu_mag']));
% export_fig(fullfile(pathname_save,[resname,'_imu_mag']),'-pdf',figimumag);
% savefig(figimumag,fullfile(pathname_save,[resname,'_imu_gyro']));
% export_fig(fullfile(pathname_save,[resname,'_imu_gyro']),'-pdf',figimugyro);
% quaternion_ekf_test_loop_plotter

%% Bias saturation illustration
% figure, hold on,  
% for kk = 1:numel(ke_vec),
%     for k = 1:3,
%         subplot(3,1,k), hold on,
%         plot(tplot,3*sigma_xhat_save(k,:,kk)*r2d, 'linewidth',2)
%         funFigureProperty
%     end
% end
% for k = 1:3,
%     subplot(3,1,k), hold on,
%     plot(tplot,ones(size(tplot))*EKF.bsat.bmax*r2d,'k--','linewidth',2)
% end
% legend({'Standard EKF','EKF w. bias saturation','$b_{max}$'},'interpreter','latex')
% funFigureProperty

%% Bias saturation illustration - UNOBSERVABILITY
% figure, hold on,  
% % for k = 1:3,
% %     hold on,
%     plot(tplot,ones(size(tplot))*EKF.bsat.bmax*r2d,'k','linewidth',2)
% % end
% for kk = 1:numel(ke_vec),
%     for k = 3,
%         hold on,
%         plot(tplot,3*sigma_xhat_save(k+3,:,kk)*r2d, 'linewidth',2)
%         funFigureProperty
%     end
% end
% 
% xlabel('Time [s]','interpreter','latex')
% ylabel('$3.\sigma_{b_{\omega,z}}$','interpreter','latex')
% legend({'Standard EKF','EKF w. bias saturation','$b_{max}$'},'interpreter','latex')
% plot(ones(1,100)*0.1*tend, linspace(0.5,2.5,100),'k:','linewidth',1.5)
% arrow3([0.3*tend 0.6],[0.1*tend 0.6])
% text(0.32*tend,0.6,'Start of unobservability', 'interpreter','latex','FontSize',20)
% set(gcf,'color','w')
% funFigureProperty
% export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',['bias_sat_ex_unobs_',time_str_short]),'-pdf',gcf)

%% Bias saturation illustration  - STABILITY

% figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');, hold on,  
% % for k = 1:3,
% %     hold on,
% %     plot(tplot,ones(size(tplot))*EKF.bsat.bmax*r2d,'k','linewidth',2)
% % end
% for kk = 1:numel(ke_vec),
%     subplot(2,1,1), hold on,
%     for k = 3,
%         hold on,
%         h(1,kk) = plot(tplot,xhat_save(k,:,kk)*r2d, 'linewidth',2);
%         ylim([-50 100])
%         funFigureProperty
%     end
%     plot(ones(1,100)*0.1*tend, linspace(-50,100,100),'k:','linewidth',1.5)
%     plot(ones(1,100)*0.9*tend, linspace(-50,100,100),'k:','linewidth',1.5)
%     ylabel('$\hat{\psi}$ [$^{\circ}$]','interpreter','latex')
%     subplot(2,1,2), hold on,
%     for k = 3,
%         hold on,
%         h(2,kk) = plot(tplot,xhat_save(k+3,:,kk)*r2d, 'linewidth',2);
%         h(3,kk) = plot(tplot,-ones(size(tplot))*EKF.bsat.bmax*r2d,'k--','linewidth',2);
%         plot(tplot,ones(size(tplot))*EKF.bsat.bmax*r2d,'k--','linewidth',2)
%         ylim([-20 45])
%         funFigureProperty
%     end
%     plot(ones(1,100)*0.1*tend, linspace(-50,50,100),'k:','linewidth',1.5)
%     plot(ones(1,100)*0.9*tend, linspace(-50,100,100),'k:','linewidth',1.5)
% end
% 
% xlabel('Time [s]','interpreter','latex')
% ylabel('$\hat{b}_{\omega,z}$ [$^{\circ}$/s]','interpreter','latex')
% legend([h(2,1), h(2,2),h(3,1)],{'Standard EKF','EKF w. bias saturation','$b_{max}$'},'interpreter','latex')
% 
% P1 = [0.15*tend -10]; P2 = [0.1*tend -10]; 
% P3 = [0.85*tend -10]; P4 = [0.9*tend -10];
% % arrow3([0.3*tend -10],[0.1*tend -10])
% arrow3(P1,P2,'k1',2,1)
% arrow3(P3,P4,'k1',2,1)
% axis normal
% text(0.17*tend,-10,'Start of unobservability', 'interpreter','latex','FontSize',20)
% text(0.63*tend,-10,'End of unobservability', 'interpreter','latex','FontSize',20)
% set(gcf,'color','w')
% funFigureProperty
% export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',['bias_sat_ex_stability_',time_str_short]),'-pdf',gcf)

%% Bias saturation illustration  - CONSISTENCY

% figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');, hold on,  
% % for k = 1:3,
% %     hold on,
% %     plot(tplot,ones(size(tplot))*EKF.bsat.bmax*r2d,'k','linewidth',2)
% % end
% for kk = 1:numel(ke_vec),
%     s1 = subplot(2,1,1); hold on,
%     for k = 3,
%         hold on,
%         h(1,kk) = plot(tplot,sigma_xhat_save(k,:,kk)*r2d, 'linewidth',2);
%         ylim([0 100])
%         funFigureProperty
%     end
%     plot(ones(1,100)*0.1*tend, linspace(-50,100,100),'k:','linewidth',1.5)
%     plot(ones(1,100)*0.9*tend, linspace(-50,100,100),'k:','linewidth',1.5)
%     ylabel('$\sigma_{{\psi}}$ [$^{\circ}$]','interpreter','latex')
%     s2 = subplot(2,1,2); hold on,
%     for k = 3,
%         hold on,
%         h(2,kk) = plot(tplot,sigma_xhat_save(k+3,:,kk)*r2d, 'linewidth',2);
%         h(3,kk) = plot(tplot,-ones(size(tplot))*EKF.bsat.bmax*r2d,'k--','linewidth',2);
%         plot(tplot,ones(size(tplot))*EKF.bsat.bmax*r2d,'k--','linewidth',2)
%         ylim([0 15])
%         funFigureProperty
%     end
%     plot(ones(1,100)*0.1*tend, linspace(-50,50,100),'k:','linewidth',1.5)
%     plot(ones(1,100)*0.9*tend, linspace(-50,100,100),'k:','linewidth',1.5)
% end
% 
% xlabel('Time [s]','interpreter','latex')
% ylabel('$\sigma_{{b}_{\omega,z}}$ [$^{\circ}$/s]','interpreter','latex')
% 
% yarrow = 12;
% P1 = [0.15*tend yarrow]; P2 = [0.1*tend yarrow]; 
% P3 = [0.85*tend yarrow]; P4 = [0.9*tend yarrow];
% % arrow3([0.3*tend -10],[0.1*tend -10])
% arrow3(P1,P2,'k1',2,1)
% arrow3(P3,P4,'k1',2,1)
% axis normal
% % subplot(s2)
% text(0.17*tend,yarrow,'Start of unobservability', 'interpreter','latex','FontSize',20)
% text(0.63*tend,yarrow,'End of unobservability', 'interpreter','latex','FontSize',20)
% set(gcf,'color','w')
% % subplot(s1)
% legend([h(2,1), h(2,2),h(3,1)],{'Standard EKF','EKF w. bias saturation','$b_{max}$'},'interpreter','latex')
% funFigureProperty
% export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',['bias_sat_ex_unobs_',time_str_short]),'-pdf',gcf)

%% Compare euler angles and error between filters
if b_plot_comp
    figcomp = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
    hold on,
    eulab = {'$\phi\,[^{\circ}]$','$\theta\,[^{\circ}]$','$\psi\,[^{\circ}]$','$||\eps||$'};
    for k = 1:3
        subplot(4,1,k); hold on; axis([0 t(end) -180 180])
        if k < 4
            ylabel(eulab(k),'interpreter','latex','fontsize',18)
        end
        yliminc = 45;
        if k == 1, ylim([euler0(1)*r2d-yliminc euler0(1)*r2d+yliminc]), end
        if k == 2, ylim([euler0(2)*r2d-yliminc euler0(2)*r2d+yliminc]), end
        hr = plot(tplot, eulerout(k,indplot)*r2d,'k','Linewidth',1);
        funFigureProperty
    end
    for kk = 1:numel(ke_vec)
        for k = 1:3
            subplot(4,1,k); hold on; axis([0 t(end) -180 180])
            hl(kk) = plot(tplot, xhat_save(k,indplot,kk)*r2d,mark_list{kk},'Linewidth',1.2);
        end
    end
    subplot(4,1,1)
    legend([hr,hl],{'Real',ke_legend{1:end}},'Orientation','horizontal','location','north', 'interpreter','latex')
    
    subplot(4,1,4), hold on
    for kk = 1:numel(ke_vec)
        plot(tplot,total_euler_error(1,indplot,kk)*r2d,mark_list{kk},'Linewidth',1.2)
    end
    ylabel('Total error [$^{\circ}$]','interpreter','latex')
    xlabel(gca,'Time [s]','interpreter','latex')
    funFigureProperty
end

%export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',['decoup_euler_',num2str(euler0(1)),'_',time_str_short]),'-pdf',gcf)

%% Compare quaternion error angle
if b_plot_comp
    if nfilt == 4 && numel(unique(ke_vec)) == 2
        ke_legend = {'DEC-IEKF',...
'$||$, $\delta\psi = \lbrace 0,\,\pi\rbrace$',...
'$||$, $\delta\psi = \lbrace 0,\,\frac{\pi}{2},\,\pi\rbrace$',...
'$||$, $\delta\psi = \lbrace 0,\,\frac{\pi}{3},\,\frac{2\pi}{3},\,\pi \rbrace$'};
    end
figcomp = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
hold on,
for kk = 1:numel(ke_vec)
   hold on; grid on, if delta_ang > 90, axis([0 t(end) 0 180]), else axis([0 t(end) 0 90]), end 
   semilogy(tplot, ((quat_error_save(1,indplot,kk)))*r2d,mark_list{kk},'Linewidth',2)
%    legend(ke_legend,'Orientation','horizontal','location','northoutside', 'interpreter','latex')
   legend({ke_legend{1:end},'$b_{max}$'},'Orientation','vertical','location','northoutside', 'interpreter','latex')
   funFigureProperty
end
ylabel('Quaternion error angle [$^{\circ}$]','interpreter','latex')
xlabel(gca,'Time [s]','interpreter','latex')
funFigureProperty
ax2 = axes('Position',[0.3 0.3 0.35 0.3]); hold on
for kk = 1:numel(ke_vec)
   iplot = intersect(find(t>10),find(t<50));
   plot(tplot(iplot),quat_error_save(1,iplot,kk)*r2d,'linewidth',1.5);
   ylim([0 30])
   grid on
   %             axis tight
   funFigureProperty
end



end
% export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',['Quaternion_error_','DEC-IEKF',time_str_short,'_',num2str(klm)]),'-pdf',gcf)

%% Compare bias estimations between filters
if b_plot_comp
figcomp = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
hold on,
eulab = {'$b_x\,[^{\circ}/s]$','$b_y\,[^{\circ}/s]$','$b_z\,[^{\circ}/s]$','$||\eps||$'};
for k = 1:3
    subplot(3,1,k); hold on; axis([0 t(end) -5 5])
    if k < 4
        ylabel(eulab(k),'interpreter','latex','fontsize',18)
    end
    yliminc = 45;
    if k == 1, ylim([euler0(1)*r2d-yliminc euler0(1)*r2d+yliminc]), end
    if k == 2, ylim([euler0(2)*r2d-yliminc euler0(2)*r2d+yliminc]), end
    hr = plot(tplot, bias_out(k,indplot)*r2d,'k','Linewidth',1);
    funFigureProperty
end
for kk = 1:numel(ke_vec)
    for k = 1:3
        subplot(3,1,k); hold on; axis([0 t(end) -10 10])
        hl(kk) = plot(tplot, xhat_save(k+3,indplot,kk)*r2d,mark_list{kk},'Linewidth',1.5);
        h2 = plot(tplot, -ones(size(tplot))*EKF.bsat.bmax*r2d,'k--', 'linewidth',1.5);
        plot(tplot, ones(size(tplot))*EKF.bsat.bmax*r2d,'k--', 'linewidth',1.5)
    end
end
legend([hr,hl, h2],{'Real',ke_legend{1:end},'$b_{max}$'},'Orientation','horizontal','location','north', 'interpreter','latex')
xlabel(gca,'Time [s]','interpreter','latex')
funFigureProperty
end
%export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',['Quaternion_error_bias_',time_str_short]),'-pdf',gcf)


%% Plot mag meas  vs real
figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
 maglabel = {'$m_x\, [G]$','$m_y\, [G]$','$m_z\, [G]$'};
for k = 1:3
    sp(k) = subplot(3,1,k);
    plot( t, magm_out(k,:), t, mag_out(k,:),'linewidth',1.5);
    ylabel(maglabel(k),'interpreter','latex')
    funFigureProperty
    psk = get(sp(k),'position');
    set(sp(k),'position',psk+[0 0 0 0.05])
    if k<3
        set(sp(k),'xticklabel',[])
    end
end
legend({'Mag. measurement', 'Mag. reference'},'interpreter','latex','fontsize',20)
%subplot(414), plot( t, norm_mag_out,'linewidth',1.5);
%ylabel('$||\vec{m}_m||\,[G]$','interpreter','latex')
xlabel('Time [s]','interpreter','latex')
funFigureProperty


%% Plot mag vs real vs pm-est, w. 3-sigma bound
% ylabmag = {'$m_x\,[G]$','$m_y\,[G]$','$m_z\,[G]$'};
% fonter = 32;
% figmag_rmp = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
% % figure
% for uio=1:3, 
%     sph(uio) = subplot(4,1,uio); hold on, 
%     hp(1) = plot(t,mag_out(uio,:),'k','linewidth',1.5);
%     hp(2) = plot(t,magm_out(uio,:),'r','linewidth',1.5);
% %     plot(t,magm_out(uio,:)+3*magsignoise,'--r'); 
% %     plot(t,magm_out(uio,:)-3*magsignoise,'--r'); 
%     hp(3) = plot(t,mhat_out(uio,:),'b','linewidth',1.5);
%     if uio == 1
%         plot(ones(100,1)*tfaultstart,linspace(0,2,100),'--k','linewidth',1)
%         plot(ones(100,1)*tfaultend,linspace(0,2,100),'--k','linewidth',1)
%     else
%         plot(ones(100,1)*tfaultstart,linspace(-1,1,100),'--k','linewidth',1)
%         plot(ones(100,1)*tfaultend,linspace(-1,1,100),'--k','linewidth',1)
%     end
% %    hp(4) = plot(t,mhat_save(uio,:),'color',[0, 0.5, 0],'linewidth',1.5);
% %     plot(t,mhat_out(uio,:)+3*sigma_mhat_out(uio,:),'--b');
% %     plot(t,mhat_out(uio,:)-3*sigma_mhat_out(uio,:),'--b');
%     
%     ylabel(ylabmag(uio),'interpreter','latex', 'fontsize',fonter)  
%     if uio == 3
%         legend(hp(1:3),{'$\mathbf{m}$','$\mathbf{m}_m$','$\mathbf{\hat{m}}$'},'interpreter','latex', 'fontsize',fonter)
%         
%     end
%     funFigureProperty
% end
% sph(4) = subplot(4,1,4); hold on
% hp(4) = plot(t, mag_status_out(1,:),'^');
% plot(ones(100,1)*tfaultstart,linspace(0,1,100),'--k','linewidth',1)
% plot(ones(100,1)*tfaultend,linspace(0,1,100),'--k','linewidth',1)
% text(tfaultstart+10,0.5,'$\leftarrow$ Magnetic disturbance $\rightarrow$', 'interpreter', 'latex','fontsize',fonter)
% xlabel('Time [s]','interpreter','latex', 'fontsize',fonter)
% set(sph(4),'ytick',[0 1])
% set(sph(4),'yticklabel',[0 1])
% ylabel('[-]','interpreter','latex','fontsize',fonter)
% l2 = legend(hp(4),'$m\_norm$','fontsize',fonter);
% set(l2,'interpreter','latex')
% funFigureProperty
% set(gcf,'color','w')



%% Mag residual plot
% figmag_res= figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
% subplot(3,1,1), hold on
%     ph(1) = plot(t, d_norm_mag_out(1,:),'color',[0.75,0,0],'linewidth',1.5);
%     ylabel('$\varepsilon_{||r_m||}$ [G]','interpreter','latex')
%     funFigureProperty
% subplot(3,1,2), hold on
%     ph(2) = plot(t, d_outlier_mag_out(1,:),'color',[0.75,0,0],'linewidth',1.5);
%     ph(3) = plot(t, d_outlier_mag_out(2,:),'color',[0,0.75,0],'linewidth',1.5);
%     ph(4) = plot(t, d_outlier_mag_out(3,:),'color',[0,0,0.75],'linewidth',1.5);
%     l2 = legend(ph(2:4),{'$\varepsilon_{r_m,1}$','$\varepsilon_{r_m,2}$','$\varepsilon_{r_m,3}$'});
%     set(l2, 'interpreter','latex','fontsize',fonter)
%     ylabel('$\varepsilon_{r_m}$ [G]','interpreter','latex')
%     funFigureProperty
% subplot(3,1,3), hold on
%     ph(5) = plot(t, d_warn_mag_out(1,:),'color',[0.75,0,0],'linewidth',1.5);
%     ph(6) = plot(t, d_warn_mag_out(2,:),'color',[0,0.75,0],'linewidth',1.5);
%     ph(7) = plot(t, d_warn_mag_out(3,:),'color',[0,0,0.75],'linewidth',1.5);
%     l2 = legend(ph(5:7),{'$\varepsilon_{r_{ref},1}$',...
%         '$\varepsilon_{r_{ref},2}$',...
%         '$\varepsilon_{r_{ref},3}$'});
%     set(l2, 'interpreter','latex','fontsize',fonter)
%     ylabel('$\varepsilon_{r_{ref}}$ [G]','interpreter','latex')
%     xlabel('Time [s]','interpreter','latex', 'fontsize',fonter)
%     funFigureProperty
% 
% 
% %% Plot mag vs real vs pm-est vs mag cons
% figmag_con= figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
% for uio=1:3, 
%     spv(uio) = subplot(4,1,uio); hold on, 
%     hp(1) = plot(t,mag_out(uio,:),'k','linewidth',1.5); 
%     hp(2) = plot(t,magm_out(uio,:),'r','linewidth',1.5);  
%     hp(3) = plot(t,mhat_out(uio,:),'b','linewidth',1.5);
%     hp(4) = plot(t, mc_out(uio,:),'color',[0,0.5,0],'linewidth',1.5);
%     ylabel(ylabmag(uio),'interpreter','latex', 'fontsize',fonter)  
%     funFigureProperty
%     if uio == 3
%         legend(hp(1:4),{'$\mathbf{m}$','$\mathbf{m}_m$','$\mathbf{\hat{m}}$','$\mathbf{\hat{m}}_c$'},'interpreter','latex', 'fontsize',fonter)
%     end
% end
% spv(4) = subplot(4,1,4); hold on
% hp(4) = plot(t, corr_status_out(2,:),'^');
% diff_mc_1 = abs(mc_out(1,:)-magm_out(1,:)) < 0.0001;
% hp(5) = plot(t(diff_mc_1), corr_status_out(2,diff_mc_1),'r^');
% hp(6) = plot(t(~diff_mc_1), corr_status_out(2,~diff_mc_1),'b^');
% plot(ones(100,1)*tfaultstart,linspace(0,1,100),'--k','linewidth',1)
% plot(ones(100,1)*tfaultend,linspace(0,1,100),'--k','linewidth',1)
% text(tfaultstart+10,0.5,'$\leftarrow$ Magnetic disturbance $\rightarrow$', 'interpreter', 'latex','fontsize',fonter)
% xlabel('Time [s]','interpreter','latex', 'fontsize',fonter)
% set(spv(4),'ytick',[0 1])
% set(spv(4),'yticklabel',[0 1])
% ylabel('$m\_status$ [-]','interpreter','latex','fontsize',fonter)
% l2 = legend(hp(5:6),{'$\mathbf{m}_c = \mathbf{m}_m$','$\mathbf{m}_c = \mathbf{\hat{m}}$'},'fontsize',fonter);
% set(l2,'interpreter','latex')
% funFigureProperty
% set(gcf,'color','w')


%% Compare GLR and X2 statistics
%     figstat = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
%     title('GLR statistic')
%     cnt = 1;
%     for k = [2 4 6]
% %         subplot(3,1,cnt), hold on
% %         plot(t, GLRvec(cnt,:))
% %         plot(t, chi2vec(cnt,:))
% %         plot(t, ones(size(t))*GLRparams_1.GLRthr)
% %         plot(t, ones(size(t))*GLRparams_3.GLRthr)
% %         legend('l_{GLR}','l_{\chi^2}','l_{det, 1}','l_{det, 3}')
% %         title(titlevec_acc(cnt))
%         sph(cnt) = subplot(3,1,cnt); hold on
%         ph(1) = plot(t, GLRvec(cnt+3,:),'linewidth',1.5);
%         ph(2) = plot(t, chi2vec(cnt+3,:),'linewidth',1.5);
%         
%         funFigureProperty
%         iplot = intersect(find(t>10),find(t<30));
%         iplot_end = intersect(find(t>70),find(t<90));
%         if cnt== 1, 
%             ylabel('$l_{||r_m||}$ [-]','interpreter','latex')
%             ph(3) = plot(t, ones(size(t))*GLRparams_1.GLRthr,'--k','linewidth',1.5);
%             plot(ones(100,1)*tfaultstart,linspace(0,400,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%             plot(ones(100,1)*tfaultend,linspace(0,400,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%             % Zoom axes, beginning
%             ax2 = axes('Position',[0.16 0.8 0.15 0.1]); hold on
% %             iplot = intersect(find(t>20),find(t<25));
%             plot(ax2,t(iplot), GLRvec(cnt+3,iplot))
%             plot(ax2,t(iplot), chi2vec(cnt+3,iplot))
%             plot(ax2,t(iplot), ones(size(iplot))*GLRparams_1.GLRthr,'--k','linewidth',1.5);
%             plot(ones(100,1)*tfaultstart,linspace(0,75,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%             ylim([0 75])
% %             axis tight
%             funFigureProperty
%             % Zoom axes, end 
%             ax2 = axes('Position',[0.66 0.8 0.15 0.1]); hold on
% %             iplot = intersect(find(t>20),find(t<25));
%             plot(ax2,t(iplot_end), GLRvec(cnt+3,iplot_end))
%             plot(ax2,t(iplot_end), chi2vec(cnt+3,iplot_end))
%             plot(ax2,t(iplot_end), ones(size(iplot_end))*GLRparams_1.GLRthr,'--k','linewidth',1.5);
%             plot(ones(100,1)*tfaultend,linspace(0,75,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%             ylim([0 75])
% %             axis tight
%             funFigureProperty
%         elseif cnt == 2
%             ylabel('$l_{r_m}$ [-]','interpreter','latex')
%             ph(3) = plot(t, ones(size(t))*GLRparams_3.GLRthr,'--k','linewidth',1.5);
%             plot(ones(100,1)*tfaultstart,linspace(0,2000,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%             plot(ones(100,1)*tfaultend,linspace(0,2000,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%             ax2 = axes('Position',[0.16 0.5 0.15 0.1]); hold on
%             
%             plot(ax2,t(iplot), GLRvec(cnt+3,iplot))
%             plot(ax2,t(iplot), chi2vec(cnt+3,iplot))
%             plot(ax2,t(iplot), ones(size(iplot))*GLRparams_3.GLRthr,'--k','linewidth',1.5);
%             plot(ones(100,1)*tfaultstart,linspace(0,75,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%             ylim([0 75])
%             funFigureProperty
%             % Zoom axes, end 
%             ax2 = axes('Position',[0.66 0.5 0.15 0.1]); hold on
% %             iplot = intersect(find(t>20),find(t<25));
%             plot(ax2,t(iplot_end), GLRvec(cnt+3,iplot_end))
%             plot(ax2,t(iplot_end), chi2vec(cnt+3,iplot_end))
%             plot(ax2,t(iplot_end), ones(size(iplot_end))*GLRparams_1.GLRthr,'--k','linewidth',1.5);
%             plot(ones(100,1)*tfaultend,linspace(0,75,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%             ylim([0 75])
%             funFigureProperty
%         elseif cnt == 3
%             ylabel('$l_{r_{ref}}$ [-]','interpreter','latex')
%             ph(3) = plot(t, ones(size(t))*GLRparams_3.GLRthr,'--k','linewidth',1.5);
%             plot(ones(100,1)*tfaultstart,linspace(0,200,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%             plot(ones(100,1)*tfaultend,linspace(0,200,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%             text(tfaultstart+10,150,'$\leftarrow$ Magnetic disturbance $\rightarrow$', 'interpreter', 'latex','fontsize',fonter)
% %             ax2 = axes('Position',[0.17 0.2 0.2 0.1]); hold on
% % %             iplot = intersect(find(t>20),find(t<25));
% %             plot(ax2,t(iplot), GLRvec(cnt+3,iplot))
% %             plot(ax2,t(iplot), chi2vec(cnt+3,iplot))
% %             plot(ax2,t(iplot), ones(size(iplot))*GLRparams_3.GLRthr,'--k','linewidth',1.5);
% %             plot(ones(100,1)*tfaultstart,linspace(0,75,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
% %             ylim([0 75])
% %             axis tight
%             funFigureProperty
%         end
%         
%         funFigureProperty
%         if cnt == 2,
%             l1 = legend(ph(1:3),{'$l_{GLR}$','$l_{\chi^2}$','$l_{det}$'});
%             set(l1,'interpreter','latex', 'fontsize',fonter)
%         end
% %         title(titlevec_mag(cnt))
%         cnt = cnt+1;
%     end

%% Exportfig
% export_fig(fullfile('ENO:\Hem_190503\Thesis\Figures\figures_FTKF','ex_mag_stat'),'-pdf',figstat)
% export_fig(fullfile('ENO:\Hem_190503\Thesis\Figures\figures_FTKF','ex_mag_cons'),'-pdf',figmag_con)
% export_fig(fullfile('ENO:\Hem_190503\Thesis\Figures\figures_FTKF','ex_mag_pmresid'),'-pdf',figmag_res)
% export_fig(fullfile('ENO:\Hem_190503\Thesis\Figures\figures_FTKF','ex_mag_pmest'),'-pdf',figmag_rmp)
%%
filt_leg = {};
filt_title_vec(1) = {'Hua, 2011'};
filt_title_vec(2)= {'Legacy EKF'};
filt_title_vec(3) = {'Measurement decoupled KF'};
filt_title_vec(4) = {'Fault tolerant nonlinear KF - uhat'};
filt_title_vec(5) = {'Hua, 2011, Global decoupling'};
filt_title_vec(6) = {'Martin + Sarras, 2018, Bias observer'};
filt_title_vec(7) = {'Decoupled MEKF'};
filt_title_vec(8) = {'Decoupled GMEKF'};
filt_title_vec(9) = {'Decoupled IEKF, Mag. PM'};
filt_title_vec(10) = {'Decoupled IEKF, Full PM'};
filt_title_vec(11) = {'Valenti, 2016, Decoupled'};
filt_title_vec(12) = {'Fault tolerant nonlinear KF - umeas'};
filt_title_vec(13) = {'Decoupled GMEKF, covmax'};
filt_title_vec(14) = {'EKF, innov rej.'};

for k = 1:numel(ke_vec)
    filt_leg(k) = filt_title_vec(ke_vec(k));
end
colvec = {'r','g','b','m','k'};
%%

% figcomperror = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
% set(gcf, 'Color', 'w');
% subplot(211),hold on,
% for k = 1:numel(ke_vec),
%     plot(inc_error_mc_out(:,k)*r2d,colvec{k},'Linewidth',1),
% end,
% ylabel('Inclination RMS error [$^{\circ}$]','interpreter','latex')
% xlabel('Simulation \#','interpreter','latex')
% funFigureProperty
% subplot(212),hold on,
% for k = 1:numel(ke_vec), 
%     plot(head_error_mc_out(:,k)*r2d,colvec{k},'Linewidth',1), 
% end
% ylabel('Heading RMS error [$^{\circ}$]','interpreter','latex')
% xlabel('Simulation \#','interpreter','latex')
% legend(filt_leg,'Interpreter','latex','fontsize',20)
% funFigureProperty
% %export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',['inc_rms_error_decoup_',num2str(scen),'_',num2str(accfaultcase),'_',num2str(magfaultcase),'_',time_str_short]),'-pdf',gcf)
% %%
% figcomperror = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
% set(gcf, 'Color', 'w');
% subplot(211),hold on,
% for k = 1:numel(ke_vec),
%     plot(inc_error_max_mc_out(:,k)*r2d,colvec{k},'Linewidth',1),
% end,
% ylabel('Inclination Max error [$^{\circ}$]','interpreter','latex')
% xlabel('Simulation \#','interpreter','latex')
% funFigureProperty
% subplot(212),hold on,
% for k = 1:numel(ke_vec), 
%     plot(head_error_max_mc_out(:,k)*r2d,colvec{k},'Linewidth',1), 
% end
% ylabel('Heading Max error [$^{\circ}$]','interpreter','latex')
% xlabel('Simulation \#','interpreter','latex')
% legend(filt_leg,'Interpreter','latex','fontsize',20)
% funFigureProperty
% %export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',['head_rms_error_decoup_',num2str(scen),'_',num2str(accfaultcase),'_',num2str(magfaultcase),'_',time_str_short]),'-pdf',gcf)
% %%
% figcomperror = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
% set(gcf, 'Color', 'w');
% subplot(211),hold on,
% for k = 1:numel(ke_vec),
%     plot((inc_error_max_mc_out(:,k)+head_error_max_mc_out(:,k))*r2d,colvec{k},'Linewidth',1),
% end,
% ylabel('Totalt angular Max error [$^{\circ}$]','interpreter','latex')
% xlabel('Simulation \#','interpreter','latex')
% funFigureProperty
% subplot(212),hold on,
% for k = 1:numel(ke_vec)
%     plot((inc_error_mc_out(:,k)+head_error_mc_out(:,k))*r2d,colvec{k},'Linewidth',1),
% end,
% ylabel('Totalt angular RMS error [$^{\circ}$]','interpreter','latex')
% xlabel('Simulation \#','interpreter','latex')
% legend(filt_leg,'Interpreter','latex','fontsize',20)
% funFigureProperty
% %export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',['total_ang_rms_error_decoup_',num2str(scen),'_',num2str(accfaultcase),'_',num2str(magfaultcase),'_',time_str_short]),'-pdf',gcf)
function [ac, Phat_ac, mc, Phat_mc, input_est_status_out] = ...
    input_consolidation(EKF,acc_hat,accm,mhat,magm,P_acc,P_mag,...
    status_vec_acc, status_vec_mag)

b_acc_norm = status_vec_acc(1);
b_acc_outlier = status_vec_acc(2);
b_acc_warn = status_vec_acc(3);

b_mag_norm = status_vec_mag(1);
b_mag_outlier = status_vec_mag(2);
b_mag_warn = status_vec_mag(3);

% Measurement consolidation
if b_acc_outlier > 0.5
    if b_acc_warn > 0.5
        if b_acc_norm > 0.5
            ac = acc_hat;
            Phat_ac = P_acc;
        else
            ac = accm;
            Phat_ac = EKF.Ra;
        end
    else
        ac = acc_hat;
        Phat_ac = P_acc;
    end
else
    if b_acc_warn > 0.5
        ac = accm;
        Phat_ac = EKF.Ra;
    else
        if b_acc_norm > 0.5
            ac = acc_hat;
            Phat_ac = P_acc;
        else
            ac = accm;
            Phat_ac = EKF.Ra;
        end
    end
end

if b_mag_outlier > 0.5
    if b_mag_warn > 0.5
        if b_mag_norm > 0.5
            mc = mhat;
            Phat_mc = P_mag;
        else
            mc = magm;
            Phat_mc = EKF.Rm;
        end
    else
        mc = mhat;
        Phat_mc = P_mag;
    end
else
    if b_mag_warn > 0.5
        if b_mag_norm > 0.5
            mc = mhat;
            Phat_mc = P_mag;
        else
            mc = magm;
            Phat_mc = EKF.Rm;
        end
    else
        if b_mag_norm > 0.5
            mc = mhat;
            Phat_mc = P_mag;
        else
            mc = magm;
            Phat_mc = EKF.Rm;
        end
    end
end

    

% Correction gating
% Estimation by default OK if measurement is available
input_est_status_out = ones(2,1);

% Acceleration status
% - OK by default
input_est_status_out(1) = 1;
% input_est_status(1) = 0;

delta_acc_c = norm(ac)-norm(EKF.gvec);
ddeltaacdac = 1/norm(ac)*ac';
P_norm_ac = ddeltaacdac*Phat_ac*ddeltaacdac';
l_delta_ac = delta_acc_c^2/P_norm_ac;

% 
% if l_delta_ac > EKF.stats.l_det_1_3sig
%     input_est_status_out(1) = 0;
% end
if b_acc_norm > 0.5
    input_est_status_out(1) = 0;
end

if b_acc_warn > 0.5
    input_est_status_out(1) = 0;
end
if b_acc_outlier > 0.5
    input_est_status_out(1) = 0;
end

if b_acc_norm > 0.5  && b_acc_warn < 0.5 && b_acc_outlier < 0.5
    input_est_status_out(1) = 1;
end

if b_acc_norm < 0.5 && b_acc_warn > 0.5 && b_acc_outlier < 0.5
    input_est_status_out(1) = 1;
end

if b_acc_norm > 0.5 && b_acc_warn < 0.5 && b_acc_outlier > 0.5
    input_est_status_out(1) = 1;
end

if b_acc_norm < 0.5 && b_acc_warn < 0.5 && b_acc_outlier > 0.5
    input_est_status_out(1) = 1;
end


% Magnetic field status
% - OK by default
input_est_status_out(2) = 1;

% Case 1 - Norm of consolidated acceleration out of bound
delta_mag_c = norm(mc)-norm(EKF.mag_ref);
ddeltamcdmc = 1/norm(mc)*mc';
P_norm_mc = ddeltamcdmc*Phat_mc*ddeltamcdmc';
l_delta_mc = delta_mag_c^2/P_norm_mc;

% if l_delta_mc > EKF.stats.l_det_1_3sig
%     input_est_status_out(1) = 0;
% end

if b_mag_norm > 0.5
    input_est_status_out(2) = 0;
end

% Case 2 - Sensor model NOK
if b_mag_warn > 0.5
    input_est_status_out(2) = 0;
end
% Case 3 - Measurement outlier detected
if b_mag_outlier > 0.5
    input_est_status_out(2) = 0;
end

if b_mag_norm > 0.5  && b_mag_warn < 0.5 && b_mag_outlier < 0.5
    input_est_status_out(2) = 1;
end

if b_mag_norm < 0.5  && b_mag_warn > 0.5 && b_mag_outlier < 0.5
    input_est_status_out(2) = 1;
end

if b_mag_norm > 0.5  && b_mag_warn < 0.5 && b_mag_outlier > 0.5
    input_est_status_out(2) = 1;
end

if b_mag_norm < 0.5  && b_mag_warn < 0.5 && b_mag_outlier > 0.5
    input_est_status_out(2) = 1;
end

end


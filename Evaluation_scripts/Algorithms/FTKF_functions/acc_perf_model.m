function [ ahat_out, P_out, status_vec, S_pm_acc, delta_pm_acc,...
        S_out_acc, delta_out_acc ] = acc_perf_model(EKF, ahat_in, P_in, inputs, accm, est_inputs, cov_est_inputs, b_norm, dt )
%SENSOR_PERF_MODEL Summary of this function goes here
%   Detailed explanation goes here

nx = numel(ahat_in);
gvec = EKF.gvec;

% State definition
ahat = ahat_in(1:3);

% Input
omega_m = inputs(1:3);
qhat = est_inputs(1:4);

Phat = P_in;
% Corrected omega
omegahat = omega_m - est_inputs(5:7);

% Rotation matrix from previous quaternion
Rot = Quaternion2RotMat(qhat); % Rotation matrix from quaternion

% ++++++++++++++++
% Prediction step
% ++++++++++++++++
% State prediction
ahat = ahat - (skew(omegahat)*ahat + EKF.accest.k_fb*(ahat-Rot'*gvec))*dt;

% State covariance
Fk = eye(3) - dt*(skew(omegahat) + eye(3)*EKF.accest.k_fb);

% Input covariance
dfdw = -dt*skew(ahat_in);
dfdbw = dt*skew(ahat_in);
[~, tmp] = ekfoutlin(qhat,gvec);
dfdq = dt*EKF.accest.k_fb*tmp;

Q_gyro = dfdw*diag(EKF.cov.gyro)*dfdw' + dfdbw*cov_est_inputs(5:7,5:7)*dfdbw';
Q_qatt = dfdq*cov_est_inputs(1:4,1:4)*dfdq';
Qk =  Q_gyro + Q_qatt;

Phat = Fk*Phat*Fk' + Qk;

b_outlier = 0;
l_nu = 0;
% +++++++++++++++++++++++++++++++++++++++++++++
% State correction
% +++++++++++++++++++++++++++++++++++++++++++++
% #### Correction by magnetometer ####

    nu = accm - ahat;
    H = eye(3);
    R = EKF.Ra;
    R_mod = R;
    
    boutlier_vec = zeros(3,1);
    % Outlier check
    nsigma = 3;
    for k = 1:3
        if abs(nu(k)) > nsigma*(sqrt(Phat(k,k))+sqrt(R(k,k)))
            R_mod(k,k) = (abs(nu(k))-nsigma*sqrt(Phat(k,k)))^2;
            boutlier_vec(k) = 1;
        end
    end
    b_outlier = double(sum(boutlier_vec)>1);
    
    S = H*Phat*H' + R;
    S_out_acc = S;
    delta_out_acc = nu;

%     l_nu = nu'*pinv(S)*nu;
%     if l_nu > EKF.stats.l_det_3_3sig
%         b_outlier = 1;
%     end
if b_norm < 0.5    
    S = H*Phat*H' + R_mod;
    K = H*Phat/S;
else
    K = zeros(3);
    R = zeros(3);
    H = zeros(3);
    nu = zeros(3,1);
end
ahat = ahat + (K*nu);
Phat =  (eye(nx)-K*H)*Phat*(eye(nx)-K*H)' + K*R_mod*K'; % Joseph form

% Magnetic reference disturbance estimation
% Estimated inertial disturbance from mag_hat
delta_pm_acc = Rot*ahat - gvec;
% [ddmdq,~] = ekfoutlin(qhat,ahat);
[ddmdq,~] = ekfoutlin(qhat,gvec);

S_pm_acc = ddmdq*cov_est_inputs(1:4,1:4)*ddmdq' + Rot*Phat*Rot';
% S_pm_acc = Rot*Phat*Rot';
l_delta_acc = delta_pm_acc(1:2)'*pinv(S_pm_acc(1:2,1:2))*delta_pm_acc(1:2);
b_warn = double(l_delta_acc > EKF.stats.l_det_3_3sig);

% Estimated inertial disturbance from mag_m
% delta_acc_m = Rot*accm - gvec;
% [ddmdq,~] = ekfoutlin(qhat,accm);

% cov_delta_acc_m = ddmdq*cov_est_inputs(1:4,1:4)*ddmdq' + Rot*EKF.Rm*Rot';
% l_delta_acc_m = delta_acc_m'*pinv(cov_delta_acc_m)*delta_acc_m;
% b_delta_acc_m = double(l_delta_acc_m > EKF.stats.l_det_3_3sig);
% 
% delta_acc_m_out = Rot'*double(b_delta_acc_m)*delta_acc_m;%*(1-double(b_delta_mag))*delta_mag_m;
% cov_delta_acc_m_out = Rot'*double(b_delta_acc_m)*(1-double(b_warn))*cov_delta_acc_m*Rot;

ahat_out = ahat;
P_out = Phat;

status_vec = [b_outlier; b_warn];
end


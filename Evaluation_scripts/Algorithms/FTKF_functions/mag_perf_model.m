function [ mhat_out, P_out, status_vec, S_pm_mag, delta_pm_mag,...
        S_out_mag, delta_out_mag ] = mag_perf_model(EKF, mhat_in, P_in, inputs, magm, est_inputs, cov_est_inputs, b_norm, dt )
%SENSOR_PERF_MODEL Summary of this function goes here
%   Detailed explanation goes here

nx = numel(mhat_in);
mag_ref = EKF.mag_ref;

% State definition
mhat = mhat_in(1:3);

% Input
omega_m = inputs(1:3);
qhat = est_inputs(1:4);

Phat = P_in;
% Corrected omega
omegahat = omega_m - est_inputs(5:7);

% Rotation matrix from previous quaternion
Rot = Quaternion2RotMat(qhat); % Rotation matrix from quaternion

% ++++++++++++++++
% Prediction step
% ++++++++++++++++
% State prediction
mhat = mhat - (skew(omegahat)*mhat + EKF.magest.k_fb*(mhat-Rot'*mag_ref))*dt;

% State covariance
Fk = eye(3) - dt*(skew(omegahat) + eye(3)*EKF.magest.k_fb);

% Input covariance
dfdw = -dt*skew(mhat_in);
dfdbw = dt*skew(mhat_in);
[~, tmp] = ekfoutlin(qhat,mag_ref);
dfdq = dt*EKF.magest.k_fb*tmp;

Q_gyro = dfdw*diag(EKF.cov.gyro)*dfdw' + dfdbw*cov_est_inputs(5:7,5:7)*dfdbw';
Q_qatt = dfdq*cov_est_inputs(1:4,1:4)*dfdq';
Qk =  Q_gyro + Q_qatt;

Phat = Fk*Phat*Fk' + Qk;

b_outlier = 0;
l_nu = 0;
% +++++++++++++++++++++++++++++++++++++++++++++
% State correction
% +++++++++++++++++++++++++++++++++++++++++++++
% #### Correction by magnetometer ####

nu_m = magm - mhat;
Hm = eye(3);
Rm = EKF.Rm;
Rm_mod = Rm;

boutlier_vec = zeros(3,1);
% Outlier check
nsigma = 3;
for k = 1:3
    if abs(nu_m(k)) > nsigma*(sqrt(Phat(k,k))+sqrt(Rm(k,k)))
        Rm_mod(k,k) = (abs(nu_m(k))/nsigma)^2;
        %             Rm_mod(k,k) = (abs(nu_m(k))-nsigma*sqrt(Phat(k,k)))^2;
        %nu_m(k) = nsigma*sqrt(Rm(k,k))*sign(nu_m(k));
        boutlier_vec(k) = 1;
    end
end
b_outlier = double(sum(boutlier_vec)>1);

Sm = Hm*Phat*Hm' + Rm;
S_out_mag = Sm;
delta_out_mag = nu_m;

% l_nu = nu_m'*pinv(Sm)*nu_m;
% if l_nu > EKF.stats.l_det_3_3sig
%     b_outlier = 1;
% end
if b_norm < 0.5    
    Sm = Hm*Phat*Hm' + EKF.kcov_mag_perf*Rm_mod;
    Km = Hm*Phat/Sm;
else
    Km = zeros(3);
    Rm = zeros(3);
    Rm_mod = Rm;
    Hm = zeros(3);
    nu_m = zeros(3,1);
end
mhat = mhat + (Km*nu_m);
Phat =  (eye(nx)-Km*Hm)*Phat*(eye(nx)-Km*Hm)' + Km*Rm_mod*Km'; % Joseph form

% Magnetic reference disturbance estimation
% Estimated inertial disturbance from mag_hat
delta_pm_mag = Rot*mhat - mag_ref;
% [ddmdq,~] = ekfoutlin(qhat,mhat);
[ddmdq,~] = ekfoutlin(qhat,mag_ref);

S_pm_mag = ddmdq*cov_est_inputs(1:4,1:4)*ddmdq' + Rot*Phat*Rot';
% S_pm_mag = Rot*Phat*Rot';
l_delta_mag = delta_pm_mag(1:2)'*pinv(S_pm_mag(1:2,1:2))*delta_pm_mag(1:2);
b_warn = double(l_delta_mag > EKF.stats.l_det_3_3sig);

% Estimated inertial disturbance from mag_m
% delta_mag_m = Rot*magm - mag_ref;
% [ddmdq,~] = ekfoutlin(qhat,magm);
% 
% cov_delta_mag_m = ddmdq*cov_est_inputs(1:4,1:4)*ddmdq' + Rot*EKF.Rm*Rot';
% % l_delta_mag = delta_mag(1:2)'*pinv(cov_delta_mag(1:2,1:2))*delta_mag(1:2);
% l_delta_mag_m = delta_mag_m'*pinv(cov_delta_mag_m)*delta_mag_m;
% b_delta_mag_m = double(l_delta_mag_m > EKF.stats.l_det_3_3sig);
% 
% delta_mag_m_out = Rot'*double(b_delta_mag_m)*delta_mag_m;%*(1-double(b_delta_mag))*delta_mag_m;
% cov_delta_mag_m_out = Rot'*double(b_delta_mag_m)*(1-double(b_warn))*cov_delta_mag_m*Rot;

mhat_out = mhat;
P_out = Phat;

status_vec = [b_outlier; b_warn];
end


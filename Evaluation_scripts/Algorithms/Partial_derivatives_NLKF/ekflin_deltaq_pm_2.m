function [ dqcdq, dqcdm ] = ekflin_deltaq_pm_2( R,qatt,mb,k, Deltapsi, y1, y2, mag_ref)
%EKFLIN_DELTAQ Calculate linearisation for partial quaternion correction
%   y1 = mref'*(R*mb x e3)
%   y2 = (mref x e3)'*(R*mb x e3)
%
%   This function calculates the partial derivatives:
%   dy1/dxi, xi = s, x, y, z, mb1, mb2, mb3
%   dy2/dxi, xi = s, x, y, z, mb1, mb2, mb3

mi = R*mb; % Magnetic measurement in inerial frame
mi1 = mi(1); mi2 = mi(2);

mr1 = mag_ref(1); mr2 = mag_ref(2); mr3 = mag_ref(3); 

s = qatt(1); x = qatt(2); y = qatt(3); z = qatt(4);
m1 = mb(1); m2 = mb(2); m3 = mb(3);

C = cos(Deltapsi); S = sin(Deltapsi);

% Calculate:  dy1/dxi, xi = s, x, y, z, mb1, mb2, mb3
%             dy2/dxi, xi = s, x, y, z, mb1, mb2, mb3
dy1dmb = [mr1*R(2,1)-mr2*R(1,1), mr1*R(2,2)-mr2*R(1,2), mr1*R(2,3)-mr2*R(1,3)];
dy2dmb = [mr1*R(1,1)+mr2*R(2,1), mr1*R(1,2)+mr2*R(2,2), mr1*R(1,3)+mr2*R(2,3)];
dy1dq = [- mr1*(2*m3*x - 2*m1*z) - mr2*(2*m3*y - 2*m2*z),...
         - mr2*(2*m2*y + 2*m3*z) - mr1*(2*m3*s + 4*m2*x - 2*m1*y),...
         mr1*(2*m1*x + 2*m3*z) - mr2*(2*m3*s + 2*m2*x - 4*m1*y),...
         mr2*(2*m2*s - 2*m3*x + 4*m1*z) + mr1*(2*m1*s + 2*m3*y - 4*m2*z)];
dy2dq = [mr1*(2*m3*y - 2*m2*z) - mr2*(2*m3*x - 2*m1*z),...
         mr1*(2*m2*y + 2*m3*z) - mr2*(2*m3*s + 4*m2*x - 2*m1*y),...
         mr2*(2*m1*x + 2*m3*z) + mr1*(2*m3*s + 2*m2*x - 4*m1*y),...
         mr2*(2*m1*s + 2*m3*y - 4*m2*z) - mr1*(2*m2*s - 2*m3*x + 4*m1*z)];

% cpsi = cos(Deltapsi); spsi = sin(Deltapsi)
% Calculate: dcpsi/dxi and dspsi/dxi, xi = s, x, y, z, mb1, mb2, mb3
dDeltapsidq = ekflin_deltaq_pm_atanpart_2( dy1dq, dy2dq, y1, y2, k);
dDeltapsidmb = ekflin_deltaq_pm_atanpart_2( dy1dmb, dy2dmb, y1, y2, k);

dcpsidq = -sin(Deltapsi)*dDeltapsidq;
dspsidq = cos(Deltapsi)*dDeltapsidq;

dcpsidm = -sin(Deltapsi)*dDeltapsidmb;
dspsidm = cos(Deltapsi)*dDeltapsidmb;

dsdq = [1 0 0 0]; dxdq = [0 1 0 0]; dydq = [0 0 1 0]; dzdq = [0 0 0 1];

dqcdq = [dcpsidq*s + C*dsdq - dspsidq*z - S*dzdq;
         dcpsidq*x + C*dxdq + dspsidq*y - S*dydq;
         dcpsidq*y + C*dydq - dspsidq*x - S*dxdq;
         dcpsidq*z + C*dzdq + dspsidq*s - S*dsdq];

dqcdm = [dcpsidm*s - dspsidm*z;
         dcpsidm*x - dspsidm*y;
         dcpsidm*y - dspsidm*x;
         dcpsidm*z - dspsidm*s];


end


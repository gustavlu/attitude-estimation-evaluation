function [ dqcdq, dqcdm ] = ekflin_deltaq_pm( R,qatt,mb,k, Deltapsi)
%EKFLIN_DELTAQ Calculate linearisation for partial quaternion correction
%   y1 = mref'*(R*mb x e3)
%   y2 = (mref x e3)'*(R*mb x e3)
%
%   This function calculates the partial derivatives:
%   dy1/dxi, xi = s, x, y, z, mb1, mb2, mb3
%   dy2/dxi, xi = s, x, y, z, mb1, mb2, mb3

mi = R*mb; % Magnetic measurement in inerial frame
mi1 = mi(1); mi2 = mi(2);

s = qatt(1); x = qatt(2); y = qatt(3); z = qatt(4);
m1 = mb(1); m2 = mb(2); m3 = mb(3);

C = cos(Deltapsi); S = sin(Deltapsi);

% Calculate:  dy1/dxi, xi = s, x, y, z, mb1, mb2, mb3
%             dy2/dxi, xi = s, x, y, z, mb1, mb2, mb3
dmi1ds = 2*m2*z+2*m3*y;
dmi1dx = -2*m2*y-2*m3*z;
dmi1dy = -4*m1*y-2*m2*x+2*m3*s;
dmi1dz = -4*m1*z+-2*m2*s+2*m3*x;

dmi2ds = 2*m1*z-2*m3*x;
dmi2dx = -4*m2*x+2*m1*y-2*m3*s;
dmi2dy = 2*m1*x+2*m3*z;
dmi2dz = -4*m2*z+2*m1*s+2*m3*y;

dmi1dmb1 = R(1,1);
dmi1dmb2 = R(1,2);
dmi1dmb3 = R(1,3);

dmi2dmb1 = R(2,1);
dmi2dmb2 = R(2,2);
dmi2dmb3 = R(2,3);

% Calculate: dpsim/dxi, xi = s, x, y, z, mb1, mb2, mb3
dpsimds = ekflin_deltaq_pm_atanpart( dmi1ds, dmi2ds, mi1, mi2, k);
dpsimdx = ekflin_deltaq_pm_atanpart( dmi1dx, dmi2dx, mi1, mi2, k);
dpsimdy = ekflin_deltaq_pm_atanpart( dmi1dy, dmi2dy, mi1, mi2, k);
dpsimdz = ekflin_deltaq_pm_atanpart( dmi1dz, dmi2dz, mi1, mi2, k);

dpsimdmb1 = ekflin_deltaq_pm_atanpart( dmi1dmb1, dmi2dmb1, mi1, mi2, k);
dpsimdmb2 = ekflin_deltaq_pm_atanpart( dmi1dmb2, dmi2dmb2, mi1, mi2, k);
dpsimdmb3 = ekflin_deltaq_pm_atanpart( dmi1dmb3, dmi2dmb3, mi1, mi2, k);
% dmi1dmb3, mi1,
% (1+(dmi2dmb3/(dmi1dmb3+eps))^2)*(dmi2dmb3*mi1-mi2*dmi1dmb3)/(mi1+eps)^2
% Calculate: dCdxi, xi = s, x, y, z, mb1, mb2, mb3
%            dSdxi, xi = s, x, y, z, mb1, mb2, mb3
dcpsids = -sin(Deltapsi)*dpsimds;
dcpsidx = -sin(Deltapsi)*dpsimdx;
dcpsidy = -sin(Deltapsi)*dpsimdy;
dcpsidz = -sin(Deltapsi)*dpsimdz;

dcpsidq = [dcpsids, dcpsidx, dcpsidy, dcpsidz];

dspsids = cos(Deltapsi)*dpsimds;
dspsidx = cos(Deltapsi)*dpsimdx;
dspsidy = cos(Deltapsi)*dpsimdy;
dspsidz = cos(Deltapsi)*dpsimdz;

dspsidq = [dspsids, dspsidx, dspsidy, dspsidz];

dcpsidmb1 = -sin(Deltapsi)*dpsimdmb1;
dcpsidmb2 = -sin(Deltapsi)*dpsimdmb2;
dcpsidmb3 = -sin(Deltapsi)*dpsimdmb3;

dcpsidm = [dcpsidmb1, dcpsidmb2, dcpsidmb3];

dspsidmb1 = cos(Deltapsi)*dpsimdmb1;
dspsidmb2 = cos(Deltapsi)*dpsimdmb2;
dspsidmb3 = cos(Deltapsi)*dpsimdmb3;

dspsidm = [dspsidmb1, dspsidmb2, dspsidmb3];

dsdq = [1 0 0 0]; dxdq = [0 1 0 0]; dydq = [0 0 1 0]; dzdq = [0 0 0 1];

dqcdq = [dcpsidq*s + C*dsdq - dspsidq*z - S*dzdq;
         dcpsidq*x + C*dxdq + dspsidq*y - S*dydq;
         dcpsidq*y + C*dydq - dspsidq*x - S*dxdq;
         dcpsidq*z + C*dzdq + dspsidq*s - S*dsdq];

dqcdm = [dcpsidm*s - dspsidm*z;
         dcpsidm*x - dspsidm*y;
         dcpsidm*y - dspsidm*x;
         dcpsidm*z - dspsidm*s];


end


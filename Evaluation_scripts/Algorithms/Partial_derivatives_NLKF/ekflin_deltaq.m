function [ dqcdq, dqcdmb, dscapdz ] = ekflin_deltaq( qatt,mref,mb,k, y1, y2, C, S, s1, c1)
%EKFLIN_DELTAQ Calculate linearisation for partial quaternion correction
%   y1 = mref'*(R*mb x e3)
%   y2 = (mref x e3)'*(R*mb x e3)
%
%   This function calculates the partial derivatives:
%   dy1/dxi, xi = s, x, y, z, mb1, mb2, mb3
%   dy2/dxi, xi = s, x, y, z, mb1, mb2, mb3

s = qatt(1); x = qatt(2); y = qatt(3); z = qatt(4);
m1 = mb(1); m2 = mb(2); m3 = mb(3);
mr1 = mref(1); mr2 = mref(2); mr3 = mref(3);

% Calculate:  dy1/dxi, xi = s, x, y, z, mb1, mb2, mb3
%             dy2/dxi, xi = s, x, y, z, mb1, mb2, mb3
dy1ds = - mr1*(2*m3*x - 2*m1*z) - mr2*(2*m3*y - 2*m2*z);
dy1dx = - mr2*(2*m2*y + 2*m3*z) - mr1*(2*m3*s + 4*m2*x - 2*m1*y);
dy1dy = mr1*(2*m1*x + 2*m3*z) - mr2*(2*m3*s + 2*m2*x - 4*m1*y);
dy1dz = mr2*(2*m2*s - 2*m3*x + 4*m1*z) + mr1*(2*m1*s + 2*m3*y - 4*m2*z);

dy2ds = mr1*(2*m3*y - 2*m2*z) - mr2*(2*m3*x - 2*m1*z);
dy2dx = mr1*(2*m2*y + 2*m3*z) - mr2*(2*m3*s + 4*m2*x - 2*m1*y);
dy2dy = mr2*(2*m1*x + 2*m3*z) + mr1*(2*m3*s + 2*m2*x - 4*m1*y);
dy2dz = mr2*(2*m1*s + 2*m3*y - 4*m2*z) - mr1*(2*m2*s - 2*m3*x + 4*m1*z);

dy1dmb1 = mr2*(2*y^2 + 2*z^2 - 1) + mr1*(2*s*z + 2*x*y);
dy1dmb2 = mr2*(2*s*z - 2*x*y) - mr1*(2*x^2 + 2*z^2 - 1);
dy1dmb3 = - mr1*(2*s*x - 2*y*z) - mr2*(2*s*y + 2*x*z);
 
dy2dmb1 =  mr2*(2*s*z + 2*x*y) - mr1*(2*y^2 + 2*z^2 - 1);
dy2dmb2 = - mr2*(2*x^2 + 2*z^2 - 1) - mr1*(2*s*z - 2*x*y);
dy2dmb3 = mr1*(2*s*y + 2*x*z) - mr2*(2*s*x - 2*y*z);
 
% dy1ds = mr2*(2*m2*z-2*m3*y)-mr1*(-2*m1*z+2*m3*x);
% dy1dx = mr2*(-2*m2*y-2*m3*z)-mr1*(4*m2-2*m1*y+2*m3*s);
% dy1dy = mr2*(4*m1*y-2*m2*x-2*m3*s)-mr1*(-2*m1*x-2*m3*z);
% dy1dz = mr2*(4*m1*z+2*m2*s-2*m3*x)-mr1*(4*m2*z-2*m1*s-2*m3*y);
% 
% dy2ds = -mr2*(-2*m1*z+2*m3*x)-mr1*(2*m2*z-2*m3*x);
% dy2dx = -mr2*(4*m2*x-2*m1*y+2*m3*s)-mr1*(-2*m2*y-2*m3*z);
% dy2dy = -mr2*(-2*m1*x-2*m3*z)-mr1*(4*m1*y-2*m2*x-2*m3*x);
% dy2dz = -mr2*(4*m2*z-2*m1*s-2*m3*y)-mr1*(4*m1*z+m2*s-2*m3*x);

% dy1dmb1 = mr2*(2*x^2+2*z^2-1)+mr1*(2*s*z+2*x*y);
% dy1dmb2 = mr2*(2*s*z-2*x*y)-mr1*(2*x^2+z^2-1);
% dy1dmb3 = mr2*(-2*s*y+2*x*z)-mr1*(2*s*x-2*y*z);
% 
% dy2dmb1 = mr2*(-2*s*z+2*x*y)-mr1*(2*y^2+z^2-1);
% dy2dmb2 = -mr2*(2*x^2+2*z^2-1)-mr1*(2*s*z-2*x*y);
% dy2dmb3 = -mr2*(2*s*x-2*y*z)+mr1*(2*s*y+2*x*z);

% Calculate: dsy/dxi, xi = s, x, y, z, mb1, mb2, mb3
%            dcy/dxi, xi = s, x, y, z, mb1, mb2, mb3
[ dsyds, dcyds ] = ektlin_deltaq_cspart( y1, y2, dy1ds, dy2ds);
[ dsydx, dcydx ] = ektlin_deltaq_cspart( y1, y2, dy1dx, dy2dx);
[ dsydy, dcydy ] = ektlin_deltaq_cspart( y1, y2, dy1dy, dy2dy);
[ dsydz, dcydz ] = ektlin_deltaq_cspart( y1, y2, dy1dz, dy2dz);

[ dsydmb1, dcydmb1 ] = ektlin_deltaq_cspart( y1, y2, dy1dmb1, dy2dmb1);
[ dsydmb2, dcydmb2 ] = ektlin_deltaq_cspart( y1, y2, dy1dmb2, dy2dmb2);
[ dsydmb3, dcydmb3 ] = ektlin_deltaq_cspart( y1, y2, dy1dmb3, dy2dmb3);

% Calculate: dS/dxi, xi = s, x, y, z, mb1, mb2, mb3
%            dC/dxi, xi = s, x, y, z, mb1, mb2, mb3
[ dscapds, dccapds ] = ekflin_deltaq_gampart( s1, c1, dsyds, dcyds, k );
[ dscapdx, dccapdx ] = ekflin_deltaq_gampart( s1, c1, dsydx, dcydx, k );
[ dscapdy, dccapdy ] = ekflin_deltaq_gampart( s1, c1, dsydy, dcydy, k );
[ dscapdz, dccapdz ] = ekflin_deltaq_gampart( s1, c1, dsydz, dcydz, k );

[ dscapdmb1, dccapdmb1 ] = ekflin_deltaq_gampart( s1, c1, dsydmb1, dcydmb1, k );
[ dscapdmb2, dccapdmb2 ] = ekflin_deltaq_gampart( s1, c1, dsydmb2, dcydmb2, k );
[ dscapdmb3, dccapdmb3 ] = ekflin_deltaq_gampart( s1, c1, dsydmb3, dcydmb3, k );

% df1ds = C+dccapds*s-dscapds*z;
% df1dx = dccapdx*s-dscapdx*z;
% df1dy = dccapdy*s-dscapdy*z;
% df1dz = dccapdz*s-S-dscapdz*z;

% df2ds = dccapds*x-dscapds*y;
% df2dx = C+dccapdx*x-dscapdx*y;
% df2dy = dccapdy*x-S-dscapdy*y;
% dfsdz = dccapdz*x-dscapdz*y;

% df3ds = dccapds*y+dscapds*x;
% df3dx = dccapdx*y+dscapdx*x+S;
% df3dy = C+dccapdy*y+dscapdy*x;
% df3dz = dccapdz*y+dscapdz*x;

% df4ds = dccapds*z+dscapds*s+S;
% df4dx = dccapdx*z+dscapdx*s;
% df4dy = dccapdy*z+dscapdy*s;
% df4dz = C+dccapdz*z+dscapdz*s;

% dfdq = [df1ds df1dx df1dy df1dz;
        % df2ds df2dx df2dy dfsdz;
        % df3ds df3dx df3dy df3dz;
        % df4ds df4dx df4dy df4dz];

dcpsidq = [dccapds, dccapdx, dccapdy, dccapdz];
dspsidq = [dscapds, dscapdx, dscapdy, dscapdz];

dcpsidm = [dccapdmb1, dccapdmb2, dccapdmb3];
dspsidm = [dscapdmb1, dscapdmb2, dscapdmb3];

dsdq = [1 0 0 0]; dxdq = [0 1 0 0]; dydq = [0 0 1 0]; dzdq = [0 0 0 1];

dqcdq = [dcpsidq*s + C*dsdq - dspsidq*z - S*dzdq;
         dcpsidq*x + C*dxdq + dspsidq*y - S*dydq;
         dcpsidq*y + C*dydq - dspsidq*x - S*dxdq;
         dcpsidq*z + C*dzdq + dspsidq*s - S*dsdq];

dqcdmb = [dcpsidm*s - dspsidm*z;
         dcpsidm*x - dspsidm*y;
         dcpsidm*y - dspsidm*x;
         dcpsidm*z - dspsidm*s];
		 
% dfdq = [df1ds 0 0 df1dz;
%         df2ds 0 0 dfsdz;
%         df3ds 0 0 df3dz;
%         df4ds 0 0 df4dz];

% dfdmb = zeros(4,3);
% dccap = [dccapdmb1, dccapdmb2, dccapdmb3];
% dscap = [dscapdmb1, dscapdmb2, dscapdmb3];
%for kl = 1:3
%    dfdmb(1,kl) = dccap(kl)*s-dscap(kl)*z;
%    dfdmb(2,kl) = dccap(kl)*x-dscap(kl)*y;
%    dfdmb(3,kl) = dccap(kl)*y-dscap(kl)*x;
%    dfdmb(2,kl) = dccap(kl)*z-dscap(kl)*s;
%end


end


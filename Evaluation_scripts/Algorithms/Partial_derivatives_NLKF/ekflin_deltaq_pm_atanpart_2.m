function [ ddeltapsidxi ] = ekflin_deltaq_pm_atanpart_2( dy1dv, dy2dv, y1, y2, k)
%EKFLIN_DELTAQ_PM_DPSIPART Calculates the partial derivative of
% k/2*atan(mi2(xi)/mi1(xi)) w.r.t. xi

ddeltapsidxi = 0.5*k * 1/(1+(y2/(y1+eps))^2) * (dy2dv*y1-y2*dy1dv)/((y1)^2+eps);

end


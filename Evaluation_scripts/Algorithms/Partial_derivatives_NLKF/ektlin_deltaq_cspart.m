function [ dsydxi, dcydxi ] = ektlin_deltaq_cspart( y1, y2, dy1dxi, dy2dxi)
%EKTLIN_DELTAQ_CSPART Calculates partial derivatve of normalized quantities
%cy and sy
%   sy = y1/sqrt(y1^2+y2^2)
%   cy = y2/sqrt(y1^2+y2^2)

dsydxi = (dy1dxi*(y1^2+y2^2)^(1/2)-y1*1/2*(y1^2+y2^2)^(-1/2)*(2*y1*dy1dxi+2*y2*dy2dxi))/(y1^2+y2^2);
dcydxi = (dy2dxi*(y1^2+y2^2)^(1/2)-y2*1/2*(y1^2+y2^2)^(-1/2)*(2*y1*dy1dxi+2*y2*dy2dxi))/(y1^2+y2^2);
end


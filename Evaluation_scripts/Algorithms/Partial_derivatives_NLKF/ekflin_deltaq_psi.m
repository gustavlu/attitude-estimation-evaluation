function [ dpsidq, dpsimdm ] = ekflin_deltaq_psi( qatt,Rot,mb,mi)
%EKFLIN_DELTAQ Calculate linearisation for partial quaternion correction
%   y1 = mref'*(R*mb x e3)
%   y2 = (mref x e3)'*(R*mb x e3)
%
%   This function calculates the partial derivatives:
%   dy1/dxi, xi = s, x, y, z, mb1, mb2, mb3
%   dy2/dxi, xi = s, x, y, z, mb1, mb2, mb3

s = qatt(1); x = qatt(2); y = qatt(3); z = qatt(4);
m1 = mb(1); m2 = mb(2); m3 = mb(3);

% Calculate dmi/xi, xi = mb, q
dmi1dm = Rot(1,1:3);
dmi2dm = Rot(2,1:3);

dmi1dq = [ 2*m3*y - 2*m2*z, 2*m2*y + 2*m3*z, 2*m3*s + 2*m2*x - 4*m1*y, 2*m3*x - 2*m2*s - 4*m1*z];
dmi2dq = [ 2*m1*z - 2*m3*x, 2*m1*y - 4*m2*x - 2*m3*s, 2*m1*x + 2*m3*z, 2*m1*s + 2*m3*y - 4*m2*z];

dpsiargdm = (dmi1dm*mi(2)-mi(1)*dmi2dm)/(mi(1)^2+eps);
dpsiargdq = (dmi1dq*mi(2)-mi(1)*dmi2dq)/(mi(1)^2+eps);

% psi_m = atan(mi1/mi2)
dpsimdm = (1/(1+(mi(2)/mi(1))^2))*dpsiargdm;
dpsidq = (1/(1+(mi(2)/mi(1))^2))*dpsiargdq;

end


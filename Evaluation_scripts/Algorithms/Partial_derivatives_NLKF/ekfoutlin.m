function [Hk, Hktp] = ekfoutlin(q,v)
% Calculates output linearization H_k for EKF given a unit quaternion and a
% measurement vector.
%
% Input: 
% q - unit quaternion
% v - measurement 3D column vector
%
% Output:
% Hk = Jacobian for R
% Hktp = Jacobian for R'

s = q(1);
x = q(2);
y = q(3);
z = q(4);

v1 = v(1); v2 = v(2); v3 = v(3);

Hk = 2 * [v3*y-v2*z, v2*y+v3*z, 	   v3*s+v2*x-2*v1*y, v3*x-v2*s-2*v1*z;
          v1*z-v3*x, v1*y-2*v2*x-v3*s, v1*x+v3*z, 	     v1*s+v3*y-2*v2*z;
          v2*x-v1*y, v2*s-2*v3*x+v1*z, v2*z-2*v3*y-v1*s, v1*x+v2*y];

Hktp  = 2 * [ v2*z-v3*y, v2*y+v3*z,           v2*x-v3*s-2*v1*y,     v2*s+v3*x-2*v1*z;
              v3*x-v1*z, v3*s-2*v2*x + v1*y, v1*x+2*v3*z,           v3*y-v1*s-2*v2*z;
              v1*y-v2*x, v1*z-2*v3*x - v2*s, v1*s-2*v3*y + 2*v2*z, v1*x+v2*y];
end
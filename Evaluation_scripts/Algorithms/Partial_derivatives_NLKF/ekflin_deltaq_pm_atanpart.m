function [ ddeltapsidxi ] = ekflin_deltaq_pm_atanpart( dmi1dxi, dmi2dxi, mi1, mi2, k)
%EKFLIN_DELTAQ_PM_DPSIPART Calculates the partial derivative of
% k/2*atan(mi2(xi)/mi1(xi)) w.r.t. xi

ddeltapsidxi = -0.5*k*1/(1+(dmi2dxi/(dmi1dxi+eps))^2)*(dmi2dxi*mi1-mi2*dmi1dxi)/((mi1)^2+eps);

end


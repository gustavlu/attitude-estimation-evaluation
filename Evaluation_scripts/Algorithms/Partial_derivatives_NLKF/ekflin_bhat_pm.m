function [tmp, dfdq, dfdmb ] = ekflin_bhat_pm( R,qatt,mb,kpsi, kb, mag_ref)
%EKFLIN_DELTAQ Calculate linearisation for gyroscope heading bias
%
%   This function calculates the partial derivatives:
%   dy1/dxi, xi = s, x, y, z, mb1, mb2, mb3
%   dy2/dxi, xi = s, x, y, z, mb1, mb2, mb3

mi = R*mb; % Magnetic measurement in inerial frame
mi1 = mi(1); mi2 = mi(2);

s = qatt(1); x = qatt(2); y = qatt(3); z = qatt(4);
m1 = mb(1); m2 = mb(2); m3 = mb(3);

% mi1 = mag_ref(1); mi2 = mag_ref(2);

% Calculate:  dy1/dxi, xi = s, x, y, z, mb1, mb2, mb3
%             dy2/dxi, xi = s, x, y, z, mb1, mb2, mb3
dmi1ds = 2*m2*z+2*m3*y;
dmi1dx = -2*m2*y-2*m3*z;
dmi1dy = -4*m1*y-2*m2*x+2*m3*s;
dmi1dz = -4*m1*z+-2*m2*s+2*m3*x;

dmi2ds = 2*m1*z-2*m3*x;
dmi2dx = -4*m2*x+2*m1*y-2*m3*s;
dmi2dy = 2*m1*x+2*m3*z;
dmi2dz = -4*m2*z+2*m1*s+2*m3*y;

dmi1dmb1 = R(1,1);
dmi1dmb2 = R(1,2);
dmi1dmb3 = R(1,3);

dmi2dmb1 = R(2,1);
dmi2dmb2 = R(2,2);
dmi2dmb3 = R(2,3);


% Calculate: dpsim/dxi, xi = s, x, y, z, mb1, mb2, mb3
dpsimds = ekflin_deltaq_pm_atanpart( dmi1ds, dmi2ds, mi1, mi2, kpsi);
dpsimdx = ekflin_deltaq_pm_atanpart( dmi1dx, dmi2dx, mi1, mi2, kpsi);
dpsimdy = ekflin_deltaq_pm_atanpart( dmi1dy, dmi2dy, mi1, mi2, kpsi);
dpsimdz = ekflin_deltaq_pm_atanpart( dmi1dz, dmi2dz, mi1, mi2, kpsi);

dpsimdmb1 = ekflin_deltaq_pm_atanpart( dmi1dmb1, dmi2dmb1, mi1, mi2, kpsi);
dpsimdmb2 = ekflin_deltaq_pm_atanpart( dmi1dmb2, dmi2dmb2, mi1, mi2, kpsi);
dpsimdmb3 = ekflin_deltaq_pm_atanpart( dmi1dmb3, dmi2dmb3, mi1, mi2, kpsi);

dfdq = kb/kpsi*[dpsimds dpsimdx dpsimdy dpsimdz];

dfdmb = kb/kpsi*[dpsimdmb1 dpsimdmb2 dpsimdmb3];
% tmp.q1 = dpsimds;
% tmp.q2 = dpsimdx;
% tmp.q3 = dpsimdy;
% tmp.q4 = dpsimdz;
% tmp.m1 = dpsimdmb1;
% tmp.m2 = dpsimdmb2;
% tmp.m3 = dpsimdmb3;

tmp.q1 = dmi1ds;
tmp.q2 = dmi1dx;
tmp.q3 = dmi1dy;
tmp.q4 = dmi1dz;
tmp.m1 = dpsimdmb1;
tmp.m2 = dpsimdmb2;
tmp.m3 = dpsimdmb3;


end


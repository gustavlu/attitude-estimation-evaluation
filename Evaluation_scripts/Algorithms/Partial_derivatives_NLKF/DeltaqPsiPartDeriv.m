% DeltaqPsiPartDeriv
%
% Symbolic calculations for the partial derivatives in the attitude EKF.
%%
% Measurements
syms a1 a2 a3 m1 m2 m3
am = [a1; a2; a3];
mm = [m1; m2; m3];

% References
syms g mr1 mr2 mr3
e3 = [0;0;1];
mref = [mr1; mr2; mr3];
gvec = [0;0;-g];

% Quaternion
syms s x y z
q = [s; x; y; z];

% assume([s x y z a1 a2 a3 m1 m2 m3 g mr1 mr2 mr3],'real')
% Rot matrix
syms R11 R12 R13 R21 R22 R23 R31 R32 R33

R11 = 1-2*(y^2+z^2);
R12 = 2*(x*y-s*z);
R13 = 2*(x*z+s*y);
R21 = 2*(x*y+s*z);
R22 = 1-2*(x^2+z^2);
R23 = 2*(y*z-s*x);
R31 = 2*(x*z-s*y);
R32 = 2*(z*y+s*x);
R33 = 1-2*(x^2+y^2);

R = [R11 R12 R13; R21 R22 R23; R31 R32 R33];


%%  deltaq calculation
syms k

y1 = mref'*(cross(R'*mm,e3));
y2 = cross(mref,e3)'*(cross(R'*mm,e3));
ym = [y1;y2];

s1 = y1/symnorm(ym);
c1 = y1/symnorm(ym);

cplx = (c1+1i*s1)^(k/2);
 
C = real(cplx);
S = imag(cplx);

deltapsi = atan(s1/c1);
Delta_psi = deltapsi*k/2;

% C = cos(Delta_psi);
% S = sin(Delta_psi);

dCdq = [diff(C,s),diff(C,x),diff(C,y),diff(C,z)];
dCdm = [diff(C,m1),diff(C,m2),diff(C,m3)];

dSdq = [diff(S,s),diff(S,x),diff(S,y),diff(S,z)];
dSdm = [diff(S,m1),diff(S,m2),diff(S,m3)];

%% Deltapsi direct covariance calc
mi = R*mm;
mi1 = mi(1);
mi2 = mi(2);

dmidm = [diff(mi,m1),diff(mi,m2),diff(mi,m3)];
dmidq = [diff(mi,s), diff(mi,x), diff(mi,y), diff(mi,z)];

dmi1dq = dmidq(1,:);
dmi2dq = dmidq(2,:);

dmi1dm = dmidm(1,:);
dmi2dm = dmidm(2,:);

dpsimdmi = 1/(1+(mi(2)/mi(1))^2);
dpsimdq = dpsimdmi*(dmi1dq*mi2-mi1*dmi2dq)/mi1^2;
dpsimdm = dpsimdmi*(dmi1dm*mi2-mi1*dmi2dm)/mi1^2;

dpsicdq = (1-k)*dpsimdq;
dpsicdm = k*dpsimdm;





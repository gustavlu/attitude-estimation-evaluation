function [ dqcdq, dqcdam, ddalphadq, ddalphadam ] = ...
    ekflin_qcorr_inclination( kalpha, qhat, uhat, am, ym, Delta_alpha)
%EKFLIN_QCORR_INCLINATION Partial derivatives calculation for covariance
% update for direct quaternion inclination correction.
%   Detailed explanation goes here
s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
u1 = uhat(1); u2 = uhat(2);
e3 = [0;0;1];

[dymdq, dymdam] = ekflin_qcorr_inclination_ympart( qhat, am, uhat );

betavar = -uhat.'*(cross(e3,ym))/((-e3.'*ym)+eps);

dbetavardym = [u2/(ym(3)+eps), -u1/(ym(3)+eps), (-u2*ym(1)+u1*ym(2))/(ym(3).^2+eps)];

dbetavardq = dbetavardym*dymdq;
dbetavardam = dbetavardym*dymdam;

datanbetadbetavar = 1/(1+betavar^2);

ddalphadq = datanbetadbetavar*dbetavardq;
ddalphadam = datanbetadbetavar*dbetavardam;

dDeltaalphadq = kalpha/2*ddalphadq;
dDeltaalphadam = kalpha/2*ddalphadam;

dcosDeltalphadDeltaalpha = -sin(Delta_alpha);
dsinDeltalphadDeltaalpha = cos(Delta_alpha);

dcadq = dcosDeltalphadDeltaalpha*dDeltaalphadq;
dsadq = dsinDeltalphadDeltaalpha*dDeltaalphadq;

dcadam = dcosDeltalphadDeltaalpha*dDeltaalphadam;
dsadam = dsinDeltalphadDeltaalpha*dDeltaalphadam;

% dqcdsa = [ - u1*x - u2*y; 
%           s*u1 - u2*z; 
%           s*u2 + u1*z; 
%           u2*x - u1*y];
%       
% dqcdca = [s; x; y; z];

ca = cos(Delta_alpha);
sa = sin(Delta_alpha);

dsdq = [1 0 0 0]; dxdq = [0 1 0 0]; dydq = [0 0 1 0]; dzdq = [0 0 0 1];

% dqcdq_1 = [ca*dsdq+dcosDadq*s;
%            ca*dxdq+dcosDadq*x;
%            ca*dydq+dcosDadq*y;
%            ca*dzdq+dcosDadq*z];
% 
% dqcdq_2 = [-u1*(sa*dxdq+dsinDadq*x)-u2*(sa*dydq+dsinDadq*y);
%            -u1*(sa*dsdq+dsinDadq*s)-u2*(sa*dzdq+dsinDadq*z);
%            -u1*(sa*dsdq+dsinDadq*s)-u2*(sa*dsdq+dsinDadq*s);
%            -u1*(sa*dxdq+dsinDadq*x)-u2*(sa*dydq+dsinDadq*y)];
% 
% dqcdq = dqcdq_1 + dqcdq_2;
% 
% dqcdam = dqcdca*dcadam + dqcdsa*dsadam;

dqcdq = [dsdq*ca + s*dcadq + dxdq*u1*sa + x*u1*dsadq + dydq*u2*sa + y*u2*dsadq;
         dsdq*u1*sa + s*u1*dsadq + dxdq*ca + x*dcadq - dzdq*u2*sa - z*u2*dsadq;
         dsdq*u2*sa + s*u2*dsadq + dydq*ca + y*dcadq + dzdq*u1*sa - z*u1*dsadq;
         dzdq*ca + z*dcadq + dxdq*u2*sa + x*u2*dsadq + dydq*u1*sa + y*u1*dsadq];
     
dqcdam = [s*dcadam + x*u1*dsadam + y*u2*dsadam;
          s*u1*dsadam + x*dcadam - z*u2*dsadam;
          s*u2*dsadam + y*dcadam + z*u1*dsadam;
          z*dcadam + x*u2*dsadam + y*u1*dsadam];
      
      
end


function [Hk] = ekfoutlin_perp(q,v)
% Calculate output linearization H_k for EKF given a unit quaternion and a
% measurement vector.
%
% Input: 
% q - unit quaternion
% v - measurement 3D column vector 
s = q(1);
x = q(2);
y = q(3);
z = q(4);

v1 = v(1); v2 = v(2); v3 = v(3);

Hk = 2*[-z*v1+x*v3    -y*v1+2*x*v2+s*v3   -x*v1-z*v3        -s*v1+2*z*v2-y*v3;
      -z*v2+y*v3       y*v2+z*v3          -2*z*v1+x*v2+s*v3  -2*z*v1-s*v2+x*v3;
            0               0                   0                       0];
end


function [ dqcdq, dqcdm ] = ekflin_deltaq_2(  qatt,mref,mb,k, y1, y2, C, S, s1, c1 )
%EKFLIN_DELTAQ_2 Summary of this function goes here
%   Detailed explanation goes here

s = qatt(1); x = qatt(2); y = qatt(3); z = qatt(4);
m1 = mb(1); m2 = mb(2); m3 = mb(3);
mr1 = mref(1); mr2 = mref(2); mr3 = mref(3);

dy1ds = - mr1*(2*m3*x - 2*m1*z) - mr2*(2*m3*y - 2*m2*z);
dy1dx = - mr2*(2*m2*y + 2*m3*z) - mr1*(2*m3*s + 4*m2*x - 2*m1*y);
dy1dy = mr1*(2*m1*x + 2*m3*z) - mr2*(2*m3*s + 2*m2*x - 4*m1*y);
dy1dz = mr2*(2*m2*s - 2*m3*x + 4*m1*z) + mr1*(2*m1*s + 2*m3*y - 4*m2*z);

dy2ds = mr1*(2*m3*y - 2*m2*z) - mr2*(2*m3*x - 2*m1*z);
dy2dx = mr1*(2*m2*y + 2*m3*z) - mr2*(2*m3*s + 4*m2*x - 2*m1*y);
dy2dy = mr2*(2*m1*x + 2*m3*z) + mr1*(2*m3*s + 2*m2*x - 4*m1*y);
dy2dz = mr2*(2*m1*s + 2*m3*y - 4*m2*z) - mr1*(2*m2*s - 2*m3*x + 4*m1*z);

dy1dmb1 = mr2*(2*y^2 + 2*z^2 - 1) + mr1*(2*s*z + 2*x*y);
dy1dmb2 = mr2*(2*s*z - 2*x*y) - mr1*(2*x^2 + 2*z^2 - 1);
dy1dmb3 = - mr1*(2*s*x - 2*y*z) - mr2*(2*s*y + 2*x*z);
 
dy2dmb1 =  mr2*(2*s*z + 2*x*y) - mr1*(2*y^2 + 2*z^2 - 1);
dy2dmb2 = - mr2*(2*x^2 + 2*z^2 - 1) - mr1*(2*s*z - 2*x*y);
dy2dmb3 = mr1*(2*s*y + 2*x*z) - mr2*(2*s*x - 2*y*z);

dy1dq = [dy1ds, dy1dx, dy1dy, dy1dz];
dy2dq = [dy2ds, dy2dx, dy2dy, dy2dz];

dy1dm = [dy1dmb1, dy1dmb2, dy1dmb3];
dy2dm = [dy2dmb1, dy2dmb2, dy2dmb3];

dsdy1 = 1/(y1^2 + y2^2)^(1/2) - y1^2/(y1^2 + y2^2)^(3/2);
dsdy2 = -(y1*y2)/(y1^2 + y2^2)^(3/2);

dcdy1 = -(y1*y2)/(y1^2 + y2^2)^(3/2);
dcdy2 = 1/(y1^2 + y2^2)^(1/2) - y2^2/(y1^2 + y2^2)^(3/2);

dcdq = dcdy1*dy1dq + dcdy2*dy2dq;
dsdq = dsdy1*dy1dq + dsdy2*dy2dq;

dcdm = dcdy1*dy1dm + dcdy2*dy2dm;
dsdm = dsdy1*dy1dm + dsdy2*dy2dm;

dcplxdq = k/2*(c1 + 1i*s1)^(k/2-1)*(dcdq + 1i*dsdq);
dcplxdm = k/2*(c1 + 1i*s1)^(k/2-1)*(dcdm + 1i*dsdm);

dcpsidq = real(dcplxdq);
dspsidq = imag(dcplxdq);

dcpsidm = real(dcplxdm);
dspsidm = imag(dcplxdm);

dsdq = [1 0 0 0]; dxdq = [0 1 0 0]; dydq = [0 0 1 0]; dzdq = [0 0 0 1];

dqcdq = [dcpsidq*s + C*dsdq - dspsidq*z - S*dzdq;
         dcpsidq*x + C*dxdq + dspsidq*y - S*dydq;
         dcpsidq*y + C*dydq - dspsidq*x - S*dxdq;
         dcpsidq*z + C*dzdq + dspsidq*s - S*dsdq];

dqcdm = [dcpsidm*s - dspsidm*z;
         dcpsidm*x - dspsidm*y;
         dcpsidm*y - dspsidm*x;
         dcpsidm*z - dspsidm*s];

% g1 = qatt; G1 = [dsdq; dxdq; dydq; dzdq];
% g2 = [-z; y; -x; s]; G2 = [-dzdq; dydq; dxdq; dsdq];
% 
% dqcdq = g1*dcpsidq + G1*C + g2*dspsidq + G2*S;
% dqcdm = g1*dcpsidm + g2*dspsidm;

end


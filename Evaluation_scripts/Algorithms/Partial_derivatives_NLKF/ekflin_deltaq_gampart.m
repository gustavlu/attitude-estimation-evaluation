function [ dscapdxi, dccapdxi ] = ekflin_deltaq_gampart( sy, cy, dsydxi, dcydxi, k )
%EKFLIN_DELTAQ_GAMPART Calculate partial derivatives for complex valued
% function gamma = (cy(xi)+i*sy(xi))^(k/2) with respct to xi. Separates 
% imaginary part and real part.

dgammadxi = k/2*(cy+1i*sy)^(k/2-1)*(dcydxi+1i*dsydxi);

dscapdxi = imag(dgammadxi);
dccapdxi = real(dgammadxi);

end


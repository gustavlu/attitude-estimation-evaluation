function [ partial_pg ] = ekfoutlin_pg( q, m, mref)
%EKFOUTLIN_PG Summary of this function goes here
%   Detailed explanation goes here
g = 9.81;
m1 = m(1); m2 = m(2); m3 = m(3);
mr1 = mref(1); mr2 = mref(2); mr3 = mref(3);
s = q(1); x = q(2); y = q(3); z = q(4);

% partial_pg = [-x+m3     -s*m3+2*x*m2    z*m3+2*y*m2     y*m3;
%               -y*m3     -2*x*m1-z*m3    -2*y*m1-s*m3    -x*m3;
%               y*m2+x*m1 z*m2+s*m1       s*m2-z*m1       x*m2-y*m1];
          
partial_pg = [2*g*mr1*z - 2*g*m3*x,...
 2*g*mr1*y - 4*g*m2*x - 2*g*m3*s, 2*g*mr1*x - 4*g*m2*y + 4*g*mr2*y - 2*g*m3*z,...
 2*g*mr1*s - 2*g*m3*y + 4*g*mr2*z;
 2*g*mr2*z - 2*g*m3*y, 4*g*m1*x - 4*g*mr1*x - 2*g*mr2*y + 2*g*m3*z,...
 4*g*m1*y - 2*g*mr2*x - 2*g*m3*s,...
 2*g*mr2*s + 2*g*m3*x - 4*g*mr1*z;
 2*g*m1*x - 2*g*mr1*x + 2*g*m2*y - 2*g*mr2*y,...
 2*g*m1*s - 2*g*mr1*s - 2*g*m2*z - 2*g*mr2*z,...
 2*g*m2*s - 2*g*mr2*s + 2*g*m1*z + 2*g*mr1*z, 2*g*m1*y - 2*g*mr2*x - 2*g*m2*x + 2*g*mr1*y];
 

end


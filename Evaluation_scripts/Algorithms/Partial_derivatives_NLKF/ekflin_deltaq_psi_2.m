function [ dpsicdq, dpsicdm ] = ekflin_deltaq_psi_2( qhat, R,magm, m_i, k)
%EKFLIN_DELTAQ_PSI Summary of this function goes here
%   Detailed explanation goes here
mi1 = m_i(1); mi2 = m_i(2);
s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
m1 = magm(1); m2 = magm(2); m3 = magm(3);

dmi1dm = [R(1,1), R(1,2), R(1,3)];
dmi2dm = [R(2,1), R(2,2), R(2,3)];

dmi1dq = [ 2*m3*y - 2*m2*z, 2*m2*y + 2*m3*z, 2*m3*s + 2*m2*x - 4*m1*y, 2*m3*x - 2*m2*s - 4*m1*z];
dmi2dq = [ 2*m1*z - 2*m3*x, 2*m1*y - 4*m2*x - 2*m3*s, 2*m1*x + 2*m3*z, 2*m1*s + 2*m3*y - 4*m2*z];
 
dpsimdmi = 1/(1+(m_i(2)/m_i(1))^2);
dpsimdq = dpsimdmi*(dmi1dq*mi2-mi1*dmi2dq)/mi1^2;
dpsimdm = dpsimdmi*(dmi1dm*mi2-mi1*dmi2dm)/mi1^2;

dpsicdq = (1-k)*dpsimdq;
dpsicdm = k*dpsimdm;

end


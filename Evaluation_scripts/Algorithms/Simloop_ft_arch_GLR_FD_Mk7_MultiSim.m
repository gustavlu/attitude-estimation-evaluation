%%
nx = 7; % state dimension
r2d = 180/pi;
d2r = pi/180;


dt = mean(diff(tvec)); % time step
dt_acc = dt;
dt_mag = dt;
dt_vec = [dt; diff(tvec)];
tend = tvec_imu(end);
t = tvec_imu'; % time vector

dt_glr = dt*ndt_glr;

% Measurement availability
acc_ok = [1;abs(diff(acc_in(:,2)))>0];
mag_ok = [1;abs(diff(mag_in(:,2)))>0];

e1 = [1;0;0];
e3 = [0;0;1];

% References
gvec = e3*-9.81;
mag_ref = mean(mag_in(1:100,2:4))';

EKF.mag_ref = mag_ref;
EKF.gvec = gvec;

cov_mag = diag([0.0067, 0.0509, 0.0441]);
% cov_mag = eye(3)*(norm(EKF.mag_ref)/3)^2;
% cov_acc = eye(3)*(norm(gvec)/3)^2;%diag((std(acc_in(1:100,2:4))).^2);
cov_acc = eye(3)*(norm(gvec)/3)^2;%diag((std(acc_in(1:100,2:4))).^2);
cov_gyro = ([25; 12; 4]/3*d2r).^2;

% First order perf. model
tau_fb_acc = 3;
EKF.accest.tau = tau_fb_acc;
EKF.accest.k_fb = 1/tau_fb_acc;

tau_fb_mag = 3;
EKF.magest.tau = tau_fb_mag;
EKF.magest.k_fb = 1/tau_fb_mag;

EKF.partq.taupsi = 10/3;
EKF.partq.taualpha = 5/3;
EKF.partq.taupsi_bias = 100/3;
EKF.partq.taualpha_bias = 100/3;
EKF.partq.dt_gain_mag = dt;
EKF.partq.dt_gain_acc = dt;

EKF.bsat.bmax = 3*d2r;
% EKF.magest.k_fb = 0;
% EKF.accest.k_fb = 0;

% Measurement covariances
Ra = cov_acc;
Rm = cov_mag;
EKF.Ra = Ra;
EKF.Rm = Rm;
EKF.cov.gyro = cov_gyro;
EKF.cov.bgyro = diag(EKF.bsat.Qd);

Scenario.t = t;
Scenario.Ts = dt;
Scenario.tsim = tend;

euler0 = pi/180*[45;45;45]*0;
% euler0 = pi/180*[10;10;10]*4.5;
q0 = eul2qua(euler0);
q = q0;
R = qua2chrSL(q);

% Init EKF
% eulerhat0 = 1*[-0, 0, 45]*d2r;
eulerhat0 = 0*[0, 45, 45]*d2r;
% eulerhat0 = euler0 + [25;25;45];
qhat0 = eul2qua(eulerhat0);
% qhat0=[1;0;0;0];
bhat0 = [0;0;0];
mrhat_cov = eye(3)*(norm(mag_ref)*0.1/3/sqrt(EKF.magest.tau))^2;
arhat_cov = eye(3)*(norm(gvec)*0.1/3/sqrt(EKF.magest.tau))^2;
bhat_cov = eye(3)*(0.5/3/r2d)^2;
euler_cov = diag(([3/3, 3/3, 3/3]*d2r).^2);
Phat0 = blkdiag(EulerCov2QuaternionCov(eulerhat0,euler_cov), bhat_cov);

xhat_ex = qhat0;
Phat_ex = Phat0(1:4,1:4);

xhat_ms = [EKF.gvec;
    EKF.mag_ref;
    zeros(9,1)];

% Init MEKF
xhat_mekf0 = [qhat0; bhat0];
Phat_mekf0 = blkdiag(euler_cov, bhat_cov);

% Init MEKF-PM Mag
xhat_mekf_pm_mag0 = [qhat0; bhat0; EKF.mag_ref];
Phat_mekf_pm_mag0 = blkdiag(euler_cov, bhat_cov, mrhat_cov);

% Init MEKF-PM Acc Mag
xhat_mekf_pm_acc_mag0 = [qhat0; bhat0; EKF.gvec; EKF.mag_ref];
Phat_mekf_pm_acc_mag0 = blkdiag(euler_cov, bhat_cov, arhat_cov, mrhat_cov);

% Init Sensor EKF
ahat0 = qua2chrSL(qhat0)'*gvec;
Phat_acc0 = Ra;
ahat_p = ahat0; Phat_acc_p = Phat_acc0;
mhat0 = qua2chrSL(qhat0)'*mag_ref;
Phat_mag0 = Rm;
mhat_p = mhat0; Phat_mag_p = Phat_mag0;

% Martin & Sarras init
ahat0 = qua2chrSL(qhat0)'*gvec;
ahat = ahat0; Phat_acc_p = Phat_acc0;
mhat0 = qua2chrSL(qhat0)'*mag_ref;
mhat = mhat0; Phat_mag_p = Phat_mag0;
bhat = zeros(3,1);

% Init comp
ka = 0.2; km = 0.8; kp = 0.1; ki = 0.1; kb = 1;
gain = [ka km kp ki kb];

k_a = 2; m_a = 10; k_b = 1; l_b = 10; m_b = 10;
gain_ms = [k_a, k_b, l_b, m_a, m_b];

tau_b = 100;
kc = 10;
ka = kc;
km = kc;

% lc = kc/2*(3*omegasignoise+1/tau_b);
lc = 0.01;
la = lc;
lm = lc;

gain_m = [ka km la lm];

% cov_in.acc = Ra;
% cov_in.mag = Rm;
% cov_in.gyro = diag(cov_gyro);

xhat_m = [ahat; mhat; zeros(3,1)];

% init
eulerhatout = zeros(3,size(t,2));
eulerhatexout = zeros(3,size(t,2));
eulerout = zeros(3,size(t,2));
sigma_euler_out = zeros(3,size(t,2));
sigma_euler_ex_out = zeros(3,size(t,2));
sigma_bias_out = zeros(3,size(t,2));

ahat_out = zeros(3,size(t,2));
sigma_ahat_out = zeros(3,size(t,2));
mhat_out = zeros(3,size(t,2));
sigma_mhat_out = zeros(3,size(t,2));

acc_status_out = zeros(3,size(t,2));
mag_status_out = zeros(3,size(t,2));
corr_status_out = zeros(2,size(t,2));

acc_out = zeros(3,size(t,2));
mag_out = zeros(3,size(t,2));

ac_out = zeros(3,size(t,2));
mc_out = zeros(3,size(t,2));

mag_ekf_out = zeros(3,size(t,2));
acc_ekf_out = zeros(3,size(t,2));

norm_acc_out = zeros(size(t));
norm_mag_out = zeros(size(t));

accm_out = zeros(3,size(t,2));
magm_out = zeros(3,size(t,2));
gyro_out = zeros(3,size(t,2));
bias_out = zeros(3,size(t,2));
bias_lp_out = zeros(3,size(t,2));

xhat_out = zeros(7,size(t,2));
Phat_out = zeros(7,7,size(t,2));

xhat_m_out = zeros(9,size(t,2));

xhat_ms_out = zeros(15,size(t,2));


norm_error_qhat_ex_out = zeros(1,size(t,2));
norm_error_euler_out = zeros(1,size(t,2));
norm_error_qhat_out = zeros(1,size(t,2));
norm_error_bhat_out = zeros(1,size(t,2));

btracep_out = zeros(1,size(t,2));
ptrace_pre_out = zeros(1,size(t,2));
ptrace_post_out = zeros(1,size(t,2));

Knorm_out = zeros(6,size(t,2));

q_out = zeros(4,size(t,2));
qsig_out = zeros(4,size(t,2));
qhat_out = zeros(4,size(t,2));
bhat_out = zeros(3,size(t,2));
mi_out = zeros(3,size(t,2));

tmp1_out = zeros(1,size(t,2));
tmp2_out =zeros(1,size(t,2));
tmp3_out =zeros(1,size(t,2));
tmp4_out =zeros(1,size(t,2));
tmp_dec_out = zeros(3,size(t,2));



% glr_params
[ GLRparams_3, GLRinit_3 ] = setGLRparams( zeros(3),eye(3),eye(3),L,PFA_GLR,PFA_chi2, dt_glr);
[ GLRparams_1, GLRinit_1 ] = setGLRparams( 0,1,1,L,PFA_GLR,PFA_chi2, dt_glr);


PHIxn_magnorm  = GLRinit_1.PHIxn;
bhatn_magnorm  = GLRinit_1.bhatn;   % filter bank estimation [nb x L]
LAMbn_magnorm  = GLRinit_1.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_magnorm     = GLRinit_1.qn;      % Filter bank age [1 x L]
GLRn_magnorm   = GLRinit_1.GLRn;    % GLR statistic storage

PHIxn_magpm  = GLRinit_3.PHIxn;
bhatn_magpm  = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_magpm  = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_magpm     = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_magpm   = GLRinit_1.GLRn;    % GLR statistic storage

PHIxn_magout = GLRinit_3.PHIxn;
bhatn_magout = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_magout = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_magout    = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_magout  = GLRinit_1.GLRn;    % GLR statistic storage

PHIxn_accnorm  = GLRinit_1.PHIxn;
bhatn_accnorm  = GLRinit_1.bhatn;   % filter bank estimation [nb x L]
LAMbn_accnorm  = GLRinit_1.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_accnorm     = GLRinit_1.qn;      % Filter bank age [1 x L]
GLRn_accnorm   = GLRinit_1.GLRn;    % GLR statistic storage

PHIxn_accpm  = GLRinit_3.PHIxn;
bhatn_accpm  = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_accpm  = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_accpm     = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_accpm   = GLRinit_1.GLRn;    % GLR statistic storage

PHIxn_accout = GLRinit_3.PHIxn;
bhatn_accout = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_accout = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_accout    = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_accout  = GLRinit_1.GLRn;    % GLR statistic storage

% Storage
GLRvec = zeros(6,numel(t));
qdetvec = zeros(6,numel(t));
chi2vec = zeros(6,numel(t));

alpha_error = zeros(1,numel(t));
psi_error = zeros(1,numel(t));

att_reset = zeros(2,numel(t));
l_gyro_out = zeros(1,numel(t));

clear eout;
qbias0 = eul2qua(rpy_in(1,2:4));
qbias = qbias0;
for k = 1:numel(t),
    qbias = quatintegrate(qbias, gyro_in(k,2:4)', dt_vec(k));
    eout(:,k) = qua2eul(qbias);
end
bhat_ol = (qua2eul(qbias)-qua2eul(qbias0))/(gyro_in(end,1));
ext_bias_out  = zeros(3, size(t,2));

% Rotated Px4
px4_rotated = (px4_in(:,2:4)-[ones(size(t'))*rpy_in(1,2), ones(size(t'))*rpy_in(1,3), ones(size(t'))*rpy_in(1,4)])*r2d;

% Gyro LP filter for bias estimation
tau = EKF.partq.taupsi_bias;
alpha = 1 - exp(-dt/tau);
bhat_lp_0 = bhat0;

biascase = 1;
biasfactor = 1;
resetcase = 1;

%%
% for ke = [ 1 2 3 4]
% for ke = [1 2 3 4 5]
for ke  = ke_sim
    
    Phat = Phat0;
    xhat = [qhat0; bhat0];
    xhat_mekf = xhat_mekf0;
    Phat_mekf = Phat_mekf0;
    xhat_m = [ahat0; mhat0; zeros(3,1)];
    Phat_mekf_pm_mag = Phat_mekf_pm_mag0;
    xhat_mekf_pm_mag = xhat_mekf_pm_mag0;
    Phat_mekf_pm_acc_mag = Phat_mekf_pm_acc_mag0;
    xhat_mekf_pm_acc_mag = xhat_mekf_pm_acc_mag0;
    q = q0;
    ahat_p = ahat0; Phat_acc_p = Phat_acc0;
    mhat_p = mhat0; Phat_mag_p = Phat_mag0;
    % Bias initialisation at ss value
    bhat_lp = bhat_ol;
    xhat(5:7) = bhat_ol;
    xhat_mekf(5:7) = bhat_ol;
    xhat_mekf_pm_mag(5:7) = bhat_ol;
    xhat_mekf_pm_acc_mag(5:7) = bhat_ol;
    %     bhat_lp = zeros(3,1);
    %     xhat(5:7) = zeros(3,1);
    %     bhat_lp = 20*ones(3,1)*d2r;
    %     xhat(5:7) = 20*ones(3,1)*d2r;
    
    h1 = waitbar(0,'Simulating...');
    for k = 1:floor(numel(t))
        
        %         tic1 = tic;
        tk = t(k);
        dt = dt_vec(k);
        
        %         fprintf('======= %3.4f ======\n',tk);
        % gyro bias
        %
        om_bias = [0;0;0];
        
        % Attitude update
        q = eul2qua(rpy_in(k,2:4));
        
        R = qua2chrSL(q);
        [PHI, THETA, PSI] = Quaternion2Euler(q);
        euler = [PHI; THETA; PSI];
        eulerout(:,k) = rpy_in(k,2:4);
        
        acc_k = R'*gvec;
        mag_k = R'*mag_ref;
        
        % Measurements
        accm = acc_in(k,2:4)';
        magm = mag_in(k,2:4)';
        
        acc_out(:,k) = acc_k;
        mag_out(:,k) = mag_k;
        
        accm_out(:,k) = accm;
        magm_out(:,k) = magm;
        gyro_out(:,k) = gyro_in(k,2:4)';
        bias_out(:,k) = om_bias;
        
        inputs = gyro_out(:,k);
        measurements = [accm; magm];
        %         xhat_pm = [q; om_bias];
        xhat_pm = xhat;
        %         tic2 = tic;
        
        l_gyro_out(:,k) = gyro_in(k,2:4)*pinv(diag(EKF.cov.gyro))*gyro_in(k,2:4)';
        % LP filtering of gyro to estimate gyro bias in open loop
        if l_gyro_out(:,k) < GLRparams_3.GLRthr
            bhat_lp = sat((1-alpha)*bhat_lp + alpha*gyro_in(k,2:4)',-EKF.bsat.bmax,EKF.bsat.bmax);
        end
        bias_lp_out(:,k) = bhat_lp;
        
        
        % ============= Sensor estimation =======================
        [b_norm_acc, b_norm_mag, l_acc, l_mag,...
            S_norm_acc, S_norm_mag, delta_norm_acc, delta_norm_mag] ...
            = measnormcheck(EKF,measurements);
        
        if b_useftarch
            [ mhat_p, Phat_mag_p, status_vec_mag, S_pm_mag, delta_pm_mag,...
                S_out_mag, delta_out_mag] = mag_perf_model(EKF, mhat_p, Phat_mag_p,...
                inputs, magm, xhat_pm, Phat, b_norm_mag, dt );
            [ ahat_p, Phat_acc_p, status_vec_acc, S_pm_acc, delta_pm_acc,...
                S_out_acc, delta_out_acc ] = acc_perf_model(EKF, ahat_p, Phat_acc_p,...
                inputs, accm, xhat_pm, Phat, b_norm_acc, dt );
            
            status_vec_acc_a = [b_norm_acc; status_vec_acc];
            status_vec_mag_a = [b_norm_mag; status_vec_mag];
            
            % =================================================================
            %               GLR fault detection
            % =================================================================
            idp_mag = 1-mag_ok(k);
            idp_acc = 1-acc_ok(k);
            kresetglr = 0;
            lamff = 1;
            
            % GLR test for acc norm
            nunp1 = delta_norm_acc;
            Knp1 = zeros(1)/2;
            Snp1 = S_norm_acc;
            if b_glr && b_useglr_vec(1)
                [qdetnp1,GLRmax,qn_accnorm,bhatn_accnorm,LAMbn_accnorm,PHIxn_accnorm, GLRn_accnorm] ...
                    = glrtest(idp_acc, kresetglr,lamff,Knp1,Snp1,nunp1,qn_accnorm,bhatn_accnorm,LAMbn_accnorm,PHIxn_accnorm,GLRn_accnorm,GLRparams_1);
                b_norm_acc_glr = double(GLRmax > GLRparams_1.GLRthr);
                b_norm_acc_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_1.chi2thr);
                b_norm_acc = double(b_norm_acc_glr + b_norm_acc_chi2 > 0);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_norm_acc = double(GLRmax > GLRparams_1.chi2thr);
            end
            
            qdetvec(1,k) = qdetnp1;
            GLRvec(1,k) = GLRmax;
            chi2vec(1,k) = nunp1'*pinv(Snp1)*nunp1;
            
            % GLR test for acc outliers
            nunp1 = delta_out_acc;
            Knp1 = zeros(3)/2;
            Snp1 = S_out_acc;
            if b_glr && b_useglr_vec(2)
                [qdetnp1,GLRmax,qn_accout,bhatn_accout,LAMbn_accout,PHIxn_accout,GLRn_accout] ...
                    = glrtest(idp_acc, kresetglr,lamff,Knp1,Snp1,nunp1,qn_accout,bhatn_accout,LAMbn_accout,PHIxn_accout,GLRn_accout,GLRparams_3);
                b_out_acc_glr = double(GLRmax > GLRparams_3.GLRthr);
                b_out_acc_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_3.chi2thr);
                b_out_acc = double(b_out_acc_glr + b_out_acc_chi2 > 0);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_out_acc = double(GLRmax > GLRparams_3.chi2thr);
            end
            
            qdetvec(2,k) = qdetnp1;
            GLRvec(2,k) = GLRmax;
            chi2vec(2,k) = nunp1'*pinv(Snp1)*nunp1;
            
            % GLR test for acc perf model
            nunp1 = delta_pm_acc;
            Knp1 = zeros(3)/2;
            Snp1 = S_pm_acc;
            if b_glr && b_useglr_vec(3)
                [qdetnp1,GLRmax,qn_accpm,bhatn_accpm,LAMbn_accpm,PHIxn_accpm,GLRn_accpm] ...
                    = glrtest(idp_acc, kresetglr,lamff,Knp1,Snp1,nunp1,qn_accpm,bhatn_accpm,LAMbn_accpm,PHIxn_accpm,GLRn_accpm,GLRparams_3);
                b_pm_acc = double(GLRmax > GLRparams_3.GLRthr);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_pm_acc = double(GLRmax > GLRparams_3.chi2thr);
            end
            
            
            qdetvec(3,k) = qdetnp1;
            GLRvec(3,k) = GLRmax;
            chi2vec(3,k) = nunp1'*pinv(Snp1)*nunp1;
            
            % GLR test for mag norm
            nunp1 = delta_norm_mag;
            Knp1 = zeros(1)/2;
            Snp1 = S_norm_mag;
            if b_glr && b_useglr_vec(4)
                [qdetnp1,GLRmax,qn_magnorm,bhatn_magnorm,LAMbn_magnorm,PHIxn_magnorm,GLRn_magnorm] ...
                    = glrtest(idp_mag, kresetglr,lamff,Knp1,Snp1,nunp1,qn_magnorm,bhatn_magnorm,LAMbn_magnorm,PHIxn_magnorm,GLRn_magnorm,GLRparams_1);
                b_norm_mag_glr = double(GLRmax > GLRparams_1.GLRthr);
                b_norm_mag_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_1.chi2thr);
                b_norm_mag = double(b_norm_mag_glr + b_norm_mag_chi2 > 0);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_norm_mag = double(GLRmax > GLRparams_1.chi2thr);
            end
            
            
            qdetvec(4,k) = qdetnp1;
            GLRvec(4,k) = GLRmax;
            chi2vec(4,k) = nunp1'*pinv(Snp1)*nunp1;
            
            % GLR test for mag outliers
            nunp1 = delta_out_mag;
            Knp1 = zeros(3)/2;
            Snp1 = S_out_mag;
            if b_glr && b_useglr_vec(5)
                [qdetnp1,GLRmax,qn_magout,bhatn_magout,LAMbn_magout,PHIxn_magout,GLRn_magout] ...
                    = glrtest(idp_mag, kresetglr,lamff,Knp1,Snp1,nunp1,qn_magout,bhatn_magout,LAMbn_magout,PHIxn_magout,GLRn_magout,GLRparams_3);
                b_out_mag_glr = double(GLRmax > GLRparams_3.GLRthr);
                b_out_mag_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_3.chi2thr);
                b_out_mag = double(b_out_mag_glr + b_out_mag_chi2 > 0);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_out_mag = double(GLRmax > GLRparams_3.chi2thr);
            end
            
            qdetvec(5,k) = qdetnp1;
            GLRvec(5,k) = GLRmax;
            chi2vec(5,k) = nunp1'*pinv(Snp1)*nunp1;
            
            % GLR test for mag perf model
            nunp1 = delta_pm_mag;
            Knp1 = zeros(3)/2;
            Snp1 = S_pm_mag;
            if b_glr && b_useglr_vec(6)
                [qdetnp1,GLRmax,qn_magpm,bhatn_magpm,LAMbn_magpm,PHIxn_magpm,GLRn_magpm] ...
                    = glrtest(idp_mag, kresetglr,lamff,Knp1,Snp1,nunp1,qn_magpm,bhatn_magpm,LAMbn_magpm,PHIxn_magpm,GLRn_magpm,GLRparams_3);
                b_pm_mag = double(GLRmax > GLRparams_3.GLRthr);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_pm_mag = double(GLRmax > GLRparams_3.chi2thr);
            end
            
            
            qdetvec(6,k) = qdetnp1;
            GLRvec(6,k) = GLRmax;
            chi2vec(6,k) = nunp1'*pinv(Snp1)*nunp1;
            
            status_vec_mag_a = [b_norm_mag; b_out_mag; b_pm_mag];
            
            status_vec_acc_a = [b_norm_acc; b_out_acc; b_pm_acc];

            %==================================================================
            % Attitude reset for GLR fault rejection
            %==================================================================
            if k > 1 && b_glr_attreset && b_glr
                % Reset on norm detection
                if (mag_status_out(1,k) > 0 &&  mag_status_out(1,k-1) < 1) && b_useglr_vec(4)
                    att_reset(1,k) = 1;
                    b_omega_tmp = xhat_out(5:7,k-qdetvec(4,k)*ndt_glr);
                    qhat_tmp = xhat_out(1:4,k-qdetvec(4,k)*ndt_glr);
                    omega_mat_tmp = gyro_out(:,k-qdetvec(4,k)*ndt_glr:k);
                    for kl = 1:qdetvec(4,k)*ndt_glr
                        qhat_tmp = quatint( qhat_tmp, omega_mat_tmp(:,kl)-b_omega_tmp, dt);
                    end
                    xhat = [qhat_tmp; b_omega_tmp];
                    % Reset on outlier detection
                elseif (mag_status_out(2,k) > 0 &&  mag_status_out(2,k-1) < 1) && b_useglr_vec(5)
                    att_reset(2,k) = 1;
                    b_omega_tmp = xhat_out(5:7,k-qdetvec(5,k)*ndt_glr);
                    qhat_tmp = xhat_out(1:4,k-qdetvec(5,k)*ndt_glr);
                    omega_mat_tmp = gyro_out(:,k-qdetvec(5,k)*ndt_glr:k);
                    for kl = 1:qdetvec(5,k)*ndt_glr
                        qhat_tmp = quatint( qhat_tmp, omega_mat_tmp(:,kl)-b_omega_tmp, dt);
                    end
                    xhat = [qhat_tmp; b_omega_tmp];
                else
                    att_reset(:,k) = 0;
                end
            end
            %==================================================================
            [ac, Phat_ac, mc, Phat_mc, corr_status] = input_consolidation(...
                EKF, ahat_p, accm, mhat_p, magm, Phat_acc_p, Phat_mag_p,...
                status_vec_acc_a, status_vec_mag_a);
        else
            ac = accm; Phat_ac = EKF.Ra;
            mc = magm; Phat_mc = EKF.Rm;
            corr_status = [1-b_norm_acc; 1-b_norm_mag];
            status_vec_acc_a = [b_norm_acc, 0, 0];
            status_vec_mag_a = [b_norm_mag, 0, 0];
        end
        
        if b_force_ok
            corr_status = [1;1];
            ac = accm; Phat_ac = EKF.Ra;
            mc = magm; Phat_mc = EKF.Rm;
        end
        
        % =================================================================
        mhat_out(:,k) = mhat_p;
        sigma_mhat_out(:,k) = diag(Phat_mag_p);
        ahat_out(:,k) = ahat_p;
        sigma_ahat_out(:,k) = diag(Phat_acc_p);
        
        acc_status_out(:,k) = status_vec_acc_a;
        mag_status_out(:,k) = status_vec_mag_a;
        
        b_nobias = 0;

        
        measurements = [ac; mc];
        
        corr_status = corr_status.*[acc_ok(k); mag_ok(k)];
        norm_acc_out(k) = norm(accm);
        norm_mag_out(k) = norm(magm);
        ac_out(:,k) = measurements(1:3);
        mc_out(:,k) = measurements(4:6);
        corr_status_out(:,k) = corr_status;
        
        % Use LP gyro estimation in case of disturbance
        ext_bias = xhat(5:7);
        if b_lpgyro
            if sum(status_vec_mag_a(1:2)) > 0
                xhat(7) = bhat_lp(3);
                xhat_mekf(7) = bhat_lp(3);
                ext_bias(3) =  bhat_lp(3);
            end
            if sum(status_vec_acc_a(1:2)) > 0
                xhat(5:6) = bhat_lp(1:2);
                xhat_mekf(5:6) = bhat_lp(1:2);
                ext_bias(1:2) =  bhat_lp(1:2);
            end
        end
        ext_bias_out(:,k) = ext_bias;
        xhat_mekf1(5:7) = bhat_lp; % Always use LP bias (comparison)
        
        % ============= Attitude estimation =======================
        switch ke
            case 1
                % ---- Comp. filter ----
                filt_title = 'Hua, 2011';
                [ xhat] = hua_2011_decoupled(EKF, xhat, inputs,  measurements, dt, gain, corr_status);
            case 2 % ---- EKF ----
                filt_title = 'Legacy EKF';
                [xhat,Phat] = ekf_update(EKF,inputs, measurements, xhat,Phat, dt, corr_status );
            case 3 % ---- DEC - EKF ----
                filt_title = 'Measurement decoupled KF';
                [tmp_dec,xhat,Phat] = deckf_update(EKF,inputs, measurements, xhat,Phat, dt, corr_status );
            case 4 % ---- NL - KF ----
                filt_title = 'Fault tolerant nonlinear KF';
                [tmpstruct2, tmpstruct, tmp, xhat,Phat] = ftkf_update(EKF,inputs,measurements,xhat,Phat, dt, corr_status, b_nobias, ext_bias);
                %                 filt_title = 'eXogenous Kalman Filter KF';
                %                 uhat_ex = xhat; cov_uhat_ex = Phat;
                %                 [xhat_ex,Phat_ex] = qhat_xkf_update(EKF,inputs,uhat_ex, cov_uhat_ex, xhat_ex,Phat_ex, dt);
            case 5 % Comp filter global decoupling
                filt_title = 'Hua, 2011, Global decoupling';
                [ xhat] = hua_2011_decoupled_global(EKF, xhat, inputs,  measurements, dt, gain, corr_status);
            case 6 % Sarras / Martin attitude observer with measurement biases
                filt_title = 'Martin + Sarras, 2018, Bias observer';
                [ xhat_ms, qhat] = martin_sarras_bias(EKF, xhat_ms, inputs,  measurements, dt, gain_ms, corr_status);
                xhat = [qhat; xhat_ms(7:9)];
                xhat_ms_out(:,k) = xhat_ms;
            case 7 % ---- DEC MEKF ----
                filt_title = 'Decoupled MEKF';
                %                 [ xhat,Phat ] = smekf_update(EKF, inputs, measurements, xhat,Phat, dt, corr_status);
                [ Phat_mekf, xhat_mekf ] = mekf( Phat_mekf, xhat_mekf, inputs, measurements, corr_status, EKF, dt, ext_bias );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf(1:3,1:3)*ddqda', Phat_mekf(4:6,4:6));
                xhat = xhat_mekf;
            case 8 % ---- DEC GMEKF ----
                filt_title = 'Decoupled GMEKF';
                %                 [ xhat,Phat ] = smekf_update(EKF, inputs, measurements, xhat,Phat, dt, corr_status);
                [ Phat_mekf, xhat_mekf ] = invariant_mekf( Phat_mekf, xhat_mekf, inputs, measurements, corr_status, EKF, dt, ext_bias );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf(1:3,1:3)*ddqda', Phat_mekf(4:6,4:6));
                xhat = xhat_mekf;
            case 9 % ---- DEC IEKF ----
                filt_title = 'Decoupled IEKF, Mag. PM';
%                 [ Phat_mekf_pm_mag, xhat_mekf_pm_mag ] = iekf( Phat_mekf_pm_mag, xhat_mekf_pm_mag, inputs, measurements, corr_status, EKF, dt );
                [ Phat_mekf_pm_mag, xhat_mekf_pm_mag, Knorm ] = iekf_dec_mag( Phat_mekf_pm_mag, xhat_mekf_pm_mag, inputs, measurements, corr_status, EKF, dt );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf_pm_mag(1:3,1:3)*ddqda', Phat_mekf_pm_mag(4:6,4:6));
                xhat = xhat_mekf_pm_mag(1:7);
                mag_ekf_out(:,k) = xhat_mekf_pm_mag(8:10);
                Knorm_out(:,k) = Knorm;
            case 10 % ---- DEC IEKF ----
                filt_title = 'Decoupled IEKF, Full PM';
%                 [ Phat_mekf_pm_mag, xhat_mekf_pm_mag ] = iekf( Phat_mekf_pm_mag, xhat_mekf_pm_mag, inputs, measurements, corr_status, EKF, dt );
                [ Phat_mekf_pm_acc_mag, xhat_mekf_pm_acc_mag, Knorm ] = iekf_dec( Phat_mekf_pm_acc_mag, xhat_mekf_pm_acc_mag, inputs, measurements, corr_status, EKF, dt );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf_pm_acc_mag(1:3,1:3)*ddqda', Phat_mekf_pm_acc_mag(4:6,4:6));
                xhat = xhat_mekf_pm_acc_mag(1:7);
                mag_ekf_out(:,k) = xhat_mekf_pm_acc_mag(11:13);
                acc_ekf_out(:,k) = xhat_mekf_pm_acc_mag(8:10);
                Knorm_out(:,k) = Knorm;
        end
        
        if ke ==  3
            tmp_dec_out(1:3,k) = 4*tmp_dec(2:4)/(tmp_dec(1)+1); % Modified Rodrigues parameter
            %             aacc = acos(tmp_dec(1));
            %             tmp_dec_out(1:3,k) = tmp_dec(2:4)/sin(aacc);
            tmp_dec_out(1:3,k) = tmp_dec_out(1:3,k) / norm(tmp_dec_out(1:3,k));
        end
        
        %         if tk == floor(tend/2)
        %             switch resetcase
        %                 case 1 % Full state reset
        %                     xhat = [qhat0; bhat0];
        %                     xhat_m = [ahat0; mhat0; zeros(3,1)];
        %                 case 2 % Attitude reset
        %                     xhat = [qhat0; xhat(5:7)];
        %                 case 3 % Bias reset
        %                     xhat = [xhat(1:4); bhat0];
        %             end
        %         end
        
        %         if tk == floor(tend/2)
        %             xhat = [eul2qua([0, 0, 45]*pi/180); xhat(5:7)];
        %             q = [1;0;0;0];
        %         end
        
        qhat = xhat(1:4);
        e3b = R'*e3;
        e1b = R'*e1;
        
        e3bhat = Quaternion2RotMat(qhat)'*e3;
        e1bhat = Quaternion2RotMat(qhat)'*e1;
        
        alpha_error(1,k) = norm(cross(e3b,e3bhat));
        psi_error(1,k) = norm(cross(e1b,e1bhat));
        
        
        %         Ptrace_post = trace(Phat);
        
        [PHI, THETA, PSI] = Quaternion2Euler(qhat);
        eulerhat = [PHI; THETA; PSI];
        eulerhatout(:,k) = eulerhat;
        eulerhatexout(:,k) = qua2eulSL(xhat_ex);
        
        sigma_euler_ex_out(:,k) = QuaternionCov2EulerSTD(Phat_ex(1:4,1:4),xhat_ex);
        sigma_euler_out(:,k) = QuaternionCov2EulerSTD(Phat(1:4,1:4),qhat);
        sigma_bias_out(:,k) = sqrt(diag(Phat(5:7,5:7)));
        [PHI, THETA, PSI] = Quaternion2Euler(q);
        euler = [PHI; THETA; PSI];
        error_euler = rpy_in(k,2:4)' - eulerhat;
        
        norm_error_euler_out(:,k) = norm(error_euler);
        norm_error_qhat_out(:,k) = norm(quaternionErrorFct(q,qhat));
        norm_error_qhat_ex_out(:,k) = norm(quaternionErrorFct(q,xhat_ex));
        norm_error_bhat_out(:,k) = norm( xhat(5:7)-om_bias);
        
        qsig_out(:,k) = sqrt(diag(Phat(1:4,1:4)));
        Phat_out(:,:,k) = Phat;
        qhat_out(:,k) = qhat;
        bhat_out(:,k) =  xhat(5:7);
        xhat_out(:,k) = xhat;
        
        q_out(:,k) = q;
        
        if abs(mod(k,100)) < 0.0001
            waitbar((t(k)/tend),h1)
        end
        %         toc(tic1)
    end
    close(h1);
end
%% Edit plot parameters
if b_glr
    filt_title  = [filt_title, [', GLR detection: L=',num2str(L),'s, P_{FA} = ',num2str(PFA_GLR)]];
else
    filt_title  = [filt_title, [', \chi^2 detection: P_{FA} = ',num2str(PFA_GLR)]];
end
titlevec_acc = {'Acc Norm','Acc Out','Acc PM'};
titlevec_mag = {'Mag Norm','Mag Out','Mag PM'};
function [ F, kaw, Qd, bmax, Nd ] = SatBiasDiscreteF3( Psat, T, Qv, dt)
%SATBIASDISCRETE Calculate parameters for saturated bias model and
%linearisation
%   
% ---- INPUT ----
% Psat: probability to saturate
% T: Anti-windup time constant
% Qv: bias noise covariance (cont. model)
%
% ---- OUTPUT ----
% F: State linearisation
% kaw: Anti-windup gain
% Qd: Discrete state noise
%
rpd2 = sqrt(pi/2);
r2dp = 1/rpd2;

rho = (1/Psat-1)*rpd2; % Normalised saturation level

cov_l = T*Qv(1,1)/2;

bmax = sqrt(cov_l)/rho;

% Equivalent gain (cont)
N = rho*(1 + rpd2*rho + 1/3*rho^2) / (rpd2 + 2*rho + rpd2*rho^2 + 1/3*rho^3);

% Saturation covariance (cont)
cov_nl = cov_l* (1 + r2dp*rho + rho^2 + 1/3*r2dp*rho^3) / (1 + r2dp*rho);

% Equivalent dynamics (cont)
A = -(1-N)/T;

% Equivalent linearisation (disc)
F = exp(A*dt);

% Bias covariance (disc)
Qd = eye(3)*(1 - F^2)*cov_nl;

kaw = 1-exp(-dt/T);

Nd = 1 - (1 - F)/kaw; % Equivalent gain (discrete)

end


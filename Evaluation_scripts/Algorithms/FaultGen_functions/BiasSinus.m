function [ oscill ] = BiasSinus( Tstart, Tend, Amp, tstart, Scenario )
%BIASSINUS Create sinusoidal (up-chirp) signal
%   Inputs:
%       Tstart: Initial sinus period [s]
%       Tend: Final sinus period [s]
%       Amp: Sinus amplitude 
%       tstart: Start of sinewave [s]

t = Scenario.t;
Ts = Scenario.Ts;
tsim = Scenario.tsim;
tsamp_start = round(tstart/Ts);

oscill = [zeros(1,tsamp_start), Amp*sin((1/Tstart+(1/Tend-1/Tstart)/tsim.*t(1:end-tsamp_start))*2*pi.*t(1:end-tsamp_start))];


end


function [ stepinput ] = BiasStep( tsteps, ampsteps, Scenario )
%BIASSTEP Create step input
%   Inputs:
%   tseps: vector of step times
%   ampsteps: vector of step amplitues

t = Scenario.t;
ts = Scenario.Ts;

stepmat = zeros(numel(tsteps),numel(t));
for kl = 1:numel(tsteps)
    tsampstart = round(tsteps(kl)/ts);
if tsampstart < 1;
    tsampstart = 1;
end
    stepmat(kl,tsampstart:end) = ampsteps(kl);
end
    stepinput = sum(stepmat,1);
end


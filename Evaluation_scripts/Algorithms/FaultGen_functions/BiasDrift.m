function [ driftinput ] = BiasDrift( kdrift, driftMax, tstart, Scenario)
%BIASDRIFT Create drift input signal
%   Inputs:
%   kdrift: drift slope
%   driftMax: Maximum/Minimum value

t = Scenario.t;
ts = Scenario.Ts;
tsamp_start = round(tstart/ts);
if kdrift > 0
    driftinput = [zeros(1, tsamp_start),min(kdrift*t(1:end-tsamp_start),driftMax)];
else
    driftinput = [zeros(1, tsamp_start),max(kdrift*t(1:end-tsamp_start),-driftMax)];
end

end


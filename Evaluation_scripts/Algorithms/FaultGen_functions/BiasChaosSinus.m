function [ oscill ] = BiasChaosSinus( Tstart, Tend, Amp, phase, tstart, tend, Scenario )
%BIASSINUS Create sinusoidal (up-chirp) signal
%   Inputs:
%       Tstart: Initial sinus period [s]
%       Tend: Final sinus period [s]
%       Amp: Sinus amplitude 
%       tstart: Start of sinewave [s]

t = Scenario.t;
Ts = Scenario.Ts;
tsim = Scenario.tsim;
samptot = floor(tsim/Ts);
tsamp_start = floor(tstart/Ts);
tsamp_end = floor(tend/Ts);

wt = (1/Tstart+(1/Tend-1/Tstart)/tsim.*t(1:tsamp_end))*2*pi.*t(1:tsamp_end);
s1 = sin(wt+phase);
s2 = sin(1./(wt+phase+eps));
s3 = sin((wt).^2);
s4 = sin(4./(wt+eps));

sintot = s1.*s2.*s3.*s4;

oscill = [zeros(1,tsamp_start), Amp*sintot, zeros(1,samptot-tsamp_end+1)];


end


% Fault Parameters (default)
MagFaultParam.seed = 1337;
MagFaultParam.type = 0;
MagFaultParam.scale = 0;
MagFaultParam.tstart = 0;
MagFaultParam.tend = tsimvec(1)-5;
MagFaultParam.drift.kx = 0.1;
MagFaultParam.drift.ky = 0.1;
MagFaultParam.drift.kz = 0.1;
MagFaultParam.drift.max = 10*norm(EKF.mag_ref);
MagFaultParam.sin.Tstart = 50;
MagFaultParam.sin.Tend = 80;

AccFaultParam.seed = 7331;
AccFaultParam.type =0;
AccFaultParam.scale = 1;
AccFaultParam.tstart = 0;
AccFaultParam.tend = tsimvec(1);
AccFaultParam.drift.kx = 0.1;
AccFaultParam.drift.ky = 0.1;
AccFaultParam.drift.kz = 0.1;
AccFaultParam.drift.max = 3*g;
AccFaultParam.sin.Tstart = 50;
AccFaultParam.sin.Tend = 80;
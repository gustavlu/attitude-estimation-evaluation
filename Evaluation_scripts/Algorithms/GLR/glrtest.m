function [qdetnp1,GLRmax,qnp1,bhatnp1,LAMbnp1,PHIxnp1,GLRnp1] ...
    = glrtest(idp, kresetglr,lamff,Knp1,Snp1,nunp1,qn,bhatn,LAMbn,PHIxn, GLRn, GLRparams)
% glrtest
%--------------------------------------------------------------------------
% GLR filter propagation under Hk-1:
% "All faults up to k-1 detected and compensated"
%--------------------------------------------------------------------------
% Features:
% > Filter bank reset on new detection
% > Replace out-of-window filter with new one starting at the current time
% > Propagate filter bank estimation, covariance
% > Calculate filter bank statistic
% > Update filter bank age
%--------------------------------------------------------------------------
% INPUT:
% kresetglrn: detecting filter index (if detection at time n-1)
% lamff: RLS forgetting factor (default=1)
% A,C,F: State space model
% Knp1,Snp1: Kalman filter matrices
% nunp1: Corrected innovation under Hk-1
%
% STATE:
% qn: vector of filter bank age [1 x L]
% bhatn: filter bank estimation [nb x L]
% LAMbn: filter bank estimation information [nb x nb*L]
% PHIxn: filter bank state estimation signatures [nx x nb*L]
%
% OUTPUT:
% GLRnp1: filter bank statistic [1 x L]
% qnp1: vector of filter bank age [1 x L]
% bhatnp1: filter bank estimation [nb x L]
% LAMbnp1: filter bank estimation information [nb x nb*L]
% PHIxnp1: filter bank state estimation signatures [nx x nb*L]
% flag_btracknp1: vector of bias tracking flags [1 x L]
%--------------------------------------------------------------------------
dt = GLRparams.dt;
GLRthr = GLRparams.GLRthr;
A = GLRparams.A;
C = GLRparams.C;
F = GLRparams.F;
% kresetglr = GLRparams.kreset;

L  = size(qn,2);
nx = size(PHIxn,1);
ny = size(nunp1,1);
nb = size(bhatn,1);

% gam_fault   = eye(ny,nb);         % Bias form (step)
% gam_fault   = F;         % Bias form (step)

%--------------------------------------------------------------------------
% GLR resetting
%--------------------------------------------------------------------------
if sum(kresetglr) > 0
    % Reinitialise filters in case of confirmed fault detection
    PHIxnp1_pre     = PHIxn;
    LAMbnp1_pre     = LAMbn;
    bhatnp1_pre     = zeros(nb,L);              % Reset bias estimation
else
    % Keep constant otherwise
    PHIxnp1_pre     = PHIxn;
    LAMbnp1_pre     = LAMbn;
    bhatnp1_pre     = bhatn;
end

% Initialise output variables
PHIxnp1	= zeros(size(PHIxn));
LAMbnp1	= zeros(size(LAMbn));
bhatnp1	= zeros(size(bhatn));
GLRnp1	= zeros(1,L);
GLRnp1 = GLRn;

% Memory allocation
PHInu = zeros(ny,nb*L);

if min(idp) == 0 % New measurement
    
    % Loop over filters for updating
    for k = 1:L
        
        % Filter index in function state matrices
        icol = zeros(1,nb);
        for kl = 1:nb
            icol(1,kl) = (k-1)*nb + kl;
        end
        
        % Current residual
        nunp1_calc = nunp1;
        
        %----------------------------------------------------------------------
        % Reset filters that leave the detection window
        %----------------------------------------------------------------------
        % If filter falls outside detection window:
        % - Reset filter
        % - Reset bias tracking flag
        if qn(k) > L-1
            qn(k) = 0;
        end
        
        % Filter recursion
        if qn(k) <= 0
            %------------------------------------------------------------------
            % Reinitialize nonactive filters
            %------------------------------------------------------------------
            % LS Regressor initialization
            PHIxnp1_pre(:,icol)	= zeros(nx,nb);
            PHInu(:,icol)       = zeros(ny,nb);
            % LS Estimate initialization
            GLRnp1(k)           = 0;
            bhatnp1_pre(:,k)    = zeros(nb,1);
            LAMbnp1_pre(:,icol)	= zeros(nb);
            
        elseif qn(k) > 0
            %------------------------------------------------------------------
            % Normal filter recursion for active filters
            %------------------------------------------------------------------
            gam_fault = F*qn(k)*dt; % Drift
            %             gam_fault = F; % Step
            % LS Regressor update
            PHInu(:,icol)	= gam_fault - C*A*PHIxnp1_pre(:,icol);
            PHIxnp1(:,icol)	= A*PHIxnp1_pre(:,icol) + Knp1*PHInu(:,icol);
            
            % LS Estimate update
            Gtmp            = PHInu(:,icol)'/Snp1;
            LAMbnp1(:,icol)	= lamff*LAMbnp1_pre(:,icol) + Gtmp * PHInu(:,icol); % information matrix
            G               = LAMbnp1(:,icol)\ Gtmp; % RLS gain
            bhatnp1(:,k)	= bhatnp1_pre(:,k) +  G * (nunp1_calc - PHInu(:,icol)*bhatnp1_pre(:,k)); % Bias estimation
            GLRnp1(k)       = bhatnp1(:,k)' * (LAMbnp1(:,icol)) * bhatnp1(:,k); % Filter statistic
            
        end
        
    end
    
    % Incrementation of the filter's datation
    qnp1    =   qn + 1;
else
    for k = 1:L
        
        % Filter index in function state matrices
        icol = zeros(1,nb);
        for kl = 1:nb
            icol(1,kl) = (k-1)*nb + kl;
        end
        
        PHIxnp1(:,icol)	= A*PHIxnp1_pre(:,icol);
    end
    qnp1 = qn;
    %     PHIxnp1     = PHIxnp1_pre;
    LAMbnp1     = LAMbnp1_pre;
    bhatnp1     = bhatnp1_pre;
end


% ========================================================================
%
qdetnp1 = -1;

kresetglrnp1 = 0;
bmaxnp1 = zeros(nb,1);

% Get the maximum likelihood amongst all active filters that have not
% already detected a fault.
% [GLRmax,kmaxnp1] = max(GLRnp1.*(qnp1>1));
[GLRmax,kmaxnp1] = max(GLRnp1.*(qnp1>0));
tmp = qnp1(1);
if min(idp) == 0 % New measurement
    % -------- Detection -------------
    % Detection if ML is over threshold and the corresponding filter has not
    % already detected a bias.
    if (GLRmax > GLRthr) % && ~flag_btracknp1(kmaxnp1)
        bmaxnp1(:,1) = bhatnp1(:,kmaxnp1); % Bias estimation set to MLE
        qdetnp1 = qnp1(kmaxnp1)-1;
    end
    
end

end



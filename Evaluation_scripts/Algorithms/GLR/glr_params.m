% GLR settings for FT-KF architecture

% ---- System ----
A = 1;
C = 1;
F = 1;
A3 = eye(3);
C3 = eye(3);
F3 = eye(3);


% GLR parameters
lamff = 1;
kresetglr = 0;
L = 100;
nb = size(F,2);
nx = size(A,2);
ny = size(C,1);

idp = 0;
GLRparams.dt = dt;

% Chi2 thresholds
p_fa = 10e-6;
sd = Confidence_Set(1-p_fa,3);
GLRparams.GLRthr_3 = sd^2;
sd = Confidence_Set(1-p_fa,1);
GLRparams.GLRthr_1 = sd^2;

PHIxn  = zeros(nx,nb*L);
bhatn  = zeros(nb,L);      % filter bank estimation [nb x L]
LAMbn  = zeros(nb,nb*L);	% filter bank estimation information [nb x nb*L]
qn     = 0:-1:-(L-1);      % Filter bank age [1 x L]


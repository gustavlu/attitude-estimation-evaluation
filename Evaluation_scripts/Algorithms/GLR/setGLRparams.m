function [ GLRparams, GLRinit ] = setGLRparams( A,C,F,L,PFA_GLR,PFA_chi2,dt )
%SETGLRPARAMS Summary of this function goes here
%   Detailed explanation goes here
% ---- System ----

% GLR parameters
lamff = 1;
kresetglr = 0;
nb = size(F,2);
nx = size(A,2);
ny = size(C,1);
L
GLRparams.dt = dt;

% Chi2 thresholds
sd = Confidence_Set(1-PFA_GLR,nb);
GLRparams.GLRthr = sd^2;
sd = Confidence_Set(1-PFA_chi2,nb);
GLRparams.chi2thr = sd^2;
GLRparams.A = A;
GLRparams.C = C;
GLRparams.F = F;

GLRinit.PHIxn  = zeros(nx,nb*L);
GLRinit.bhatn  = zeros(nb,L);      % filter bank estimation [nb x L]
GLRinit.LAMbn  = zeros(nb,nb*L);	% filter bank estimation information [nb x nb*L]
GLRinit.qn     = 0:-1:-(L-1);      % Filter bank age [1 x L]
GLRinit.GLRn   = zeros(1,L);       % GLR statistics storage
end


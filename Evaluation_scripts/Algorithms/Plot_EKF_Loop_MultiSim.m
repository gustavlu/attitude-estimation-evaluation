% Plot evolution of FT-KF
clear

% startdir = 'C:\Users\Gustav\Documents\MATLAB\India_2019\Results';
startdir = pwd;
[plotfiles, pathname] = uigetfile(startdir, 'Multiselect', 'on');

fonty = 24;
b_savefig = [0,0,0]; % 1: Euler, 2: Euler 3 sigma, 3: Euler error
% b_savefig = [1,1,1];
b_savefig_con = 0;
b_plot = 0; % Plot boolean
b_plot_con = 0;
b_px4 = 0; % PX4 plot boolean

test_vec = 0;
for k = 1:numel(plotfiles)
    ind_test = find(plotfiles{k}=='_',2);
    tc = str2double(plotfiles{k}(ind_test(1)+1:ind_test(2)-1));
    if sum(test_vec==tc) == 0
        if test_vec == 0
            test_vec(end) = tc;
        else
            test_vec(end+1) = tc;
        end
    end
end

n_down = 5; % Downsampling rate for plots
numsig = 3; % Number of STD to plot
euler_order = {'\phi [\circ]', '\theta [\circ]', '\psi [\circ]'};
e3 = [0;0;1];

r2d = 180/pi;
d2r = pi/180;

exp_desc_11 = {'Cross traj.','||','||',...
    'Cross + Mag. dist.','||','||',...
    'Slow + Mag. dist.','||','||',...
    'Fast + Mag. dist.','||'}';


%% Sort tests into separate cells
plotfiles_sorted = {};
for k = 1:numel(test_vec)
    tc = test_vec(k);
    IndexC = strfind(plotfiles,num2str(tc));
    Index = find(not(cellfun('isempty',IndexC)));
    plotfiles_sorted(1:numel(Index),k) = plotfiles(Index);
end

numcase = size(plotfiles_sorted,1);
numtc = size(plotfiles_sorted,2);
% res_tabs = cell(numtc,8,numcase);
res_tabs = cell(numtc,6);


%%
for jk = 1:numel(test_vec)
    testcase = test_vec(jk);
    testfiles = plotfiles_sorted(:,jk);
    [tstart, tstop] = LoadTime(testcase);
    if b_plot
        figeuler = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
        sfh(1) = subplot(3,1,1); hold on; grid on;
        sfh(2) = subplot(3,1,2); hold on; grid on;
        sfh(3) = subplot(3,1,3); hold on; grid on;
        
        figeulererror = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
        sfi(1) = subplot(3,1,1); hold on; grid on;
        sfi(2) = subplot(3,1,2); hold on; grid on;
        sfi(3) = subplot(3,1,3); hold on; grid on;
        
        figeulerstd = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
        sfg(1) = subplot(3,1,1); hold on; grid on;
        sfg(2) = subplot(3,1,2); hold on; grid on;
        sfg(3) = subplot(3,1,3); hold on; grid on;
        
        if 1%testcase == 30
            figheadcon = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
            for k = 1:numcase
                sfj(k) = subplot(numcase,1,k); hold on; grid on;
            end
            figinccon = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
            for k = 1:numcase
                sfk(k) = subplot(numcase,1,k); hold on; grid on;
            end
            
            
        end
    elseif ~b_plot && b_plot_con
        figheadcon = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
        for k = 1:numcase
            sfj(k) = subplot(numcase,1,k); hold on; grid on;
        end
        figinccon = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
        for k = 1:numcase
            sfk(k) = subplot(numcase,1,k); hold on; grid on;
        end
        figconmeas = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
        for k = 1:numcase
            sfc(k) = subplot(numcase,1,k); hold on; grid on;
        end
       
    end
    numres = numel(testfiles);
    % figbias = figure;
    
    simleg = cell(numel(testfiles),1);
    eul_rmse_vec = zeros(numres,1);
    eul_maxe_vec = zeros(numres,1);
    eul_stde_vec = zeros(numres,1);
    
    eul_inc_rmse_vec = zeros(numres,1);
    eul_inc_maxe_vec = zeros(numres,1);
    eul_inc_stde_vec = zeros(numres,1);
    
    eul_head_rmse_vec = zeros(numres,1);
    eul_head_maxe_vec = zeros(numres,1);
    eul_head_stde_vec = zeros(numres,1);
    
    %     colour_vec = {'b','g','r','c','m','k'};
    colour_vec = {'r','g--','b-.','k:','k+','k+--','k+-.','k+:'};
    r2d = 180/pi; d2r = pi/180;
    
    max_eul_sig = [0;0;0];
    max_eul_err = 0;
    max_eul = [0;0;0];
    min_eul = [0;0;0];
    
    % Extract sim_mat
    if ~exist('sim_mat','var')
        sim_mat = zeros(numel(testfiles),6);
        for imat = 1:numel(testfiles)
            pat = '\D'; 
            namecell = regexp(testfiles{imat}(5:end-4),pat,'split');
            for icol = 1:6
                sim_mat(imat,icol) =  str2double(namecell{icol});
            end
            
        end
        sim_mat = sim_mat(:,2:end);
    end
    
    for ik = 1:numel(testfiles)
        savename = testfiles{ik}(1:end-4);
        
        try
            load(fullfile(pathname, testfiles{ik}), '*out','t','*px4*',...
                'savename','testcase', 'rpy_in','EKF')
        catch
            load(fullfile(pathname, testfiles{ik}), '*out','t','*px4*',...
                'savename','testcase', 'rpy_in')
        end
        %         try
        %             S = load(fullfile(pathname, plotfiles{ik}), 'testcase');
        %             testcase = S.testcase;
        %         catch
        %             tc_ind = find(savename=='_');
        %             testcase = savename(tc_ind(1)+1:tc_ind(2)-1);
        %         end
        
        
        simleg(ik) = {strrep(savename,'_','\_')};
        rpy_out = rpy_in(:,2:4)*r2d;
        
       
        
%         if exist('EKF','var')
%             sigma_euler_out(1:2,:) = sigma_euler_out(1:2,:)./sqrt(EKF.kcov);
%             sigma_euler_out(3,:) = sigma_euler_out(3,:)./sqrt(EKF.kcov_mag);
%         end
        
        % Plotting params
        %         indplot = (1:numel(t));
        %         tplot = t(indplot);
        indplot = find(t<tstop);
        tplot = t(indplot);
        
        ndown = 1;
        
        px4_error = rpy_out-px4_rotated;
        euler_error = rpy_out-eulerhatout'*r2d;
        
        norm_error_px4 = sum(abs(rpy_out-px4_rotated),2);
        norm_error_ekf = sum(abs(rpy_out-eulerhatout'*r2d),2);
        
        norm_inc_error_ekf = sum(abs(rpy_out(:,1:2)-eulerhatout(1:2,:)'*r2d),2);
        norm_head_error_ekf = sum(abs(rpy_out(:,3)-eulerhatout(3,:)'*r2d),2);
        
        max_eul_err =  max(max(max(norm_error_ekf),max(norm_error_px4)),max_eul_err);
        max_eul = max(max(max((eulerhatout*r2d),[],2),max((px4_rotated),[],1)'),max_eul);
        min_eul = min(min(min((eulerhatout*r2d),[],2),min((px4_rotated),[],1)'),min_eul);
        
        max_eul_sig = max(max((sigma_euler_out*r2d),[],2),max_eul_sig);
        
        %% Calculate inclination error 
        alpha_error = zeros(size(t));
        sigma_alpha = zeros(size(t));
        for iinc = 1:numel(t)
            % Error rotation matrix
            Rote = Euler2RotMat(euler_error(iinc,1)/r2d,euler_error(iinc,2)/r2d,0);
            % Error inclination angle
            ue = null([e3'*Rote'; e3']);
            ea = atan2(-ue'*cross(e3, Rote'*e3),e3'*Rote'*e3)*180/pi;
            alpha_error(iinc) = ea;
            sigma_alpha(iinc) = sqrt(max(abs(eig(diag(sigma_euler_out(1:2,iinc))^2))));
        end
        %%
        
        
        if b_plot
            % Euler plot
            figure(figeuler)
            for klm = 1:3
                %sfh = subplot(4,1,klm); hold on,
                axis([0 tplot(end) -max_eul(klm)*1.1 max_eul(klm)*1.1]);
                
                if ik == 1
                    if b_px4,
                        plot(sfh(klm),downsample(tplot,n_down), downsample(px4_rotated(indplot,klm),n_down),'k--','linewidth',1.5),
                    end
                    plot(sfh(klm),downsample(tplot,n_down), downsample(rpy_out(indplot,klm),n_down))
                end
                heul(ik) =  plot(sfh(klm),downsample(tplot,n_down),downsample(eulerhatout(klm,indplot),n_down)*r2d,colour_vec{ik},'linewidth',1.5);
                %         plotShaded(downsample(tplot,ndown),...
                %             [downsample(eulerhatout(klm,indplot)+3*sigma_euler_out(klm,indplot),ndown);...
                %             downsample(eulerhatout(klm,indplot)-3*sigma_euler_out(klm,indplot),ndown)]*r2d,'r',0.5)
            end
            
            %         subplot(4,1,4),hold on, axis([0 t(end) 0 max_eul_err*1.1])
            %
            %         heul(ik) = plot(downsample(tplot,n_down), downsample(norm_error_ekf(indplot),n_down),colour_vec{ik});
            
            
            if ik == numel(plotfiles) && b_px4% Plot PX4 error
                heul(ik+1) = plot(downsample(tplot,n_down), downsample(norm_error_px4(indplot),n_down),'k--','linewidth',1.5);
                eul_rmse_vec(ik+1) = rms(norm_error_px4);
                eul_maxe_vec(ik+1) = max(norm_error_px4);
                eul_stde_vec(ik+1) = std(norm_error_px4);
            end
            
            % Euler error
            figure(figeulererror)
            for klm = 1:3
                %sfh = subplot(4,1,klm); hold on,
                axis([0 tplot(end) -max_eul(klm)*1.1 max_eul(klm)*1.1]);
                
                if ik == 1
                    if b_px4,
                        plot(sfi(klm),downsample(tplot,n_down), downsample(px4_error(indplot,klm),n_down),'k--','linewidth',1),
                    end
                end
                heulerr(ik) = plot(sfi(klm),downsample(tplot,n_down),downsample(euler_error(indplot,klm),n_down),colour_vec{ik},'linewidth',1.5);
                %         plotShaded(downsample(tplot,ndown),...
                %             [downsample(eulerhatout(klm,indplot)+3*sigma_euler_out(klm,indplot),ndown);...
                %             downsample(eulerhatout(klm,indplot)-3*sigma_euler_out(klm,indplot),ndown)]*r2d,'r',0.5)
            end
            %         subplot(4,1,4),hold on, axis([0 t(end) 0 max_eul_err*1.1])
            %
            %         heulerr(ik) = plot(downsample(tplot,n_down), downsample(norm_error_ekf(indplot),n_down),colour_vec{ik});
            
            % Euler STD
            figure(figeulerstd)
            for klm = 1:3
                %sfh = subplot(4,1,klm); hold on,
                axis([0 tplot(end) -max_eul(klm)*1.1 max_eul(klm)*1.1]);
                plot(sfg(klm),downsample(tplot,n_down),downsample(numsig*sigma_euler_out(klm,indplot),n_down)*r2d,colour_vec{ik},'linewidth',1.5)
            end
        end
        
        % Error stats
        eul_rmse_vec(ik) = rms(norm_error_ekf);
        eul_maxe_vec(ik) = max(norm_error_ekf);
        eul_stde_vec(ik) = std(norm_error_ekf);
        
        eul_inc_rmse_vec(ik) = rms(norm_inc_error_ekf);
        eul_inc_maxe_vec(ik) = max(norm_inc_error_ekf);
        eul_inc_stde_vec(ik) = std(norm_inc_error_ekf);
        
        eul_head_rmse_vec(ik) = rms(norm_head_error_ekf);
        eul_head_maxe_vec(ik) = max(norm_head_error_ekf);
        eul_head_stde_vec(ik) = std(norm_head_error_ekf);
        
        %         res_tabs(jk,1,ik) = {num2str(jk)};
        %         res_tabs(jk,3,ik) = {num2str(eul_inc_maxe_vec(ik),'%3.2f')};
        %         res_tabs(jk,4,ik) = {num2str(eul_inc_rmse_vec(ik),'%3.2f')};
        %         res_tabs(jk,5,ik) = {num2str(eul_inc_stde_vec(ik),'%3.2f')};
        %         res_tabs(jk,6,ik) = {num2str(eul_head_maxe_vec(ik),'%3.2f')};
        %         res_tabs(jk,7,ik) = {num2str(eul_head_rmse_vec(ik),'%3.2f')};
        %         res_tabs(jk,8,ik) = {num2str(eul_head_stde_vec(ik),'%3.2f')};
        
        if exist('sim_mat','var')
                simleg = cell(1,numel(testfiles));
                for icase = 1:numcase
                    simleg{icase} = GetFiltName(sim_mat(icase,:));
                end
            else
                simleg = {'$FT-KF$','$IEKF$','$FT-IEKF-\chi^2$','$FT-IEKF-GLR$'};
        end
            
        conheadleg = {'$|\psi-\hat{\psi}|$','$3\hat{\sigma}_{\psi}$','$PL_{\psi}$','$AL_{\psi}$'};
        conincleg = {'$|\alpha-\hat{\alpha}|$','$3\hat{\alpha}_{\alpha}$','$PL_{\alpha}$','$AL_{\alpha}$'};
        conconsleg = {'$CM_{\alpha}$','$CM_{\psi}$','$CM_{Limit}$'};
        %             conincleg = {'$|\phi-\hat{\phi}|$','$3\hat{\sigma}_{\phi}$'...
        %                 '$|\theta-\hat{\theta}|$','$3\hat{\sigma}_{\theta}$','$PL_{\alpha}$','$AL_{\alpha}$'};
        
%% ============== Integrity calculation ===================================
        AL_alpha = 15; % Alert limit, inclination, degrees
        AL_psi = 10; % Alert limit, heading, degrees
        
        P_ir = 10^-7;
        %     P_ir = 1-0.9967;
        P_f     = 1-0.9967;	% Probabilité d'apparition des pannes
        q_alpha = 2;
        q_psi = 1;
        % Risk allocation
        amin = max([0 ; 1-(1-P_f)/P_ir]);
        amax = min([1 ; P_f/P_ir]);
        ira_1   = 1-10^(-3);	% Integrity Risk Allocation level 1
        %     ira_1   = 0.1;	% Integrity Risk Allocation level 1
        ira_2   = 10^(-2);      % Integrity Risk Allocation level 2
        alloc_1 = ira_1 * amin + (1-ira_1) * amax;
        % Probabilities afetr first allocation level
        P_ff = P_ir * (1-alloc_1)/(1-P_f);
        P_md = P_ir * alloc_1/P_f;
        
        % Inclination integrity
        P_ff_alpha = 2*gammaincinv(P_ff,q_alpha/2,'upper');
        % Heading integrity
        P_ff_psi = 2*gammaincinv(P_ff,q_psi/2,'upper');
        
        PL_alpha = zeros(size(tplot));
        PL_psi = zeros(size(tplot));
        for klm = 1:numel(tplot)
            P_alpha = diag(sigma_euler_out(1:2,klm))^2;
            P_psi = sigma_euler_out(3,klm)^2;
            PL_alpha(klm) = sqrt(max(abs(eig(P_alpha*r2d^2)))*P_ff_alpha);
            PL_psi(klm) = sqrt(max(abs(eig(P_psi*r2d^2)))*(P_ff_psi));
        end
%=========================================================================
        %     figure,
        %     subplot(211), hold on,
        %     plot(tplot, PL_alpha,'b', 'linewidth',1.5)
        %     plot(tplot, ones(size(tplot))*AL_alpha,'g--','linewidth',1.5)
        %     plot(tplot, 3*sigma_euler_out(1:2,indplot)*r2d,'r','linewidth',1.5)
        % %     plot(tplot, euler_error(indplot,1:2))
        %     plot(tplot, error_euler_out(1:2,indplot)*r2d,'linewidth',1.5)
        %
        %     subplot(212), hold on
        %     plot(tplot, PL_psi,'b','linewidth',1.5)
        %     plot(tplot, ones(size(tplot))*AL_psi,'g--','linewidth',1.5)
        %     plot(tplot, 3*sigma_euler_out(3,indplot)*r2d,'r','linewidth',1.5)
        % %     plot(tplot, euler_error(indplot,3))
        %     plot(tplot, error_euler_out(3,indplot)*r2d,'linewidth',1.5)
%         ind_alarm = find(PL_psi>AL_psi);
%         try
%             alarm_start = ind_alarm(1);
%         catch
%             alarm_start = 1;
%         end
%         try
%             alarm_end = ind_alarm(end);
%         catch
%             alarm_end = 1;
%         end
%         tmp = 30;
%         
%         fig_int = figure;
%         s1 = subplot(1,1,1); hold on
%         p1 = plot(tplot, PL_psi,'b','linewidth',1.5);
%         %p1 = plot(tplot, -PL_psi,'b','linewidth',1.5);
%         p2 = plot(tplot, ones(size(tplot))*AL_psi,'g--','linewidth',1.5);
%         %p2 = plot(tplot, -ones(size(tplot))*AL_psi,'g--','linewidth',1.5);
%         p3 = plot(tplot, 3*sigma_euler_out(3,indplot)*r2d,'r:','linewidth',1.5);
%         %p3 = plot(tplot, -3*sigma_euler_out(3,indplot)*r2d,'r:','linewidth',1.5);
%         %     plot(tplot, euler_error(indplot,3))
%         p4 = plot(tplot, abs(euler_error(indplot,3)),'linewidth',1.5);
%         %     p4 = plot(tplot, abs(error_euler_out(3,indplot))*r2d,'linewidth',1.5);
%         legend([p1,p2,p3,p4],{'$PL_{\psi}$','$AL$','$3\sigma_{\psi}$','$|\epsilon_{\psi}|$'},...
%             'interpreter','latex','location','northwest')
        %plot(ones(100)*tplot(alarm_start),linspace(-tmp*2/3,tmp*2/3,100),'r','linewidth',1.5)
        %     plot(ones(100)*tplot(alarm_end),linspace(-25,25,100),'k:','linewidth',1.5)
        
        %plot(ones(100)*0.3*tend,linspace(-tmp,tmp,100),'k','linewidth',1.5)
        %plot(ones(100)*0.9*tend,linspace(-tmp,tmp,100),'k','linewidth',1.5)
%         ylim([-0 tmp])
%         set(s1,'YTick',-tmp:10:tmp)
%         ylabel('$[^{\circ}]$','interpreter','latex')
%         xlabel('Time [s]','interpreter','latex')
%         funFigureProperty
%         set(gcf,'color','w')

%% =============== Consistency measurement calculation =====================
    % Heading consistency
    w = 303;                      % sliding window size
    x = euler_error(indplot,3)';      % input signal
    N = length(x);              % length of the signal
    
    % element count in each window
    n = conv(ones(1, N), ones(1, w), 'same');
    
    % calculate s vector
    s = conv(x, ones(1, w), 'same');
    
    % calculate q vector
    q = x .^ 2;
    q = conv(q, ones(1, w), 'same');
    
    % calculate output values
    o = (q - s .^ 2 ./ n) ./ (n - 1);
    
    % have to take the square root since output o is the
    % square of the standard deviation currently
    o = o .^ 0.5;
    
    cons_meas_2_head = (3*sigma_euler_out(3,indplot)*r2d - (3*o + abs(x) ) )./ (3*o + abs(x) );
    % Inclination consisitency
    x = alpha_error(indplot);      % input signal
    N = length(x);              % length of the signal
    
    % element count in each window
    n = conv(ones(1, N), ones(1, w), 'same');
    
    % calculate s vector
    s = conv(x, ones(1, w), 'same');
    
    % calculate q vector
    q = x .^ 2;
    q = conv(q, ones(1, w), 'same');
    
    % calculate output values
    o = (q - s .^ 2 ./ n) ./ (n - 1);
    
    % have to take the square root since output o is the
    % square of the standard deviation currently
    o = o .^ 0.5;
    
    cons_meas_2_inc = (3*sigma_alpha(indplot)*r2d - (3*o + abs(x) ) )./ (3*o + abs(x) );
    
    if b_plot_con
        figure(figconmeas), hold on
        hconmeas(1,ik) = plot(sfc(ik),downsample(tplot,n_down),(downsample(cons_meas_2_inc,n_down)),'b','linewidth',2);
        hconmeas(2,ik) = plot(sfc(ik),downsample(tplot,n_down),(downsample(cons_meas_2_head,n_down)),'r','linewidth',2);
        hconmeas(3,ik) = plot(sfc(ik),downsample(tplot,n_down),zeros(size(downsample(tplot,n_down))),'g--','linewidth',2);
        title(sfc(ik),strrep(simleg{ik},'$',''),'fontsize',fonty+2,'units', 'normalized','Position',[0.5 0.85 0])
%         sfc(ik).YAxis.Limits = [-2,3];
        %axis([0 tplot(end) -2 3])
    end
%==========================================================================    
    %figure, plot(tplot, cons_meas_2)
    
%     mean(cons_meas_2)
    
%     figure
%     s1 = subplot(211); hold on
%     plot(tplot, abs(euler_error(indplot,3)),'linewidth',1.5)
%     plot(tplot, sigma_euler_out(3,indplot)*r2d,'linewidth',1.5)
%     plot(tplot, 3*o,'linewidth',1.5)
%     l1 = legend({'$\epsilon_{\psi}$','$3\,\sigma_{\psi}$','$3\,\sigma_{\epsilon_{\psi}}$'},...
%         'interpreter','latex');
%     xlim([0,tplot(end)])
%     ylim([0, max(euler_error(indplot,3))])
%     s1.Position  = s1.Position + [0 0 0 0];
%     set(gca,'xticklabel',[])
%     ylabel('$[^{\circ}]$','interpreter','latex')
%     set(s1,'YTick',0:10:50)
%     funFigureProperty(l1)
%     s2 = subplot(212); hold on
%     plot(tplot, cons_meas_2,'linewidth',1.5)
%     plot(tplot, zeros(size(tplot)),'k--','linewidth',1.5)
%     xlim([0,tplot(end)])
%     ylim([-1 max(cons_meas_2)*1.1])
%     ylabel('CM $[-]$','interpreter','latex')
%     xlabel('Time [s]','interpreter','latex')
%     s2.Position  = s2.Position + [0 0.07 0 0];
%     funFigureProperty
        
%%        
        if b_plot_con %testcase == 30
            textyaw(ik) = max(max(max(3*sigma_euler_out(3,indplot))*r2d, max(euler_error(indplot,3))),1.5*max(AL_psi,max(PL_psi)));
            textpitch(ik) = max(max(max(3*sigma_euler_out(2,indplot))*r2d, max(euler_error(indplot,2))),1.5*max(AL_alpha,max(PL_alpha)));
            textroll(ik) = max(max(max(3*sigma_euler_out(1,indplot))*r2d, max(euler_error(indplot,1))),1.5*max(AL_alpha,max(PL_alpha)));
            textrp(ik) = max(textpitch(ik), textroll(ik));
        
            figure(figheadcon)
            heulheadcon(1,ik) = plot(sfj(ik),downsample(tplot,n_down),abs(downsample(euler_error(indplot,3),n_down)),'k','linewidth',2);
            heulheadcon(2,ik) = plot(sfj(ik),downsample(tplot,n_down),3*downsample(sigma_euler_out(3,indplot)*r2d,n_down),'k--','linewidth',2);
            heulheadcon(3,ik) = plot(sfj(ik),downsample(tplot,n_down), downsample(PL_psi,n_down),'b','linewidth',2);
            heulheadcon(4,ik) = plot(sfj(ik),downsample(tplot,n_down),ones(size(downsample(tplot,n_down)))*AL_psi,'g--','linewidth',2);
            %             text(3,0.75*textyaw(ik),strrep(simleg{ik},'$',''),'FontName','Times', 'FontSize',18)
            title(sfj(ik),strrep(simleg{ik},'$',''),'fontsize',fonty+2,'units', 'normalized','Position',[0.5 0.85 0])
            
            figure(figinccon)
%             heulinccon(1,ik) = plot(sfk(ik),downsample(tplot,n_down),abs(downsample(euler_error(indplot,1),n_down)),'r','linewidth',2);
%             heulinccon(3,ik) = plot(sfk(ik),downsample(tplot,n_down),abs(downsample(euler_error(indplot,2),n_down)),'k','linewidth',2);
%             heulinccon(2,ik) = plot(sfk(ik),downsample(tplot,n_down),3*downsample(sigma_euler_out(1,indplot)*r2d,n_down),'r--','linewidth',2);
%             heulinccon(4,ik) = plot(sfk(ik),downsample(tplot,n_down),3*downsample(sigma_euler_out(2,indplot)*r2d,n_down),'k--','linewidth',2);
            heulinccon(1,ik) = plot(sfk(ik),downsample(tplot,n_down),abs(downsample(alpha_error(indplot),n_down)),'k','linewidth',2);
            heulinccon(2,ik) = plot(sfk(ik),downsample(tplot,n_down),3*downsample(sigma_alpha(indplot)*r2d,n_down),'k--','linewidth',2);
            heulinccon(3,ik) = plot(sfk(ik),downsample(tplot,n_down), downsample(PL_alpha,n_down),'b','linewidth',2);
            heulinccon(4,ik) = plot(sfk(ik),downsample(tplot,n_down), ones(size(downsample(tplot,n_down)))*AL_alpha,'g--','linewidth',2);
            %             text(3,0.75*textrp(ik),strrep(simleg{ik},'$',''),'FontName','Times', 'FontSize',18)
            title(sfk(ik),strrep(simleg{ik},'$',''),'fontsize',fonty+2,'units', 'normalized','Position',[0.5 0.85 0])
        end
        
    end
    
    for k = 1:numcase
        if k == numcase
            sep = '';
        else
            sep = ' / ';
        end
        [~,ind] = min(eul_inc_maxe_vec);
        if ind == k
            strbf11 = '\textbf{'; strbf12 = '}';
        else
            strbf11 = ''; strbf12 = '';
        end
        [~,ind] = min(eul_inc_rmse_vec);
        if ind == k
            strbf21 = '\textbf{'; strbf22 = '}';
        else
            strbf21 = ''; strbf22 = '';
        end
        [~,ind] = min(eul_head_maxe_vec);
        if ind == k
            strbf31 = '\textbf{'; strbf32 = '}';
        else
            strbf31 = ''; strbf32 = '';
        end
        [~,ind] = min(eul_head_rmse_vec);
        if ind == k
            strbf41 = '\textbf{'; strbf42 = '}';
        else
            strbf41 = ''; strbf42 = '';
        end
        
        res_tabs(jk,1) = {num2str(jk)};
        res_tabs(jk,3) = {[res_tabs{jk,3},strbf11, num2str(eul_inc_maxe_vec(k),'%3.2f'),strbf12,sep]};
        res_tabs(jk,4) = {[res_tabs{jk,4},strbf21, num2str(eul_inc_rmse_vec(k),'%3.2f'),strbf22,sep]};
        res_tabs(jk,5) = {[res_tabs{jk,5},strbf31, num2str(eul_head_maxe_vec(k),'%3.2f'),strbf32,sep]};
        res_tabs(jk,6) = {[res_tabs{jk,6},strbf41, num2str(eul_head_rmse_vec(k),'%3.2f'),strbf42,sep]};
    end
    
    %%
    
    
    if b_plot && 1 == 2
        sfh(1).Title.String = ['Euler angles vs Time, Testcase: ',num2str(testcase)];
        for klm = 1:3
            sfh(klm).Position = sfh(klm).Position + [0.0 0.01 -0.09 0.01];
            sfh(klm).XAxis.Limits = [0,tplot(end)];
            %sfh(klm).YAxis.Limits = [min_eul(klm)*1.1,max_eul(klm)*1.1];
            sfh(klm).YLabel.String = euler_order{klm};
            axis(sfh(klm), 'tight');
            a = get(sfh(klm),'XTickLabel');
            set(sfh(klm),'XTickLabel',a,'FontName','Times','fontsize',18)
            legend(sfh(klm).Children,simleg,'location','best','interpreter','latex', 'fontsize', fonty);
        end
        sfh(3).XLabel.String = 'Time [s]';
        
        sfi(1).Title.String = ['Euler angles error vs Time, Testcase: ',num2str(testcase)];
        for klm = 1:3
            sfi(klm).Position = sfi(klm).Position + [0.0 0.01 -0.09 0.01];
            sfi(klm).XAxis.Limits = [0,tplot(end)];
            %sfi(klm).YAxis.Limits = [min_eul(klm)*1.1,max_eul(klm)*1.1];
            sfi(klm).YLabel.String = euler_order{klm};
            axis(sfi(klm), 'tight');
            a = get(sfi(klm),'XTickLabel');
            set(sfi(klm),'XTickLabel',a,'FontName','Times','fontsize',18)
            legend(heulerr,simleg,'location','best','interpreter','latex', 'fontsize', fonty);
        end
        sfi(3).XLabel.String = 'Time [s]';
        
        sfg(1).Title.String = ['Euler angles ' ,num2str(numsig), 'xSTD vs Time, Testcase: ',num2str(testcase)];
        for klm = 1:3
            sfg(klm).Position = sfg(klm).Position + [0.0 0.01 -0.09 0.01];
            sfg(klm).XAxis.Limits = [0,tplot(end)];
            %sfg(klm).YAxis.Limits = [0,3*max_eul_sig(klm)*1.1];
            sfg(klm).YLabel.String = euler_order{klm};
            axis(sfg(klm), 'tight');
            a = get(sfg(klm),'XTickLabel');
            set(sfg(klm),'XTickLabel',a,'FontName','Times','fontsize',18)
            legend(sfg(1),simleg,'location','best','interpreter','latex', 'fontsize', fonty);
        end
        sfg(3).XLabel.String = 'Time [s]';
        
        if b_px4,
            simleg(end+1) = {'PX4'};
        end
        
        
        %         h1 = legend(heul,simleg,'location','best','interpreter','latex', 'fontsize', fonty);
        %         funFigureProperty(h1);
        set(gcf,'color','w')
        set(gcf, 'Color', 'w');
        
        %         h2 = legend(heulerr,simleg,'location','best','interpreter','latex', 'fontsize', fonty);
        %         funFigureProperty(h2);
        set(gcf,'color','w')
        set(gcf, 'Color', 'w');
        
        figure(figeulerstd)
        %         h3 = legend(sfg(1),simleg,'location','best','interpreter','latex', 'fontsize', fonty);
        %         funFigureProperty(h3);
        %     set(gca,'ytick',[0 1]);
        %     set(gca,'yticklabel',[0 1]);
        %     set(gca,'xticklabel',[]);
        %     axis([0 euler_ekf.time(end) 0 1])
        
    end
    if b_plot_con
        figure(figconmeas)
        for klm = 1:numcase
            if klm == 1
                ht = legend(hconmeas(:,klm),conconsleg,'location','eastoutside',...
                    'interpreter','latex', 'fontsize', fonty,'orientation','vertical');
                posy_first = sfc(klm).Position;
            end
            sfc(klm).Position = [sfc(klm).Position(1:2) posy_first(3)-0.01 posy_first(4)];
            sfc(klm).XAxis.Limits = [0,tplot(end)];
            %sfc(klm).YAxis.Limits = [-2,3];
            sfc(klm).YLabel.String = '[-]';
            axis(sfc(klm), 'tight');
            a = get(sfc(klm),'XTickLabel');
            set(sfc(klm),'XTickLabel',a,'FontName','Times','fontsize',fonty)
%             a = get(sfc(klm),'XTickLabel'); b = get(sfc(klm),'YTickLabel');
%             set(sfc(klm),'XTickLabel',a,'FontName','Times','fontsize',fonty)
%             set(sfc(klm),'YTickLabel',b,'FontName','Times','fontsize',fonty)
            
        end
        for k = 1:numcase, set(sfc(k),'YLim',[-1 3]), end
        
        texty = textyaw;
        figure(figheadcon)
        for klm = 1:numcase
            if klm == 1
                ht = legend(heulheadcon(:,klm),conheadleg,'location','eastoutside',...
                    'interpreter','latex', 'fontsize', fonty,'orientation','vertical');
                posy_first = sfj(klm).Position;
            end
            %                 texty(klm) = max(max(sfj(klm).Children(1).YData), max(sfj(klm).Children(2).YData)) ;
            sfj(klm).Position = [sfj(klm).Position(1:2) posy_first(3)-0.01 posy_first(4)];
            sfj(klm).XAxis.Limits = [0,tplot(end)];
            %sfh(klm).YAxis.Limits = [min_eul(klm)*1.1,max_eul(klm)*1.1];
            sfj(klm).YLabel.String = '[{\circ}]';
            %                 annotation(sfj(klm),'test',[1 1 3 3])
            
            
            axis(sfj(klm), 'tight');
            a = get(sfj(klm),'XTickLabel');
            set(sfj(klm),'XTickLabel',a,'FontName','Times','fontsize',fonty)
            %if klm ~= numcase, set(sfj(klm),'XTickLabel',[]), end
            nticks = 2;
            if round(ceil(texty(klm))/5)*5 > 10, nticks = 5; end
            if round(ceil(texty(klm))/5)*5 > 25, nticks = 10; end
            if round(ceil(texty(klm))/5)*5 > 50, nticks = 15; end
            if round(ceil(texty(klm))/5)*5 > 75, nticks = 25; end
            if round(ceil(texty(klm))/5)*5 > 100, nticks = 30; end
            if round(ceil(texty(klm))/5)*5 > 125, nticks = 35; end
            if round(ceil(texty(klm))/5)*5 > 150, nticks = 40; end
            %                 set(sfj(klm),'YTick',round((0:ceil(texty(klm)/(3)):ceil(texty(klm)))./5)*5)
            %set(sfj(klm),'YTick',0:nticks:ceil(texty(klm)))
            %                 funFigureProperty(ht);
            %                 funFigureProperty
        end
        for k = 1:numcase, set(sfj(k),'YLim',[0 textyaw(k)]), end
        
        figure(figinccon)
        texty = textrp;
        for klm = 1:numcase
            if klm == 1
                ht = legend(heulinccon(:,klm),conincleg,'location','eastoutside','interpreter','latex', 'fontsize', fonty);
                posy_first = sfk(klm).Position;
            end
            sfk(klm).Position = [sfk(klm).Position(1:2) posy_first(3)-0.01 posy_first(4)];
            
            %texty(klm) = max(max(sfk(klm).Children(1).YData), max(sfk(klm).Children(2).YData)) ;
            %sfk(klm).Position = sfh(klm).Position + [0.0 0.01 -0.09 0.01];
            sfk(klm).XAxis.Limits = [0,tplot(end)];
            %sfh(klm).YAxis.Limits = [min_eul(klm)*1.1,max_eul(klm)*1.1];
            sfk(klm).YLabel.String = '[{\circ}]';
            %text(sfk(klm),3,0.75*texty(klm),strrep(simleg{klm},'$',''),'FontName','Times', 'FontSize',18)
            axis(sfk(klm), 'tight');
            a = get(sfk(klm),'XTickLabel');
            set(sfk(klm),'XTickLabel',a,'FontName','Times','fontsize',fonty)
            nticks = 2;
            if round(ceil(texty(klm))/5)*5 > 10, nticks = 5; end
            if round(ceil(texty(klm))/5)*5 > 25, nticks = 10; end
            if round(ceil(texty(klm))/5)*5 > 50, nticks = 15; end
            if round(ceil(texty(klm))/5)*5 > 75, nticks = 25; end
            if round(ceil(texty(klm))/5)*5 > 100, nticks = 30; end
            if round(ceil(texty(klm))/5)*5 > 125, nticks = 35; end
            if round(ceil(texty(klm))/5)*5 > 150, nticks = 40; end
            %                 a = get(sfk(klm),'XTickLabel');
            %                 set(sfk(klm),'XTickLabel',a,'FontName','Times','fontsize',fonty)
            %                 ylim([0 12]);
            %                 set(sfk(k),'YLim',[0 12])
            %                 set(sfk(klm),'YTick',round((0:ceil(texty(klm)/(3)):ceil(texty(klm)))./5)*5)
            %set(sfk(klm),'YTick',0:nticks:ceil(texty(klm)))
            %                 funFigureProperty(ht);
        end
        for k = 1:numcase, set(sfk(k),'YLim',[0 textrp(k)]), end
    end
    
%%
acc_out = zeros(size(acc_out));
mag_out = zeros(size(mag_out));
for kimu = 1:numel(tplot)
    acc_out(:,kimu) = Euler2RotMat(rpy_in(kimu,2),rpy_in(kimu,3),rpy_in(kimu,4))'*EKF.gvec;
    mag_out(:,kimu) = Euler2RotMat(rpy_in(kimu,2),rpy_in(kimu,3),rpy_in(kimu,4))'*EKF.mag_ref;
end
% Plot_IMU    
Plot_Euler    
    
    %%
    % Save comparison results
    resname = [num2str(testcase), '_results'];
    pathname_save = fullfile(pathname,'Figures');
    mkdir(pathname_save);
    
    save(fullfile(pathname,resname),'simleg','eul_rmse_vec', 'eul_maxe_vec','eul_stde_vec');
    if b_savefig(1) && b_plot
        savefig(figeuler,fullfile(pathname_save,[resname,'_euler']));
        export_fig(fullfile(pathname_save,[resname,'_euler']),'-pdf',figeuler);
    end
    if b_savefig(2) && b_plot
        savefig(figeulerstd,fullfile(pathname_save,[resname,'_euler_3sigma']));
        export_fig(fullfile(pathname_save,[resname,'_euler_3sigma']),'-pdf',figeulerstd);
    end
    if b_savefig(3) && b_plot
        savefig(figeulererror,fullfile(pathname_save,[resname,'_euler_error']));
        export_fig(fullfile(pathname_save,[resname,'_euler_error']),'-pdf',figeulererror);
    end
    
    if b_savefig_con && b_plot_con
        savefig(figheadcon,fullfile(pathname_save,[resname,'_euler_head_con']));
        export_fig(fullfile(pathname_save,[resname,'_euler_head_con']),'-pdf',figheadcon);
    end
    if b_savefig_con && b_plot_con
        savefig(figinccon,fullfile(pathname_save,[resname,'_euler_inc_con']));
        export_fig(fullfile(pathname_save,[resname,'_euler_inc_con']),'-pdf',figinccon);
    end
    %     close all
    
end

for klm = 1:numcase
    res_tabs(:,2) = exp_desc_11;
    matrix2latex(res_tabs, fullfile(pathname_save,...
        ['restab_full.tex']),'size', 'tiny');
end

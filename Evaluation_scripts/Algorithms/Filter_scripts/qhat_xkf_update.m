function [xhat_out,P_out] ...
    = qhat_xkf_update(EKF,inputs,uhat_ex, cov_uhat_ex, xhat_in,P_in, dt)
% Nonlinear Kalman filter estimating rotation and translation.
%
% ---- Outputs :
% nu : innovations [13x1]
% omegahat : estimated body angle rates (p, q, r) [3x1]
% qatt_out : rotation quaternion (s, x, y, z) [4x1]
% Rot : Rotational matrix R(qatt_out) [3x3]
% euler : Euler angles (phi, theta, psi) [3x1]
% xhat_out : state output
% P_out : state covariance output
%
% ---- State: [qatt; bw; V; P; ba]
% qatt : rotation quaternion [s x y z]'
% bw : gyro bias [bx]
%
% ---- Dynamic model :
% q_dot = 0.5*Omega(w-bw)q
% bw_dot = -1/tau*sat(bw)
%
% Integration method : Euler forward
%
% ---- Measurements:
% Accelerometer: acc_m [3x1]
% Magnetometer : mag_m [3x1]
%
% Sequential treatment of measurements

tmp = zeros(20,1);
% tmpstruct2 = zeros(10,1);

nx = numel(xhat_in);

qhat_ex = uhat_ex(1:4);
bwhat = uhat_ex(5:7);

% State definition
qhat = xhat_in(1:4);

% Quaternion elements
s = qhat_ex(1); x = qhat_ex(2); y = qhat_ex(3); z = qhat_ex(4);

% State covariance
Phat = P_in;

% Input
omega_m = inputs(1:3);

% Corrected omega
omegahat = omega_m - bwhat;

% ++++++++++++++++
% Prediction step
% ++++++++++++++++
% State prediction
% Quaternion
Omega_mat = [0,       -omegahat';
            omegahat, skew(omegahat)'];
qdot = 0.5*Omega_mat*qhat;
qhat = qhat + qdot*dt; % prediction of quaternion
qhat = qhat/norm(qhat); % normalization of predicted quaternion

% State covariance
% Jacobian
dfqdq = 0.5*Omega_mat*dt + eye(4);

Fk = dfqdq;     % q
    
dfqdw = 0.5*[0 -x -y -z;
    0 s -z y;
    0 z s -x;
    0 -y -x s]*dt;
Vkq = diag([0; EKF.cov.gyro]);

dfqdbw = 0.5*[-x -y -z;
    s -z y;
    z s -x;
    -y -x s] * dt;

Vbw = cov_uhat_ex(5:7,5:7)*dt;

Qq = dfqdw*Vkq*dfqdw' + dfqdbw*Vbw*dfqdbw';

Qk = Qq;

Phat = Fk*Phat*Fk' + Qk;

% Quaternion elements
s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
% +++++++++++++++++++++++++++++++++++++++++++++
% Covariance correction due to normalization
% +++++++++++++++++++++++++++++++++++++++++++++

% h = q'q = 1;
Rkq = 1e-5;
Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
Skq = Hkq*Phat*Hkq'+Rkq;
Kkq = Phat*Hkq'*(Skq\eye(1));
Phat = (eye(nx)-Kkq*Hkq)*Phat;

% ################################
% ####  Quaternion correction ####
% ################################
% [~, nu] = quaternionErrorFct(qhat,qhat_ex);
nu = qhat - qhat_ex;
Hm = eye(4);
Rm = cov_uhat_ex(1:4,1:4);
Sm = Hm*Phat*Hm' + Rm;

Km = Phat*Hm'*pinv(Sm);

qhat = qhat + Km*nu;
qhat = qhat/norm(qhat); % normalization of predicted quaternion

Phat =  (eye(nx)-Km*Hm)*Phat*(eye(nx)-Km*Hm)' + Km*Rm*Km'; % Joseph form
% +++++++++++++++++++++++++++++++++++++++++++++
% Covariance correction due to normalization
% +++++++++++++++++++++++++++++++++++++++++++++

% h = q'q = 1;
Rkq = 1e-5;
Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
Skq = Hkq*Phat*Hkq'+Rkq;
Kkq = Phat*Hkq'*(Skq\eye(1));
Phat = (eye(nx)-Kkq*Hkq)*Phat;

% ++++++++++++
% Output
% ++++++++++++

xhat_out = qhat;
P_out = Phat;


end
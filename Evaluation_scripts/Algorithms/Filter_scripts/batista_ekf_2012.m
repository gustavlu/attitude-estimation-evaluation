function [ xhat_out,Phat_out, qhat_out, P_q] = batista_ekf_2012(EKF, inputs, measurements, xhat_in,Phat_in, dt, corr_status, qhat_in )
%DECKF_ Summary of this function goes here
%   Detailed explanation goes here

nx = numel(xhat_in);
gvec = EKF.gvec;
mag_ref = EKF.mag_ref;

bmax = EKF.bsat.bmax;

% Measurement covariance (continuous observer)
Rtot = blkdiag(EKF.Ra, EKF.Rm)*dt;

Rot = Quaternion2RotMat(qhat_in);
gbest = Rot*EKF.gvec;
% Input covariance
Qb = EKF.bsat.Qd;
Qw = diag(EKF.cov.bgyro);

Qtot = dt*blkdiag(zeros(3),zeros(3),Qw);

% State definition
xhat = xhat_in;
bhat = xhat_in(7:9);
% State covariance
Phat = Phat_in;

% Measurements
accm = measurements(1:3);
magm = measurements(4:6);

% Corrected omega
omega = inputs(1:3);
omegahat = omega - bhat;

S_w = skew(omega);
yvec = zeros(6,1);
if corr_status(1) > 0
    S_y1 = skew(accm);
    C11 = eye(3);
    yvec(1:3) = accm;
else
    S_y1 = zeros(3);
    C11 = zeros(3);
end
if corr_status(2) > 0 && corr_status(1) > 0 
    S_y2 = skew(magm);
    C22 = eye(3);
    yvec(4:6) = magm;
%     S_y2 = skew(cross(accm,magm));
%     C22 = eye(3);
%     yvec(4:6) = cross(accm,magm);
%     S_y2 = skew(cross(gbest,magm));
%     C22 = eye(3);
%     yvec(4:6) = cross(gbest,magm);
else
    S_y2 = zeros(3);
    C22 = zeros(3);
end

dfbwdbw = -EKF.bsat.kaw_sto*eye(3)*(eye(3)- diag([erf(bmax/(sqrt(2*Phat(7,7)))),...
            erf(bmax/(sqrt(2*Phat(8,8)))),...
            erf(bmax/(sqrt(2*Phat(9,9))))]));

Ak = [-S_w, zeros(3), -S_y1;
      zeros(3), -S_w, -S_y2;
      zeros(3,9)];
  
Ck = [C11, zeros(3,6);
     zeros(3), C22, zeros(3)];
% Ck = [C11, zeros(3,6);
%       -skew(xhat(1:3)), skew(xhat(4:6)), zeros(3)];

Kk = Phat*Ck.'/Rtot;
Kk = sat(Kk, -10, 10);
xhatdot = Ak*xhat + Kk*(yvec - Ck*xhat);
Pdot = Ak*Phat + Phat*Ak + Qtot - Phat*Ck.'/Rtot*Ck*Phat;

xhat = xhat + xhatdot*dt;
Phat = Phat + Pdot*dt;

Phat = 0.5*(Phat+Phat.'); % Force Symmetry

% Markley's optimal attitude algorithm 
% [Markley, F. L. (2008). Optimal Attitude Matrix from Two Vector ]

avec = [0.5 0.5];
a1 = avec(1); a2 = avec(2);

b1 = xhat(1:3);
b2 = xhat(4:6);
r1 = EKF.gvec;
r2 = EKF.mag_ref;

% b1 = xhat(1:3);
% b2 = xhat(4:6);
% r1 = EKF.gvec;
% r2 = cross(EKF.gvec,EKF.mag_ref);

lam2 = a1^2 + a2^2 + 2*a1*a2*((b1.'*b2)*(r1.'*r2) + norm(cross(b1,b2))*norm(cross(r1,r2)));

lam = abs(sqrt(lam2));

bx = cross(b1,b2)/norm(cross(b1,b2));
rx = cross(r1,r2)/norm(cross(r1,r2));

Aopt = (a1/lam)*(b1*r1.' + cross(b1,bx)*cross(r1,rx)') +...
       (a2/lam)*(b2*r2.' + cross(b2,bx)*cross(r2,rx)') + bx*rx.';     

qhat = RotMat2Quaternion(Aopt.');


    
% ahat = xhat(1:3);
% mhat = xhat(4:6);
% R_i = [EKF.gvec/norm(EKF.gvec), ...
%        cross(EKF.gvec,EKF.mag_ref)/norm(cross(EKF.gvec,EKF.mag_ref)),...
%        cross(EKF.gvec, cross(EKF.gvec, EKF.mag_ref))/norm(cross(EKF.gvec, cross(EKF.gvec, EKF.mag_ref)))];
%          
% Rot_trans = [ahat/norm(ahat), ...
%              cross(ahat,mhat)/norm(cross(ahat,mhat)),...
%              cross(ahat, cross(ahat, mhat))/norm(cross(ahat, cross(ahat, mhat)))]*R_i';
% 
% Rot_trans = orthonormalize(Rot_trans);
% 
% qhat = RotMat2Quaternion(Rot_trans);

P_rot = 1/norm(cross(b1,b2))^2*((b2*bx.'-(a1/lam)*bx*b2.')*Phat(1:3,1:3)*(b2*bx.'-(a1/lam)*bx*b2.').' + ...
        (b1*bx.'-(a2/lam)*bx*b1.')*Phat(4:6,4:6)*(b1*bx.'-(a2/lam)*bx*b1.').');
[phi, theta, psi] = Quaternion2Euler(qhat);
P_q  = EulerCov2QuaternionCov([phi, theta, psi]',P_rot);
% ++++++++++++
% Output
% ++++++++++++

xhat_out = xhat;
Phat_out = Phat;
qhat_out = qhat;
end



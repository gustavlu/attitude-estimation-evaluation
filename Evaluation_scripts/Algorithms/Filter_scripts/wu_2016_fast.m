function [ xhat_out] = wu_2016_fast(EKF, xhat_in, inputs,  measurements, dt, gain_wu, corr_status, b_norm_mag)
%MARTIN_2017_SENSOR Summary of this function goes here
%   Detailed explanation goes here

ab = measurements(1:3)/norm(measurements(1:3));
mb = measurements(4:6)/norm(measurements(4:6));


gam_a = gain_wu(1);
gam_m = gain_wu(2);
w = gain_wu(3);

qhat = xhat_in(1:4);
bhat = xhat_in(5:7);
omega = inputs(1:3);

om_x = [0 -omega'; 
        omega, -skew(omega)];
W_a = [ab(3), ab(2), -ab(1), 0;
       ab(2), -ab(3), 0, ab(1);
       -ab(1), 0, -ab(3), ab(2);
       0, ab(1), ab(2), ab(3)];

% Predicted quaternion corrected for drift w. accelerometer
qest_aw = (eye(4)+(1-gam_a)*dt/2*om_x + gam_a*(W_a-eye(4))/2)*qhat;
qest_aw = qest_aw/norm(qest_aw);

% Calculate mag correction quaternion
gb = Quaternion2RotMat(qest_aw)'*(EKF.gvec/norm(EKF.gvec)); % Body frame gravity

b1 = gb;
r1 = (EKF.gvec/norm(EKF.gvec));
b2 = mb;
r2 = EKF.mag_ref/norm(EKF.mag_ref);
r_x = cross(r1,r2)/norm(cross(r1,r2));
b_x = cross(b1,b2)/norm(cross(b1,b2));

rho = (1+b_x'*r_x)*(w*b1'*r1+(1-w)*b2'*r2) + cross(b_x,r_x)'*(cross(w*b1,r1)+cross((1-w)*b2,r2));
sig = (b_x+r_x)'*(cross(w*b1,r1)+cross((1-w)*b2,r2));
zet = sqrt(rho^2+sig^2);

G = 1/(2*sqrt(zet*(zet+abs(rho))*(1+b_x'*r_x)));
if rho >= 0
    q_gm = G*[(rho+zet)*(1+b_x'*r_x);
              (zet+rho)*cross(b_x,r_x)+sig*(b_x+r_x)];
else
    q_gm = G*[sig*(1+b_x'*r_x);
              sig*cross(b_x,r_x)+(zet-rho)*(b_x+r_x)];
end

if b_norm_mag || corr_status(2) < 0.5
    qhat = qest_aw;
else
    qhat = (1-gam_m)*qest_aw + gam_m*q_gm;
    qhat = qhat/norm(qhat);
end

xhat_out = [qhat; bhat];
end


function [ xhat_out] = hua_2011_decoupled_global(EKF, xhat_in, inputs,  measurements, dt, gain, corr_status)
%MARTIN_2017_SENSOR Summary of this function goes here
%   Detailed explanation goes here

ka = gain(1);
km = gain(2);
kp = gain(3);
ki = gain(4);
kb = gain(5);

if corr_status(1) < 1
    ka = 0;
end
if corr_status(2) < 1
    km = 0;
end

qhat = xhat_in(1:4);
bhat = xhat_in(5:7);


Rot = qua2chr(xhat_in(1:4));

innov_dec  = cross(ka*measurements(1:3), Rot'*EKF.gvec) ...
    + ka*km*(cross(measurements(4:6), Rot'*EKF.mag_ref)'*measurements(1:3))*measurements(1:3); 
omegahat = inputs(1:3) - bhat + kp*innov_dec;
Om_x = [0 -omegahat'; omegahat, -skew(omegahat)];

% State propagation
qhat = (cos(dt/2*norm(omegahat))*eye(4) + dt/2*sinc(dt/2*norm(omegahat))*Om_x)*qhat;
bhat = bhat + dt*(kb*(sat(bhat, -EKF.bsat.bmax, EKF.bsat.bmax)-bhat) - ki*innov_dec);

xhat_out = [qhat; bhat];
end


function [ xhat_out ] = valenti_2016_fast(EKF, inputs, measurements, xhat_in, dt )
%DECKF_ Summary of this function goes here
%   Detailed explanation goes here

nx = numel(xhat_in);

% Correction gains
dt_gain_mag = EKF.partq.dt_gain_mag;
dt_gain_acc = EKF.partq.dt_gain_acc;
kpsi = 1-exp(-1*dt_gain_mag/EKF.partq.taupsi); % Positive correction gain [0, 1];
kalpha = 1-exp(-1*dt_gain_acc/EKF.partq.taualpha); % Positive correction gain [0, 1];

% State definition
qhat = xhat_in(1:4);
bhat = xhat_in(5:7);



% Measurements
accm = measurements(1:3);
magm = measurements(4:6);

% Corrected omega
omegahat = inputs(1:3) - bhat;



% ++++++++++++++++
% Prediction step
% ++++++++++++++++
Omega_x = [0,       -omegahat';
           omegahat, -skew(omegahat)];

% State prediction
qhat = qhat + 0.5*Omega_x*qhat*dt; % prediction of quaternion
qhat = qhat/norm(qhat); % Normalization of predicted quaternion

xhat = [qhat; bhat];

% Rotation matrix from previous quaternion
Rot = Quaternion2RotMat(qhat); % Rotation matrix from quaternion

% +++++++++++++++++++++++++++++++++++++++++++++
% State correction
% +++++++++++++++++++++++++++++++++++++++++++++
% disp('"""""""""""""""""""""""""""""""""')
q_0 = [1;0;0;0];
eps_thr = 0.9;
% kalpha = 1;
% #### Quaternion from accelerometer ####
% 1. Acceleration measurement in inertial frame
accm_n = accm/norm(accm);

a_b = accm_n;
a_i = Rot*accm_n;
% 2. Inclination quaternion
deltaq_acc = [sqrt((a_i(3)+1)/2); 
               -a_i(2)/sqrt(2*(a_i(3)+1)); 
               a_i(1)/sqrt(2*(a_i(3)+1));
               0];

%            eulerhat = qua2eulSL(deltaq_acc)'
% 3. Angle between identity (zero-error) and the inclination quaternion (inclination error)           
omega_angle = acos(q_0'*deltaq_acc);
% If inclination error is small, use LERP, otherwise SLERP
if deltaq_acc(1) > eps_thr; % LERP
    deltaq_alpha_bar = (1-kalpha)*q_0 + kalpha*deltaq_acc;
    deltaq_alpha_hat = deltaq_alpha_bar/norm(deltaq_alpha_bar);
else % SLERP
    deltaq_alpha_hat = (sin((1-kalpha)*omega_angle)/sin(omega_angle))*q_0...
        + (sin(kalpha*omega_angle)/sin(omega_angle))*deltaq_acc;
end

corr_dq_alpha = [deltaq_alpha_hat(1);-deltaq_alpha_hat(2:4)];
% corr_dq_alpha = deltaq_alpha_hat;
qhat = QuaternionProduct(qhat,corr_dq_alpha);
% eulerhat = qua2eulSL(qhat)'
xhat = [qhat; bhat];

Rot = Quaternion2RotMat(qhat);
% #### Quaternion from magnetometer ####
% 1. Magentometer measurement in inertial frame
magm_n = magm/norm(magm);
m_i = Rot*magm_n;
% 2. Quaternion from magnetometer measurement
Gamma = m_i(1)^2+m_i(2)^2;
deltaq_mag = [sqrt(Gamma + m_i(1)*sqrt(Gamma))/sqrt(2*Gamma);
                0;
                0;
                m_i(2)/sqrt(2*(Gamma + m_i(1)*sqrt(Gamma)))];
omega_angle = acos(q_0'*deltaq_mag);
% If heading error is small, use LERP, otherwise SLERP
if deltaq_mag(1) > eps_thr; % LERP
    deltaq_psi_bar = (1-kpsi)*q_0 + kpsi*deltaq_mag;
    deltaq_psi_hat = deltaq_psi_bar/norm(deltaq_psi_bar);
else % SLERP
    deltaq_psi_hat = (sin((1-kpsi)*omega_angle)/sin(omega_angle))*q_0...
        + (sin(kpsi*omega_angle)/sin(omega_angle))*deltaq_mag;
end

corr_dq_psi = [deltaq_psi_hat(1);-deltaq_psi_hat(2:4)];
% corr_dq_psi = deltaq_psi_hat;
qhat = QuaternionProduct(qhat,corr_dq_psi);
% eulerhat = qua2eulSL(qhat)'
xhat = [qhat; bhat];


% ++++++++++++
% Output
% ++++++++++++

xhat_out = xhat;
end



function [ qhat, xhat_out, Phat_out] = martin_2017_sensor(xhat_in, Phat_in, omega,  accm, magm, dt, gain, cov_in)
%MARTIN_2017_SENSOR Summary of this function goes here
%   Detailed explanation goes here

ka = gain(1);
km = gain(2);
la = gain(3);
lm = gain(4);

ahat = xhat_in(1:3);
mhat = xhat_in(4:6);
bhat = xhat_in(7:9);

omegahat = omega - bhat;



% Complementary covariance propagation
dxdx = [eye(3)-skew(omegahat) - eye(3)*ka, zeros(3), -skew(ahat);
        zeros(3), eye(3)-skew(omegahat) - eye(3)*ka, -skew(mhat);
        -la*skew(accm), -lm*skew(magm), eye(3)];
dxdm = [eye(3)*ka, zeros(3), skew(ahat);
        zeros(3), eye(3)*km, skew(mhat);
        la*skew(ahat), lm*skew(mhat), zeros(3)];


% State propagation
ahat = ahat + (cross(ahat,(omegahat)) - ka*(ahat - accm))*dt;
mhat = mhat + (cross(mhat,(omegahat)) - ka*(mhat - magm))*dt;
bhat = bhat + (la*cross(xhat_in(1:3),accm) + lm*cross(xhat_in(4:6),magm))*dt;

% R_i = [EKF.gvec/norm(EKF.gvec), ...
%        cross(EKF.gvec,EKF.mag_ref)/norm(cross(EKF.gvec,EKF.mag_ref)),...
%        cross(EKF.gvec, cross(EKF.gvec, EKF.mag_ref))/norm(cross(EKF.gvec, cross(EKF.gvec, EKF.mag_ref)))];
%          
% Rot_trans = [alphahat/norm(EKF.gvec), ...
%              cross(alphahat,betahat)/norm(cross(EKF.gvec,EKF.mag_ref)),...
%              cross(alphahat, cross(alphahat, betahat))/norm(cross(EKF.gvec, cross(EKF.gvec, EKF.mag_ref)))]*R_i';
% 
% Rot_trans = orthonormalize(Rot_trans);
% 
% qhat = chr2qua(Rot_trans);
qhat = [1;0; 0: 0];

xhat_out = [ahat; mhat; bhat];

Phat_out = eye(9);


% Phat_out = dxdx*Phat_in*dxdx' + dxdm*blkdiag(cov_in.acc, cov_in.mag, cov_in.gyro)*dxdm';
% if trace(Phat_out) > trace(Phat_in)
%     Phat_out = Phat_in;
% end
end


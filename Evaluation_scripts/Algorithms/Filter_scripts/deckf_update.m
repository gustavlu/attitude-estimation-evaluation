function [tmp, xhat_out,Phat_out ] = deckf_update(EKF, inputs, measurements, xhat_in,Phat_in, dt, corr_status)
%DECKF_ Summary of this function goes here
%   Detailed explanation goes here

nx = numel(xhat_in);
gvec = EKF.gvec;
mag_ref = EKF.mag_ref;

bmax = EKF.bsat.bmax;

% State definition
qhat = xhat_in(1:4);
bhat = xhat_in(5:7);

% Quaternion elements
s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
% Rotation matrix from previous quaternion
Rot = Quaternion2RotMat(qhat); % Rotation matrix from quaternion

% State covariance
Phat = Phat_in;

% Measurements
accm = measurements(1:3);
magm = measurements(4:6);

% Corrected omega
omegahat = inputs(1:3) - bhat;



% ++++++++++++++++
% Prediction step
% ++++++++++++++++
% State prediction
% Quaternion
Omega_mat = [0,       -omegahat';
             omegahat, -skew(omegahat)];
qdot = 0.5*Omega_mat*qhat;
qhat = qhat + qdot*dt; % prediction of quaternion
qhat = qhat/norm(qhat); % normalization of predicted quaternion

% Gyro bias
bhat = bhat - EKF.bsat.kaw_sto*(bhat - sat(bhat, -bmax, bmax)); % Nonlinear prediction

% State concatenation
xhat = [qhat; bhat];

% State covariance
% Jacobian
dfqdq = 0.5*Omega_mat*dt + eye(4);

dfqdbw = 0.5*[-x -y -z;
    s -z y;
    z s -x;
    -y -x s] * dt;

Qbw = EKF.bsat.Qd;

dfbwdbw = eye(3)-EKF.bsat.kaw_sto*eye(3)*(eye(3)- diag([erf(bmax/(sqrt(2*Phat(5,5)))),...
    erf(bmax/(sqrt(2*Phat(6,6)))),...
    erf(bmax/(sqrt(2*Phat(7,7))))]));

Fk = [dfqdq, -dfqdbw, zeros(4,nx-7);     % q
    zeros(3,nx-3), dfbwdbw, zeros(3,nx-7)];           % bw


dfqdw = 0.5*[0 -x -y -z;
    0 s -z y;
    0 z s -x;
    0 -y -x s]*dt;
Vkq = diag([0; EKF.cov.gyro]);

Qq = dt^2*dfqdw*Vkq*dfqdw';

Qk = [Qq,         zeros(4,3);   % q
    zeros(3,4), dt*Qbw]; % bw

Phat = Fk*Phat*Fk' + Qk;

% +++++++++++++++++++++++++++++++++++++++++++++
% Covariance correction due to normalization
% +++++++++++++++++++++++++++++++++++++++++++++
% z = q'q - 1;
s = xhat(1); x = xhat(2); y = xhat(3); z = xhat(4);
Rkq = 1e-5;
Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
Skq = Hkq*Phat*Hkq'+Rkq;
Kkq = Phat*Hkq'*(Skq\eye(1));
Phat = (eye(nx)-Kkq*Hkq)*Phat*(eye(nx)-Kkq*Hkq)' + Kkq*Rkq*Kkq';

% +++++++++++++++++++++++++++++++++++++++++++++
% State correction
% +++++++++++++++++++++++++++++++++++++++++++++
if corr_status(1) > 0
    % #### Correction by accelerometer ####
    nu =  accm - Rot'*gvec;
    [~,Ha] = ekfoutlin(qhat,gvec);
    Ha = [Ha zeros(3,nx-4)];
    Sa_q = Ha*Phat*Ha';
    Sa_am = EKF.Ra;
    Sa = Sa_q + Sa_am;
    
    Ka = Phat*Ha'*(Sa\eye(3));   % Kalman gain
    l_nu = nu'/Sa*nu;
%     if l_nu > EKF.stats.l_det_3_3sig
%         nu = zeros(3,1);
%         Ka = zeros(7,3);
%     end
    % State
    xhat = xhat + (Ka*nu);
    % Covariance
    Phat = (eye(nx)-Ka*Ha)*Phat;
    % Normalization
    xhat(1:4) = xhat(1:4)./norm(xhat(1:4));
    
    % -----------------------------------------------------
    % #### Covariance correction due to normalization ####
    % z = q'q - 1;
    % Quaternion elements
    s = xhat(1); x = xhat(2); y = xhat(3); z = xhat(4);
    Rkq = 1e-5;
    Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
    Skq = Hkq*Phat*Hkq'+Rkq;
    Kkq = Phat*Hkq'*(Skq\eye(1));
    Phat = (eye(nx)-Kkq*Hkq)*Phat*(eye(nx)-Kkq*Hkq)' + Kkq*Rkq*Kkq';
    % -----------------------------------------------------
end
tmp = zeros(4,1);
% #### Correction by magnetometer ####
if corr_status(2) > 0
%     mag_meas_vec = cross(accm,magm);
    mag_meas_vec = cross(Rot'*gvec,magm);
    mag_ref_vec = Rot'*cross(gvec,mag_ref);
    
    % nu = [1 0 0; 0 1 0]*(mag_meas_vec - mag_ref_vec); % Innovation
    nu = (mag_meas_vec - mag_ref_vec); % Innovation
    
    [~, dnumdq] = ekfoutlin(xhat(1:4),cross(gvec,mag_ref)); % Jacobian
    
    dnumdmm = skew(measurements(1:3));
    dnumdam = skew(-measurements(4:6));
    
    Hm = [dnumdq zeros(3,nx-4)];
    % Hm = Hm(1:2,:);
    Rm = dnumdmm*EKF.Rm*dnumdmm' + dnumdam*EKF.Ra*dnumdam';
    % Rm = Rm(1:2,1:2);
    Sm = (Hm*Phat*Hm'+Rm);
    Km = Phat*Hm'/Sm;   % Kalman gain
    
    l_nu = nu'/Sm*nu;
%     if l_nu > EKF.stats.l_det_3_3sig
%         nu = zeros(3,1);
%         Km = zeros(7,3);
%     end
    tmp = [eye(4,7)]*(Km*nu);
    % State and covariance update
    xhat = xhat + (Km*nu); % Correction without decoupling
    Phat =  (eye(nx)-Km*Hm)*Phat*(eye(nx)-Km*Hm)' + Km*Rm*Km'; % Joseph form
    
    % Normalization
    xhat(1:4) = xhat(1:4)./norm(xhat(1:4));
    
    % -----------------------------------------------------
    % #### Covariance correction due to normalization ####
    % z = q'q - 1;
    % Quaternion elements
    s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
    Rkq = 1e-5;
    Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
    Skq = Hkq*Phat*Hkq'+Rkq;
    Kkq = Phat*Hkq'*(Skq\eye(1));
    Phat = (eye(nx)-Kkq*Hkq)*Phat*(eye(nx)-Kkq*Hkq)' + Kkq*Rkq*Kkq';
    % -----------------------------------------------------
end
% ++++++++++++
% Output
% ++++++++++++

xhat_out = xhat;
Phat_out = Phat;
end



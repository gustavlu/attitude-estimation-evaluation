function [ Phat_out, xhat_out, Knorm ] = invariant_mekf( Phat_in, xhat_in,...
    inputs, measurements, corr_status, EKF, dt, ext_bias )
%INVARIANT_MEKF Summary of this function goes here
%   Detailed explanation goes here

iekf_type = 1; % 0: LIEKF, 1: RIEKF (Bonnabel, Martin, Sala�n 2009)

% Init
gvec = EKF.gvec;
mag_ref = EKF.mag_ref;
e3 = [0;0;1];

bmax = EKF.bsat.bmax;

% State
qhat = xhat_in(1:4);
bhat = xhat_in(5:7);

% qhat_inv = [qhat(1); -qhat(2:4)];

% Input + measurements
omega = inputs(1:3);
accm = measurements(1:3);
magm = measurements(4:6);

% Ra = EKF.Ra*100;
% Rm = EKF.Rm*100;

Ra = EKF.Ra*EKF.kcov;
Rm = EKF.Rm*EKF.kcov_mag;

% omegahat = omega - bhat;
omegahat = omega - ext_bias;

% Input covariance
Qb = EKF.bsat.Qd;
Qw = diag(EKF.cov.gyro);

Rtot = dt*blkdiag(Ra,Rm);
Qtot = dt*blkdiag(Qw, Qb);

% State covariance
% Pak = Phat_in(1:3,1:3);
% Pck = Phat_in(1:3,4:6);
% Pbk = Phat_in(4:6,4:6);

Phat = Phat_in;
ddqda = [zeros(1,3); eye(3)*0.5];
Pq = ddqda*Phat(1:3,1:3)*ddqda';

% Rotation matrix
Rot = qua2chrSL(qhat);
% Transformation matrix for decoupling
T = eye(2,3);
% T = [1 0 0; 0 1 0; 0 0 0];
% T = eye(3);

ind_var = 3+size(T,1);

% Measurement decoupling
% Estimated gravity
mag_ref = cross(gvec,mag_ref);
magm = cross(Rot'*gvec,magm);
% [~,drote3dq] = ekfoutlin(qhat,e3);
% Rm = T*(skew(Rot'*e3)*Rm*skew(Rot'*e3)' + skew(-magm)*drote3dq*Pq*drote3dq'*skew(-magm)')*T';
Rm = T*Rm*T';
% Mesaured gravity ["Martin decoupling"]
% mag_ref = cross(gvec,mag_ref);
% magm = cross(accm,magm); 
% Rm = skew(accm)*Rm*skew(accm)'+ skew(-magm)*Ra*skew(-magm)';

Rtot = dt*blkdiag(Ra,Rm);

% Equivalent stochastic gain
dfbwdbw = -1/EKF.bsat.tau*eye(3)*(eye(3)- diag([erf(bmax/(sqrt(2*Phat(4,4)))),...
            erf(bmax/(sqrt(2*Phat(5,5)))),...
            erf(bmax/(sqrt(2*Phat(6,6))))]));

% Propagation of filter

if iekf_type == 0 % LIEKF update
    
    % Invariants LIEKF
    I_w = omegahat;
    I_a = Rot'*gvec;
    I_m = Rot'*mag_ref;
    
    A = [-skew(I_w), -eye(3);
        zeros(3), dfbwdbw];
    
    B = blkdiag(eye(3), eye(3));
    C = [zeros(ind_var,6)]; % initialised measurement matrix
    E = zeros(ind_var,1);
    
    % LIEKF
    if corr_status(1) > 0 %&& corr_status(2) < 1 % Acc only
        C(1:3,1:3) = skew(I_a);
        E(1:3) = Rot'*gvec - accm; % Error in body frame
    end
    
    if corr_status(2) > 0 %&& corr_status(1) < 1 % Mag only
        C(4:ind_var,1:3) = T*(Rot*skew(I_m));
        E(4:ind_var) = T*Rot*(Rot'*mag_ref - magm); % Error in body frame
    end
    
    % Gain
    K = -Phat*C'/Rtot;
    K = sat(K, -15, 15);
    Kq = K(1:3,:);
    Kb = K(4:6,:);
    
    % Quaternion update
    om_tmp = omegahat + Kq*E;
    Omx = [0, -om_tmp'; om_tmp, -skew(om_tmp)];
    qhatdot = 0.5*Omx*qhat;
    qhat = qhat + qhatdot*dt;
    qhat = qhat/norm(qhat);
    
    % Bias update
    bhat = bhat + dt*(-1/EKF.bsat.tau*(bhat - sat(bhat, -bmax, bmax)) + Kb*E); % Saturated bias saturation
    % bhat = bhat + dt*(Rot'*Kb*E); % Normal bias propagation
else    % RIEKF update
    
    A = [zeros(3), -eye(3); zeros(3), skew(Rot*omegahat) + dfbwdbw];
    C = zeros(ind_var,6);
    E = zeros(ind_var,1);
    
    if corr_status(1) > 0 %&& corr_status(2) < 1 % Acc only
        C(1:3,1:3) = skew(gvec);
        E(1:3) = gvec - Rot*accm; % Error in inertial frame
    end
    
    if corr_status(2) > 0 %&& corr_status(1) < 1 % Mag only
        C(4:ind_var,1:3) = T*(skew(mag_ref));
        E(4:ind_var) = T*(mag_ref - Rot*magm); % Error in inertial frame
    end

    % Gain
    K = -Phat*C'/Rtot;
    K = sat(K, -15, 15);
    Kq = K(1:3,:); %Kq(1:2,4:6) = 0; Kq(3,1:3) = 0;
    Kb = K(4:6,:); %Kb(1:2,4:6) = 0; Kb(3,1:3) = 0;
    
   
    % Quaternion update
    om_tmp = omegahat + Rot'*Kq*E + (1-norm(qhat)^2);
    Omx = [0, -om_tmp'; om_tmp, -skew(om_tmp)];
    qhatdot = 0.5*Omx*qhat;
    qhat = qhat + qhatdot*dt;
    qhat = qhat/norm(qhat);
    
    % Bias update
    bhat = bhat + dt*(-1/EKF.bsat.tau*(bhat - sat(bhat, -bmax, bmax)) + Rot'*Kb*E); % Saturated bias saturation
    % bhat = bhat + dt*(Rot'*Kb*E); % Normal bias propagation
    
end

xhat_out = [qhat; bhat];

% Covariance update
Pdot = A*Phat + Phat*A' + Qtot - Phat*C'/Rtot*C*Phat;
Phat = Phat + Pdot*dt;
Phat = 0.5*(Phat + Phat');
Phat = diag(diag(sign(Phat)))*Phat; % Force positive diaginal elements

Phat_out = Phat;

Knorm = diag(K*K');
% Knorm = norm(K);
end


function [ Phat_out, xhat_out, Knorm ] = mekf( Phat_in, xhat_in, inputs,...
    measurements, corr_status, EKF, dt, ext_bias )
%INVARIANT_MEKF Summary of this function goes here
%   Detailed explanation goes here

% Init
gvec = EKF.gvec;
mag_ref = EKF.mag_ref;
e3 = [0;0;1];

bmax = EKF.bsat.bmax;

% State
qhat = xhat_in(1:4);
bhat = xhat_in(5:7);

% qhat_inv = [qhat(1); -qhat(2:4)];

% Input + measurements
omega = inputs(1:3);
accm = measurements(1:3);
magm = measurements(4:6);

Ra = EKF.Ra;
Rm = EKF.Rm;

% omegahat = omega - bhat;
omegahat = omega - ext_bias;

% Input covariance
Qb = EKF.bsat.Qd;
Qw = diag(EKF.cov.gyro);

% State covariance
% Pak = Phat_in(1:3,1:3);
% Pck = Phat_in(1:3,4:6);
% Pbk = Phat_in(4:6,4:6);

Rtot = dt*blkdiag(Ra,Rm);
Qtot = dt*blkdiag(Qw, Qb);

Phat = Phat_in;
sq = qhat(1);
vq = qhat(2:4);

% Rotation matrix
Rot = Quaternion2RotMat(qhat);

% Measurement decoupling
% mag_ref = cross(gvec,mag_ref);
% magm = cross(accm,magm);
% Rm = skew(accm)*Ra*skew(accm)' + skew(-magm)*Rm*skew(-magm)';
% Rm = Ra + Rm;

% mag_ref = cross(e3,mag_ref);
% magm = cross(Rot'*e3,magm);

% mag_ref = cross(gvec,mag_ref);
% magm = cross(accm,magm);

% ========================================================================
% MEKF implementation by Qin
% 
% % Prediction
% Tq = [-vq'; sq*eye(3)+skew(vq)];
% 
% F = [-omegahat, -eye(3); zeros(3), zeros(3)];
G = [-eye(3), zeros(3); zeros(3), eye(3)];
% 
% qhatdot = 0.5*Tq*omegahat;
% Phatdot = F*Phat + P*F' + G*Qtot*G';
% 
% qhat = qhat + qhatdot*dt; qhat = qhat/norm(qhat);
% Phat = Phat + Phatdot*dt;

% Correction
C = zeros(6,6);
E = zeros(6,1);

if corr_status(1) > 0 %&& corr_status(2) < 1 % Acc only
    C(1:3,1:3) = skew(Rot'*gvec);
%     C(1:3,1:3) = Rot';
    E(1:3) = Rot'*gvec - accm;
end

if corr_status(2) > 0 %&& corr_status(1) < 1 % Mag only
    C(4:6,1:3) = skew(Rot'*mag_ref);
%     C(4:6,1:3) = Rot';
    E(4:6) = Rot'*mag_ref - magm;
end


% ========================================================================

% Equivalent stochastic gain
dfbwdbw = -1/EKF.bsat.tau*eye(3)*(eye(3)- diag([erf(bmax/(sqrt(2*Phat(4,4)))),...
           erf(bmax/(sqrt(2*Phat(5,5)))),...
           erf(bmax/(sqrt(2*Phat(6,6))))]));

% Linearised error matrices
A = [-skew(omegahat), -eye(3); 
     zeros(3), dfbwdbw];

% if corr_status(1) > 0 && corr_status(2) > 0 % Both meas available 
%     C(1:3,1:3) = skew(gvec);
%     E(1:3) = gvec - Rot*accm;
% end

% Covariance prediction
% Pdot = A*Phat + Phat*A' + G*Qtot*G';
% Phat = Phat + Pdot*dt;
% Phat = 0.5*(Phat + Phat'); % Force symmetry
% Phat = diag(diag(sign(Phat)))*Phat; % Force positive diaginal elements

% Gain 
% K = -Phat*C'/(C*Phat*C' + dt*Rtot);

K = -Phat*C'/(Rtot);
K = sat(K, -15, 15);
Kq = K(1:3,:);
Kb = K(4:6,:);

% Quaternion update
om_tmp = omegahat + Kq*E;
Omx = [0, -om_tmp'; om_tmp, -skew(om_tmp)];
qhatdot = 0.5*Omx*qhat;
qhat = qhat + qhatdot*dt;
qhat = qhat/norm(qhat);

% Bias update
bhat = bhat + dt*(-1/EKF.bsat.tau*(bhat - sat(bhat, -bmax, bmax)) + Kb*E); % Saturated bias saturation
% bhat = bhat + dt*(Rot'*Kb*E); % Normal bias propagation 

xhat_out = [qhat; bhat];

% Covariance update 
Pdot = A*Phat + Phat*A' + G*Qtot*G' - Phat*C'/(Rtot)*C*Phat;
Phat = Phat + Pdot*dt;
Phat = 0.5*(Phat + Phat'); % Force symmetry
%Phat = diag(diag(sign(Phat)))*Phat; % Force positive diaginal elements

for k = 1:6 % Force positive diagonal elements
    Phat(k,k) = abs(Phat(k,k));
end

Phat_out = Phat;

Knorm = diag(K*K');
% Knorm = norm(K);
end


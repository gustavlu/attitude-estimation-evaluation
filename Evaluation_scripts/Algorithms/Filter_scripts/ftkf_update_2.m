function [tmpstruct2, tmpstruct, tmp, xhat_out,P_out] ...
    = ftkf_update_2(EKF,inputs,measurements,xhat_in,P_in,...
                dt, corr_status, b_nobias, ext_bias)
% Nonlinear Kalman filter estimating rotation and translation.
%
% ---- Outputs :
% nu : innovations [13x1]
% omegahat : estimated body angle rates (p, q, r) [3x1]
% qatt_out : rotation quaternion (s, x, y, z) [4x1]
% Rot : Rotational matrix R(qatt_out) [3x3]
% euler : Euler angles (phi, theta, psi) [3x1]
% xhat_out : state output
% P_out : state covariance output
%
% ---- State: [qatt; bw; V; P; ba]
% qatt : rotation quaternion [s x y z]'
% bw : gyro bias [bx]
%
% ---- Dynamic model :
% q_dot = 0.5*Omega(w-bw)q
% bw_dot = -1/tau*(bw-sat(bw))
%
% Integration method : Euler forward
%
% ---- Measurements:
% Accelerometer: acc_m [3x1]
% Magnetometer : mag_m [3x1]
%
% Sequential treatment of measurements

tmp = zeros(20,1);
% tmpstruct2 = zeros(10,1);

nx = numel(xhat_in);
g = 9.81;
gvec = EKF.gvec;%[0;0;-g];
e3 = [0;0;1];
mag_ref = EKF.mag_ref;

bmax = EKF.bsat.bmax;

% State definition
qhat = xhat_in(1:4);
bwhat = xhat_in(5:7);

% Quaternion elements
s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);

% State covariance
Phat = P_in;

% Input
omega_m = inputs(1:3);

% Measurements
accm = measurements(1:3);
magm = measurements(4:6);

P_acchat = EKF.Ra*EKF.kcov;
P_maghat = EKF.Rm*EKF.kcov_mag;

% P_acchat = EKF.Ra;
% P_maghat = EKF.Rm;

% Corrected omega
% omegahat = omega_m - bwhat;
omegahat = omega_m - ext_bias;


% Heading correction gain
dt_gain_mag = EKF.partq.dt_gain_mag;
dt_gain_acc = EKF.partq.dt_gain_acc;
kpsi = 1-exp(-1*dt_gain_mag/EKF.partq.taupsi); % Positive correction gain [0, 1];
kpsi_bias = 1-exp(-1*dt_gain_mag/(EKF.partq.taupsi_bias)); % Positive correction gain [0, 1];
kalpha = 1-exp(-1*dt_gain_acc/EKF.partq.taualpha); % Positive correction gain [0, 1];
kalpha_bias = 1-exp(-1*dt_gain_acc/(EKF.partq.taualpha_bias)); % Positive correction gain [0, 1];

if b_nobias > 0.5
    kpsi_bias = 0;
    kalpha_bias = 0;
end% ++++++++++++++++
% Prediction step
% ++++++++++++++++
% State prediction
% Quaternion
Omega_mat = [0,       -omegahat';
             omegahat, -skew(omegahat)];
qdot = 0.5*Omega_mat*qhat;
qhat = qhat + qdot*dt; % prediction of quaternion
qhat = qhat/norm(qhat); % normalization of predicted quaternion

% Gyro bias
bwhat = bwhat - EKF.bsat.kaw_sto*(bwhat - sat(bwhat, -bmax, bmax)); % Nonlinear prediction

% State concatenation
xhat = [qhat; bwhat];

% State covariance
% Jacobian
dfqdq = 0.5*Omega_mat*dt + eye(4);

dfqdbw = 0.5*[-x -y -z;
               s -z y;
               z s -x;
              -y -x s] * dt;

Qbw = EKF.bsat.Qd;

dfbwdbw = eye(3)-EKF.bsat.kaw_sto*eye(3)*(eye(3)- diag([erf(bmax/(sqrt(2*P_in(5,5)))),...
    erf(bmax/(sqrt(2*P_in(6,6)))),...
    erf(bmax/(sqrt(2*P_in(7,7))))]));

Fk = [dfqdq, -dfqdbw, zeros(4,nx-7);     % q
    zeros(3,nx-3), dfbwdbw, zeros(3,nx-7)];           % bw


dfqdw = 0.5*[0 -x -y -z;
    0 s -z y;
    0 z s -x;
    0 -y -x s]*dt;
Vkq = diag([0; EKF.cov.gyro]);

Qq = dfqdw*Vkq*dfqdw';

Qk = [Qq,         zeros(4,3);   % q
    zeros(3,4), Qbw]; % bw

Phat = Fk*Phat*Fk' + Qk;


% Quaternion elements
s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
% Rotation matrix from predicted quaternion
Rot = Quaternion2RotMat(qhat); % Rotation matrix from quaternion

% +++++++++++++++++++++++++++++++++++++++++++++
% Covariance correction due to normalization
% +++++++++++++++++++++++++++++++++++++++++++++

% h = q'q = 1;
Rkq = 1e-5;
Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
Skq = Hkq*Phat*Hkq'+Rkq;
Kkq = Phat*Hkq'*(Skq\eye(1));
Phat = (eye(nx)-Kkq*Hkq)*Phat;

% ################################
% #### Inclination correction ####
% ################################
if corr_status(1) > 0
    % #### Correction by partial inclination quaternion correction
    a_dir = accm/norm(accm);
    
    A = [e3'; a_dir'];
    umeas = [1;0;0];
    na = null(A);
    if size(na,2) > 1   % Choose randomly the inclination axis in case of multiple solutions
        randbit = rand>0.5;
        if randbit
            umeas(1:3) = na(:,1);
        else
            umeas(1:3) = na(:,2);
        end
    else
        umeas(1:3) = na(:,1);
    end

    uhat = umeas;
    uhat = uhat/norm(uhat); % u^T.u = 1
    ux = skew(uhat);
    
    % Calculate R_alpha_u - verticality rotation matrix
    calpha = e3'*Rot'*e3;
    salpha = -uhat'*(cross(e3,Rot'*e3));
    Rot_alpha_u_hat = eye(3)+salpha*ux+(1-calpha)*ux^2;
    
    if EKF.gvec(3) < 0
        ym = Rot_alpha_u_hat*a_dir;
    else
        ym = -Rot_alpha_u_hat*a_dir;
    end
    
    calpha_ym = -e3'*ym;
    salpha_ym = uhat'*cross(e3,ym);
    
    % Calculate attitude error angle dalpha
    dalpha = atan2(salpha_ym,calpha_ym);
    Delta_alpha = kalpha*dalpha/2;
    
    delta_q = [cos(Delta_alpha); uhat*sin(Delta_alpha)];
    % State correction
    
    % Bias correction
    zeta = 0.7;
    kb = - (kalpha/(2*zeta))^2;
%     kb = - kalpha_bias;
    
    kp = kb*uhat(1); % Gain for roll speed bias correction
    kq = kb*uhat(2); % Gain for pitch speed bias correction
    
    bp = xhat(5) + kp*dalpha;
    bq = xhat(6) + kq*dalpha;
    
    
    [ dqcdq, dqcdam, ddalphadq, ddalphadam , ~, ~ , ~] =...
        ekflin_qcorr_inclination_2( kalpha, qhat, uhat, accm, ym, Delta_alpha);
%     if max(max(isnan(dqcdq))) > 0
%         dqcdq = eye(4);
%         dqcdam = zeros(4,3);
%     end
    
    dbpdq = uhat(1)*kb*ddalphadq;
    dbqdq = uhat(2)*kb*ddalphadq;
    dbpdam = uhat(1)*kb*ddalphadam;
    dbqdam = uhat(2)*kb*ddalphadam;
    
    ImKH = [dqcdq, zeros(4,3); [dbpdq; dbqdq; zeros(1,4)], [1 0 0; 0 1 0; 0 0 1]];
    K = [dqcdam; dbpdam; dbqdam; zeros(1,3)];
    
    tmp(1) = trace(ImKH*ImKH');
    tmp(2) = trace(K*K');
    tmp(5) = uhat(1);
    tmp(6) = uhat(2);
    tmp(7) = dalpha;
% if corr_status(1) > 0
    qhat = QuaternionProduct(qhat,delta_q);
    %qhat = QuaternionProduct(qhat,delta_q_perp);
    xhat = [qhat; bp; bq; xhat(7)];
%     Phat = ImKH*Phat*ImKH' + K*P_acchat*K';



% else
%     xhat = [xhat(1:4); bp; bq; xhat(7)];
    
%     bcorr = xhat(5:7) - [kb; kb; 0].*cross(Rot'*gvec,accm);
%     xhat = [qhat; bcorr];
    % Correct covariance only if it does not increase
    p_trace_pre = trace(Phat); % Predicted covariance trace
    P_hat_pre = Phat; % Predicted covariance
    
    % ==== BEGIN Covariance Hack ====
% %     nu_a = accm - Rot'*gvec;
    [~,Ha] = ekfoutlin(qhat,gvec);
    Ha = [Ha zeros(3,nx-4)];
    Sa_q = Ha*Phat*Ha';
    Ra = P_acchat;
    Sa = Sa_q + P_acchat;
    Ka = Phat*Ha'*(Sa\eye(3));   % Kalman gain
    Ka([4, 7],:) = 0; % Cancel gain for yaw elements
    % Covariance
    Phat = (eye(nx)-Ka*Ha)*Phat*(eye(nx)-Ka*Ha)' + Ka*Ra*Ka';
%     xhat = xhat + Ka*nu_a;
%     xhat(1:4) = xhat(1:4)/norm(xhat(1:4));
    % ==== END Covariance Hack ====
    % Apply ccovariance correction
%     Phat =  ImKH*Phat*ImKH'+K*P_acchat*K'; % Joseph form
    Phat = 1/2*(Phat+Phat');
    
    
    p_trace_corr = trace(Phat); % Corrected covariance trace
    
    % Accept or reject covariance correction
        if p_trace_corr > p_trace_pre
            Phat = P_hat_pre;
        end
end

tmpstruct.dCdq = 0;
tmpstruct.dSdq = 0;
tmpstruct.dCdm = 0;
tmpstruct.dSdm = 0;
tmpstruct.dqcdm1 = 0;
tmpstruct.dqcdm2 = 0;
tmpstruct.dqcdm3 = 0;
tmpstruct.dqcdm4 = 0;

tmpstruct2.q1 = 0; tmpstruct2.q2 = 0; tmpstruct2.q3 = 0; tmpstruct2.q4 = 0;
tmpstruct2.m1 = 0; tmpstruct2.m2 = 0; tmpstruct2.m3 = 0;

Rot = qua2chrSL(xhat(1:4));
% ############################
% #### Heading correction ####
% ############################
if corr_status(2) > 0
    % Direct quaternion correction
    m_i = Rot*magm;
    % Calulate correction quaternion
%     if corr_status(1) > 0
%         y1 = mag_ref.'*Rot*cross(magm,-accm);
%         y2 = cross(mag_ref,-e3)'*Rot*cross(magm,-accm);
%         uhat = e3;
%     else
        y1 = mag_ref.'*cross(m_i,-e3);
        y2 = cross(mag_ref,-e3)'*cross(m_i,-e3);
        uhat = e3;
%     end
    s1 = y1/sqrt(y1^2+y2^2);
    c1 = y2/sqrt(y1^2+y2^2);
    psidiff = atan2(s1,c1);
    Ccap = cos(kpsi*psidiff/2);
    Scap = sin(kpsi*psidiff/2);
%     deltaq_m = [Ccap;0;0;Scap];
    deltaq_m = [Ccap;uhat*Scap];
    
    % Calculate corrected covariance
    
    [tmpstruct, dqcdq, dqcdmm ] =...
        ekflin_deltaq_part_dpsi_2( mag_ref, magm, qhat, Ccap, Scap, kpsi);
    if max(max(isnan(dqcdq))) > 0
        dqcdq = eye(4);
        dqcdmm = zeros(4,3);
    end
    
    % ----------------------------
    % Bias gain calculation
    zeta = 0.7;
    kb = - (kpsi/(2*zeta))^2;
%     kb = -kpsi_bias;
    
    % Calculate corrected covariance - quaternion correction
    [tmpstruct2, dbhatdq, dbhatdmm] = ekflin_bhat_pm( Rot,xhat(1:4),magm,kpsi, kb, EKF.mag_ref);
    ImKH = [dqcdq, zeros(4,3); [zeros(2,4); dbhatdq], eye(3,3)];
    K = [dqcdmm; zeros(2,3); dbhatdmm];
    
%     ImKH = [dqcdq, zeros(4,3); zeros(3,4), eye(3,3)];
%     K = [dqcdmm; zeros(2,3); zeros(1,3)];
    

    tmp(3) = trace(ImKH*ImKH');
    tmp(4) = trace(K*K');
    tmp(6) = psidiff;
    tmp(8) = trace(dqcdmm*dqcdmm');
    tmp(9) = trace(dbhatdmm*dbhatdmm');
    tmp(10) = trace(dqcdq*dqcdq');
    tmp(11) = trace(dbhatdq*dbhatdq');
    
    
    
    % Correct covariance only if it does not increase
    p_trace_pre = trace(Phat); % Predicted covariance trace
    P_hat_pre = Phat; % Predicted covariance
    
    % ==== BEGIN Covariance Hack ====
    [~,Hm] = ekfoutlin(qhat,mag_ref);
    Hm = [Hm zeros(3,nx-4)];
    Rm = P_maghat;
    Sm = Hm*Phat*Hm' + Rm;
    
    Km = Phat*Hm'*(Sm\eye(3));   % Kalman gain
    Km([2, 3, 5, 6],:) = 0; % Cancel gain for yaw elements
    
    % Covariance
    Phat = (eye(nx)-Km*Hm)*Phat*(eye(nx)-Km*Hm)' + Km*Rm*Km';
    
    % ==== END Covariance Hack ====
    
    % Apply ccovariance correction
%     Phat =  ImKH*Phat*ImKH'+K*P_maghat*K'; % Joseph form
    Phat = 1/2*(Phat+Phat');
    
    p_trace_corr = trace(Phat); % Corrected covariance trace
    
    % Accept or reject covariance correction
        if p_trace_corr > p_trace_pre
            Phat = P_hat_pre;
        end
    
    % === Start Covariance HACK
    
    % === End Covariance HACK
    
    % Correct state
    qhat = QuaternionProduct(deltaq_m,qhat);
    %     qhat_2 = QuaternionProduct([deltaq_m(1); -deltaq_m(2:4)],qhat);
    %     if qhat'*qhat_1 > 0
    %         qhat = qhat_1;
    %     else
    %         qhat = qhat_2;
    %     end
    
    bhat_corr = xhat(7) + kb*psidiff;
% if corr_status(2) > 0
    xhat = [qhat; xhat(5:6); bhat_corr];
%     Phat = ImKH*Phat*ImKH' + K*P_maghat*K';
% else
%     xhat = [xhat(1:4); xhat(5:6); bhat_corr];
%     bcorr = xhat(5:7) - [0; 0; kb].*cross(Rot'*mag_ref,magm);
%     xhat = [qhat; bcorr];
end
% ++++++++++++
% Output
% ++++++++++++

xhat_out = xhat;
P_out = Phat;


end
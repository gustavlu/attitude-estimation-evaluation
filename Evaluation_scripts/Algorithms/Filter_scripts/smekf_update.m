function [ xhat_out,Phat_out ] = smekf_update(EKF, inputs, measurements, xhat_in,Phat_in, dt, corr_status)
%DECKF_ Summary of this function goes here
%   Detailed explanation goes here

nx = numel(xhat_in);
gvec = EKF.gvec;
mag_ref = EKF.mag_ref;

bmax = EKF.bsat.bmax;

% State definition
qhat = xhat_in(1:4);
bhat = xhat_in(5:7);

% Quaternion elements
s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
% Rotation matrix from previous quaternion
Rot = Quaternion2RotMat(qhat); % Rotation matrix from quaternion

% State covariance
Phat = Phat_in(2:end, 2:end);

% Measurements
accm = measurements(1:3);
magm = measurements(4:6);

% Corrected omega
omegahat = inputs(1:3) - bhat;



% ++++++++++++++++
% Prediction step
% ++++++++++++++++
% State prediction
% Quaternion
Omega_mat = -skew(omegahat);
xi_qhat = [-qhat(2:4)'; qhat(1)*eye(3)+skew(qhat(2:4))];

qdot = 0.5*xi_qhat*omegahat;
qhat = qhat + qdot*dt; % prediction of quaternion
qhat = qhat/norm(qhat); % normalization of predicted quaternion

% Gyro bias
bhat = bhat - EKF.bsat.kaw_sto*(bhat - sat(bhat, -bmax, bmax)); % Nonlinear prediction

% Modified Rodriguez parameter
% ahat = 4*tmp_dec(2:4)/(tmp_dec(1)+1); % Modified Rodrigues parameter
% ahat = qhat(1:3,k) = qhat(1:3,k) / norm(qhat(1:3,k));

% State concatenation
xhat = [qhat; bhat];

% State covariance
% Jacobian
Qbw = EKF.bsat.Qd*dt;

dfbwdbw = -EKF.bsat.kaw_sto*eye(3)*(eye(3)- diag([erf(bmax/(sqrt(2*Phat(4,4)))),...
            erf(bmax/(sqrt(2*Phat(5,5)))),...
            erf(bmax/(sqrt(2*Phat(6,6))))]));

Fk = [Omega_mat, -eye(3);     % q
      zeros(3,3), dfbwdbw]; % bw


% dfqdw = 0.5*xi_qhat*dt;
% Vkq = diag([0; EKF.cov.gyro]);
% 
% Qq = dfqdw*Vkq*dfqdw';

Qk = [diag([EKF.cov.gyro])*dt, zeros(3,3);   % q
      zeros(3,3), Qbw]; % bw

Gk = [-eye(3), zeros(3); zeros(3), eye(3)];
Phatdot = Fk*Phat*Fk' + Gk*Qk*Gk';
Phat = Phat + Phatdot*dt;
Phat = 0.5*(Phat+Phat');

% % +++++++++++++++++++++++++++++++++++++++++++++
% % Covariance correction due to normalization
% % +++++++++++++++++++++++++++++++++++++++++++++
% % z = q'q - 1;
% s = xhat(1); x = xhat(2); y = xhat(3); z = xhat(4);
% Rkq = 1e-5;
% Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
% Skq = Hkq*Phat*Hkq'+Rkq;
% Kkq = Phat*Hkq'*(Skq\eye(1));
% Phat = (eye(nx)-Kkq*Hkq)*Phat*(eye(nx)-Kkq*Hkq)' + Kkq*Rkq*Kkq';



% +++++++++++++++++++++++++++++++++++++++++++++
% State correction
% +++++++++++++++++++++++++++++++++++++++++++++
if corr_status(1) > 0
    % #### Correction by accelerometer ####
    nu =  accm - Rot'*gvec;
%     [~,H] = ekfoutlin(qhat,gvec);
    Ha = [skew(Rot'*gvec), zeros(3,3)];
    Ra = EKF.Ra;
    Sa = Ha*Phat*Ha' + Ra;
    
    Ka = Phat*Ha'*(Sa\eye(3));   % Kalman gain

    % State
    %xhat = xhat + (Ka*nu);
    
    % Normalization
    % xhat(1:4) = xhat(1:4)./norm(xhat(1:4));

    delta_x = Ka*nu;
    delta_angle = delta_x(1:3);
    delta_bhat = delta_x(4:6);
    
    xi_qhat = [-qhat(2:4)'; qhat(1)*eye(3)+skew(qhat(2:4))];
    qhat = qhat + 0.5*xi_qhat*delta_angle;
    bhat = bhat + delta_bhat;
    
    qhat = qhat/norm(qhat);
    
    % Covariance
    Phat = (eye(6)-Ka*Ha)*Phat;
%     % -----------------------------------------------------
%     % #### Covariance correction due to normalization ####
%     % z = q'q - 1;
%     % Quaternion elements
%     s = xhat(1); x = xhat(2); y = xhat(3); z = xhat(4);
%     Rkq = 1e-5;
%     Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
%     Skq = Hkq*Phat*Hkq'+Rkq;
%     Kkq = Phat*Hkq'*(Skq\eye(1));
%     Phat = (eye(nx)-Kkq*Hkq)*Phat*(eye(nx)-Kkq*Hkq)' + Kkq*Rkq*Kkq';
%     % -----------------------------------------------------
end

% #### Correction by magnetometer ####
if corr_status(1) > 0
    % Quaternion elements
%     s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
%     
%     mag_meas_vec = cross(accm,magm);
% %     mag_meas_vec = cross(Rot'*gvec,magm);
%     mag_ref_vec = Rot'*cross(gvec,mag_ref);
%     
%     % nu = [1 0 0; 0 1 0]*(mag_meas_vec - mag_ref_vec); % Innovation
%     nu = (mag_meas_vec - mag_ref_vec); % Innovation
%     
%     [~, dnumdq] = ekfoutlin(qhat,cross(gvec,mag_ref)); % Jacobian
%     
%     dnumdr3t = [ (-2)*y,    2*z, (-2)*s, 2*x;
%                  2*x,    2*s,    2*z, 2*y;
%                  0, (-4)*x, (-4)*y,   0];
%              
%     dnumdmm = skew(Rot'*gvec);
%     dnumdam = skew(-measurements(4:6));
%     
%     g = norm(gvec);
%     m1 = magm(1); m2 = magm(2); m3 = magm(3);
%     
%     dnudq = [2*g*m3*x           , 2*g*m3*s + 4*g*m2*x, 4*g*m2*y + 2*g*m3*z, 2*g*m3*y;
%              2*g*m3*y           , -4*g*m1*x - 2*g*m3*z, 2*g*m3*s - 4*g*m1*y , (-2)*g*m3*x;
%              -2*g*m1*x - 2*g*m2*y, 2*g*m2*z - 2*g*m1*s, -2*g*m2*s - 2*g*m1*z, 2*g*m2*x - 2*g*m1*y]...
%              + dnumdq;
%          
%     Rq =  Phat_in(1:4,1:4);
%     Rm = dnumdmm*EKF.Rm*dnumdmm' + dnudq*Rq*dnudq';
%     Rm = dnumdmm*EKF.Rm*dnumdmm' + dnumdam*EKF.Ra*dnumdam';
%     
%     Hm = [skew(Rot'*cross(gvec,mag_ref)), zeros(3,3)];
%     
%     Sm = (Hm*Phat*Hm'+Rm);
%     Km = Phat*Hm'*pinv(Sm);   % Kalman gain
% 
%     % State
%     %xhat = xhat + (Ka*nu);
%     
%     % Normalization
%     % xhat(1:4) = xhat(1:4)./norm(xhat(1:4));
% 
%     delta_x = Km*nu;
%     delta_angle = delta_x(1:3);
%     delta_bhat = delta_x(4:6);
    
    nu =  cross(accm,magm) - Rot'*cross(gvec,mag_ref);
%     [~,H] = ekfoutlin(qhat,gvec);
    Ha = [skew(Rot'*cross(gvec,mag_ref)), zeros(3,3)];
    dnumdmm = skew(Rot'*gvec);
    dnumdam = skew(-measurements(4:6));
    Sa = Ha*Phat*Ha' + dnumdam*Ra*dnumdam + dnumdmm*EKF.Rm*dnumdmm';
    
    Ka = Phat*Ha'*pinv(Sa);   % Kalman gain

    % State
    %xhat = xhat + (Ka*nu);
    
    % Normalization
    % xhat(1:4) = xhat(1:4)./norm(xhat(1:4));

    delta_x = Ka*nu;
    delta_angle = delta_x(1:3);
    delta_bhat = delta_x(4:6);
    
    xi_qhat = [-qhat(2:4)'; qhat(1)*eye(3)+skew(qhat(2:4))];
    qhat = qhat + 0.5*xi_qhat*delta_angle;
    bhat = bhat + delta_bhat;
    
    qhat = qhat/norm(qhat);
    
    % Covariance
    Phat = (eye(6)-Ka*Ha)*Phat;
    
    xi_qhat = [-qhat(2:4)'; qhat(1)*eye(3)+skew(qhat(2:4))];
    qhat = qhat + 0.5*xi_qhat*delta_angle;
    bhat = bhat + delta_bhat;
    
    qhat = qhat/norm(qhat);
    
    
    
    % Covariance
    Phat = (eye(6)-Ka*Ha)*Phat;
    
    
%     % State and covariance update
%     xhat = xhat + (Km*nu); % Correction without decoupling
%     Phat =  (eye(nx)-Km*Hm)*Phat*(eye(nx)-Km*Hm)' + Km*Rm*Km'; % Joseph form
%     
%     % Normalization
%     xhat(1:4) = xhat(1:4)./norm(xhat(1:4));
    
    % -----------------------------------------------------
    % #### Covariance correction due to normalization ####
    % z = q'q - 1;
    % Quaternion elements
%     s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
%     Rkq = 1e-5;
%     Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
%     Skq = Hkq*Phat*Hkq'+Rkq;
%     Kkq = Phat*Hkq'*(Skq\eye(1));
%     Phat = (eye(nx)-Kkq*Hkq)*Phat*(eye(nx)-Kkq*Hkq)' + Kkq*Rkq*Kkq';
    % -----------------------------------------------------
end

xhat = [qhat; bhat];
% ++++++++++++
% Output
% ++++++++++++

xhat_out = xhat;
Phat_out = blkdiag(0,Phat);
end



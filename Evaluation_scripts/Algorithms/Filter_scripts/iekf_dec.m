function [ Phat_out, xhat_out, Knorm ] = iekf_dec( Phat_in, xhat_in,...
    inputs, measurements, corr_status, EKF, dt)
%INVARIANT_MEKF Summary of this function goes here
%   Detailed explanation goes here

iekf_type = 0; % 0: LIEKF, 1: RIEKF (Bonnabel, Martin, Sala�n 2009)

% Init
gvec = EKF.gvec;
mag_ref = EKF.mag_ref;
e3 = [0;0;1];

bmax = EKF.bsat.bmax;

% State
qhat = xhat_in(1:4);
bhat = xhat_in(5:7);
arhat = xhat_in(8:10);
mrhat = xhat_in(11:13);

% qhat_inv = [qhat(1); -qhat(2:4)];

% Input + measurements
omega = inputs(1:3);
accm = measurements(1:3);
magm = measurements(4:6);

Ra = EKF.Ra*EKF.kcov;
Rm = EKF.Rm*EKF.kcov_mag;

omegahat = omega - bhat;

% Input covariance
Qb = EKF.bsat.Qd;
Qw = diag(EKF.cov.gyro);
Qa = eye(3)*(norm(gvec)*0.1/3)/sqrt(EKF.accest.tau);
Qm = eye(3)*(norm(mag_ref)*0.1/3)/sqrt(EKF.magest.tau);

Rtot = dt*blkdiag(Ra,Rm);
Qtot = dt*blkdiag(Qw, Qb, Qa, Qm);

% State covariance
% Pak = Phat_in(1:3,1:3);
% Pck = Phat_in(1:3,4:6);
% Pbk = Phat_in(4:6,4:6);

Phat = Phat_in;

% Rotation matrix
Rot = qua2chr(qhat);

% Measurement decoupling
% mag_ref = cross(e3,mag_ref);
% magm = cross(Rot'*e3,magm);
% magm = magm - Rot'*(mrhat-mag_ref); % Corrected mag measurement

mrhat_dec = cross(e3,mrhat);
magm_dec = cross(Rot'*e3,magm);

% Rm = skew(Rot'*e3)*Rm*skew(Rot'*e3)' + skew(-magm)*Ra*skew(-magm)';

% Equivalent stochastic gain
dfbwdbw = -1/EKF.bsat.tau*eye(3)*(eye(3)- diag([erf(bmax/(sqrt(2*Phat(4,4)))),...
    erf(bmax/(sqrt(2*Phat(5,5)))),...
    erf(bmax/(sqrt(2*Phat(6,6))))]));

% Propagation of filter

if iekf_type == 0 % LIEKF update
    
    % Invariants LIEKF
    I_0 = Rot'*arhat;
    I_1 = Rot'*mrhat_dec;
    I_w = omegahat;
    I_a = Rot'*gvec;
    I_m = Rot'*mag_ref;
    
    A = [-skew(I_w), -eye(3), zeros(3), zeros(3);
         zeros(3), dfbwdbw, zeros(3), zeros(3);
         zeros(3), zeros(3), -skew(I_w)-eye(3)/EKF.accest.tau, zeros(3);
         zeros(3), zeros(3), zeros(3), -skew(I_w)-eye(3)/EKF.magest.tau];
    
    B = blkdiag(-eye(3), eye(3), Rot', Rot');
%     C = [zeros(6,6),blkdiag(skew(Rot'), skew(Rot'*e3))]; % initialised measurement matrix
    C = [zeros(6,6),blkdiag(-eye(3), -eye(3))]; % initialised measurement matrix
    
    D = -eye(6);
    
    E = zeros(6,1);
    
    % LIEKF
    if corr_status(1) > 0 %&& corr_status(2) < 1 % Acc only
        C(1:3,1:3) = skew(I_0);
        E(1:3) = I_0 - accm;
    end
    
    if corr_status(2) > 0 %&& corr_status(1) < 1 % Mag only
        C(4:6,1:3) = skew(I_1);
        E(4:6) = I_1 - magm_dec;
    end
    
    % Gain
    L = -Phat*C'/(D*Rtot*D');
    L = sat(L, -15, 15);
    
    Lq = L(1:3,:);
    Lb = L(4:6,:);
    La = L(7:9,:);
    Lm = L(10:12,:);
    
    % ========== LIEKF ======================
    % Quaternion update
    om_tmp = omegahat + Lq*E;
    Omx = [0, -om_tmp'; om_tmp, -skew(om_tmp)];
    qhatdot = 0.5*Omx*qhat;
    qhat = qhat + qhatdot*dt;
    qhat = qhat/norm(qhat);
    
    % Bias update
    bhat = bhat + dt*(-EKF.bsat.kaw_sto*(bhat - sat(bhat, -bmax, bmax)) + Lb*E); % Saturated bias saturation
    % bhat = bhat + dt*(Rot'*Kb*E); % Normal bias propagation
    
    % Acc reference update
    arhat = arhat + dt*(-eye(3)/EKF.accest.tau*(arhat-EKF.gvec) + Rot*La*E);
    
    % Mag reference update
    mrhat = mrhat + dt*(-eye(3)/EKF.magest.tau*(mrhat-EKF.mag_ref) + Rot*Lm*E);
    
    
else    % RIEKF update
    
    % Invariants RIEKF
    I_1 = mrhat_dec;
    I_w = Rot*omegahat;
    I_a = gvec;
    I_m = mag_ref;
    
    
    A = [zeros(3), -eye(3), zeros(3);
        zeros(3), skew(I_w) + dfbwdbw, zeros(3);
        zeros(3), zeros(3), -skew(I_w)-eye(3)/EKF.magest.tau];
    
    B = blkdiag(-eye(3), eye(3), eye(3));
    C = [zeros(6,6),[zeros(3,3); eye(3)]]; % initialised measurement matrix
    D = -eye(6);
    
    E = zeros(6,1);
    
    % RIEKF
    if corr_status(1) > 0 %&& corr_status(2) < 1 % Acc only
        C(1:3,1:3) = skew(I_a);
        E(1:3) = gvec - Rot*accm;
    end
    
    if corr_status(2) > 0 %&& corr_status(1) < 1 % Mag only
        C(4:6,1:3) = skew(I_1);
        E(4:6) = mrhat_dec - Rot*magm;
    end
    
    % Gain
    L = -Phat*C'/(D*Rtot*D');
    Lq = L(1:3,:);
    Lb = L(4:6,:);
    Lm = L(7:9,:);
    
    % =========== RIEKF =========================
    % Quaternion update
    om_tmp = omegahat + Rot'*Lq*E;
    Omx = [0, -om_tmp'; om_tmp, -skew(om_tmp)];
    qhatdot = 0.5*Omx*qhat;
    qhat = qhat + qhatdot*dt;
    qhat = qhat/norm(qhat);
    
    % Exact integration
    % qhat = 1/2*e
    
    % Bias update
    bhat = bhat + dt*(-EKF.bsat.kaw_sto*(bhat - sat(bhat, -bmax, bmax)) + Rot'*Lb*E); % Saturated bias saturation
    % bhat = bhat + dt*(Rot'*Kb*E); % Normal bias propagation
    
    % Mag reference update
    mrhat = mrhat + dt*(-eye(3)/EKF.magest.tau*(mrhat-EKF.mag_ref) + Lm*E);
    
    % ===========================================
    
end


xhat_out = [qhat; bhat; arhat; mrhat];

% Covariance update
Pdot = A*Phat + Phat*A' + B*Qtot*B' - Phat*C'/(D*Rtot*D')*C*Phat;
Phat = Phat + Pdot*dt;
Phat = 0.5*(Phat + Phat');
Phat = diag(diag(sign(Phat)))*Phat; % Force positive diaginal elements

Phat_out = Phat;

Knorm = eig(A(1:6,1:6)+L(1:6,:)*C(:,1:6));
% Knorm = diag(L(1:6,:)*L(1:6,:)');
% Knorm = norm(L);
% Zamani implementation (full of bugs)
% State update
% innov = omegahat - Pak*deltak;
% Omx = [0 -innov'; innov, -skew(innov)];
% norm(qhat)
% Quaternion
% qhat = qhat + 0.5*dt*Omx*qhat;
% qhat = 0.5*expm(dt*Omx)*qhat;
% norm(qhat)
% qhat = qhat/norm(qhat);

% Gyro bias
% bhat = bhat + dt*(Pbk*Rot'*deltak);
%
% xhat_out = [qhat; bhat];
%

% Covariance update
% Pakdot = Qw - 2*symproj(Pbk) - Pak*Sk*Pak;
% Pckdot = Pbk*skew(Rot*omegahat) - Pak*Sk*Pbk - Pck;
% Pbkdot = 2*symproj(skew(Rot*omegahat))*Pck + Qb - Pbk*Sk*Pbk;
%
% Pak = Pak + dt*Pakdot;
% Pbk = Pbk + dt*Pbkdot;
% Pck = Pck + dt*Pckdot;
%
% Phat_out = [Pak, Pck; Pck', Pbk];
end


function [xhat_out,P_out] ...
    = nlkf_update(EKF,inputs,measurements,xhat_in,P_in, dt, corr_status)
% Nonlinear Kalman filter estimating rotation and translation.
%
% ---- Outputs :
% nu : innovations [13x1]
% omegahat : estimated body angle rates (p, q, r) [3x1]
% qatt_out : rotation quaternion (s, x, y, z) [4x1]
% Rot : Rotational matrix R(qatt_out) [3x3]
% euler : Euler angles (phi, theta, psi) [3x1]
% xhat_out : state output
% P_out : state covariance output
%
% ---- State: [qatt; bw; V; P; ba]
% qatt : rotation quaternion [s x y z]'
% bw : gyro bias [bx]
%
% ---- Dynamic model :
% q_dot = 0.5*Omega(w-bw)q
% bw_dot = -1/tau*sat(bw)
%
% Integration method : Euler forward
%
% ---- Measurements:
% Accelerometer: acc_m [3x1]
% Magnetometer : mag_m [3x1]
%
% Sequential treatment of measurements

nx = numel(xhat_in);
g = 9.81;
gvec = EKF.gvec;%[0;0;-g];
e3 = [0;0;1];
mag_ref = EKF.mag_ref;

bmax = EKF.bsat.bmax;

% State definition
qhat = xhat_in(1:4);
bwhat = xhat_in(5:7);

% Quaternion elements
s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);

% State covariance
Phat = P_in;

% Input
omega_m = inputs(1:3);

% Measurements
accm = measurements(1:3);
magm = measurements(4:6);

P_acchat = EKF.Ra;
P_maghat = EKF.Rm;

% P_acchat = EKF.Ra;
% P_maghat = EKF.Rm;

% Corrected omega
omegahat = omega_m - bwhat;

% Heading correction gain
dt_gain_mag = EKF.partq.dt_gain_mag;
dt_gain_acc = EKF.partq.dt_gain_acc;
kpsi = 1-exp(-1*dt_gain_mag/EKF.partq.taupsi); % Positive correction gain [0, 1];
kpsi_bias = 1-exp(-1*dt_gain_mag/(EKF.partq.taupsi_bias)); % Positive correction gain [0, 1];
kalpha = 1-exp(-1*dt_gain_acc/EKF.partq.taualpha); % Positive correction gain [0, 1];
kalpha_bias = 1-exp(-1*dt_gain_acc/(EKF.partq.taualpha_bias)); % Positive correction gain [0, 1];

% ++++++++++++++++
% Prediction step
% ++++++++++++++++
% State prediction
% Quaternion
Omega_mat = [0,       -omegahat';
    omegahat, -skew(omegahat)'];
qdot = 0.5*Omega_mat*qhat;
qhat = qhat + qdot*dt; % prediction of quaternion
qhat = qhat/norm(qhat); % normalization of predicted quaternion

% Gyro bias
bwhat = bwhat - EKF.bsat.kaw_sto*(bwhat - sat(bwhat, -bmax, bmax)); % Nonlinear prediction

% State concatenation
xhat = [qhat; bwhat];

% State covariance
% Jacobian
dfqdq = 0.5*Omega_mat*dt + eye(4);

dfqdbw = 0.5*[-x -y -z;
    s -z y;
    z s -x;
    -y -x s] * dt;

Qbw = EKF.bsat.Qd*dt;

dfbwdbw = eye(3)-EKF.bsat.kaw_sto*eye(3)*(eye(3)- diag([erf(bmax/(sqrt(2*P_in(5,5)))),...
    erf(bmax/(sqrt(2*P_in(6,6)))),...
    erf(bmax/(sqrt(2*P_in(7,7))))]));

Fk = [dfqdq, -dfqdbw, zeros(4,nx-7);     % q
    zeros(3,nx-3), dfbwdbw, zeros(3,nx-7)];           % bw


dfqdw = 0.5*[0 -x -y -z;
    0 s -z y;
    0 z s -x;
    0 -y -x s]*dt;
Vkq = diag([0; EKF.cov.gyro]);

Qq = dfqdw*Vkq*dfqdw';

Qk = [Qq,         zeros(4,3);   % q
    zeros(3,4), Qbw]; % bw

Phat = Fk*Phat*Fk' + Qk;

% Quaternion elements
s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
% Rotation matrix from predicted quaternion
Rot = Quaternion2RotMat(qhat); % Rotation matrix from quaternion

% +++++++++++++++++++++++++++++++++++++++++++++
% Covariance correction due to normalization
% +++++++++++++++++++++++++++++++++++++++++++++

% h = q'q = 1;
Rkq = 1e-5;
Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
Skq = Hkq*Phat*Hkq'+Rkq;
Kkq = Phat*Hkq'*(Skq\eye(1));
Phat = (eye(nx)-Kkq*Hkq)*Phat;

% ################################
% #### Inclination correction ####
% ################################
if corr_status(1) > 0
    % #### Correction by partial inclination quaternion correction
    a_dir = accm/norm(accm);
    
    A = [e3'; e3'*Rot];
    umeas = [1;0;0];
    na = null(A);
    umeas(1:3) = na(:,1);
    
    % umeas = cross(a_dir, Rot*(e3));
    uhat = umeas;
    uhat = uhat/norm(uhat); % u^T.u = 1
    ux = skew(uhat);
    
    % Calculate R_alpha_u - verticality rotation matrix
    calpha = e3'*Rot'*e3;
    salpha = -uhat'*(cross(e3,Rot'*e3));
    Rot_alpha_u_hat = eye(3)+salpha*ux+(1-calpha)*ux^2;
    
    ym = Rot_alpha_u_hat*a_dir;
    
    calpha_ym = -e3'*ym;
    salpha_ym = uhat'*cross(e3,ym);
    
    % Calculate attitude error angle dalpha
    dalpha = atan2(salpha_ym,calpha_ym);
    Delta_alpha = kalpha*dalpha/2;
    
    delta_q = [cos(Delta_alpha); uhat*sin(Delta_alpha)];
    % State correction
    
    
    % Bias correction
    kb = -kalpha_bias;
    
    kp = kb*uhat(1); % Gain for roll speed bias correction
    kq = kb*uhat(2); % Gain for pitch speed bias correction
    
    bp = xhat(5) + kp*dalpha;
    bq = xhat(6) + kq*dalpha;
    
    
    [ dqcdq, dqcdam, ddalphadq, ddalphadam , ~, ~ , ~] =...
        ekflin_qcorr_inclination_2( kalpha, qhat, uhat, accm, ym, Delta_alpha);
    
    dbpdq = uhat(1)*kb*ddalphadq;
    dbqdq = uhat(2)*kb*ddalphadq;
    dbpdam = uhat(1)*kb*ddalphadam;
    dbqdam = uhat(2)*kb*ddalphadam;
    
    ImKH = [dqcdq, zeros(4,3); [dbpdq; dbqdq; zeros(1,4)], [1 0 0; 0 1 0; 0 0 1]];
    K = [dqcdam; dbpdam; dbqdam; zeros(1,3)];
    
    qhat = QuaternionProduct(qhat,delta_q);
    xhat = [qhat; bp; bq; xhat(7)];
    
    % Correct covariance only if it does not increase
    p_trace_pre = trace(Phat); % Predicted covariance trace
    P_hat_pre = Phat; % Predicted covariance
    
    % Apply ccovariance correction
    Phat =  ImKH*Phat*ImKH'+K*P_acchat*K'; % Joseph form
    Phat = 1/2*(Phat+Phat');
    
    p_trace_corr = trace(Phat); % Corrected covariance trace
    
    % Accept or reject covariance correction
    if p_trace_corr > p_trace_pre
        Phat = P_hat_pre;
    end
end

% ############################
% #### Heading correction ####
% ############################
if corr_status(2) > 0
    % Direct quaternion correction
    m_i = Rot*magm;
    % Calulate correction quaternion
    y1 = mag_ref.'*cross(m_i,-e3);
    y2 = cross(mag_ref,-e3)'*cross(m_i,-e3);
    
    s1 = y1/sqrt(y1^2+y2^2);
    c1 = y2/sqrt(y1^2+y2^2);
    psidiff = atan2(s1,c1);
    Ccap = cos(kpsi*psidiff/2);
    Scap = sin(kpsi*psidiff/2);
    deltaq_m = [Ccap;0;0;Scap];
    
    % Calculate corrected covariance
    
    [ dqcdq, dqcdmm ] =...
        ekflin_deltaq_part_dpsi_2( mag_ref, magm, qhat, Ccap, Scap, kpsi);
    if max(max(isnan(dqcdq))) > 0
        dqcdq = eye(4);
        dqcdmm = zeros(4,3);
    end
    
    % ----------------------------
    % Bias gain calculation
    kb = -kpsi_bias;
    
    % Calculate corrected covariance - quaternion correction
    [dbhatdq, dbhatdmm] = ekflin_bhat_pm( Rot,xhat(1:4),magm,kpsi, kb);
    ImKH = [dqcdq, zeros(4,3); [zeros(2,4); dbhatdq], eye(3,3)];
    K = [dqcdmm; zeros(2,3); dbhatdmm];
    
    % Correct covariance only if it does not increase
    p_trace_pre = trace(Phat); % Predicted covariance trace
    P_hat_pre = Phat; % Predicted covariance
    
    % Apply ccovariance correction
    Phat =  ImKH*Phat*ImKH'+K*P_maghat*K'; % Joseph form
    Phat = 1/2*(Phat+Phat');
    
    p_trace_corr = trace(Phat); % Corrected covariance trace
    
    % Accept or reject covariance correction
    if p_trace_corr > p_trace_pre
        Phat = P_hat_pre;
    end
    
    % Correct state
    qhat = QuaternionProduct(deltaq_m,qhat);
    bhat_corr = xhat(7) + kb*psidiff;
    xhat = [qhat; xhat(5:6); bhat_corr];
end
% ++++++++++++
% Output
% ++++++++++++

xhat_out = xhat;
P_out = Phat;


end
function [ Phat_out, xhat_out, Knorm, l_error] = invariant_mekf_covmax( Phat_in, xhat_in,...
    inputs, measurements, corr_status, EKF, dt, ext_bias, ncov)
%INVARIANT_MEKF Summary of this function goes here
%   Detailed explanation goes here

iekf_type = 1; % 0: LIEKF, 1: RIEKF (Bonnabel, Martin, Sala�n 2009)
nx = 6;
ny = 5;

% Init
gvec = EKF.gvec;
mag_ref = EKF.mag_ref;
e3 = [0;0;1];

bmax = EKF.bsat.bmax;

% State
qhat = xhat_in(1:4);
bhat = xhat_in(5:7);

% qhat_inv = [qhat(1); -qhat(2:4)];

% Input + measurements
omega = inputs(1:3);
accm = measurements(1:3);
magm = measurements(4:6);

% Ra = EKF.Ra*100;
% Rm = EKF.Rm*100;

Ra = EKF.Ra*EKF.kcov;
Rm = EKF.Rm*EKF.kcov_mag;

% omegahat = omega - bhat;
omegahat = omega - ext_bias;

% Input covariance
Qb = EKF.bsat.Qd;
Qw = diag(EKF.cov.gyro);

Rtot = dt*blkdiag(Ra,Rm);
Qtot = dt*blkdiag(Qw, Qb);

% State covariance
% Pak = Phat_in(1:3,1:3);
% Pck = Phat_in(1:3,4:6);
% Pbk = Phat_in(4:6,4:6);
% disp('==============================')
Phat = Phat_in;
ddqda = [zeros(1,3); eye(3)*0.5];
Pq = ddqda*Phat(1:3,1:3)*ddqda';

% Rotation matrix
Rot = qua2chrSL(qhat);

% Transformation matrix for decoupling
T = eye(2,3);
% T = eye(3);

ind_var = 3+size(T,1);

% Measurement decoupling
% Estimated gravity
mag_ref = cross(e3,mag_ref);
magm = cross(Rot'*e3,magm);
% mag_ref = cross(gvec,mag_ref);
% magm = cross(accm,magm);

% [~,drote3dq] = ekfoutlin(qhat,e3);
% Rm = T*(skew(Rot'*e3)*Rm*skew(Rot'*e3)' + skew(-magm)*drote3dq*Pq*drote3dq'*skew(-magm)')*T';
% Rm = T*(skew(Rot'*e3)*Rm*skew(Rot'*e3)')*T';
Rm = T*Rm*T';
% Rm = T*(abs(skew(accm)*Rm*skew(accm)' -skew(magm)*Ra*skew(magm)'))*T';

% disp('==============')
% real(Rm)
% imag(Rm)

% Mesaured gravity ["Martin decoupling"]
% mag_ref = cross(gvec,mag_ref);
% magm = cross(accm,magm); 
% Rm = T*(skew(accm)*Rm*skew(accm)'+ skew(-magm)*Ra*skew(-magm)')*T';

Rtot = dt*blkdiag(Ra,Rm);

% Equivalent stochastic gain
dfbwdbw = -1/EKF.bsat.tau*eye(3)*(eye(3)- diag([erf(bmax/(sqrt(2*abs(Phat(4,4))))),...
    erf(bmax/(sqrt(2*abs(Phat(5,5))))),...
    erf(bmax/(sqrt(2*abs(Phat(6,6)))))]));

% Additional linaerisation points for covariance majoration
% ncov = 20; % Number of extra points
psi_set = linspace(0,pi,ncov);
A_set = zeros(nx,nx,ncov);
C_set = zeros(ind_var,nx,ncov);
P_set = zeros(nx,nx,ncov);
Rot_set = zeros(3,3,ncov);
for k = 1:ncov
    Rot_set(:,:,k) = Rot*Euler2RotMat(0,0,psi_set(k));
%     Rot_set(:,:,k) = Rot*Euler2RotMat(psi_set(k),psi_set(k),0);
end
% ===================================================================
% Calculate error angle from NL-KF
% ===================================================================
% if corr_status(1) > 0
%     % #### Correction by partial inclination quaternion correction
%     a_dir = accm/norm(accm);
%     
%     A = [e3'; e3'*Rot];
%     umeas = [1;0;0];
%     na = null(A);
%     if size(na,2) > 1   % Choose randomly the inclination axis in case of multiple solutions
%         randbit = rand>0.5;
%         if randbit
%             umeas(1:3) = na(:,1);
%         else
%             umeas(1:3) = na(:,2);
%         end
%     else
%         umeas(1:3) = na(:,1);
%     end
% 
%     uhat = umeas;
%     uhat = uhat/norm(uhat); % u^T.u = 1
%     ux = skew(uhat);
%     
%     % Calculate R_alpha_u - verticality rotation matrix
%     calpha = e3'*Rot'*e3;
%     salpha = -uhat'*(cross(e3,Rot'*e3));
%     Rot_alpha_u_hat = eye(3)+salpha*ux+(1-calpha)*ux^2;
%     
%     if EKF.gvec(3) < 0
%         ym = Rot_alpha_u_hat*a_dir;
%     else
%         ym = -Rot_alpha_u_hat*a_dir;
%     end
%     
%     
%     calpha_ym = -e3'*ym;
%     salpha_ym = uhat'*cross(e3,ym);
%     
%     % Calculate attitude error angle dalpha
%     dalpha = atan2(salpha_ym,calpha_ym);
%     lin_inc = uhat(1:2)'*dalpha;
% else
%     lin_inc = [0,0];
% end
% if corr_status(2) > 0
%     % Direct quaternion correction
%     m_i = Rot*measurements(4:6);
%     % Calulate correction quaternion
% %     if corr_status(1) > 0
% %         y1 = mag_ref.'*Rot*cross(magm,-accm);
% %         y2 = cross(mag_ref,-e3)'*Rot*cross(magm,-accm);
% %         uhat = e3;
% %     else
%         y1 = mag_ref.'*cross(m_i,-e3);
%         y2 = cross(mag_ref,-e3)'*cross(m_i,-e3);
% %     end
%     s1 = y1/sqrt(y1^2+y2^2);
%     c1 = y2/sqrt(y1^2+y2^2);
%     psidiff = atan2(s1,c1);
% else
%     psidiff = 0;
% end
% for k = 1:ncov
% %     Rot_set(:,:,k) = Rot*Euler2RotMat(0,0,psi_set(k));
%     Rot_set(:,:,k) = Rot*Euler2RotMat(abs(lin_inc(1)),abs(lin_inc(2)),abs(psidiff));
%     if k == ncov
%         Rot_set(:,:,k) = Rot*Euler2RotMat(0,0,psi_set(k));
%     end
% end
% ==================================================================
% ===================================================================

% Propagation of filter

if iekf_type == 0 % LIEKF update
    
    % Invariants LIEKF
    I_w = omegahat;
    I_a = Rot'*gvec;
    I_m = Rot'*mag_ref;
    
    A = [-skew(I_w), -eye(3);
        zeros(3), dfbwdbw];
    for k = 1:ncov;
        A_set(:,:,k) = A;
    end
    
    B = blkdiag(eye(3), eye(3));
    C = [zeros(ind_var,6)]; % initialised measurement matrix
    
    
    E = zeros(ind_var,1);
    
    % LIEKF
    if corr_status(1) > 0 %&& corr_status(2) < 1 % Acc only
        C(1:3,1:3) = skew(I_a);
        for k = 1:ncov
            C_set(1:3,1:3,k) = skew(Rot_set(:,:,k)'*gvec);
        end
        E(1:3) = Rot'*gvec - accm; % Error in body frame
    end
    
    if corr_status(2) > 0 %&& corr_status(1) < 1 % Mag only
        C(4:3+size(T,1),1:3) = T*skew(I_m);
        for k = 1:ncov
            C_set(4:ind_var,1:3,k) = T*skew(Rot_set(:,:,k)'*mag_ref);
        end
        E(4:ind_var) = T*(Rot'*mag_ref - magm); % Error in body frame
    end
    
    % Gain
    K = -Phat*C'/Rtot;
    K = sat(K, -15, 15);
    Kq = K(1:3,:);
    Kb = K(4:6,:);
    
    % Quaternion update
    om_tmp = omegahat + Kq*E;
    Omx = [0, -om_tmp'; om_tmp, -skew(om_tmp)];
    qhatdot = 0.5*Omx*qhat;
    qhat = qhat + qhatdot*dt;
    qhat = qhat/norm(qhat);
    
    % Bias update
    bhat = bhat + dt*(-1/EKF.bsat.tau*(bhat - sat(bhat, -bmax, bmax)) + Kb*E); % Saturated bias saturation
    % bhat = bhat + dt*(Rot'*Kb*E); % Normal bias propagation
else    % RIEKF update
    
    A = [zeros(3), -eye(3); zeros(3), skew(Rot*omegahat) + dfbwdbw];
    for k = 1:ncov
        A_set(:,:,k) = [zeros(3), -eye(3); zeros(3), skew(Rot_set(:,:,k)*omegahat) + dfbwdbw];
    end
    
    C = zeros(ind_var,6);
    E = zeros(ind_var,1);
    
    if corr_status(1) > 0 %&& corr_status(2) < 1 % Acc only
        C(1:3,1:3) = skew(gvec);
        E(1:3) = gvec - Rot*accm; % Error in inertial frame
    end
    
    if corr_status(2) > 0 %&& corr_status(1) < 1 % Mag only
        C(4:ind_var,1:3) = T*skew(mag_ref);
        E(4:ind_var) = T*(mag_ref - Rot*magm); % Error in inertial frame
    end
    for k = 1:ncov
        C_set(:,:,k) = C;
    end
    
    % Gain
    K = -Phat*C'/Rtot;
    K = sat(K, -15, 15);
    Kq = K(1:3,:); %Kq(1:2,4:6) = 0; Kq(3,1:3) = 0;
    Kb = K(4:6,:); %Kb(1:2,4:6) = 0; Kb(3,1:3) = 0;
    
   
    % Quaternion update
    om_tmp = omegahat + Rot'*Kq*E;
    Omx = [0, -om_tmp'; om_tmp, -skew(om_tmp)];
    qhatdot = 0.5*Omx*qhat;
    qhat = qhat + qhatdot*dt;
    qhat = qhat/norm(qhat);
    
    % Bias update
    bhat = bhat + dt*(-1/EKF.bsat.tau*(bhat - sat(bhat, -bmax, bmax)) + Rot'*Kb*E); % Saturated bias saturation
    % bhat = bhat + dt*(Rot'*Kb*E); % Normal bias propagation
    
end

xhat_out = [qhat; bhat];


Ecov = C*Phat*C' + Rtot;
l_error = E'/Ecov*E;


% Covariance update
% Nominal
Pdot = A*Phat + Phat*A' + Qtot - Phat*C'/Rtot*C*Phat;
Phat = Phat + Pdot*dt;
Phat = 0.5*(Phat + Phat');
% Additional linearisation points
for k = 1:ncov
    Pdot = A_set(:,:,k)*Phat_in + Phat_in*A_set(:,:,k)' + Qtot - Phat_in*C_set(:,:,k)'/Rtot*C_set(:,:,k)*Phat_in;
    P_set(:,:,k) = Phat_in + Pdot*dt;
    P_set(:,:,k) = 0.5*(P_set(:,:,k) + P_set(:,:,k)');
end

% Phat = maxcov(P_set);
% for k = 1:6 % Force positive diagonal elements
%     Phat(k,k) = abs(Phat(k,k));
% end
% Phat = diag(diag(sign(Phat)))*Phat; % Force positive diaginal elements

Phat_out = Phat;

Knorm = diag(K*K');
% Knorm = norm(K);
end


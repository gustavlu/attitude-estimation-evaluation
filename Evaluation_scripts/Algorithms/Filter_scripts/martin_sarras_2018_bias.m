function [ xhat_out, qhat] ...
    = martin_sarras_2018_bias(xhat_in, inputs,  measurements, EKF,  dt, gain)
%MARTIN_2017_SENSOR Summary of this function goes here
%   Detailed explanation goes here

accm = measurements(1:3);
magm = measurements(4:6);

omega = inputs(1:3);

ka = gain(1);
km = gain(2);
la = gain(3);
lm = gain(4);
ma = gain(5);
mm = gain(6);

ahat = xhat_in(1:3);
mhat = xhat_in(4:6);
bhat = xhat_in(7:9);
bahat = xhat_in(10:12);
bmhat = xhat_in(13:15);

omegahat = omega - bhat;
% disp('====================')
% (cross((ahat-bahat),(omegahat)) - ka*(ahat - accm))*dt;
% (cross((mhat),(omegahat)) - km*(mhat - magm))*dt;
% (lm*cross(xhat_in(4:6),magm))*dt;
% % State propagation - Only gyro + acc bias
% ahat = ahat + (cross((ahat-bahat),(omegahat)) - ka*(ahat - accm))*dt;
% mhat = mhat + (cross((mhat),(omegahat)) - km*(mhat - magm))*dt;
% bhat = bhat + (lm*cross(xhat_in(4:6),magm))*dt;
% bahat = bahat + (ma*cross(omegahat,(xhat_in(1:3)-accm)))*dt;

% State propagation- Gyro + Acc + Mag bias
ahat = ahat + (cross((ahat-bahat),(omegahat)) - ka*(ahat - accm))*dt;
mhat = mhat + (cross((mhat-bmhat),(omegahat)) - km*(mhat - magm))*dt;
bhat = bhat + (la*cross(xhat_in(1:3),accm) + lm*cross(xhat_in(4:6),magm))*dt;
bahat = bahat + (ma*cross(omegahat,(xhat_in(1:3)-accm)))*dt;
bmhat = bmhat + (mm*cross(omegahat,(xhat_in(4:6)-magm)))*dt;

R_i = [EKF.gvec/norm(EKF.gvec), ...
       cross(EKF.gvec,EKF.mag_ref)/norm(cross(EKF.gvec,EKF.mag_ref)),...
       cross(EKF.gvec, cross(EKF.gvec, EKF.mag_ref))/norm(cross(EKF.gvec, cross(EKF.gvec, EKF.mag_ref)))];
         
Rot_trans = [ahat/norm(ahat), ...
             cross(ahat,mhat)/norm(cross(ahat,mhat)),...
             cross(ahat, cross(ahat, mhat))/norm(cross(ahat, cross(ahat, mhat)))]*R_i';

Rot_trans = orthonormalize(Rot_trans);

qhat = RotMat2Quaternion(Rot_trans);

xhat_out = [ahat; mhat; bhat; bahat; bmhat];

end


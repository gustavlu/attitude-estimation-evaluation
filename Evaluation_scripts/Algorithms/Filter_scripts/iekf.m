function [ Phat_out, xhat_out, E ] = iekf( Phat_in, xhat_in, inputs, measurements, corr_status, EKF, dt )
%INVARIANT_MEKF Summary of this function goes here
%   Detailed explanation goes here

% Init
gvec = EKF.gvec;
mag_ref = EKF.mag_ref;
e3 = [0;0;1];

bmax = EKF.bsat.bmax;

% State
qhat = xhat_in(1:4);
bhat = xhat_in(5:7);
mrhat = xhat_in(8:10);

% qhat_inv = [qhat(1); -qhat(2:4)];

% Input + measurements
omega = inputs(1:3);
accm = measurements(1:3);
magm = measurements(4:6);

Ra = EKF.Ra;
Rm = EKF.Rm;

omegahat = omega - bhat;

% Input covariance
Qb = EKF.bsat.Qd;
Qw = diag(EKF.cov.gyro);
Qm = eye(3)*(norm(mag_ref)*0.1/3)/sqrt(EKF.magest.tau);

Rtot = dt*blkdiag(Ra,Rm);
Qtot = dt*blkdiag(Qw, Qb, Qm);

% State covariance
% Pak = Phat_in(1:3,1:3);
% Pck = Phat_in(1:3,4:6);
% Pbk = Phat_in(4:6,4:6);

Phat = Phat_in;

% Rotation matrix
Rot = qua2chr(qhat);

% Measurement decoupling
% mag_ref = cross(e3,mag_ref);
% magm = cross(Rot'*e3,magm);
% Rm = skew(Rot'*e3)*Rm*skew(Rot'*e3)' + skew(-magm)*Ra*skew(-magm)';

% Invariants
I_0 = Rot'*mrhat;
I_w = omegahat;
I_a = Rot'*gvec;
I_m = Rot'*mag_ref;

% Equivalent stochastic gain
dfbwdbw = -EKF.bsat.kaw_sto*eye(3)*(eye(3)- diag([erf(bmax/(sqrt(2*Phat(4,4)))),...
           erf(bmax/(sqrt(2*Phat(5,5)))),...
           erf(bmax/(sqrt(2*Phat(6,6))))]));

A = [-skew(I_w), -eye(3), zeros(3); 
     zeros(3), zeros(3), zeros(3);
     zeros(3), zeros(3), -skew(I_w)-eye(3)/EKF.magest.tau];
B = blkdiag(-eye(3), eye(3), Rot');
C = [zeros(6,6),[zeros(3,3); eye(3)]]; % initialised measurement matrix
D = -eye(6);

E = zeros(6,1);

if corr_status(1) > 0 %&& corr_status(2) < 1 % Acc only
    C(1:3,1:3) = skew(I_a);
    E(1:3) = Rot'*gvec - accm;
end

if corr_status(2) > 0 %&& corr_status(1) < 1 % Mag only
    C(4:6,1:3) = skew(I_0);
    E(4:6) = Rot'*mrhat - magm;
end

% if corr_status(1) > 0 && corr_status(2) > 0 % Both meas available 
%     C(1:3,1:3) = skew(gvec);
%     E(1:3) = gvec - Rot*accm;
% end

% Gain 
L = -Phat*C'/(D*Rtot*D');
Lq = L(1:3,:);
Lb = L(4:6,:);
Lm = L(7:9,:);

% Quaternion update
om_tmp = omegahat + Lq*E;
Omx = [0, -om_tmp'; om_tmp, -skew(om_tmp)];
qhatdot = 0.5*Omx*qhat;
qhat = qhat + qhatdot*dt;
qhat = qhat/norm(qhat);

% Exact integration
% qhat = 1/2*e

% Bias update
bhat = bhat + dt*(-EKF.bsat.kaw_sto*(bhat - sat(bhat, -bmax, bmax)) + Lb*E); % Saturated bias saturation
% bhat = bhat + dt*(Rot'*Kb*E); % Normal bias propagation 

mrhat = mrhat + dt*(-eye(3)/EKF.magest.tau*(mrhat-EKF.mag_ref) + Rot*Lm*E);

xhat_out = [qhat; bhat; mrhat];

% Covariance update
Pdot = A*Phat + Phat*A' + B*Qtot*B' - Phat*C'/(D*Rtot*D')*C*Phat;
Phat = Phat + Pdot*dt;
Phat = 0.5*(Phat + Phat');
Phat = diag(diag(sign(Phat)))*Phat; % Force positive diaginal elements

Phat_out = Phat;

% Zamani implementation (full of bugs)
% State update
% innov = omegahat - Pak*deltak;
% Omx = [0 -innov'; innov, -skew(innov)];
% norm(qhat)
% Quaternion
% qhat = qhat + 0.5*dt*Omx*qhat;
% qhat = 0.5*expm(dt*Omx)*qhat;
% norm(qhat)
% qhat = qhat/norm(qhat);

% Gyro bias
% bhat = bhat + dt*(Pbk*Rot'*deltak);
% 
% xhat_out = [qhat; bhat];
% 

% Covariance update
% Pakdot = Qw - 2*symproj(Pbk) - Pak*Sk*Pak;
% Pckdot = Pbk*skew(Rot*omegahat) - Pak*Sk*Pbk - Pck;
% Pbkdot = 2*symproj(skew(Rot*omegahat))*Pck + Qb - Pbk*Sk*Pbk;
% 
% Pak = Pak + dt*Pakdot;
% Pbk = Pbk + dt*Pbkdot;
% Pck = Pck + dt*Pckdot;
% 
% Phat_out = [Pak, Pck; Pck', Pbk];
end


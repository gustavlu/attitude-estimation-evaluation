function [ xhat_out,Phat_out, nu_a, nu_m, ka_norm, km_norm] = ekf_update(EKF, inputs, measurements, xhat_in,Phat_in, dt, corr_status )
%DECKF_ Summary of this function goes here
%   Detailed explanation goes here

nx = numel(xhat_in);
gvec = EKF.gvec;
mag_ref = EKF.mag_ref;

bmax = EKF.bsat.bmax;

% State definition
qhat = xhat_in(1:4);
bhat = xhat_in(5:7);

% Quaternion elements
s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
% Rotation matrix from previous quaternion
Rot = Quaternion2RotMat(qhat); % Rotation matrix from quaternion

% State covariance
Phat = Phat_in;

% Measurements
accm = measurements(1:3);
magm = measurements(4:6);

% Corrected omega
omegahat = inputs(1:3) - bhat;



% ++++++++++++++++
% Prediction step
% ++++++++++++++++
Omega_x = [0,       -omegahat';
           omegahat, -skew(omegahat)];

% State covariance
dfqdq = 0.5*Omega_x*dt + eye(4);

dfqdbw = 0.5*[-x -y -z;
               s -z y;
               z s -x;
              -y -x s] * dt;

Qbw = EKF.bsat.Qd;


dfbwdbw = eye(3)-EKF.bsat.kaw_sto*eye(3)*(eye(3)-...
    diag([erf(EKF.bsat.bmax/(sqrt(2*Phat(5,5)))),...
    erf(EKF.bsat.bmax/(sqrt(2*Phat(6,6)))),...
    erf(EKF.bsat.bmax/(sqrt(2*Phat(7,7))))]));

Fk = [dfqdq, -dfqdbw, zeros(4,nx-7);     % q
    zeros(3,nx-3), dfbwdbw, zeros(3,nx-7)];           % bw


dfqdw = 0.5*[0 -x -y -z;
    0 s -z y;
    0 z s -x;
    0 -y -x s]*dt;
Vkq = diag([0; EKF.cov.gyro]);

Qq = dfqdw*Vkq*dfqdw';

Qk = [Qq,         zeros(4,3);   % q
      zeros(3,4), Qbw]; % bw

Phat = Fk*Phat*Fk' + Qk;

% State prediction
qhat = qhat + 0.5*Omega_x*qhat*dt; % prediction of quaternion
qhat = qhat/norm(qhat); % Normalization of predicted quaternion
bhat = bhat;

xhat = [qhat; bhat];

% +++++++++++++++++++++++++++++++++++++++++++++
% Covariance correction due to normalization
% +++++++++++++++++++++++++++++++++++++++++++++
% z = q'q - 1;
s = xhat(1); x = xhat(2); y = xhat(3); z = xhat(4);
Rkq = 1e-5;
Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
Skq = Hkq*Phat*Hkq'+Rkq;
Kkq = Phat*Hkq'*(Skq\eye(1));
Phat = (eye(nx)-Kkq*Hkq)*Phat*(eye(nx)-Kkq*Hkq)' + Kkq*Rkq*Kkq';

ka_norm = 0;
nu_a = zeros(3,1);
% +++++++++++++++++++++++++++++++++++++++++++++
% State correction
% +++++++++++++++++++++++++++++++++++++++++++++
% #### Correction by accelerometer ####
if corr_status(1) > 0
    nu =  accm - Rot'*gvec; nu_a = nu;
    [~,Ha] = ekfoutlin(qhat,gvec);
    Ha = [Ha zeros(3,nx-4)];
    Ra = EKF.Ra;
    Sa = Ha*Phat*Ha' + Ra;
    
    Ka = Phat*Ha'*(Sa\eye(3));   % Kalman gain
    ka_norm = norm(Ka);
    % State
    xhat = xhat + (Ka*nu);
    % Covariance
    Phat =  (eye(nx)-Ka*Ha)*Phat*(eye(nx)-Ka*Ha)' + Ka*Ra*Ka'; % Joseph form
    % Normalization
    xhat(1:4) = xhat(1:4)./norm(xhat(1:4));
    
    Rot = Quaternion2RotMat(qhat); % Rotation matrix from quaternion
    % -----------------------------------------------------
    % #### Covariance correction due to normalization ####
    % z = q'q - 1;
    % Quaternion elements
    s = xhat(1); x = xhat(2); y = xhat(3); z = xhat(4);
    Rkq = 1e-5;
    Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
    Skq = Hkq*Phat*Hkq'+Rkq;
    Kkq = Phat*Hkq'*(Skq\eye(1));
    Phat = (eye(nx)-Kkq*Hkq)*Phat*(eye(nx)-Kkq*Hkq)' + Kkq*Rkq*Kkq';
    % -----------------------------------------------------
end

Rot = Quaternion2RotMat(qhat);

km_norm = 0;
nu_m = zeros(3,1);
% #### Correction by magnetometer ####
% mag_meas_vec = cross(accm,magm);
% mag_ref_vec = Rot'*cross(gvec,mag_ref);
if corr_status(2) > 0
    nu = magm - Rot'*mag_ref; % Innovation
    nu_m = nu;
    [~, dnumdq] = ekfoutlin(xhat(1:4),mag_ref); % Jacobian
    
    Hm = [dnumdq zeros(3,nx-4)];
    Rm = EKF.Rm;
    Sm = (Hm*Phat*Hm'+Rm);
    Km = Phat*Hm'*(Sm\eye(3));   % Kalman gain
    km_norm = norm(Km);
    % State and covariance update
    xhat = xhat + (Km*nu); % Correction without decoupling
    Phat =  (eye(nx)-Km*Hm)*Phat*(eye(nx)-Km*Hm)' + Km*Rm*Km'; % Joseph form
    
    % Normalization
    xhat(1:4) = xhat(1:4)./norm(xhat(1:4));
    
    % -----------------------------------------------------
    % #### Covariance correction due to normalization ####
    % z = q'q - 1;
    % Quaternion elements
    s = qhat(1); x = qhat(2); y = qhat(3); z = qhat(4);
    Rkq = 1e-5;
    Hkq = [2*s 2*x 2*y 2*z zeros(1,nx-4)];
    Skq = Hkq*Phat*Hkq'+Rkq;
    Kkq = Phat*Hkq'*(Skq\eye(1));
    Phat = (eye(nx)-Kkq*Hkq)*Phat*(eye(nx)-Kkq*Hkq)' + Kkq*Rkq*Kkq';
    % -----------------------------------------------------
end
% ++++++++++++
% Output
% ++++++++++++

xhat_out = xhat;
Phat_out = Phat;
end



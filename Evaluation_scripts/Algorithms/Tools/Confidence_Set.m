function [r] = Confidence_Set(p,n)

% [r] = Confidence_Set(p,n);
%==========================================================================
% Confidence region for multi-dimensionnal Gaussian
%==========================================================================
% p :  probability level
% n :  dimension
%
% r :  ellipsoid size
%==========================================================================
% The confidence region D is an ellipsoid defined by:
%   mu : center
%   P :  covariance
%   r :  radius
%
% Its equation is:
%   D = { x | (x-mu)^T * P^(-1) * (x-mu) =< r^2 }
%==========================================================================

%==========================================================================
% x is assumed to be a n-dimensional gaussian random variable
%   x~Gauss(mu,P)
%
% Probability to live within the ellipsoid
%   p = Proba( x in D | x~Gauss(mu,P) )
%
% y = (x-mu)^T*P^(-1)*(x-mu) is a khi2 with n dof
%   y~Khi2(n)
%
% Cumulative Distribution Function of a khi2 with n dof
%   p  = proba( y =< r^2 | y~Khi2(n) )  
% 
% Incomplete gamma function (normalized in matlab)
%   p  = gammainc(r^2/2,n/2)
%
% Inverse incomplete gamma function (normalized in matlab)
%   r^2/2 = gammaincinv(p,n/2)
%
%==========================================================================

x = gammaincinv(p,n/2);
r = sqrt(2*x);

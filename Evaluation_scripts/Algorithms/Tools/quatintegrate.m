function [ qhat_out ] = quatintegrate( qhat_in, omega, dt )
%QUATINTEGRATE Summary of this function goes here
%   Detailed explanation goes here
Om_x = [0 -omega'; omega, -skew(omega)];
qhat_out = (eye(4) + 0.5*Om_x*dt)*qhat_in;

qhat_out = qhat_out/norm(qhat_out);
end


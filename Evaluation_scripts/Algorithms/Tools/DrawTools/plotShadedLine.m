% varargout = plotShadedLine(x,y,fstr,transparency)
%
% x: x coordinates
% y: either just one y vector, or 2xN or 3xN matrix of y-data
% fstr: format ('r' or 'b--' etc)
%
% example
% x=[-10:.1:10];plotshaded(x,[sin(x.*1.1)+1;sin(x*.9)-1],'r');

function varargout = plotShadedLine(x,y,fstr,transparency)
 
    Line_width = 1;     % prct
    y_ampl = abs(max(y)-min(y));
    y_patch_lim = [y+y_ampl*Line_width/100;y-y_ampl*Line_width/100];
    
    plotShaded(x, y_patch_lim, fstr, transparency);

end
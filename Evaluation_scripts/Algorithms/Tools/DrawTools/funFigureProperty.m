%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure properties
% Author: Charles Poussot-Vassal [ONERA/DCSD]
% Date: August 2009
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Description:
% Modifies figure properties (adapt text font and line size for article &
% report easy insertion).
% 
% Input:
%  no input
% 
% Output:
%  no output
%
%%%% Example %%%
% funPlotProperty()
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function funFigureProperty(h, varargin)

    if nargin > 0
        %set(h,'interpreter','latex')
    end
    
    if nargin > 1
        fonty = varargin{1};
    else
        fonty = 28;
    end
    
    set(get(gca,'XLabel'),'FontSize',fonty)%,'Interpreter','latex')
    set(get(gca,'YLabel'),'FontSize',fonty)%,'Interpreter','latex')
    set(get(gca,'ZLabel'),'FontSize',fonty)%,'Interpreter','latex')
    set(get(gca,'Title'),'FontSize',fonty+4)%,'Interpreter','latex')
    set(gca,'FontSize',fonty-2)
%     set(findobj('type','line'),'linewidth',2.5);
    set(gca,'box','on');
end
%arrow draw a 3D arrow
%
%   arrow(O1, O2, varargin)
%
%   01:     first point coordinates [2x1], or [3x1]
%   02:     first point coordinates [2x1], or [3x1]
%   vargin: optional properties of a line (see line help),
%
%   See also: line, multiLine, plotCircle3D

function arrow(O1, O2, varargin)

    %#codegen
    coder.extrinsic('cylinder')
    
    if length(O1) ~= length(O2)
        error('DrawTools:line2:InconsistentDimensions', 'Dimension of O1 and O2 are different');
    else
        % draw the main line
        if (length(O1) == 2)
            h = line([O1(1) O2(1)], [O1(2), O2(2)]);
        elseif (length(O1) == 3)
            h = line([O1(1) O2(1)], [O1(2), O2(2)], [O1(3), O2(3)], 'LineWidth', 2);
        else
            error('DrawTools:line2:InvalidDimensions', 'Dimension of O1 and O2 can be only 2x1 or 3x1');
        end
        
        color = [0, 0, 1];
        for i=1:2:nargin-2
            set(h, varargin{i}, varargin{i+1});
            if strcmp(varargin{i}, 'Color')
                color = varargin{i+1};
            end
        end
        % draw the end marker
        normal = O2-O1;
            % determine the corresponding roll an pitch angle
        phi = -atan2(normal(2),normal(3));
        theta = atan2(normal(1),normal(3));
            % creat a cylinder
        t = [1; 0];
        N = 10;
        X = zeros(2, N+1);
        Y = zeros(2, N+1);
        Z = zeros(2, N+1);
        [X,Y,Z] = cylinder(t, N);
            % adjust the cylinder size to be 5% of the arrow length
        l = norm(normal);
        s = .1;
        P1 = [X(1, :)*.5*s*l;...
              Y(1, :)*.5*s*l;...
              Z(1, :)*.5*s*l];
        P2 = [X(2, :)*s*l;...
              Y(2, :)*s*l;...
              Z(2, :)*s*l];
            % apply the rotation to the cylinder
        n = length(X(1, :));
        R = Yrotation(theta)*Xrotation(phi);
        P1 = [     R     , O2;...
               zeros(1,3),  1]*[P1; ones(1,n)];
        P2 = [     R     , O2;...
               zeros(1,3),  1]*[P2; ones(1,n)];
        X = [P1(1, :);...
             P2(1, :)];
        Y = [P1(2, :);...
             P2(2, :)];
        Z = [P1(3, :);...
             P2(3, :)];
            % draw the cylinder as a terminal marker
        hold on
        %surf(X,Y,Z, 'EdgeColor', 'None', 'FaceColor', color);
%         maxi = max([max(O1), max(O2) max(P2) max(P1)]);
%         lim = maxi*1.1;
%         xlim([-lim lim])
%         ylim([-lim lim])
%         zlim([-lim lim])
%         xlabel('x')
%         ylabel('y')
%         zlabel('z')
%         grid
    end

end
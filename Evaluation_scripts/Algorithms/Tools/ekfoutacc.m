function [ Ha, Hg] = ekfoutacc( qatt, acc_m, g)
%EKFOUTACC Summary of this function goes here
%   Detailed explanation goes here
s = qatt(1); x = qatt(2); y = qatt(3); z = qatt(4);
a1 = acc_m(1); a2 = acc_m(2); a3 = acc_m(3);

Ha = [-z*a3+(-y+x)*a2   -x*a3+(-z+s)*a2     x*a3-s*a2-z*a2      (-s-2*z)*a3+(-x-y)*a2;
      (y-x)*a1-z*a3     (z-s)*a1-y*3        (s+z)*a1+(2*y-x)*a3 (x+y)*a1+(2*z+s)*a3;
      z*a2+z*a1         y*a2+(-y+z*x)*a1    (-2*y+x)*a2-x*a1    (-2*z+s)*a2+(s+2*z)*a1];
  
Hg = [z     y       2*y+x   2*z+s;
      z     -y-2*x  -x      s-2*z;
      -y-x  -z-s    -s+z    -x+y]*2*g;
end


function [ p_out ] = QuaternionProjection( p_in, q_in )
%QUATERNIONPROJECTION Projection body to inertial with direct quaternion
%rotation.
%   p_in: quaternion to project
%   q_in: rotation quaternion

q_in_inv = [q_in(1); q_in(2:4)];

p_out = QuaternionProduct(q_in, QuaternionProduct(p_in, q_in_inv));

end


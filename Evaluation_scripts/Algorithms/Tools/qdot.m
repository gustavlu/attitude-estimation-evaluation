function [ qdot_out] = qdot(omega,qatt)
% Calculate time derivative of unit quaternion using angular rates
%
% Input:
% omega - angular rates 3D
% q - unit quaternion
p = omega(1);
q = omega(2);
r = omega(3);


omega_mat = [0  -p  -q  -r;
             p  0   -r   -q;
             q  r  0   -p;
             r  -q   p  0];

qdot_out = 0.5*omega_mat*qatt;      



end


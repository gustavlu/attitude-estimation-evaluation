function [X] = maxcov(P)

% [X] = maxcov(P);
%--------------------------------------------------------------------------
% Pk = P(:,:,k) est un ensemble de matrices Pk semi-definies positives
% 
% X est un majorant semi-defini positif des Pk
%--------------------------------------------------------------------------

[nX,~,nC]=size(P);

X = squeeze(P(1:nX,1:nX,1));

for kC = 2:nC
    P2 = squeeze(P(1:nX,1:nX,kC));
    [V,~] = eig(X,P2);
    Vi	= inv(V);
    S   = max(V'*X*V,V'*P2*V);
    X	= Vi'*S*Vi;
end;


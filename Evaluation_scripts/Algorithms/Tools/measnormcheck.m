function [b_acc_norm, b_mag_norm, l_acc, l_mag, S_acc, S_mag, d_acc, d_mag] = measnormcheck(EKF,measurements)
%#codegen
acc = measurements(1:3);
mag = measurements(4:6);

cov_u = EKF.Rm;
u_ref = EKF.mag_ref;
l_det = EKF.stats.l_det_1_3sig;

d_mag = norm(mag)-norm(u_ref);
ddeltadu = mag'/norm(mag);
% ddeltadu = u_ref'/norm(u_ref);
S_mag = ddeltadu*cov_u*ddeltadu';
l_delta_mag = d_mag^2/S_mag;
l_mag = l_delta_mag;

b_mag_norm = double(l_delta_mag > l_det);

cov_u = EKF.Ra;
u_ref = EKF.gvec;
l_det = EKF.stats.l_det_1_3sig;

d_acc = norm(acc)-norm(u_ref);
ddeltadu = acc'/norm(acc);
% ddeltadu = u_ref'/norm(u_ref);
S_acc = ddeltadu*cov_u*ddeltadu';
l_delta_acc = d_acc^2/S_acc;
l_acc = l_delta_acc;
b_acc_norm = double(l_delta_acc > l_det);


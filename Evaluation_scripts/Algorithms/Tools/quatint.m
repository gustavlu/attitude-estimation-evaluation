function [ qhat_out ] = quatint( qhat_in, omegahat, dt)
%QUATINT Summary of this function goes here
%   Detailed explanation goes here
    Omx = [0 -omegahat'; 
           omegahat, -skew(omegahat)];
    qdot = 0.5*Omx*qhat_in;
    qhat_out = qhat_in + qdot*dt;
    qhat_out = qhat_out/norm(qhat_out);

end


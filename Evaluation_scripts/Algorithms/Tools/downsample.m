function y_out = downsample(y_in, N)
% function downsample
% Downsample data with an integer factor
if size(y_in,1) > size(y_in,2)
    y_out = y_in(1:N:end,:);
else
    y_out = y_in(:,1:N:end);
end
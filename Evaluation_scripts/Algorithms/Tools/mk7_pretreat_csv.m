function [ IMU, AUX] = mk7_pretreat_csv( evaldir, evalfile, batchtype )
%MK7_PRETREAT_CSV Summary of this function goes here
% Pre-treatmeant ulog csv files

if batchtype == 1
    % IMU structure
    filer = fullfile(evaldir,[evalfile, '_sensor_combined_0.csv']);
    % f1 = fopen(filer);
    % tline = fgetl(f1);
    % header_imu_raw = regexp(tline,',','split');
    
    data = csvread(filer,1,0);
    
    IMU.time = (data(:,1)-data(1,1))/1e6;
    IMU.timestamp = data(:,1);
    % Gyro [rad/s]
    IMU.p = data(:,2);
    IMU.q = data(:,3);
    IMU.r = data(:,4);
    IMU.gyro_dt  = data(:,5); % Gyro integral dt
    
    % Acc [m/s]
    IMU.acc_rel_time = data(:, 6);
    IMU.ax = data(:,7);
    IMU.ay = data(:,8);
    IMU.az = data(:,9);
    IMU.acc_dt = data(:,10);
    
    % Mag [Gauss]
    IMU.mag_rel_time = data(:,11);
    % [Y,I] = sort(X,DIM,MODE)
    IMU.mx = data(:,12);
    IMU.my = data(:,13);
    IMU.mz = data(:,14);
    
    % Baro
    IMU.baro_rel_time = data(:,15);
    IMU.baro_alt = data(:,16);
    IMU.baro_temp = data(:,17);
    
elseif batchtype == 2
    
    teststruct = importdata(fullfile(evaldir,evalfile));
    data = teststruct.data;
    time_raw = (data(:,2)-data(1,2))/1e9;
    isort = diff(time_raw) ~= 0; % Remove doubles
    
    IMU.time = time_raw(isort);
    IMU.timestamp = data(isort,2);
    
    % Gyro [rad/s]
    IMU.p = data(isort,18);
    IMU.q = data(isort,19);
    IMU.r = data(isort,20);
    
    % Acc [m/s]
    IMU.ax = data(isort,15);
    IMU.ay = data(isort,16);
    IMU.az = data(isort,17);
    
    % Mag [Gauss]
    IMU.mx = data(isort,21);
    IMU.my = data(isort,22);
    IMU.mz = data(isort,23);
    
    tmp_rpy_ot = [data(isort,9),-data(isort,10), -data(isort,11)];
    tmp_rpy_px4 = [data(isort,12),data(isort,13), data(isort,14)];
    tmp_xyz_ot = [data(isort,3),data(isort,4), data(isort,5)];
    tmp_vxyz_ot = [data(isort,6),data(isort,7), data(isort,8)];
    
    isort_ot = diff(tmp_rpy_ot(:,1)) ~= 0; % Remove doubles
    isort_px4 = diff(tmp_rpy_px4(:,1)) ~= 0; % Remove doubles
    
    AUX.RPY_OT = tmp_rpy_ot(isort_ot,:);
    AUX.RPY_px4 = tmp_rpy_px4(isort_px4,:);
    
    AUX.V_XYZ_OT = tmp_vxyz_ot(isort_ot,:);
    AUX.XYZ_OT = tmp_xyz_ot(isort_ot,:);
    AUX.time_px4 = IMU.time(isort_px4);
    AUX.time_ot = IMU.time(isort_ot);
    
    
elseif batchtype == 3
    
    teststruct = importdata(fullfile(evaldir,evalfile));
    data = teststruct.data;
    time_raw = (data(:,2)-data(1,2))/1e9;
    isort = diff(time_raw) ~= 0; % Remove doubles
    
    IMU.time = time_raw(isort);
    IMU.timestamp = data(isort,2);
    tend = IMU.time(end);
    IMU.time_old = IMU.time;
    IMU.time = (0:1/250:tend)'; % redefine time scale
    
    tmp_rpy_ot = [data(isort,9),-data(isort,10), -data(isort,11)];
    tmp_rpy_px4 = [data(isort,12),data(isort,13), data(isort,14)];
    tmp_xyz_ot = [data(isort,3),data(isort,4), data(isort,5)];
    
    isort_ot = diff(tmp_rpy_ot(:,1)) ~= 0; % Remove doubles
    isort_px4 = diff(tmp_rpy_px4(:,1)) ~= 0; % Remove doubles
    
    AUX.RPY_OT = tmp_rpy_ot(isort_ot,:);
    AUX.RPY_px4 = tmp_rpy_px4(isort_px4,:);
    
    AUX.XYZ_OT = tmp_xyz_ot(isort_ot,:);
    AUX.time_px4 = IMU.time_old(isort_px4);
    AUX.time_ot = IMU.time_old(isort_ot);
    
    % Interpolate data to fixed timescale
    % Gyro [rad/s]
    IMU.p = interp1(IMU.time_old,data(isort,18),IMU.time,'pchip');
    IMU.q = interp1(IMU.time_old,data(isort,19),IMU.time,'pchip');
    IMU.r = interp1(IMU.time_old,data(isort,20),IMU.time,'pchip');
    
    % Acc [m/s]
    IMU.ax = interp1(IMU.time_old,data(isort,15),IMU.time,'pchip');
    IMU.ay = interp1(IMU.time_old,data(isort,16),IMU.time,'pchip');
    IMU.az = interp1(IMU.time_old,data(isort,17),IMU.time,'pchip');
    
    % Mag [Gauss]
    IMU.mx = interp1(IMU.time_old,data(isort,21),IMU.time,'pchip');
    IMU.my = interp1(IMU.time_old,data(isort,22),IMU.time,'pchip');
    IMU.mz = interp1(IMU.time_old,data(isort,23),IMU.time,'pchip');
    
end


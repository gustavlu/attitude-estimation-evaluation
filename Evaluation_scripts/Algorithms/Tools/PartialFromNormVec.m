function [ dvnormdv ] = PartialFromNormVec( v_in )
%PARTIALFROMNORMVEC Calculates partial derivatives of normalized 3D vector
%w.r.t nominal vector.
%   Used for calculating the covariance of a normalized vector.

v1 = v_in(1); v2 = v_in(2); v3 = v_in(3);

dvnormdv = eye(3)/norm(v_in) - [v1^2   v1*v2       v1*v3;
            v1*v2       v2^2   v2*v3;
            v1*v3       v2*v3  v3^2]/(norm(v_in)^3);
end


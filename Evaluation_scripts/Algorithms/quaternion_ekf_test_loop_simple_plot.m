% quaternion_ekf_test_loop_simple_plot

%     % Euler plot
%     fig_eul= figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
% %     subplot(311),title(filt_title)
%     ylabvec = {'$\phi\,[^{\circ}]$','$\theta\,[^{\circ}]$','$\psi\,[^{\circ}]$'};
%     legvec = {'$x_{real}$','$\hat{x}$','$\hat{x}\pm 3\sigma$'};
%     for k = 1:3
%         se(k) = subplot(3,1,k);hold on, 
%         axis([0, t(end), -1.5*abs(min(eulerhatout(k,indplot)-3*sigma_euler_out(k,indplot)))*r2d,1.5*max(eulerhatout(k,indplot)+3*sigma_euler_out(k,indplot))*r2d])
%         plot(tplot,eulerout(k,indplot)*r2d,'k-',tplot,eulerhatout(k,indplot)*r2d,'r-','linewidth',1.5);
%         plot(downsample(tplot,ndown),downsample(eulerhatout(k,indplot)+3*sigma_euler_out(k,indplot),ndown)*r2d,'r--','linewidth',1.5) 
%         plot(downsample(tplot,ndown),downsample(eulerhatout(k,indplot)-3*sigma_euler_out(k,indplot),ndown)*r2d,'r--','linewidth',1.5) 
%         %         plotShaded(downsample(tplot,ndown),...
% %             [downsample(eulerout(k,indplot)+3*sigma_euler_out(k,indplot),ndown);...
% %             downsample(eulerout(k,indplot)-3*sigma_euler_out(k,indplot),ndown)]*r2d,'r',0.5)
%         ylabel(ylabvec{k},'interpreter','latex')
%         iplot = intersect(find(t>0),find(t<20));
%         iplot_end = intersect(find(t>60),find(t<80));
%         if k == 1
%             l1 = legend(legvec);
%             set(l1,'interpreter','latex','location','eastoutside','orientation','vertical')
%             pos{1} = get(se(1),'position');
%             funFigureProperty
%             % Zoom axes, beginning
% %             plot([60 75],[0 40],'k--')
%             if resetcase < 1
%                 ax2 = axes('Position',[0.55 0.84 0.15 0.1]); hold on
%                 %             iplot = intersect(find(t>20),find(t<25));
%                 plot(tplot(iplot_end),eulerout(k,iplot_end)*r2d,'k-',tplot(iplot_end),eulerhatout(k,iplot_end)*r2d,'r-','linewidth',1.5);
%                 plot(tplot(iplot_end),(eulerhatout(k,iplot_end)+3*sigma_euler_out(k,iplot_end))*r2d,'r--','linewidth',1.5)
%                 plot(tplot(iplot_end),(eulerhatout(k,iplot_end)-3*sigma_euler_out(k,iplot_end))*r2d,'r--','linewidth',1.5)
%                 %             plot(ones(100,1)*tfaultstart,linspace(0,75,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%                 ylim([-12 12])
%                 %             axis tight
%                 funFigureProperty
%             end
%         end
%         if k == 3
%             %ylim([-100 50])
%             xlabel('Time [s]','interpreter','latex')
%             funFigureProperty
%             % Zoom axes, beginning
% %             plot([60 75],[0 40],'k--')
%             if resetcase < 1
%                 ylim([-150 30])
%                 ax2 = axes('Position',[0.15 0.22 0.15 0.1]); hold on
%                 %             iplot = intersect(find(t>20),find(t<25));
%                 plot(tplot(iplot),eulerout(k,iplot)*r2d,'k-',tplot(iplot),eulerhatout(k,iplot)*r2d,'r-','linewidth',1.5);
%                 plot(tplot(iplot),(eulerhatout(k,iplot)+3*sigma_euler_out(k,iplot))*r2d,'r--','linewidth',1.5)
%                 plot(tplot(iplot),(eulerhatout(k,iplot)-3*sigma_euler_out(k,iplot))*r2d,'r--','linewidth',1.5)
%                 %             plot(ones(100,1)*tfaultstart,linspace(0,75,100),'--','color',[0.2,0.2,0.2],'linewidth',1)
%                 %             set(gca,'xticklabel','')
%                 %             set(gca,'yticklabel','')
%                 ylim([-5 5])
%                 %             axis tight
%                 funFigureProperty
%             end
%         end
%         funFigureProperty
%     end
%     dx = 0.05; dx0 = -0.03;
%     dy = 0.0; dy0 = 0.05;
%     
%     pos{1} = get(se(1),'position');
%     pos{2} = get(se(2),'position');
%     pos{3} = get(se(3),'position');
%     set(se(1),'position',[pos{1}(1:2)+[dx0 dy0] pos{1}(3:4)+[dx dy]])
%     set(se(2),'position',[pos{2}(1:2)+[dx0 dy0] pos{1}(3:4)+[dx dy]])
%     set(se(3),'position',[pos{3}(1:2)+[dx0 dy0] pos{1}(3:4)+[dx dy]])
%     
%     linkaxes(se,'x')
%%    
%     export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',...
%     ['Eulerplot_',num2str(ke),'_',num2str(accfaultcase),...
%     '_',num2str(magfaultcase),'_',num2str(b_force_ok),num2str(b_force_nok)]),'-pdf',fig_eul)


%     subplot(4,1,4), hold on, axis([0 t(end) 0 180])
%     plot(tplot, norm_error_euler_out(indplot)*r2d)
%     funFigureProperty
%     
    %% Quaternion
%         indplot = (1:numel(t));
%         tplot = t(indplot);
%         ndown = 1;
%         % Quaternion plot
%         figure,
%         subplot(511),title([filt_title,' Quaternion'])
%         for k = 1:4
%             subplot(5,1,k),hold on, axis auto %axis([0 t(end) -2 2])
%             plot(tplot,xhat_out(k,indplot),tplot,xhat_out(k,indplot))
% %             plotShaded(downsample(tplot,ndown),...
% %                 [downsample(q_out(k,indplot)+3*qsig_out(k,indplot),ndown);...
% %                 downsample(q_out(k,indplot)-3*qsig_out(k,indplot),ndown)],'r',0.5)
%         end
%         subplot(5,1,5)
%         plot(tplot, norm_error_qhat_out(indplot))
    
    %%
%     figure,
%     subplot(411),title(['Bias estimation, ',filt_title])
%     for k = 1:3
%         subplot(4,1,k),hold on, axis([0 t(end) -EKF.bsat.bmax*2*r2d EKF.bsat.bmax*2*r2d])
%         plot(tplot,xhat_out(k+4,indplot)*r2d,tplot,bias_out(k,indplot)*r2d)
%         plotShaded(downsample(tplot,ndown),...
%             [downsample(bias_out(k,indplot)+3*sigma_bias_out(k,indplot),ndown);...
%             downsample(bias_out(k,indplot)-3*sigma_bias_out(k,indplot),ndown)]*r2d,'r',0.5)
%     end
%     subplot(4,1,4), axis([0 t(end) 0 EKF.bsat.bmax*2*r2d])
%     plot(tplot, norm_error_bhat_out(indplot)*r2d)
    
    % subplot(3,1,1),hold on, axis tight
    % plot(tplot,eulerout(2,indplot)*r2d,tplot,eulerhatout(2,indplot)*r2d)
    % plotShaded(tplot,[eulerhatout(2,indplot)+3*sigma_euler(2,indplot); eulerhatout(2,indplot)-3*sigma_euler(2,indplot)]*r2d,'r',0.5)
    % subplot(3,1,1),hold on, axis tight
    % plot(tplot,eulerout(1,indplot)*r2d,tplot,eulerhatout(1,indplot)*r2d)
    % plotShaded(tplot,[eulerhatout(1,indplot)+3*sigma_euler(1,indplot); eulerhatout(3,indplot)-3*sigma_euler(1,indplot)]*r2d,'r',0.5)
    
%{    %% Mag measurements
%     figure,
%     subplot(311), plot( t, magm_out(1,:), t, mag_out(1,:), t, mhat_out(1,:), t, mc_out(1,:));
%     subplot(312), plot( t, magm_out(2,:), t, mag_out(2,:), t, mhat_out(2,:), t, mc_out(2,:));
%     subplot(313), plot( t, magm_out(3,:), t, mag_out(3,:), t, mhat_out(3,:), t, mc_out(3,:));
%     legend('mag_m', 'mag_r_e_a_l', 'mag_e_s_t', 'mag_c')
    
    %% Mag measurements
%     figure,
%     subplot(311), plot( t, magm_out(1,:), t, mag_out(1,:), t, mhat_out(1,:));
%     subplot(312), plot( t, magm_out(2,:), t, mag_out(2,:), t, mhat_out(2,:));
%     subplot(313), plot( t, magm_out(3,:), t, mag_out(3,:), t, mhat_out(3,:));
%     legend('mag_m', 'mag_r_e_a_l', 'mag_e_s_t')
    %% Acc measurements
%     figure
%     subplot(311), plot( t, accm_out(1,:), t, acc_out(1,:), t, ahat_out(1,:), t, ac_out(1,:));
%     subplot(312), plot( t, accm_out(2,:), t, acc_out(2,:), t, ahat_out(2,:), t, ac_out(2,:));
%     subplot(313), plot( t, accm_out(3,:), t, acc_out(3,:), t, ahat_out(3,:), t, ac_out(3,:))
%     
    %% Acc measurements
%     figure
%     subplot(311), plot( t, accm_out(1,:), t, acc_out(1,:), t, ahat_out(1,:));
%     subplot(312), plot( t, accm_out(2,:), t, acc_out(2,:), t, ahat_out(2,:));
%     subplot(313), plot( t, accm_out(3,:), t, acc_out(3,:), t, ahat_out(3,:));
%     legend('acc_m', 'acc_r_e_a_l', 'acc_e_s_t')
%     
    %% Calculate norm difference
    
    %% Mag and Acc estimates for Martin/Sarras
%     if ke == 6
%         figure,
%         subplot(311),
%         plot(t,xhat_ms_out(1,:), t, accm_out(1,:)),
%         subplot(312),
%         plot(t,xhat_ms_out(2,:), t, accm_out(2,:)),
%         subplot(313),
%         plot(t,xhat_ms_out(3,:), t, accm_out(3,:))
%         
%         figure,
%         subplot(311),
%         plot(t,xhat_ms_out(4,:), t, magm_out(1,:)),
%         subplot(312),
%         plot(t,xhat_ms_out(5,:), t, magm_out(2,:)),
%         subplot(313),
%         plot(t,xhat_ms_out(6,:), t, magm_out(3,:))
%     end
    
    %% Plotty
    % GLR statistic
    % figure,
    % subplot(211)
    % plot(t, GLRvec)
    % title('GLR statistic')
    % hold on
    % plot(t, chi2vec)
    % plot(t, ones(size(t))*GLRparams.GLRthr)
    % legend({'GLR','\chi^2'},'Location', 'Best'),
    % subplot(212)
    % plot(t, qvec)
    % title('Change detection date')
    
    %%
    % Compare GLR and X2 statistics
%     figstat = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
%     title('GLR statistic')
%     cnt = 1;
%     for k = [1 3 5]
%         subplot(3,2,k), hold on
%         plot(t, GLRvec(cnt,:))
%         plot(t, chi2vec(cnt,:))
%         plot(t, ones(size(t))*GLRparams_1.GLRthr)
%         plot(t, ones(size(t))*GLRparams_3.GLRthr)
%         legend('l_{GLR}','l_{\chi^2}','l_{det, 1}','l_{det, 3}')
%         title(titlevec_acc(cnt))
%         subplot(3,2,k+1), hold on
%         plot(t, GLRvec(cnt+3,:))
%         plot(t, chi2vec(cnt+3,:))
%         plot(t, ones(size(t))*GLRparams_1.GLRthr)
%         plot(t, ones(size(t))*GLRparams_3.GLRthr)
%         legend('l_{GLR}','l_{\chi^2}','l_{det, 1}','l_{det, 3}')
%         title(titlevec_mag(cnt))
%         cnt = cnt+1;
%     end
    
    %%
%        figstat = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
%     title('GLR Fault date')
%     cnt = 1;
%     for k = [1 3 5]
%         subplot(3,2,k), hold on
%         plot(t, qdetvec(cnt,:))
%         legend('q_{GLR}','l_{\chi^2}')
%         title(titlevec_acc(cnt))
%         subplot(3,2,k+1), hold on
%         plot(t, qdetvec(cnt+3,:))
%         legend('q_{GLR}','l_{\chi^2}')
%         title(titlevec_mag(cnt))
%         cnt = cnt+1;
%     end
    
    %% Fault detection booleans
%     figbool = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
%     cnt = 1;
%     for k=[1 3 5]
%         subplot(3,2,k),
%         plot(t, acc_status_out(cnt,:),'*')
%         title(titlevec_acc(cnt))
%         subplot(3,2,k+1),
%         plot(t, mag_status_out(cnt,:),'*')
%         title(titlevec_mag(cnt))
%         cnt = cnt+1;
%     end
    

%% Bias estimation plot
% figure,
% for klm = 1:3
% subplot(3,1,klm), hold on,
% plot(t, bhat_out(klm,:)*r2d)
% plot(t, bias_lp_out(klm,:)*r2d)
% plot(t, ones(size(t))*EKF.bsat.bmax*r2d,'k--')
% plot(t, -ones(size(t))*EKF.bsat.bmax*r2d,'k--')
% legend({'bhat_{KF}','bhat_{gyro}'})
% end

%% Bias estimation comparison - MEASUREMENTS
figbias_meas = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
fonty = 24;
% Magnetometer measurement
sh(1) = subplot(311); hold on, grid on

colmat = [0.5,0,0; 0, 0.5, 0; 0, 0, 0.5];
linevec = {'k--','k-.','k:'};
for k = 1:3
    plot( tplot, magm_out(k,indplot),'-',...
        tplot, mag_out(k,indplot),linevec{k}, 'linewidth',2); xlim([0 tplot(end)]);
end
legend({'$m_{m,x}$','$m_{x}$','$m_{m,y}$','$m_{y}$','$m_{m,z}$','$m_{z}$'},'interpreter','latex', 'fontsize',20, 'location','northeastoutside')
ylabel('$\mathbf{m}\,[G]$','interpreter', 'latex','fontsize', fonty)
% set(gca,'xticklabel',[])
funFigureProperty,
shpos1 = get(gca,'Position');

sh(2) = subplot(312); hold on, grid on
for k = 1:3
    plot( tplot, accm_out(k,indplot),'-', tplot, acc_out(k,indplot),linevec{k}, 'linewidth',2); xlim([0 tplot(end)]);
end
legend({'$a_{m,x}$','$a_{x}$','$a_{m,y}$','$a_{y}$','$a_{m,z}$','$a_{z}$'},'interpreter','latex', 'fontsize',20, 'location','northeastoutside')
ylabel('$\mathbf{a}\,[m/s^2]$','interpreter', 'latex','fontsize', fonty)
% set(gca,'xticklabel',[])
funFigureProperty,
shpos2 = get(gca,'Position');
set(gca,'Position',[shpos1(1), shpos2(2), shpos1(3:4)])

sh(3) = subplot(313); hold on, grid on
for k = 1:3
    plot( tplot, gyro_out(k,indplot)*r2d,'-', tplot, omegareal(k,indplot)*r2d,linevec{k}, 'linewidth',2); xlim([0 tplot(end)]);
end
legend({'$\omega_{m,x}$','$\omega_{x}$','$\omega_{m,y}$','$\omega_{y}$','$\omega_{m,z}$','$\omega_{z}$'},'interpreter','latex', 'fontsize',20, 'location','northeastoutside')
ylabel('$\mathbf{\omega}\,[^{\circ}/s]$','interpreter', 'latex','fontsize', fonty)
% set(gca,'xticklabel',[])
xlabel('Time [s]','interpreter', 'latex','fontsize', fonty)
funFigureProperty,
shpos3 = get(gca,'Position');
set(gca,'Position',[shpos1(1), shpos3(2), shpos1(3:4)])

dypos = 0.04;
dxpos = 0.03;
set(sh(1),'Position',[shpos1(1)-dxpos, shpos1(2)+dypos, shpos1(3:4)+[3*dxpos 0]])
set(sh(2),'Position',[shpos1(1)-dxpos, shpos2(2)+dypos, shpos1(3:4)+[3*dxpos 0]])
set(sh(3),'Position',[shpos1(1)-dxpos, shpos3(2)+dypos, shpos1(3:4)+[3*dxpos 0]])

% export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',...
%     ['Sim_mag_omega_biasest_meas_',num2str(scen),'_',num2str(accfaultcase),...
%     '_',num2str(magfaultcase),'_',conf_string]),'-pdf',figbias_meas)

%% Bias estimation comparison - BIAS PLOT
% figbias_bias = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
% 
% % shpos2 = get(gca,'Position');
% % set(gca,'Position',[shpos1(1), shpos2(2), shpos1(3:4)]);
% %sh(2).Position = sh(2).Position + [0 0 -0.008 0];
% % shpos3 = get(gca,'Position');
% yleglab = {'$b_{\omega,x}\,[^{\circ}/s]$','$b_{\omega,y}\,[^{\circ}/s]$','$b_{\omega,z}\,[^{\circ}/s]$'};
% for k = 1:3
%     sh(k) = subplot(3,1,k); hold on, grid on
%     shpos{k} = get(gca,'Position');
%     plot( tplot, xhat_out(k+4,indplot)*r2d,'-',...
%           tplot, bias_lp_out(k,indplot)*r2d,'-',...
%           downsample(tplot,500), downsample(ext_bias_out(k,indplot)*r2d,500),'--*',...
%           tplot, bias_out(k,indplot)*r2d,'r--',...
%           'linewidth',2); xlim([0 tplot(end)]);
%     ylabel(yleglab(k),'interpreter', 'latex','fontsize', fonty)
%     ylim([-min(abs(om_bias(k)*2),1.1*abs(min(xhat_out(k+4,indplot)))),...
%            max(abs(om_bias(k)*2),1.1*max(xhat_out(k+4,indplot)))]*r2d);
% %     ylim([min(0,om_bias(k)*2), max(om_bias(k)*2,0)]*r2d)
%     if k==1
% %         legend({'$\hat{b}_x$','$\hat{b}_{LP,x}$', '$\hat{b}_{c}$','$b_{z}$' },'interpreter','latex', 'fontsize',20, 'location','northeastoutside')
%     end
%     if k == 2
%         legend({'$\hat{b}_{\omega}$','$\hat{b}_{\omega,LP}$', '$\hat{b}_{\omega,c}$','$b_{\omega}$' },'interpreter','latex', 'fontsize',20, 'location','northeastoutside')
%     end
%     if k == 3
%         xlabel('Time [s]','interpreter', 'latex','fontsize', fonty)
%     end
%     funFigureProperty
% end
% dypos = 0.04;
% dxpos = 0.03;
% 
% set(sh(1),'Position',[shpos{2}(1)-dxpos, shpos{1}(2)+dypos, shpos{2}(3:4)])
% set(sh(2),'Position',[shpos{2}(1)-dxpos, shpos{2}(2)+dypos, shpos{2}(3:4)])
% set(sh(3),'Position',[shpos{2}(1)-dxpos, shpos{3}(2)+dypos, shpos{2}(3:4)])
% 
% 
% set(gcf, 'Color', 'w');

% export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',...
%     ['Sim_mag_omega_biasest_bias_',num2str(scen),'_',num2str(accfaultcase),...
%     '_',num2str(magfaultcase),'_',conf_string]),'-pdf',figbias_bias)

%% Diagonal elements of Phat
% figure, plot(t, qsig_out)
% title(['Diag(Phat), ', filt_title])

%% Gain norm
% figure, plot(t, Knorm_out)
% title(['||K||, ', filt_title])


%% Compare euler angles and error between filters
% if b_plot_comp
% figure(figcompeuler), hold on,
% for k = 1:3
%     subplot(3,1,k); hold on; axis([0 t(end) -180 180])
%     plot(tplot, eulerhatout(k,indplot)*r2d,'Linewidth',1)
%     legend(ke_legend,'Orientation','horizontal','location','north')
% end
% subplot(4,1,4), hold on,
% plot(tplot, norm_error_euler_out(indplot)*r2d,'Linewidth',1)
% legend(ke_legend,'Orientation','horizontal','location','north')

%% Bias illustration for introduction
% figintro_bias = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');

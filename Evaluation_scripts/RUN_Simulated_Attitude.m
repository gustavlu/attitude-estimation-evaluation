clear
warning('off','all')
rng(0);
InitUavParameters_mod_X16_LinAcc_Sim

clk = fix(clock);
time_str_short = ['-', num2str(clk(1)), '-' ,num2str(clk(2)), '-', ...
    num2str(clk(3)), '-', num2str(clk(4)), '-', num2str(clk(5))];

% ke_vec = [1 4 12 5 8 11 ]; % Filters to be simulated
% ke_vec = [2 1 5 11 4 8]; % Filters to be simulated
% ke_vec = [13 13 13 13]; % Filters to be simulated
% ke_vec = [8 13 13 13];
ke_vec = [8];
% ke_vec = [7 13 13 13];
% ke_vec = [8 8 8 8 8];
% ke_vec = [4 4 4 4 4];
nfilt = numel(ke_vec);
nsims = 1;

b_force_ok = 0; b_force_nok = 0;
% MC saved variables
inc_error_mc_out = zeros(nsims,nfilt);
head_error_mc_out = zeros(nsims,nfilt);
inc_error_max_mc_out = zeros(nsims,nfilt);
head_error_max_mc_out = zeros(nsims,nfilt);
tic
b_plot_comp = 0;
b_plot_mc = 0;



if b_plot_mc, figmcquat = figure; end
hsims = waitbar(0,['/',num2str(nsims*nfilt),' Simulations done...']);

ntmp = sum(ke_vec == 13);
ncov_vec = round(linspace(1,3,ntmp));
delta_ang_vec = round(linspace(30,180,nfilt));
dist_delta_ang = [rand(1,2), 0];
dist_delta_ang = [ 0 0 1];
reset_ang_vec = round(linspace(30,180,nsims)); 
reset_ang = 150;
lpg_vec = [0 1 0 1 0 1];
accfvec = [2 2 0 0 2 2];
magfvec = [0 0 2 2 2 2];

cnt = 0;
for klm = 1:nsims
%     reset_ang = reset_ang_vec(klm);
nx = 7; % state dimension
r2d = 180/pi;
d2r = pi/180;
    
dt = 1/100; % time step
dt_acc = dt;
dt_mag = dt;
tend = 50;
t = 0:dt:tend; % time vector
    
% Simulation flags
b_glr = 0;  % Enable fault detection by GLR
b_glr_attreset = 0; % Enable attitude reset on fault detection
b_lpgyro = 1; % Enable low pass filter of gyro for bias estimation
b_useftarch = 1;

biascase = 1; % Bias amplitude flag
biasfactor = 1; % Bias multi factor
b_accoffset = 0;
b_magoffset = 0;
resetcase = 0; % Mid simu. attitude reset

% b_lpgyro = lpg_vec(klm);
% magfaultcase = magfvec(klm);
% accfaultcase = accfvec(klm);

conf_string = [num2str(b_useftarch),num2str(b_glr),num2str(b_lpgyro),'_',num2str(b_force_ok),num2str(b_force_nok)];
% GLR Parameters
b_useglr_vec = [0, 0, 0, 1, 1, 0]; % [1/4]: Norm, [2/5]: Outlier, [3/6]: PM
L = 50;
% PFA_GLR = 1e-5;
PFA_GLR = 1-0.9967;   % 3 sigma
% PFA_GLR = 1-0.99;
% PFA_chi2 = PFA_GLR;
PFA_chi2 = 1-0.9967;   % 3 sigma
ndt_glr = 1;
dt_glr = dt*ndt_glr;

e1 = [1;0;0];
e2 = [0;1;0];
e3 = [0;0;1];

% References
gvec = e3*-9.81;
% mag_ref = [0.5; 0.2; -1];
% gvec = -e3;
mag_ref = 0.2*e1 + 0.5*e2 + 0.4*e3;
% mag_ref = e1+e3;
% gvec = gvec/norm(gvec);
% mag_ref = mag_ref/norm(mag_ref);

EKF.mag_ref = mag_ref;
EKF.gvec = gvec;

% First order perf. model
tau_fb_acc = 3;
EKF.accest.tau = tau_fb_acc;
EKF.accest.k_fb = 1/tau_fb_acc;
EKF.kcov_perf = 100;

tau_fb_mag = 3;
EKF.magest.tau = tau_fb_mag;
EKF.magest.k_fb = 1/tau_fb_mag;
EKF.kcov_mag_perf = 100;


EKF.partq.taupsi = 5/3;
EKF.partq.taualpha = 5/3;
EKF.partq.taupsi_bias = 300/3;
EKF.partq.taualpha_bias = 300/3;
EKF.partq.dt_gain_mag = dt;
EKF.partq.dt_gain_acc = dt;

EKF.bsat.bmax = 1*d2r;
EKF.bsat.tau = dt;
EKF.bsat.kaw = 1-exp(-dt/(EKF.bsat.tau));
EKF.bsat.kaw_sto = EKF.bsat.kaw;
EKF.magest.k_fb = 0;
EKF.accest.k_fb = 0;


% Noise case
bnoise = 1;
bcone = 0; % Coning simulation for gyro
noisecase = 2;
switch noisecase
    case 1 % Noise powers from Martin 2017
        omegasignoise = sqrt(2e-7); % gyroscope noise std
        meassignoise = sqrt(2e-6); % acc, mag, meas noise
    case 2 % Realistic MEMS noise
        omegasignoise = 10/3*d2r; % gyroscope noise std
        meassignoise = 1/3/3; % meas noise std
    case 3 % Realistic MEMS noise - Low
        omegasignoise = 1/3*d2r; % gyroscope noise std
        meassignoise = 0.1/3/3; % meas noise std
%         meassignoise = 1; % meas noise std
end
accsignoise = norm(EKF.gvec)*meassignoise;
magsignoise = norm(EKF.mag_ref)*meassignoise;

% Measurement covariances
Ra = eye(3)*accsignoise^2;
Rm = eye(3)*magsignoise^2;
cov_gyro = ones(3,1)*omegasignoise.^2;
bGyr_max = 1*pi/180; T_bGyr = 1;
cov_bgyro = (bGyr_max*[1;1;1]/3/sqrt(T_bGyr)).^2;
EKF.Ra = Ra;
EKF.Rm = Rm;
EKF.cov.gyro = ones(3,1)*omegasignoise^2;

h0 = 0.1*100;
amp = 200*d2r;
scen = 5;
switch scen
    case 1
        % Full rotation
        omega_t = amp*[sin(5*h0/r2d*t); sin(20*h0/r2d*t); sin(2*h0/r2d*t)];
    case 2
        % Roll only
        omega_t = amp*[ones(size(t)); zeros(size(t)); zeros(size(t))];
    case 3
        % Pitch only
        omega_t = amp*[zeros(size(t)); ones(size(t)); zeros(size(t))];
        %         omega_t = amp*[zeros(size(t)); sin(20*h0/r2d*t); zeros(size(t))];
    case 4
        % Yaw only
        omega_t = amp*[zeros(size(t)); zeros(size(t)); ones(size(t))];
    case 5
        % No rotation
        omega_t = amp*[zeros(size(t)); zeros(size(t)); zeros(size(t))];
    case 6
        % Full rotation
        omega_t = amp*[sin(pi/(2+rand*2)*t); cos(pi/(2+rand*2)*t); sin(pi/(2+rand*2)*t)];
%         omega_t = 1*d2r*[ones(size(t)); ones(size(t)); zeros(size(t))];
    case 7
        omega_t = [(2*pi)/(tend*0.25)*ones(size(t)); (2*pi)/(tend*0.25)*ones(size(t)); zeros(size(t))];
        omega_t(2,round(numel(t)*0.25):end) = 0;
        omega_t(1,1:round(numel(t)*0.25)) = 0;
        omega_t(1,round(numel(t)*0.5):end) = 0;
    case 10
        omega_t = [avionic.IMU.p';
                   avionic.IMU.q';
                   avionic.IMU.r'];
end
% omega_t(:,1:round(0.5*numel(t))) = 0;
% omega_t(:,round(0.625*numel(t)):round(0.75*numel(t))) = 0;
% omega_t(:,round(0.875*numel(t)):round(1*numel(t))) = 0;

omegareal = omega_t;
if bnoise
    omega_t = omega_t + bnoise*randn(size(omega_t))*omegasignoise;
end
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% From Kang 2013
% 6  Simulation and test result
% 6.1  Pure coning case
% Our first simulation result is a test for pure coning motion
% such as v = [a*?*cos(?*t) b*?*cos(?*t+?) 0 ]^T (25)
% where a=b= 1 rad,?= 30 Hz,?= 90° and sampling rate is
% 100 Hz. Also, the gyro signals have noise of 1°/h/Hz,
% and bias of 1°/h.

if bcone
    om_cone = 30; % Hz
    dp_cone = 90*d2r;
    a = 0.1;
    b = 0.1;
    c = 0.1;
%     omega_cone = [a*om_cone*cos(om_cone*2*pi*t);
%                   b*om_cone*cos(om_cone*2*pi*t+dp_cone); 
%                   c*om_cone*cos(om_cone*2*pi*t+dp_cone*0.3)];
    omega_cone = [a*om_cone*cos(om_cone*2*pi*t);
                  b*om_cone*cos(om_cone*2*pi*t+dp_cone); 
                  zeros(size(t))];
    omega_t = omega_t + omega_cone;
end
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
tau_bias_mag = 5/3; % Bias buildup time constant
tau_bias_acc = 1/3; % Bias buildup time constant
Gbias_mag = tf([1],[tau_bias_mag 1]); % Bias buildup transfer dunction
Gbias_acc = tf([1],[tau_bias_acc 1]); % Bias buildup transfer dunction

Scenario.t = t;
Scenario.Ts = dt;
Scenario.tsim = tend;

magfaultcase = 2;
% rng(0);
tfaultstart = 0.2*tend;
tfaultend = 0.8*tend;
tfault = 0.2*tend;
nbias = 20; etabias = 0.1; etabiasnorm = 0.5; 
etabiasnormvec = 2*[0.5 1 0.5];
switch magfaultcase
    case 0
        magfault = zeros(3,size(t,2));
    case 1
        % -- Mag. Bias --
%         magfault = [BiasStep([tfaultstart tfaultend], randn(10,1)*norm(mag_ref), Scenario);
%             BiasStep([tfaultstart tfaultend], randn(10,1)*norm(mag_ref), Scenario);
%             BiasStep([tfaultstart tfaultend], randn(10,1)*norm(mag_ref), Scenario)];
        magfault = [BiasStep(tfaultstart+cumsum(abs(randn(nbias,1))*tfaultend*etabias), randn(nbias,1)*norm(mag_ref)*etabiasnorm, Scenario);
            BiasStep(tfaultstart+cumsum(abs(randn(nbias,1))*tfaultend*etabias), randn(nbias,1)*norm(mag_ref)*etabiasnorm, Scenario);
            BiasStep(tfaultstart+cumsum(abs(randn(nbias,1))*tfaultend*etabias), randn(nbias,1)*norm(mag_ref)*etabiasnorm, Scenario)];
        magfault(:,round(tfaultend/dt):end) = 0;
    case 2
        % -- Mag. Sinus --
        magfault_1 = [BiasSinus(tfault, tfault, randn*norm(mag_ref)*etabiasnormvec(1), tfaultstart, Scenario);
                    BiasSinus(tfault, tfault, randn*norm(mag_ref)*etabiasnormvec(2), tfaultstart, Scenario);
                    BiasSinus(tfault, tfault, randn*norm(mag_ref)*etabiasnormvec(3), tfaultstart, Scenario)];
        magfault_2 = [BiasSinus(tfault*rand, tfault*rand, randn*norm(mag_ref)*etabiasnormvec(1)*0.3, tfaultstart, Scenario);
                    BiasSinus(tfault*rand, tfault*rand, randn*norm(mag_ref)*etabiasnormvec(2)*0.3, tfaultstart, Scenario);
                    BiasSinus(tfault*rand, tfault*rand, randn*norm(mag_ref)*etabiasnormvec(3)*0.3, tfaultstart, Scenario)];
        magfault = magfault_1 + magfault_2; 
        magfault(:,round(tfaultend/dt):end) = 0;
end
% magfault(:,1:round(0.250*numel(t))) = 0;
% magfault(:,round(0.375*numel(t)):round(0.750*numel(t))) = 0;
% magfault(:,round(0.875*numel(t)):end) = 0;

% magfault(:,1:round(0.50*numel(t))) = 0;
% magfault(:,round(0.875*numel(t)):end) = 0;

% magfault(:,1:round(0.5*numel(t))) = 0;
% magfault(:,round(0.8*numel(t)):end) = 0;
for k = 1:3
    magfault(k,:) = lsim(ss(Gbias_mag),magfault(k,:),t);
end
% magfault(:,round(tfaultend/tend*numel(t)):end) = 0;

accfaultcase = 0;
rng(0);
tfaultstart = 0*tend;
tfaultend = 1*tend;
tfault = 0.3*tend;
nbias = 5; etabias = 0.1; etabiasnorm = 0.5;
etabiasnormvec = 1*[1 2 1];
switch accfaultcase
    case 0
        accfault = zeros(3,size(t,2));
    case 1
        % -- Acc. Bias --
%         accfault = [BiasStep([tfaultstart tfaultend], randn*norm(gvec)*[1 -1], Scenario);
%             BiasStep([tfaultstart tfaultend], randn*norm(gvec)*[1 -1], Scenario);
%             BiasStep([tfaultstart tfaultend], randn*norm(gvec)*[1 -1], Scenario)];
        accfault = [BiasStep(tfaultstart+cumsum(abs(randn(nbias,1))*tfaultend*etabias), randn(nbias,1)*norm(gvec)*etabiasnorm, Scenario);
            BiasStep(tfaultstart+cumsum(abs(randn(nbias,1))*tfaultend*etabias), randn(nbias,1)*norm(gvec)*etabiasnorm, Scenario);
            BiasStep(tfaultstart+cumsum(abs(randn(nbias,1))*tfaultend*etabias), randn(nbias,1)*norm(gvec)*etabiasnorm, Scenario)];
        accfault(:,round(tfaultend/dt):end) = 0;
    case 2
        % -- Acc. Sinus --
        accfault = [BiasSinus(tfault, tfault, randn*norm(gvec)*etabiasnormvec(1), tfaultstart, Scenario);
                    BiasSinus(tfault, tfault, randn*norm(gvec)*etabiasnormvec(2), tfaultstart, Scenario);
                    BiasSinus(tfault, tfault, randn*norm(gvec)*etabiasnormvec(3), tfaultstart, Scenario)];
        accfault(:,round(tfaultend/dt):end) = 0;
end
% accfault(:,1:round(0.250*numel(t))) = 0;
% accfault(:,round(0.375*numel(t)):round(0.750*numel(t))) = 0;
% accfault(:,round(0.875*numel(t)):end) = 0;

accfault(:,1:round(0.2*numel(t))) = 0;
accfault(:,round(0.7*numel(t)):end) = 0;
for k = 1:3
    accfault(k,:) = lsim(ss(Gbias_acc),accfault(k,:),t);
end
% accfault(3,:) = 0;

euler0 = pi/180*[0;0;randn*30]*1;
% euler0 = pi/180*[1/3;1/3;1/3]*180; % PHI, THETA, PSI
% euler0 = pi/180*[0;0;0]*180; % PHI, THETA, PSI
% euler0 = pi/180*[rand*180;rand*180;rand*180]/4;
q0 = Euler2Quaternion(euler0(1),euler0(2),euler0(3));
q = q0;
R = qua2chrSL(q);

% Init EKF
% eulerhat0 = 180*[0, 0, 0]*d2r; % PHI, THETA, PSI
% eulerhat0 = [euler0(1:2); 0]; % PHI, THETA, PSI
% eulerhat0 = 180*[0, 1/3, 0]*d2r;
% eulerhat0 = [rand;rand;rand]*180*d2r;
% eulerhat0 = euler0 + [rand*30;rand*30;rand*0]*d2r;
eulerhat0 = euler0;
qhat0 = Euler2Quaternion(eulerhat0(1),eulerhat0(2),eulerhat0(3));
% qhat0 = q0; eulerhat0 = euler0;
bhat0 = [0;0;0];
% qhat0=[1;0;0;0];
mrhat_cov = eye(3)*(norm(mag_ref)*0.1/3/sqrt(EKF.magest.tau))^2;
arhat_cov = eye(3)*(norm(gvec)*0.1/3/sqrt(EKF.magest.tau))^2;
bhat_cov = eye(3)*(0.1/3/r2d)^2;
euler_cov = diag((0.*[5/3, 5/3, 5/3]*d2r).^2);
Phat0 = blkdiag(EulerCov2QuaternionCov(eulerhat0,euler_cov), bhat_cov);

xhat_ex = qhat0;
Phat_ex = Phat0(1:4,1:4);

xhat_ms = [EKF.gvec;
    EKF.mag_ref;
    zeros(9,1)];

% Init Batista sensor (decoupled)
xhat_bat0 = [EKF.gvec;
            cross(EKF.gvec,EKF.mag_ref);
            zeros(3,1)];
Phat_bat = blkdiag(arhat_cov, mrhat_cov,bhat_cov);

% Init MEKF
xhat_mekf0 = [qhat0; bhat0];
Phat_mekf0 = blkdiag(euler_cov, bhat_cov);

% Init MEKF-PM Mag
xhat_mekf_pm_mag0 = [qhat0; bhat0; EKF.mag_ref];
Phat_mekf_pm_mag0 = blkdiag(euler_cov, bhat_cov, mrhat_cov);

% Init MEKF-PM Acc Mag
xhat_mekf_pm_acc_mag0 = [qhat0; bhat0; EKF.gvec; EKF.mag_ref];
Phat_mekf_pm_acc_mag0 = blkdiag(euler_cov, bhat_cov, arhat_cov, mrhat_cov);

% Init Sensor EKF
ahat0 = qua2chrSL(qhat0)'*gvec;
Phat_acc0 = Ra;
ahat_p = ahat0; Phat_acc_p = Phat_acc0;
mhat0 = qua2chrSL(qhat0)'*mag_ref;
Phat_mag0 = Rm;
mhat_p = mhat0; Phat_mag_p = Phat_mag0;

% Martin & Sarras init
ahat0 = qua2chrSL(qhat0)'*gvec;
ahat = ahat0; Phat_acc_p = Phat_acc0;
mhat0 = qua2chrSL(qhat0)'*mag_ref;
mhat = mhat0; Phat_mag_p = Phat_mag0;
bhat = zeros(3,1);

% Init comp
ka = 0.6; km = 0.6; kp = 1; ki = 1; kb = 1;
gain = [ka km kp ki kb];

k_a = 2; k_b = 1; l_a = 10; l_b = 10; m_a = 0; m_b = 0;
gain_ms = [k_a, k_b, l_a, l_b, m_a, m_b];

tau_b = 100;
kc = 10;
ka = kc;
km = kc;

lc = kc/2*(3*omegasignoise+1/tau_b);
% lc = 0.01;
la = lc;
lm = lc;

gain_m = [ka km la lm];

tau_a = 1/3; tau_psi = 1/3;
kpsi = 1-exp(-1*dt/tau_a); % Positive correction gain [0, 1];
kalpha = 1-exp(-1*dt/tau_psi); % Positive correction gain [0, 1];
w_bar  = 0.5;
gain_wu = [kpsi; kalpha; w_bar];

cov_in.acc = Ra;
cov_in.mag = Rm;
cov_in.gyro = diag(cov_gyro);

xhat_m = [ahat; mhat; zeros(3,1)];
% ======== INTEGRITY ========
AL_alpha = 15; % Alert limit, inclination, degrees
AL_psi = 10; % Alert limit, heading, degrees

P_ir = 10^-7;
%     P_ir = 1-0.9967;
P_f     = 1-0.9967;	% Probabilité d'apparition des pannes
q_alpha = 2;
q_psi = 1;
% Risk allocation
amin = max([0 ; 1-(1-P_f)/P_ir]);
amax = min([1 ; P_f/P_ir]);
ira_1   = 1-10^(-3);	% Integrity Risk Allocation level 1
%     ira_1   = 0.1;	% Integrity Risk Allocation level 1
ira_2   = 10^(-2);      % Integrity Risk Allocation level 2
alloc_1 = ira_1 * amin + (1-ira_1) * amax;
% Probabilities afetr first allocation level
P_ff = P_ir * (1-alloc_1)/(1-P_f);
P_md = P_ir * alloc_1/P_f;

% Inclination integrity
P_ff_alpha = 2*gammaincinv(P_ff,q_alpha/2,'upper');
% Heading integrity
P_ff_psi = 2*gammaincinv(P_ff,q_psi/2,'upper');

PL_alpha = zeros(size(t));
PL_psi = zeros(size(t));
% =============================

% init saved time series
eulercompout = zeros(3,size(t,2),numel(ke_vec));
eulererrorcompout = zeros(1,size(t,2),numel(ke_vec));
quatcompout = zeros(4,size(t,2),numel(ke_vec));
quaterrorcompout = zeros(4,size(t,2),numel(ke_vec));

eulerhatout = zeros(3,size(t,2));
eulerhatexout = zeros(3,size(t,2));
eulerout = zeros(3,size(t,2));
sigma_euler_out = zeros(3,size(t,2));
sigma_euler_ex_out = zeros(3,size(t,2));
sigma_bias_out = zeros(3,size(t,2));

ahat_out = zeros(3,size(t,2));
sigma_ahat_out = zeros(3,size(t,2));
mhat_out = zeros(3,size(t,2));
sigma_mhat_out = zeros(3,size(t,2));

acc_status_out = zeros(3,size(t,2));
mag_status_out = zeros(3,size(t,2));
corr_status_out = zeros(2,size(t,2));

acc_out = zeros(3,size(t,2));
mag_out = zeros(3,size(t,2));

ac_out = zeros(3,size(t,2));
mc_out = zeros(3,size(t,2));

mag_ekf_out = zeros(3,size(t,2));
acc_ekf_out = zeros(3,size(t,2));

norm_acc_out = zeros(size(t));
norm_mag_out = zeros(size(t));

accm_out = zeros(3,size(t,2));
magm_out = zeros(3,size(t,2));
gyro_out = zeros(3,size(t,2));
bias_out = zeros(3,size(t,2));

xhat_out = zeros(7,size(t,2));
Phat_out = zeros(7,7,size(t,2));

xhat_m_out = zeros(9,size(t,2));

xhat_ms_out = zeros(15,size(t,2));
xhat_bat_out = zeros(9,size(t,2));

norm_error_qhat_ex_out = zeros(1,size(t,2));
norm_error_euler_out = zeros(1,size(t,2));
norm_error_qhat_out = zeros(1,size(t,2));
norm_error_bhat_out = zeros(1,size(t,2));
error_euler_out = zeros(3,size(t,2));

d_norm_mag_out = zeros(1,size(t,2));
d_outlier_mag_out = zeros(3,size(t,2));
d_warn_mag_out = zeros(3,size(t,2));

btracep_out = zeros(1,size(t,2));
ptrace_pre_out = zeros(1,size(t,2));
ptrace_post_out = zeros(1,size(t,2));

q_out = zeros(4,size(t,2));
qsig_out = zeros(4,size(t,2));
qhat_out = zeros(4,size(t,2));
bhat_out = zeros(3,size(t,2));
mi_out = zeros(3,size(t,2));

tmp1_out = zeros(1,size(t,2));
tmp2_out =zeros(1,size(t,2));
tmp3_out =zeros(1,size(t,2));
tmp4_out =zeros(1,size(t,2));
tmp_dec_out = zeros(3,size(t,2));
Knorm_out = zeros(1,size(t,2));

l_mekf_out = zeros(1, size(t,2));
sigma_xhat_save = zeros(6,size(t,2),numel(ke_vec));
xhat_save = zeros(6,size(t,2),numel(ke_vec));
quat_error_save = zeros(1,size(t,2),numel(ke_vec));
total_euler_error = zeros(1,size(t,2),numel(ke_vec));
% glr_params
[ GLRparams_3, GLRinit_3 ] = setGLRparams( zeros(3),eye(3),eye(3),L,PFA_GLR,PFA_chi2, dt_glr);
[ GLRparams_1, GLRinit_1 ] = setGLRparams( 0,1,1,L,PFA_GLR,PFA_chi2, dt_glr);


PHIxn_magnorm  = GLRinit_1.PHIxn;
bhatn_magnorm  = GLRinit_1.bhatn;   % filter bank estimation [nb x L]
LAMbn_magnorm  = GLRinit_1.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_magnorm     = GLRinit_1.qn;      % Filter bank age [1 x L]
GLRn_magnorm   = GLRinit_1.GLRn;    % GLR statistic storage          

PHIxn_magpm  = GLRinit_3.PHIxn;
bhatn_magpm  = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_magpm  = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_magpm     = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_magpm   = GLRinit_1.GLRn;    % GLR statistic storage          

PHIxn_magout = GLRinit_3.PHIxn;
bhatn_magout = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_magout = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_magout    = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_magout  = GLRinit_1.GLRn;    % GLR statistic storage          

PHIxn_accnorm  = GLRinit_1.PHIxn;
bhatn_accnorm  = GLRinit_1.bhatn;   % filter bank estimation [nb x L]
LAMbn_accnorm  = GLRinit_1.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_accnorm     = GLRinit_1.qn;      % Filter bank age [1 x L]
GLRn_accnorm   = GLRinit_1.GLRn;    % GLR statistic storage          

PHIxn_accpm  = GLRinit_3.PHIxn;
bhatn_accpm  = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_accpm  = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_accpm     = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_accpm   = GLRinit_1.GLRn;    % GLR statistic storage          

PHIxn_accout = GLRinit_3.PHIxn;
bhatn_accout = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_accout = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_accout    = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_accout  = GLRinit_1.GLRn;    % GLR statistic storage          

% Storage
GLRvec = zeros(6,numel(t));
qdetvec = zeros(6,numel(t));
chi2vec = zeros(6,numel(t));
chi2cusumvec = zeros(6,numel(t));

% Chi2-CUSUM storage
Lcusum = 10; % Length of detection window
statvec_acc_norm = zeros(1,Lcusum);
statvec_acc_out = zeros(1,Lcusum);
statvec_acc_pm = zeros(1,Lcusum);
statvec_mag_norm = zeros(1,Lcusum);
statvec_mag_out = zeros(1,Lcusum);
statvec_mag_pm = zeros(1,Lcusum);
        
alpha_error = zeros(1,numel(t));
psi_error = zeros(1,numel(t));

att_reset = zeros(2,numel(t));
l_gyro_out = zeros(1,numel(t));

% Knorm_out = zeros(1,numel(t));
% clear eout;
% qbias0 = eul2qua(rpy_in(1,2:4)); 
% qbias = qbias0; 
% for k = 1:numel(t), 
%     qbias = quatintegrate(qbias, gyro_in(k,2:4)', dt_vec(k)); 
%     eout(:,k) = qua2eul(qbias); 
% end
% bhat_ol = (qua2eul(qbias)-qua2eul(qbias0))/(gyro_in(end,1));
ext_bias_out  = zeros(3, size(t,2));

% Gyro LP filter for bias estimation
tau = EKF.partq.taupsi_bias;
alpha = 1 - exp(-dt/tau);
alpha_fuse = 1 - exp(-dt/10);
bhat_lp_0 = bhat0;

bias_lp_out = zeros(3,numel(t));



% EKF.cov.gyro = 50*EKF.cov.gyro; 

cnt_ke = 0;
%%
if ismember(klm,[1:nsims])
% for ke = [ 1 2 3 4]
% for ke = [1 2 3 4 5]
if b_plot_comp
%     figcompeuler = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');;
%     figcompquat = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');;
% figcompbias = figure;
%     figsigma = figure;
end
for kk  = 1:nfilt
    if ntmp > 0 && ke_vec(kk) == 13
        cnt_ke = cnt_ke+1;
        ncov = ncov_vec(cnt_ke);
    end
%     if kk == 1
%         EKF.bsat.tau = 10000;
%     else
%         EKF.bsat.tau = dt;
%     end
%     ncov = 0;
    if numel(find(ke_vec==ke_vec(1))) == nfilt && 1==0
        delta_ang = delta_ang_vec(kk);
    else
        delta_ang = reset_ang;
    end
%     ncov = 3;
%     if kk == 1
%         EKF.bsat.bmax = 1*d2r;
%         EKF.bsat.tau = 1/eps;
%         EKF.bsat.kaw = 1-exp(-dt/(EKF.bsat.tau));
%         EKF.bsat.kaw_sto = EKF.bsat.kaw;
%     else
%         EKF.bsat.bmax = 1*d2r;
%         EKF.bsat.tau = dt;
%         EKF.bsat.kaw = 1-exp(-dt/(EKF.bsat.tau));
%         EKF.bsat.kaw_sto = EKF.bsat.kaw;
%     end

    if numel(find(ke_vec==ke_vec(1))) == numel(ke_vec) && numel(ke_vec) > 1 
        switch kk
            case 1
                b_lpgyro = 0;
                b_useftarch = 0;
                b_glr = 0;
                b_force_ok = 1;
            case 2
                b_lpgyro = 0;
                b_useftarch = 0;
                b_glr = 0;
                b_force_ok = 0;
            case 3
                b_lpgyro = 0;
                b_useftarch = 1;
                b_glr = 0;
                b_force_ok = 0;
            case 4
                b_lpgyro = 1;
                b_useftarch = 1;
                b_glr = 0;
                b_force_ok = 0;
            case 5
                b_lpgyro = 1;
                b_useftarch = 1;
                b_glr = 1;
                b_force_ok = 0;
        end
    end
    
%     if numel(find(ke_vec==ke_vec(1))) == numel(ke_vec) && numel(ke_vec) == 2 
%         switch kk
%             case 1
% %                 EKF.bsat.bmax = 3;
%                 b_lpgyro = 0;
%                 b_useftarch = 0;
%                 b_glr = 0;
%                 b_force_ok = 1;
%             case 2
% %                 EKF.bsat.bmax = 1000;
%                 b_lpgyro = 1;
%                 b_useftarch = 1;
%                 b_glr = 0;
%                 b_force_ok = 0;
%         end
%     end
    
    rng(0);
%     if kk == 1
%         EKF.bsat.kaw_sto = 0;
%     else
%         EKF.bsat.kaw_sto = EKF.bsat.kaw;
%     end
    
    ke = ke_vec(kk);
    
%     bhat_lp = bhat_ol;
%     xhat(5:7) = bhat_ol;
    
    Phat = Phat0;
    Phat_mekf = Phat_mekf0;
    xhat_mekf = xhat_mekf0;
    Phat_mekf_pm_mag = Phat_mekf_pm_mag0;
    xhat_mekf_pm_mag = xhat_mekf_pm_mag0;
    Phat_mekf_pm_acc_mag = Phat_mekf_pm_acc_mag0;
    xhat_mekf_pm_acc_mag = xhat_mekf_pm_acc_mag0;
    xhat = [qhat0; bhat0];
    xhat_m = [ahat0; mhat0; zeros(3,1)];
    xhat_bat = xhat_bat0;
    q = q0;
    ahat_p = ahat0; Phat_acc_p = Phat_acc0;
    mhat_p = mhat0; Phat_mag_p = Phat_mag0;
    bhat_lp = zeros(3,1);
    qhat = qhat0;
    
    
    switch biascase
        case 1 % No bias
            om_bias = zeros(3,1);
        case 2 % Constant bias
            om_bias = 0.5*[1;1;1]/r2d;
        case 3 % Time varying linear bias
            om_bias = 1*(0.05*[1;2;-1]+0.05/tend*[1;1;-1]*tk);
        case 4 % Constant bias (from Hua, 2011)
            om_bias = [-0.03; 0.03; -0.00];
    end
    % Initialise bias estimation at real value
    xhat(5:7) = om_bias; % Bias starting as known
    xhat_mekf(5:7) = om_bias;
    xhat_mekf_pm_mag(5:7) = om_bias;
    xhat_mekf_pm_acc_mag(5:7) = om_bias;
    xhat_bat(7:9) = om_bias;
    bhat_lp = om_bias;
    
    if b_accoffset
        accoffset = ones(3,1)*norm(EKF.gvec)*0.1;
    else
        accoffset = zeros(3,1);
    end
    if b_magoffset
        magoffset = ones(3,1)*norm(EKF.mag_ref)*0.1;
    else
        magoffset = zeros(3,1);
    end
    h1 = waitbar(0,'Simulating...');
    for k = 1:numel(t)
        tk = t(k);
        % gyro bias
        %
        
        om_bias = biasfactor*om_bias;
        
        % Attitude update
        Om = manti(omegareal(:,k));
        Om_q = [0 -omegareal(:,k)'; omegareal(:,k), -Om];
        
        qdot = 1/2*Om_q*q;
        q = q + qdot*dt;
        q = q/norm(q);
        
%         if t(k) == round(0.5*tend) %&& t(k)<0.75*tend
%             [PHI, THETA, PSI] = Quaternion2Euler(q);
%             q = Euler2Quaternion(PHI+randn*30*d2r,THETA+randn*30*d2r,PSI+randn*30*d2r);
% %             q = Euler2Quaternion(PHI,THETA,PSI+randn*30*d2r);
% %             q = Euler2Quaternion(PHI,THETA+randn*30*d2r,PSI);
% %             q = Euler2Quaternion(PHI+randn*30*d2r,THETA,PSI);
%         end
%         if t(k) == round(0.8*tend) %&& t(k)<0.75*tend
% %             [PHI, THETA, PSI] = Quaternion2Euler(q);
%             q = Euler2Quaternion(0,0,0);
% %             q = Euler2Quaternion(PHI,THETA,PSI+randn*30*d2r);
% %             q = Euler2Quaternion(PHI,THETA+randn*30*d2r,PSI);
% %             q = Euler2Quaternion(PHI+randn*30*d2r,THETA,PSI);
%         end
        
        R = qua2chrSL(q);
        [PHI, THETA, PSI] = Quaternion2Euler(q);
        euler = [PHI; THETA; PSI];
        eulerout(:,k) = euler;
        
        acc_k = R'*EKF.gvec;
        mag_k = R'*EKF.mag_ref;
        
        % Measurements
        accm = acc_k + bnoise*randn(3,1)*accsignoise + R'*accfault(:,k) + accoffset;
        magm = mag_k + bnoise*randn(3,1)*magsignoise + R'*magfault(:,k) + magoffset;
        acc_ok = 1;
        mag_ok = 1;
        
        acc_out(:,k) = acc_k;
        mag_out(:,k) = mag_k;
        
        accm_out(:,k) = accm;
        magm_out(:,k) = magm;
        gyro_out(:,k) = omega_t(:,k) + om_bias;
        bias_out(:,k) = om_bias;
        
        if t(k) > 0.1*tend %&& t(k)<0.75*tend
            gyro_out(3,k) =  gyro_out(3,k);% + 20*d2r;
        end
        
        
        
        inputs = gyro_out(:,k);
        measurements = [accm; magm];
                xhat_pm = [q; om_bias];
%         xhat_pm = xhat;
        
        
        % Low-pass gyro bias estimation
        l_gyro_out(:,k) = gyro_out(1:3,k)'*pinv(diag(EKF.cov.gyro))*gyro_out(1:3,k);
        % LP filtering of gyro to estimate gyro bias in open loop
        if l_gyro_out(:,k) < GLRparams_3.chi2thr
            bhat_lp = sat((1-alpha)*bhat_lp + alpha*gyro_out(1:3,k),-EKF.bsat.bmax,EKF.bsat.bmax);
        end
        
        
        bias_lp_out(:,k) = bhat_lp;
        
        
        % ============= Sensor estimation =======================
        [~, ~, l_acc, l_mag,...
        S_norm_acc, S_norm_mag, delta_norm_acc, delta_norm_mag] ...
        = measnormcheck(EKF,measurements);
        
        
        
%         status_vec_acc_a = [b_norm_acc; status_vec_acc];
%         status_vec_mag_a = [b_norm_mag; status_vec_mag];
        
        
        % =================================================================
        %               GLR fault detection
        % =================================================================
        idp_acc = 1 - double(rem(t(k),dt_glr) < 0.001*dt);
        idp_mag = 1 - double(rem(t(k),dt_glr) < 0.001*dt);
%         idp_mag = 1-mag_ok(k);
%         idp_acc = 1-acc_ok(k);
        kresetglr = 0;
        lamff = 1;
        
        % GLR test for acc norm
        nunp1 = delta_norm_acc;
        Knp1 = zeros(1)/2;
        Snp1 = S_norm_acc;
        if b_glr && b_useglr_vec(1) && b_useftarch
            [qdetnp1,GLRmax,qn_accnorm,bhatn_accnorm,LAMbn_accnorm,PHIxn_accnorm, GLRn_accnorm] ...
                = glrtest(idp_acc, kresetglr,lamff,Knp1,Snp1,nunp1,qn_accnorm,bhatn_accnorm,LAMbn_accnorm,PHIxn_accnorm,GLRn_accnorm,GLRparams_1);
            b_norm_acc_glr = double(GLRmax > GLRparams_1.GLRthr);
            b_norm_acc_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_1.chi2thr);
            b_norm_acc = double(b_norm_acc_glr + b_norm_acc_chi2 > 0);
        else
            GLRmax = nunp1'*pinv(Snp1)*nunp1;
            qdetnp1 = 0;
            b_norm_acc = double(GLRmax > GLRparams_1.chi2thr);
        end
        
        
        qdetvec(1,k) = qdetnp1;
        GLRvec(1,k) = GLRmax;
        chi2vec(1,k) = nunp1'*pinv(Snp1)*nunp1;
        
        % Run performance model acc
        [ ahat_p, Phat_acc_p, status_vec_acc, S_pm_acc, delta_pm_acc,...
        S_out_acc, delta_out_acc ] = acc_perf_model(EKF, ahat_p, Phat_acc_p,...
            inputs, accm, xhat_pm, Phat, b_norm_acc, dt );
        
        % GLR test for acc outliers
        nunp1 = delta_out_acc;
        Knp1 = zeros(3)/2;
        Snp1 = S_out_acc;
        if b_glr && b_useglr_vec(2) && b_useftarch
            [qdetnp1,GLRmax,qn_accout,bhatn_accout,LAMbn_accout,PHIxn_accout,GLRn_accout] ...
                = glrtest(idp_acc, kresetglr,lamff,Knp1,Snp1,nunp1,qn_accout,bhatn_accout,LAMbn_accout,PHIxn_accout,GLRn_accout,GLRparams_3);
            b_out_acc_glr = double(GLRmax > GLRparams_3.GLRthr);
            b_out_acc_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_3.chi2thr);
            b_out_acc = double(b_out_acc_glr + b_out_acc_chi2 > 0);
        else
            GLRmax = nunp1'*pinv(Snp1)*nunp1;
            qdetnp1 = 0;
            b_out_acc = double(GLRmax > GLRparams_3.chi2thr);
        end
        
        qdetvec(2,k) = qdetnp1;
        GLRvec(2,k) = GLRmax;
        chi2vec(2,k) = nunp1'*pinv(Snp1)*nunp1;
        
        % GLR test for acc perf model
        nunp1 = delta_pm_acc;
        Knp1 = zeros(3)/2;
        Snp1 = S_pm_acc;
        if b_glr && b_useglr_vec(3) && b_useftarch
            [qdetnp1,GLRmax,qn_accpm,bhatn_accpm,LAMbn_accpm,PHIxn_accpm,GLRn_accpm] ...
                = glrtest(idp_acc, kresetglr,lamff,Knp1,Snp1,nunp1,qn_accpm,bhatn_accpm,LAMbn_accpm,PHIxn_accpm,GLRn_accpm,GLRparams_3);
            b_pm_acc = double(GLRmax > GLRparams_3.GLRthr);
        else
            GLRmax = nunp1'*pinv(Snp1)*nunp1;
            qdetnp1 = 0;
            b_pm_acc = double(GLRmax > GLRparams_3.chi2thr);
        end
        
        
        qdetvec(3,k) = qdetnp1;
        GLRvec(3,k) = GLRmax;
        chi2vec(3,k) = nunp1'*pinv(Snp1)*nunp1;
        
        % GLR test for mag norm
        nunp1 = delta_norm_mag;
        Knp1 = zeros(1)/2;
        Snp1 = S_norm_mag;
        if b_glr && b_useglr_vec(4) && b_useftarch
            [qdetnp1,GLRmax,qn_magnorm,bhatn_magnorm,LAMbn_magnorm,PHIxn_magnorm,GLRn_magnorm] ...
                = glrtest(idp_mag, kresetglr,lamff,Knp1,Snp1,nunp1,qn_magnorm,bhatn_magnorm,LAMbn_magnorm,PHIxn_magnorm,GLRn_magnorm,GLRparams_1);
            b_norm_mag_glr = double(GLRmax > GLRparams_1.GLRthr);
            b_norm_mag_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_1.chi2thr);
            b_norm_mag = double(b_norm_mag_glr + b_norm_mag_chi2 > 0);
        else
            GLRmax = nunp1'*pinv(Snp1)*nunp1;
            qdetnp1 = 0;
            b_norm_mag = double(GLRmax > GLRparams_1.chi2thr);
        end
        
        qdetvec(4,k) = qdetnp1;
        GLRvec(4,k) = GLRmax;
        chi2vec(4,k) = nunp1'*pinv(Snp1)*nunp1;
        
        % Run performance model - Mag
        [ mhat_p, Phat_mag_p, status_vec_mag, S_pm_mag, delta_pm_mag,...
        S_out_mag, delta_out_mag] = mag_perf_model(EKF, mhat_p, Phat_mag_p,...
            inputs, magm, xhat_pm, Phat, b_norm_mag, dt );
        
        % GLR test for mag outliers
        nunp1 = delta_out_mag;
        Knp1 = zeros(3)/2;
        Snp1 = S_out_mag;
        if b_glr && b_useglr_vec(5) && b_useftarch
            [qdetnp1,GLRmax,qn_magout,bhatn_magout,LAMbn_magout,PHIxn_magout,GLRn_magout] ...
                = glrtest(idp_mag, kresetglr,lamff,Knp1,Snp1,nunp1,qn_magout,bhatn_magout,LAMbn_magout,PHIxn_magout,GLRn_magout,GLRparams_3);
            b_out_mag_glr = double(GLRmax > GLRparams_3.GLRthr);
            b_out_mag_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_3.chi2thr);
            b_out_mag = double(b_out_mag_glr + b_out_mag_chi2 > 0);
        else
            GLRmax = nunp1'*pinv(Snp1)*nunp1;
            qdetnp1 = 0;
            b_out_mag = double(GLRmax > GLRparams_3.chi2thr);
        end
        
        qdetvec(5,k) = qdetnp1;
        GLRvec(5,k) = GLRmax;
        chi2vec(5,k) = nunp1'*pinv(Snp1)*nunp1;
        
        % GLR test for mag perf model
        nunp1 = delta_pm_mag;
        Knp1 = zeros(3)/2;
        Snp1 = S_pm_mag;
        if b_glr && b_useglr_vec(6)
            [qdetnp1,GLRmax,qn_magpm,bhatn_magpm,LAMbn_magpm,PHIxn_magpm,GLRn_magpm] ...
                = glrtest(idp_mag, kresetglr,lamff,Knp1,Snp1,nunp1,qn_magpm,bhatn_magpm,LAMbn_magpm,PHIxn_magpm,GLRn_magpm,GLRparams_3);
            b_pm_mag = double(GLRmax > GLRparams_3.GLRthr);
        else
            GLRmax = nunp1'*pinv(Snp1)*nunp1;
            qdetnp1 = 0;
            b_pm_mag = double(GLRmax > GLRparams_3.chi2thr);
        end
        
        
        qdetvec(6,k) = qdetnp1;
        GLRvec(6,k) = GLRmax;
        chi2vec(6,k) = nunp1'*pinv(Snp1)*nunp1;
        
        status_vec_mag_a = [b_norm_mag; b_out_mag; b_pm_mag];
        
        status_vec_acc_a = [b_norm_acc; b_out_acc; b_pm_acc];
        % =================================================================
        mhat_out(:,k) = mhat_p;
        sigma_mhat_out(:,k) = diag(Phat_mag_p);
        ahat_out(:,k) = ahat_p;
        sigma_ahat_out(:,k) = diag(Phat_acc_p);
        
        acc_status_out(:,k) = status_vec_acc_a;
        mag_status_out(:,k) = status_vec_mag_a;
        
        d_norm_mag_out(1,k) = norm(delta_norm_mag);
        d_outlier_mag_out(:,k) = (delta_out_mag);
        d_warn_mag_out(:,k) = (delta_pm_mag);
        
        %==================================================================
        % Attitude reset for GLR fault rejection
        %==================================================================
        if k > 1 && b_glr_attreset && b_glr
            % Reset on norm detection
            if (mag_status_out(1,k) > 0 &&  mag_status_out(1,k-1) < 1) && b_useglr_vec(4)
                att_reset(1,k) = 1;
                b_omega_tmp = xhat_out(5:7,k-qdetvec(4,k)*ndt_glr);
                qhat_tmp = xhat_out(1:4,k-qdetvec(4,k)*ndt_glr);
                omega_mat_tmp = gyro_out(:,k-qdetvec(4,k)*ndt_glr:k);
                for kl = 1:qdetvec(4,k)*ndt_glr
                    qhat_tmp = quatint( qhat_tmp, omega_mat_tmp(:,kl)-b_omega_tmp, dt);
                end
%                 xhat = [qhat_tmp; b_omega_tmp];
                xhat = [qhat_tmp; xhat(5:7)];
            % Reset on outlier detection
            elseif (mag_status_out(2,k) > 0 &&  mag_status_out(2,k-1) < 1) && b_useglr_vec(5)
                att_reset(2,k) = 1;
                b_omega_tmp = xhat_out(5:7,k-qdetvec(5,k)*ndt_glr);
                qhat_tmp = xhat_out(1:4,k-qdetvec(5,k)*ndt_glr);
                omega_mat_tmp = gyro_out(:,k-qdetvec(5,k)*ndt_glr:k);
                for kl = 1:qdetvec(5,k)*ndt_glr
                    qhat_tmp = quatint( qhat_tmp, omega_mat_tmp(:,kl)-b_omega_tmp, dt);
                end
%                 xhat = [qhat_tmp; b_omega_tmp];
                xhat = [qhat_tmp; xhat(5:7)];
            else
                att_reset(:,k) = 0;
            end
        end
        %==================================================================
        
        % =================================================================
        %               Chi2-CUSUM fault detection
        % =================================================================
%         statvec_acc_norm = [chi2vec(1,k), statvec_acc_norm(1:end-1)];
%         statvec_acc_out = [chi2vec(2,k), statvec_acc_out(1:end-1)];
%         statvec_acc_pm = [chi2vec(3,k), statvec_acc_pm(1:end-1)];
%         statvec_mag_norm = [chi2vec(1,k), statvec_mag_norm(1:end-1)];
%         statvec_mag_out = [chi2vec(2,k), statvec_mag_out(1:end-1)];
%         statvec_mag_pm = [chi2vec(3,k), statvec_mag_pm(1:end-1)];
%         
%         % Chi2-CUSUM for acc norm
%         l_chi2cusum_acc_norm = sum(statvec_acc_norm)/numel(statvec_acc_norm);
%         chi2cusumvec(1,k) = l_chi2cusum_acc_norm;
%         % Chi2-CUSUM for acc outlier
%         l_chi2cusum_acc_out = sum(statvec_acc_out)/numel(statvec_acc_out);
%         chi2cusumvec(2,k) = l_chi2cusum_acc_out;
%         % Chi2-CUSUM for acc perfmod
%         l_chi2cusum_acc_pm = sum(statvec_acc_pm)/numel(statvec_acc_pm);
%         chi2cusumvec(3,k) = l_chi2cusum_acc_pm;
%         % Chi2-CUSUM for acc norm
%         l_chi2cusum_mag_norm = sum(statvec_mag_norm)/numel(statvec_mag_norm);
%         chi2cusumvec(4,k) = l_chi2cusum_mag_norm;
%         % Chi2-CUSUM for acc outlier
%         l_chi2cusum_mag_out = sum(statvec_mag_out)/numel(statvec_mag_out);
%         chi2cusumvec(5,k) = l_chi2cusum_mag_out;
%         % Chi2-CUSUM for acc perfmod
%         l_chi2cusum_mag_pm = sum(statvec_mag_pm)/numel(statvec_mag_pm);
%         chi2cusumvec(6,k) = l_chi2cusum_mag_pm;
        
        % =================================================================
        if b_useftarch
            [ac, Phat_ac, mc, Phat_mc, corr_status] = input_consolidation(...
             EKF, ahat_p, accm, mhat_p, magm, Phat_acc_p, Phat_mag_p,...
             status_vec_acc_a, status_vec_mag_a);
        else
            ac = accm; Phat_ac = EKF.Ra;
            mc = magm; Phat_mc = EKF.Rm;
            corr_status = [1-b_norm_acc; 1-b_norm_mag];
        end
        
%         [ xhat_m] = martin_2017_sensor(xhat_m, Phat, inputs(1:3),...
%             accm, magm, dt, gain_m, cov_in);
%         xhat_m_out(:,k) = xhat_m;
        
        %         xhat(5:6) = xhat_m(7:8);
        b_nobias = 0;

        %         ac = ahat_p; mc = mhat_p;
        % if ke == 3
%         if tk > 0.2*tend && tk < 0.8*tend
%             corr_status = [1;0];
%         end
%         ac = accm; mc = magm;
%                 ac = ahat_p; 
%         mc = mhat_p;
        % if ke == 3
%         if t(k) > tend*0.25 && t(k) < tend*0.75 
%             corr_status = [0;0];
%         end
%         corr_status = [0;0];
        corr_status = [1;1];
        % end
        
        
        
%         ==== Force measurement use and correction ====
        if b_force_ok
            ac = accm; mc = magm;
            corr_status = [1;1];
        end
%         ==============================================
        if b_force_nok && t(k) >0.5*tend && t(k) <0.8*tend
            corr_status = [1;0];
        end
        % end
        
        measurements = [ac; mc];
        
        corr_status = corr_status.*[acc_ok; mag_ok];
        norm_acc_out(k) = norm(accm);
        norm_mag_out(k) = norm(magm);
        ac_out(:,k) = measurements(1:3);
        mc_out(:,k) = measurements(4:6);
        corr_status_out(:,k) = corr_status;
        

        % Use LP gyro estimation in case of disturbance
        ext_bias = xhat(5:7);
        if b_lpgyro
            if sum(status_vec_mag_a(1:2)) > 0
                xhat(7) = bhat_lp(3);
                xhat_mekf(7) = bhat_lp(3);
                ext_bias(3) =  bhat_lp(3);
            end
            if sum(status_vec_acc_a(1:2)) > 0
                xhat(5:6) = bhat_lp(1:2);
                xhat_mekf(5:6) = bhat_lp(1:2);
                ext_bias(1:2) =  bhat_lp(1:2);
            end
        end
        ext_bias_out(:,k) = ext_bias;
        
        % ============= Attitude estimation =======================
        switch ke
            case 1
                % ---- Comp. filter ----
                filt_title = 'Hua';
                [ xhat] = hua_2011_decoupled(EKF, xhat, inputs,  measurements, dt, gain, corr_status);
            case 2 % ---- EKF ----
                filt_title = 'EKF';
                [xhat,Phat] = ekf_update(EKF,inputs, measurements, xhat,Phat, dt, corr_status );
            case 3 % ---- DEC - EKF ----
                filt_title = 'Measurement decoupled KF';
                [tmp_dec,xhat,Phat] = deckf_update(EKF,inputs, measurements, xhat,Phat, dt, corr_status );
            case 4 % ---- NL - KF ----
                filt_title = 'NL-KF';
                [tmpstruct2, tmpstruct, tmp, xhat,Phat] = ftkf_update(EKF,inputs,measurements,xhat,Phat, dt, corr_status, b_nobias, ext_bias);
                %                 filt_title = 'eXogenous Kalman Filter KF';
                %                 uhat_ex = xhat; cov_uhat_ex = Phat;
                %                 [xhat_ex,Phat_ex] = qhat_xkf_update(EKF,inputs,uhat_ex, cov_uhat_ex, xhat_ex,Phat_ex, dt);
            case 5 % Comp filter global decoupling
                filt_title = 'Hua, Global';
                [ xhat] = hua_2011_decoupled_global(EKF, xhat, inputs,  measurements, dt, gain, corr_status);
            case 6 % Sarras / Martin attitude observer with measurement biases
                filt_title = 'Martin + Sarras, 2018, Bias observer';
%                 [ xhat_ms, qhat] = martin_sarras_bias(EKF, xhat_ms, inputs,  measurements, dt, gain_ms, corr_status);
                [ xhat_ms, qhat] = martin_sarras_2018_bias(xhat_ms, inputs,  measurements, EKF,  dt, gain_ms);
                %                 measurements = xhat_ms(1:6);
                %                 [ xhat] = hua_2011_decoupled_global(EKF, xhat, inputs,  measurements, dt, gain, corr_status);
                
                xhat = [qhat; xhat_ms(7:9)];
                xhat_ms_out(:,k) = xhat_ms;
            case 7 % ---- DEC MEKF ----
                filt_title = 'MEKF';
%                 [ xhat,Phat ] = smekf_update(EKF, inputs, measurements, xhat,Phat, dt, corr_status);
                [ Phat_mekf, xhat_mekf, Knorm ] = mekf( Phat_mekf, xhat_mekf, inputs, measurements, corr_status, EKF, dt, ext_bias );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf(1:3,1:3)*ddqda', Phat_mekf(4:6,4:6));
                xhat = xhat_mekf;
%                 Knorm_out(:,k) = Knorm;
            case 8 % ---- DEC GMEKF ----
                filt_title = 'DEC-IEKF';
%                 [ xhat,Phat ] = smekf_update(EKF, inputs, measurements, xhat,Phat, dt, corr_status);
                [ Phat_mekf, xhat_mekf, Knorm ] = invariant_mekf( Phat_mekf, xhat_mekf, inputs, measurements, corr_status, EKF, dt, ext_bias );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf(1:3,1:3)*ddqda', Phat_mekf(4:6,4:6));
                xhat = xhat_mekf;
                Knorm_out(:,k) = norm(Knorm);
            case 9 % ---- DEC IEKF ----
                filt_title = 'Decoupled IEKF, Mag. PM';
%                 [ Phat_mekf_pm_mag, xhat_mekf_pm_mag ] = iekf( Phat_mekf_pm_mag, xhat_mekf_pm_mag, inputs, measurements, corr_status, EKF, dt );
                [ Phat_mekf_pm_mag, xhat_mekf_pm_mag, Knorm ] = iekf_dec_mag( Phat_mekf_pm_mag, xhat_mekf_pm_mag, inputs, measurements, corr_status, EKF, dt );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf_pm_mag(1:3,1:3)*ddqda', Phat_mekf_pm_mag(4:end,4:end));
                xhat = xhat_mekf_pm_mag;
                mag_ekf_out(:,k) = xhat_mekf_pm_mag(8:10);
%                 Knorm_out(:,k) = Knorm;
            case 10 % ---- DEC IEKF ----
                filt_title = 'Decoupled IEKF, Full PM';
%                 [ Phat_mekf_pm_mag, xhat_mekf_pm_mag ] = iekf( Phat_mekf_pm_mag, xhat_mekf_pm_mag, inputs, measurements, corr_status, EKF, dt );
                [ Phat_mekf_pm_acc_mag, xhat_mekf_pm_acc_mag, Knorm ] = iekf_dec( Phat_mekf_pm_acc_mag, xhat_mekf_pm_acc_mag, inputs, measurements, corr_status, EKF, dt );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf_pm_acc_mag(1:3,1:3)*ddqda', Phat_mekf_pm_acc_mag(4:end,4:end));
                xhat = xhat_mekf_pm_acc_mag;
                mag_ekf_out(:,k) = xhat_mekf_pm_acc_mag(11:13);
                acc_ekf_out(:,k) = xhat_mekf_pm_acc_mag(8:10);
                Knorm_out(:,k) = Knorm;
            case 11 % ---- Valenti ----
                filt_title = 'Valenti';
                [ xhat ] = valenti_2016_fast(EKF, inputs, measurements, xhat, dt );
            case 12 % ---- FT-KF with measured uhat ----
                filt_title = 'NL-KF - umeas';
                [tmpstruct2, tmpstruct, tmp, xhat,Phat] = ftkf_update_2(EKF,inputs,measurements,xhat,Phat, dt, corr_status, b_nobias, ext_bias);
            case 13 % ---- DEC GMEKF ----
                filt_title = ['DEC-IEKF, ncov=',num2str(ncov)];
%                 [ xhat,Phat ] = smekf_update(EKF, inputs, measurements, xhat,Phat, dt, corr_status);
                [ Phat_mekf, xhat_mekf, Knorm , l_error] = invariant_mekf_covmax( Phat_mekf, xhat_mekf, inputs, measurements, corr_status, EKF, dt, ext_bias,ncov );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf(1:3,1:3)*ddqda', Phat_mekf(4:6,4:6));
                xhat = xhat_mekf;
                l_mekf_out(k) = l_error;
            case 14 % ---- EKF ----
                filt_title = 'FT_EKF';
                [xhat,Phat] = ft_ekf_update(EKF,inputs, measurements, xhat,Phat, dt, corr_status );
            case 15
                % ---- Wu Fast Comp. filter ----
                filt_title = 'Wu';
                [ xhat] = wu_2016_fast(EKF, xhat, inputs,  measurements, dt, gain_wu, corr_status, b_norm_mag);
            case 16
                filt_title = 'Batista';
                [ xhat_bat,Phat_bat, qhat, P_q] = batista_ekf_2012(EKF, inputs, measurements, xhat_bat,Phat_bat, dt, corr_status, qhat);
                xhat = [qhat;xhat_bat(7:9)];
                xhat_bat_out(:,k) = xhat_bat;
                Phat = blkdiag(P_q, Phat_bat(7:9,7:9));
            case 17 % ---- MXKF ----
                filt_title = 'MXKF';
                %[tmpstruct2, tmpstruct, tmp, xhat,Phat] = ftkf_update(EKF,inputs,measurements,xhat,Phat, dt, corr_status, b_nobias, ext_bias);
                filt_title = 'eXogenous Kalman Filter KF';
                uhat_ex = xhat; cov_uhat_ex = Phat;
                [xhat_ex,Phat_ex] = qhat_xkf_update(EKF,inputs,uhat_ex, cov_uhat_ex, xhat_ex,Phat_ex, dt);
        end
        
%         xhat(5:7) = 0;
%         xhat_mekf(5:7) = 0;
%         dalpha_out(k) = tmp(7);
%         dpsi_out(k) = tmp(8);
%         uhat_out(1:2,k) = tmp(5:6);
        ac_n = ac/norm(ac);
        mc_n = mc/norm(mc);
        
        %         Rot_meas = [ac_n, cross(ac_n, mc_n), cross(ac_n, cross(ac_n, mc_n))];
        Rot_meas = [cross(ac_n,cross(ac_n, mc_n)), cross(ac_n, mc_n), ac_n];
        
        eul_meas(:,k) = chr2eul(Rot_meas');
        
        if ke ==  3
            tmp_dec_out(1:3,k) = 4*tmp_dec(2:4)/(tmp_dec(1)+1); % Modified Rodrigues parameter
            %             aacc = acos(tmp_dec(1));
            %             tmp_dec_out(1:3,k) = tmp_dec(2:4)/sin(aacc);
            tmp_dec_out(1:3,k) = tmp_dec_out(1:3,k) / norm(tmp_dec_out(1:3,k));
        end
        
        %         if tk == floor(tend/2)
        %             switch resetcase
        %                 case 1 % Full state reset
        %                     xhat = [qhat0; bhat0];
        %                     xhat_m = [ahat0; mhat0; zeros(3,1)];
        %                 case 2 % Attitude reset
        %                     xhat = [qhat0; xhat(5:7)];
        %                 case 3 % Bias reset
        %                     xhat = [xhat(1:4); bhat0];
        %             end
        %         end
        
        if abs((tk) - 10) < dt^2 && resetcase >0
%             qhat_res = eul2qua(0.5*[0 0 0]*d2r);
%             qhat_res = QuaternionProduct(qhat,eul2qua([ delta_ang 0 0  ]*d2r));
            qhat_res = QuaternionProduct(qhat,eul2qua(dist_delta_ang*delta_ang*d2r));
            xhat(1:4) = qhat_res;
            xhat_mekf(1:4) = qhat_res;
            xhat_mekf_pm_mag(1:4) = qhat_res;
            xhat_mekf_pm_acc_mag(1:4) = qhat_res;
%             q = [1;0;0;0];
        end
        
        qhat = xhat(1:4);
        e3b = R'*e3;
        e1b = R'*e1;
        
        e3bhat = Quaternion2RotMat(qhat)'*e3;
        e1bhat = Quaternion2RotMat(qhat)'*e1;
        
        alpha_error(1,k) = norm(cross(e3b,e3bhat));
        psi_error(1,k) = norm(cross(e1b,e1bhat));
        
        
        
        Ptrace_post = trace(Phat);
        
        [PHI, THETA, PSI] = Quaternion2Euler(qhat);
        eulerhat = [PHI; THETA; PSI];
        eulerhatout(:,k) = eulerhat;
        eulerhatexout(:,k) = qua2eulSL(xhat_ex);
        eulercompout(:,k,kk) = eulerhat;
        quatcompout(:,k,kk) = qhat;
        
%         sigma_euler_ex_out(:,k) = QuaternionCov2EulerSTD(Phat_ex(1:4,1:4),xhat_ex);
        sigma_euler_out(:,k) = QuaternionCov2EulerSTD(Phat(1:4,1:4),qhat);
        sigma_bias_out(:,k) = sqrt(diag(Phat(5:7,5:7)));
        [PHI, THETA, PSI] = Quaternion2Euler(q);
        euler = [PHI; THETA; PSI];
        error_euler = euler - eulerhat;
        error_euler_out(:,k) =  error_euler;
        
%         inc_error_abs = abs(error_euler(1)) + abs(error_euler(2));
%         head_error_abs = abs(error_euler(3));
        e_q = norm(quaternionErrorFct(q,qhat));
        if e_q > pi
            e_q = abs(2*pi-e_q);
        end
        norm_error_euler_out(:,k) = sum(abs(error_euler));
        norm_error_qhat_out(:,k) = e_q;
%         norm_error_qhat_ex_out(:,k) = norm(quaternionErrorFct(q,xhat_ex));
        norm_error_bhat_out(:,k) = sum(abs( xhat(5:7)-om_bias));
%         eulercompout(:,k,kk) = norm(error_euler);
        
        qsig_out(:,k) = sqrt(diag(Phat(1:4,1:4)));
        Phat_out(:,:,k) = Phat(1:7,1:7);
        qhat_out(:,k) = qhat;
        bhat_out(:,k) =  xhat(5:7);
        xhat_out(:,k) = xhat(1:7);
        
        q_out(:,k) = q;
        
        % ============== Integrity calculation ===================================
        P_alpha = diag(sigma_euler_out(1:2,k))^2;
        P_psi = sigma_euler_out(3,k)^2;
        PL_alpha(k) = sqrt(max(abs(eig(P_alpha*r2d^2)))*P_ff_alpha);
        PL_psi(k) = sqrt(max(abs(eig(P_psi*r2d^2)))*(P_ff_psi));
        % ========================================================================
        
        if mod(k,10) == 0
            waitbar((t(k)/tend),h1)
        end
    end
    cnt = cnt+1;
    waitbar(cnt/(nsims*nfilt),hsims,[num2str(cnt),'/',num2str(nsims*nfilt),' Simulations done.'])
    close(h1);
    
%     figure(figsigma), hold on, 
%     plot(t, sigma_euler_out(3,:)*r2d);
%     set(gcf,'color','w')
    
    % Save some stuff
    sigma_xhat_save(:,:,kk) = [sigma_euler_out; sigma_bias_out];
    xhat_save(:,:,kk) = [eulerhatout; bhat_out];
    quat_error_save(:,:,kk) = norm_error_qhat_out;
    total_euler_error(:,:,kk) = norm_error_euler_out;
    
    %% Edit plot parameters
    if numel(find(ke_vec==ke_vec(1))) == numel(ke_vec) && numel(ke_vec) > 1 
        ke_legend(kk) = {[filt_title,'-',num2str(kk)]};
    else
        ke_legend(kk) = {filt_title};
    end
    
    if b_glr
        filt_title  = [filt_title, [', GLR detection: L=',num2str(L),'s, P_{FA} = ',num2str(PFA_GLR)]];
    else
        filt_title  = [filt_title, [', \chi^2 detection: P_{FA} = ',num2str(PFA_chi2)]];
    end
    titlevec_acc = {'Acc Norm','Acc Out','Acc PM'};
    titlevec_mag = {'Mag Norm','Mag Out','Mag PM'};
    indplot = (1:numel(t));
    tplot = t(indplot);
    ndown = 1;
    mark_list = {'-','--','-.','-','--','-.',...
                 '-','--','-.','-','--','-.',...
                 '-','--','-.','-','--','-.'};
            

    %% Plotty

quaternion_ekf_test_loop_simple_plot
end
%% Compare quaternion and quaternion error between filters
% if b_plot_comp
% figure(figcompquat), hold on,
% for k = 1:4
%     subplot(5,1,k); hold on; axis([0 t(end) -1 1])
%     plot(tplot, qhat_out(k,indplot),'Linewidth',1)
%     legend(ke_legend,'Orientation','horizontal','location','north')
% end
% subplot(5,1,5), hold on,
% plot(tplot, norm_error_qhat_out(indplot)*r2d,'Linewidth',1)
% 
% end
%% Compare bias estimation and error between filters
% if b_plot_comp
% figure(figcompbias), hold on,
% for k = 1:3
%     subplot(4,1,k); hold on; axis([0 t(end) -180 180])
%     plot(tplot, bhat_out(k,indplot)*r2d,'Linewidth',1)
%     
% end
% subplot(4,1,4), hold on,
% plot(tplot, norm_error_bhat_out(indplot)*r2d,'Linewidth',1)
% legend(ke_legend)
% end

%% Save inclination and heading error statistics
inc_error_mc_out(klm,kk) = rms(abs(error_euler_out(1,:)) + abs(error_euler_out(2,:)));
head_error_mc_out(klm,kk) = rms(abs(error_euler_out(3,:)));
inc_error_max_mc_out(klm,kk) = max(abs(error_euler_out(1,:)) + abs(error_euler_out(2,:)));
head_error_max_mc_out(klm,kk) = max(abs(error_euler_out(3,:)));

% figure, plot(t, Knorm_out)
end

% eulab = {'$\phi\,[^{\circ}]$','$\theta\,[^{\circ}]$','$\psi\,[^{\circ}]$','$||\eps||$'};
% if b_plot_comp
% figure(figcompeuler), hold on,
% for k = 1:3
%     subplot(3,1,k); hold on; axis([0 t(end) -180 180])
%     if k < 4
%         ylabel(eulab(k),'interpreter','latex','fontsize',18)
%     end
%     plot(tplot, eulerout(k,indplot)*r2d,'k--','Linewidth',1)
%     funFigureProperty
% end
% xlabel(gca,'Time [s]','interpreter','latex')
% funFigureProperty
% % export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',['euler_plot_',num2str(scen),'_',num2str(accfaultcase),'_',num2str(magfaultcase),'_',time_str_short]),'-pdf',gcf)
% 
% % figure(figcompquat), hold on,
% % for k = 1:4
% %     subplot(5,1,k); hold on; axis([0 t(end) -1 1])
% %     plot(tplot, q_out(k,indplot),'k--','Linewidth',1)
% % end
% % 
% % figure(figcompbias), hold on,
% % for k = 1:3
% %     subplot(4,1,k); hold on; axis([0 t(end) -180 180])
% %     plot(tplot, bias_out(k,indplot)*r2d,'k--','Linewidth',1)
% % end
% end
% klm
% pfa_est(1:4,klm) = [sum(acc_status_out(1,:))/numel(acc_status_out(1,:));
% sum(acc_status_out(2,:))/numel(acc_status_out(2,:));
% sum(mag_status_out(1,:))/numel(mag_status_out(1,:));
% sum(mag_status_out(2,:))/numel(mag_status_out(2,:))];
% end


% if b_plot_mc
%     figure(figmcquat), hold on,
%     plot(tplot, norm_error_qhat_out(indplot)*r2d,'Linewidth',1)
% end

Plot_Simulation
end
% if b_plot_mc
%     figure(figmcquat), title(filt_title)
% end
close(hsims);
toc


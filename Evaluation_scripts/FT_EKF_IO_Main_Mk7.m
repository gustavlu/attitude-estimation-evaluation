% FT_EKF_IO_Main_mk7
%
% Init script to run data from mk7 drone at ISAE
%
% Structure:
%   - Magnetic calibration
%   - Calculation of magnetic reference
%   - Creation of input signals

% evaldir = 'C:\Users\Gustav\Documents\MATLAB\GIT_Thesis_Data\attitude-estimation-evaluation';
evaldir  = 'C:\Users\g.omanlundin-ext\Documents\MATLAB\attitude-estimation-evaluation'

clear tstart tstop

InitUavParameters_mod_X16_LinAcc

% ---- START magnetic reference and calibration ----
% Magnetic calibration
load('mk7_calib.mat')

isel = 1000:7500;
[ofs,gain,rotM]=ellipsoid_fit(imu_mag(isel,:),3);

% Find magnetic reference
fprintf('Loading data + calibration...\n')

% load('mk7_magrefcheck.mat')
imu_mag_calib = zeros(size(imu_mag));
for kl = 1:size(imu_mag,1)
    imu_mag_calib(kl,1:3) = (1./gain).*(rotM*(imu_mag(kl,:)'-ofs));
end

mag_ref_mat = zeros(size(imu_mag));
for kl = 1:size(imu_mag,1)
    mag_ref_mat(kl,1:3) = (Euler2RotMat(optitrack.att.roll(kl),optitrack.att.pitch(kl),optitrack.att.yaw(kl))...
        *imu_mag_calib(kl,:)')';
end

% Magnetic reference by averaging
mag_ref_mat = zeros(numel(isel),3);
for kl = 1:numel(isel)
    indy = isel(1)+kl-1;
    mag_ref_mat(kl,1:3) = (Euler2RotMat(optitrack.att.roll(kl),...
        optitrack.att.pitch(kl),...
        optitrack.att.yaw(kl))...
        *imu_mag_calib(indy,:)')';
end
mref = mean(mag_ref_mat)';
EKF.mag_ref = mref;

% Magnetic reference by LSQ
mbmat = zeros(3*numel(isel),1);
Rmat = zeros(3*numel(isel),3);

for kl = 1:numel(isel)
    icol = 1+(kl-1)*3:kl*3;
    mbmat(icol) = imu_mag_calib(kl,:)';
    Rmat(icol,:) = Euler2RotMat(optitrack.att.roll(kl),...
        optitrack.att.pitch(kl),...
        optitrack.att.yaw(kl));
end

mref_2 = Rmat\mbmat;
EKF.mag_ref = mref_2;

% ---- END magnetic reference and calibration ----

[ EKF, IMU, AUX, px4, optitrack, control, testdir, isel,...
    i_std_acc, i_mag_ref,tstart, tstop] ...
    = LoadFlightTest( testcase, px4, evaldir, EKF );

%% Magnetometer rearranging
IMU.mx_s = IMU.mx;
IMU.my_s = IMU.my;
IMU.mz_s = IMU.mz;

rpy_data = interp1(AUX.time_ot,AUX.RPY_OT,IMU.time,'pchip');
px4_data = interp1(AUX.time_px4,AUX.RPY_px4,IMU.time,'pchip');
%     v_data = [Vx, Vy, Vz];
imu_mag_2 = [IMU.mx, IMU.my, IMU.mz];
imu_acc_2 = [IMU.ax, IMU.ay, IMU.az];
imu_gyro_2 = [IMU.p, IMU.q, IMU.r];
v_data = interp1(AUX.time_ot,AUX.V_XYZ_OT,IMU.time,'pchip');


p_recon = imu_gyro_2(:,1); q_recon = imu_gyro_2(:,2); r_recon = imu_gyro_2(:,3);
ax_recon = imu_acc_2(:,1); ay_recon = imu_acc_2(:,2); az_recon = imu_acc_2(:,3);

%%
% print
fprintf('Calibrating magnetometer data...\n')

% Find magnetic reference

% Calibrate magnetometer data
imu_mag_calib = zeros(size(imu_mag_2));
for kl = 1:size(imu_mag_2,1)
    imu_mag_calib(kl,1:3) = (1./gain).*(rotM*(imu_mag_2(kl,:)'-ofs));
end

i_sel_mag = i_std_acc;

%% Input signals
% print
fprintf('Storing input signals...\n')
% time vector construction
tvec_px4 = px4.time';
tvec_imu = IMU.time;
Te = 1/250;

tvec = tvec_imu;

mag_in = [tvec_imu imu_mag_calib(:,:)];
rpy_in = [tvec_imu, rpy_data];%, rpy_data(:,2), rpy_data(:,3)];


% redefine time vector
istart = find(tvec_imu >= tstart,1);
tvec_imu = tvec_imu(istart:end)-tvec_imu(istart);
i_mag_ref_cut = max(istart,i_mag_ref(1))-istart+[1:numel(i_mag_ref)];
i_std_acc_cut = max(istart,i_std_acc(1))-istart+[1:numel(i_std_acc)];

gyro_in = [tvec_imu imu_gyro_2(istart:end,:)];
acc_in = [tvec_imu imu_acc_2(istart:end,:)];
mag_in = [tvec_imu imu_mag_calib(istart:end,:)];

px4_in = [tvec_imu, px4_data(istart:end,:)];

rpy_in = [tvec_imu, rpy_data(istart:end,:)];%, rpy_data(:,2), rpy_data(:,3)];

dt_vec = tvec_imu-[0; tvec_imu(1:end-1)];
dt_in = [tvec_imu dt_vec];

%% Magnetic reference calculation
% print
fprintf('Calculating magnetic reference...\n')

% Magnetic reference by LSQ
mbmat = zeros(3*numel(i_mag_ref),1);
Rmat = zeros(3*numel(i_mag_ref),3);

for kl = 1:numel(i_mag_ref)
    icol = 1+(kl-1)*3:kl*3;
    mbmat(icol) = imu_mag_calib(i_mag_ref(1)+kl,:)';
    Rmat(icol,:) = Euler2RotMat(rpy_data(kl,1),...
        rpy_data(kl,2),...
        rpy_data(kl,3));
end

mref = Rmat\mbmat;
%         mref = [-0.4756;-0.1397;0.9531]; % From testcase 9

mag_ref_b_lsq = zeros(numel(tvec),3);
for kl = 1:size(mag_ref_b_lsq,1)
    mag_ref_b_lsq(kl,1:3) = Euler2RotMat(rpy_data(kl,1),...
        rpy_data(kl,2),...
        rpy_data(kl,3))'...
        *mref;
end

g_ref_b = zeros(numel(tvec),3);
for kl = 1:size(g_ref_b,1)
    g_ref_b(kl,1:3) = Euler2RotMat(rpy_data(kl,1),...
        rpy_data(kl,2),...
        rpy_data(kl,3))'...
        *EKF.gvec;
end

EKF.mag_ref = mref;

%% Covariance calculation
% print
fprintf('Calculating covariances...\n')
% - Covariance calculation
% Sensor covariance
k_acc = 1;
k_gyro = 1;
k_mag = 1;
% ---- Magnetometer ----
std_mag = std(mag_in(i_std_acc_cut,2:4));
EKF.Rm = k_mag*diag(std_mag.^2);
normmag = sqrt(mag_in(:,2).^2+mag_in(:,3).^2+mag_in(:,4).^2);

% ---- Acc ----
i_check_gyro_acc = i_std_acc_cut;
std_acc = std(acc_in(i_check_gyro_acc,2:4));%(i_check_gyro_acc,2:4));
EKF.Ra = k_acc*diag(std_acc.^2);
normacc = sqrt(acc_in(:,2).^2+acc_in(:,3).^2+acc_in(:,4).^2);

% ---- Gyro ----
std_gyro = std(gyro_in(i_check_gyro_acc,2:4))';%(i_check_gyro_acc,2:4))';
EKF.cov.gyro = k_gyro*std_gyro.^2;
normgyro = sqrt(gyro_in(:,2).^2+gyro_in(:,3).^2+gyro_in(:,4).^2);


%%
fprintf('Time and auxiliary inputs...\n')

% ---- Start downsample test ----
% ds_rate = 5;
% gyro_in = downsample(gyro_in,ds_rate);
% acc_in = downsample(acc_in,ds_rate);
% mag_in = downsample(mag_in,ds_rate);
% Te  = 1/250*ds_rate;
% Sensors.IMU.acc.fs = 1/Te;  % [Hz] sampling frequency
% Sensors.IMU.gyr.fs = 1/Te;  % [Hz] sampling frequency
% Sensors.IMU.mag.fs = 1/Te;  % [Hz] sampling frequency

% ---- End downsample test ----

EKF.gvec = [0;0;-9.81];
if exist('tstop','var')
    if tstop > 0
        tsim = tstop;
    else
        tsim = tvec_imu(end);
    end
else
    tsim = tvec_imu(end);
end

Ts_attitudeLoop = Te;

Rmat = zeros(3,3,numel(rpy_in(:,1)));
for kl = 1:numel(rpy_in(:,1))
    Rmat(:,:,kl) = Euler2RotMat(double(rpy_in(kl,2)),...
        double(rpy_in(kl,3)),...
        double(rpy_in(kl,4)));
end
R_in.time = rpy_in(:,1);
R_in.signals.values = Rmat;
R_in.signals.dimensions  = [3 3];

try
    Rmat_px4 = zeros(3,3,numel(px4_in(:,1)));
    for kl = 1:numel(px4_in(:,1))
        Rmat_px4(:,:,kl) = Euler2RotMat(double(px4_in(kl,2)),...
            double(px4_in(kl,3)),...
            double(px4_in(kl,4)-(px4_in(5,4)-rpy_in(5,4))));
    end
    Rpx4_in.time = px4_in(:,1);
    Rpx4_in.signals.values = Rmat_px4;
    Rpx4_in.signals.dimensions  = [3 3];
catch
    Rpx4_in.time = tvec_imu;
    Rpx4_in.signals.values = Rmat;
    Rpx4_in.signals.dimensions  = [3 3];
end

init_q_gyro = eul2qua(rpy_in(1,2:4));
EKF.gyrobias = zeros(3,1);

Sensors.IMU.acc.fs = 250;  % [Hz] sampling frequency
Sensors.IMU.gyr.fs = 250;  % [Hz] sampling frequency
Sensors.IMU.mag.fs = 250;  % [Hz] sampling frequency

% Sensor data availability
t250 = (0:1/250:tvec(end))';
dataok =  zeros(size(t250));

for k = 1:numel(tvec)
    [~, ipick] = min(abs(t250 - tvec(k)));
    dataok(ipick) = 1;
end

dataok_in = [t250, dataok];
%% Plotty
% - Magnetic reference reprojection
figure
subplot(311), hold on
plot(tvec,imu_mag_calib(:,1)),  plot(tvec,mag_ref_b_lsq(:,1)),axis tight % plot(tvec,mag_ref_b(:,1)),
subplot(312), hold on
plot(tvec,imu_mag_calib(:,2)),  plot(tvec,mag_ref_b_lsq(:,2)),axis tight % plot(tvec,mag_ref_b(:,2)),
subplot(313), hold on
plot(tvec,imu_mag_calib(:,3)),  plot(tvec,mag_ref_b_lsq(:,3)),axis tight % plot(tvec,mag_ref_b(:,3)),
legend({'Mag-meas','Mag-ref-body_L_S_Q','Mag-ref-body'})

figure
subplot(311), hold on
plot(tvec,imu_acc_2(:,1)),  plot(tvec,g_ref_b(:,1)),axis tight % plot(tvec,mag_ref_b(:,1)),
subplot(312), hold on
plot(tvec,imu_acc_2(:,2)),  plot(tvec,g_ref_b(:,2)),axis tight % plot(tvec,mag_ref_b(:,2)),
subplot(313), hold on
plot(tvec,imu_acc_2(:,3)),  plot(tvec,g_ref_b(:,3)),axis tight % plot(tvec,mag_ref_b(:,3)),
legend({'acc-meas','g-ref-body'})

% figure
% subplot(311), hold on
% plot(tvec,imu_mag_calib(isel,1)), plot(tvec,mag_ref_b_lsq(isel,1))
% subplot(312), hold on
% plot(tvec,imu_mag_calib(isel,2)), plot(tvec,mag_ref_b_lsq(isel,2))
% subplot(313), hold on
% plot(tvec,imu_mag_calib(isel,3)), plot(tvec,mag_ref_b_lsq(isel,3))

% - Accelerometer reading + norm
figure,
subplot(311)
plot(acc_in(:,1),normacc),axis tight, legend('Norm(Acc.)')
subplot(312)
plot(mag_in(:,1),normmag),axis tight, legend('Norm(Mag.)')
subplot(313)
plot(gyro_in(:,1),normgyro),axis tight, legend('Norm(Gyro.)')

% Acc raw vs filtered
%         figure
%         subplot(311), hold on
%         plot(tvec,imu_acc(isel,1),tvec,imu_acc_filt_fft(:,1),tvec,imu_acc_filt_low(:,1)),axis tight
%         subplot(312), hold on
%         plot(tvec,imu_acc(isel,2),tvec,imu_acc_filt_fft(:,2),tvec,imu_acc_filt_low(:,2)),axis tight
%         subplot(313), hold on
%         plot(tvec,imu_acc(isel,3),tvec,imu_acc_filt_fft(:,3),tvec,imu_acc_filt_low(:,3)),axis tight

% Gyro raw vs filtered
figure
subplot(311), hold on
plot(gyro_in(:,1),gyro_in(:,2)), legend('p')%,tvec,imu_gyro_filt_fft(:,1),tvec,imu_gyro_filt_low(:,1)),axis tight
subplot(312), hold on
plot(gyro_in(:,1),gyro_in(:,3)), legend('q')%,tvec,imu_gyro_filt_fft(:,2),tvec,imu_gyro_filt_low(:,2)),axis tight
subplot(313), hold on
plot(gyro_in(:,1),gyro_in(:,4)), legend('r')%,tvec,imu_gyro_filt_fft(:,3),tvec,imu_gyro_filt_low(:,3)),axis tight

%%
% print
fprintf('Calculating Allan variances...\n')
EKF.cov.bgyro = 1.0e-03 * [0.3350; 0.0292; 0.0044]; % Fixed for testcase 9-16

%%
%         AppelAcc;
%
% acc_in = [acc_in(:,1), imu_acc-b_est];
%% ---- State jump for convergence test ----
if ~exist('kinit','var')
    kinit = 0;
end
if kinit == 0
    EKF.bjump = 0; % Jump boolean
else
    EKF.bjump = 1; % Jump boolean
end
EKF.tjump = min(50,round(tsim/2)); % Jump time [s]
EKF.ajump = 180; % Jump amplitude on all axes [deg]
EKF.setjump = [0 0 1];
% ---------------------------

%         % Reverse inputs vector
%         indrev = numel(gyro_in(:,1)):-1:1;
%         gyro_in_rev = [gyro_in(:,1) gyro_in(indrev,2:4)];
%         acc_in_rev = [acc_in(:,1) acc_in(indrev,2:4)];
%         mag_in_rev = [mag_in(:,1) mag_in(indrev,2:4)];
%         gyro_in = gyro_in_rev;
%         acc_in = acc_in_rev;
%         mag_in = mag_in_rev;
%% Update bus structure
% print
fprintf('Done!\n')
% BusInfo = struct2BusObject(EKF, 'EKFBus');
% BusInfo = struct2BusObject(Sensors, 'SensorsBus');

%         close all
%         sim('FT_EKF_IO')
%     end
% end
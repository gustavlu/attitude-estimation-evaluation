% JACOBIANROTMATEULER compute the linearized jacobian of rotation matrix R0
% with respect to Euler angles (Euler Sequence: ZYX)
% [dRdPhi, dRdTheta, dRdPsi] = JACOBIANROTMATEULER(E0)
% We assume: 
% R0 = Rz(psi0)*Ry(theta0)*Rx(phi0) with phi=E0(1), theta=E0(2), psi=E0(3)
% E0: A rotation matrix representing the attitude
% dRdPhi: the corresponding 3x3 Jacobian matrix. i.e., d(R)/d(phi) evaluated at
% R = R0
% dRdTheta: the corresponding 3x3 Jacobian matrix. i.e., d(R)/d(phi) evaluated at
% R = R0
% dRdPsi: the corresponding 3x3 Jacobian matrix. i.e., d(R)/d(phi) evaluated at
% R = R0
% see also JACOBIANQUATERNIONEULER JACOBIANEULERQUATERNION
% JACOBIANROTMATQUATERNION

function [dRdPhi, dRdTheta, dRdPsi] = JacobianRotMatEuler(E0)

phi = E0(1); theta = E0(2); psi = E0(3);
c_phi = cos(phi); s_phi = sin(phi);
c_theta = cos(theta); s_theta = sin(theta);
c_psi = cos(psi); s_psi = sin(psi);

dRdPhi = [0, c_phi*s_theta*c_psi+s_phi*s_psi ,   c_phi*s_psi-s_phi*s_theta*c_psi;...
          0, -s_phi*c_psi+c_phi*s_theta*s_psi, -s_phi*s_theta*s_psi-c_phi*c_psi;...
          0,         c_phi*c_theta           ,           -s_phi*c_theta        ];
dRdTheta = [-s_theta*c_psi, s_phi*c_theta*c_psi, c_phi*c_theta*c_psi;...
            -s_theta*s_psi, s_phi*c_theta*s_psi, c_phi*c_theta*s_psi;...
               -c_theta   ,  -s_phi*s_theta    , -c_phi*s_theta];   
dRdPsi = [-c_theta*s_psi, -s_phi*s_theta*s_psi-c_phi*c_psi, s_phi*c_psi-c_phi*s_theta*s_psi;...
           c_theta*c_psi, -c_phi*s_psi+s_phi*s_theta*c_psi, c_phi*s_theta*c_psi+s_phi*s_psi;...
                 0      ,                 0               ,                 0              ];
     
end
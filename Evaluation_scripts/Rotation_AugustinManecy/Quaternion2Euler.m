%Quaternion2Euler Quaternion to Euler angles conversion
%
%   [PHI, THETA, PSI] = Quaternion2Euler(Q) computes the Euler angles
%   corresponding to the Quaternion Q.
%
%   The convention used here is:
%       - X axis forward  (roll rotation's axis)
%       - Y axis leftward (pitch rotation's axis)
%       - Z axis Upwrad   (yaw rotation's axis)
%   The Combination of intrinsec rotations is assumed to be the yaw rotation first, the pitch
%   and to finish the roll rotation: Q = Qz.Qy.Qx
%
%   Return the Euler angles in radians corresponding to the
%   unit-quaternion Q
%       PHI:   the roll angle in radians
%       THETA: the pitch angle in radians
%       PSI:   the yaw angle in radians
%
%   See also: Euler2Quaternion, Quaternion2AngleAxis, Quaternion2RotMat,
%   RotMat2Quaternion, Euler2RotMat, RotMat2Euler

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Augustin Manecy
%
% Copyright (C) 2011-2014 Augustin Manecy
%
% augustin.manecy@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This file is part of RT-MaG Toolbox.
%
%   RT-MaG Toolbox is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   RT-MaG Toolbox is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with RT-MaG Toolbox.  If not, see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This version of GPL is at https://www.gnu.org/licenses/gpl-3.0.txt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [PHI, THETA, PSI] = Quaternion2Euler(Q)

    %#codegen

    PHI     = atan2(2*(Q(3,:).*Q(4,:)+Q(1,:).*Q(2,:)), 1-2*(Q(2,:).^2+Q(3,:).^2));
    THETA   = -asin(2*(Q(2,:).*Q(4,:)-Q(1,:).*Q(3,:)));
    PSI     = atan2(2*(Q(3,:).*Q(2,:)+Q(1,:).*Q(4,:)), 1-2*(Q(3,:).^2+Q(4,:).^2));
  
%     PHI = atan2((Q(2,:)+Q(4,:)),(Q(1,:)-Q(3,:)))-atan2((Q(4,:)-Q(2,:)),(Q(1,:)+Q(3,:)));
%     THETA = -asin(2*(Q(2,:).*Q(4,:)-Q(1,:).*Q(3,:)));
%     PSI = atan2((Q(2,:)+Q(4,:)),(Q(1,:)-Q(3,:)))+atan2((Q(4,:)-Q(2,:)),(Q(1,:)+Q(3,:)));

end

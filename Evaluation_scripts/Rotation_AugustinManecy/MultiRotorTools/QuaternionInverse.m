function [ q_inv ] = QuaternionInverse( q )
%QUATERNIONINVERSE Compute the inverse of a quaternion
%   For a quaternion q = (s,v1,v2,v3)^T = (s,v^T)^T, the inverse quaternion
%   q^(-1) = q*/(q^T q), where q* = (s,-v^T)^T is the conjugate quaternion.

q_conj = [q(1); -q(2:4)];
q_inv = q_conj/(q.'*q);


end


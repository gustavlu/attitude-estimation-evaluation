clc;
close all;
clear all;

deg2rad = pi/180;
rad2deg = 180/pi;

phi = -720:0.05:720;
theta = zeros(size(phi));
psi = zeros(size(phi));

Euler = [phi;theta;psi]*deg2rad;

Q_est = zeros(4, length(phi));
Q_ref = zeros(4, length(phi));
Q_tilde = zeros(4, length(phi));
e = zeros(3, length(phi));
for i=1:1:length(phi)
    Q_est(:,i) = Euler2Quaternion(Euler(1,i), Euler(2,i), Euler(3,i));
    Q_ref(:,i) = Euler2Quaternion(0, 0, 0);
    [e(:,i), Q_tilde(:,i)] = quaternionErrorFct(Q_est(:,i), Q_ref(:,i));
end
subplot(3,1,1)
plot(phi, e(1,:), phi, e(2,:), phi, e(3,:))
subplot(3,1,2)
plot(phi, Q_est)
subplot(3,1,3)
plot(phi, Q_tilde)
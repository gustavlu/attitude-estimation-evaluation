clc;
clear all;
close all;

uav = 'S80';
safety_ratio = 0.5;
v_asc = 5;

cT_S800 = 1.07907e-5;   % [kg]
fmax_S800 = 100;        % [Hz]
m_S800 = 3.3;           % [kg]
r_S800 = 0.19;          % [m]
n_S800 = 6;             % [-] number of rotor

cT_x4mag = 2.55e-7;   % [kg]
fmax_x4mag = 120;     % [Hz]
m_x4mag = 0.307;      % [kg]
r_x4mag = 0.06;       % [m]
n_x4mag = 4;          % [-] number of rotor

if (strcmp(uav, 'S800'))
    cT = cT_S800;
    fmax = fmax_S800;
    m = m_S800;
    r = r_S800;
    n = n_S800;
else
    cT = cT_x4mag;
    fmax = fmax_x4mag;
    m = m_x4mag;
    r = r_x4mag;
    n = n_x4mag;
end

g = 9.81;
rho = 1.225;
omega_max = 2*pi*fmax;
S = pi*r^2;
Tmax = cT*g*omega_max^2;        % [N]
Tsafe = cT*g*omega_max^2*safety_ratio
vimax = sqrt(Tmax/(2*rho*S))    % [m/s]
visafe = sqrt(Tsafe/(2*rho*S))  % [m/s]
omega_0 = sqrt(m/cT/n)          % [rad/s]
T0 = cT*g*omega_0^2
vi0 = sqrt(m*g/n/(2*rho*S))
vi_asc = vi0/(v_asc/(2*vi0)+sqrt(1+((v_asc/(2*vi0))^2)))
cT_asc = 2*rho*S*vi_asc^2/omega_0^2
cT_ratio = cT_asc/(cT*g)

vv = -7*vi0:0.1:7*vi0;
vh = -7*vi0:0.1:7*vi0;
for i=1:1:length(vv)
    viv(i) = vi0/(vv(i)/(2*vi0)+sqrt(1+(vv(i)/(2*vi0))^2));
    a = 4*rho^2*S^2;
    b = 4*rho^2*S^2*vh(i)^2;
    c = -(m*g)^2;
    delta = b^2-4*a*c;
    vih(i) = sqrt((-b+sqrt(delta))/(2*a))/2;
end
plot(vv/vi0, viv./vi0, 'b')
hold on
plot(vh/vi0, vih./vi0, 'r')
plot(vh/vi0, vih2./vi0, 'g')
plot(vh/vi0, vih3./vi0, 'c')
legend('vitesse vertical', 'vitesse horizontal')
function Uav = preprocessMultiRotor(Uav)
    
    rhoAir = 1.225; % [kg.m^-3] air density
    deg2rad = pi/180;
    
    % compute the Uav mixer matrix
    [Uav.Mixer, Uav.D] = computeMultiRotorMixer(Uav);
    % compute the inverse mixer matrix
    Uav.iMixer = pinv(Uav.Mixer);
    % compute some constant properties
    Uav.propellers.Izz     = Uav.propellers.I(3,3);                                           % inertia around z axis
    Uav.propellers.lock    = rhoAir*Uav.propellers.a*Uav.propellers.c*Uav.propellers.r^4/Uav.propellers.Izz;   % Lock number
    Uav.propellers.sigma   = 2*Uav.propellers.c/(pi*Uav.propellers.r);
    Uav.propellers.S       = pi*Uav.propellers.r^2;
    Uav.propellers.theta0_rad = deg2rad*Uav.propellers.theta0;
    Uav.propellers.theta1_rad = deg2rad*Uav.propellers.theta1;
    % compute saturation due to physical elements
    [Uav.Thrust_sat, Uav.Torques_sat] = computeMultiRotorSat(Uav);

end
%computeMultiRotorMixer computes the thrust mixer matrix of a multirotor
%
%   [M_tm, OT] = computeHexaMixer(multiRotor)
%
%   multiRotor: a multiRotor structure
%
%   Return a [4xn] matrix, where n is the number of rotor
%
%   See also: plotHexa

function [M_tm, OT] = computeMultiRotorMixer(multiRotor)

    deg2rad = pi/180;
    bz = [0; 0; 1];

    Rphi_CW     = Xrotation(-multiRotor.gamma*deg2rad);    % rotation matrix resulting from the motor wedge (Clock Wise motors)
    Rphi_CCW    = Xrotation(multiRotor.gamma*deg2rad);     % rotation matrix resulting from the motor wedge (Counter Clock Wise motors)
    Rtheta      = Yrotation(multiRotor.alpha*deg2rad);     % rotation matrix resulting from the arm tilt
    
    Tp_CW   = multiRotor.propellers.cT*Rphi_CW*Rtheta*bz;     % direction of the thrust in the body fixed frame (for a rotor with beta = 0 deg)
    Tp_CCW  = multiRotor.propellers.cT*Rphi_CCW*Rtheta*bz;    % direction of the thrust in the body fixed frame (for a rotor with beta = 0 deg)
    
    beta    = zeros(1, multiRotor.nbRotor);  % yaw angle of the rotors, in the body fixed frame
    OT      = zeros(3, multiRotor.nbRotor);  % origin of the thrust center of the rotors, in the body fixed frame
    T       = zeros(3, multiRotor.nbRotor);  % thrust of the rotors, in the body fixed frame
    D       = zeros(3, multiRotor.nbRotor);  % torque resulting to drag of the rotors, in the body fixed frame
    M_Gamma = zeros(3, multiRotor.nbRotor);  % torques mixer matrix
    % for each rotor
    for i=1:1:multiRotor.nbRotor
        beta(i)  = multiRotor.beta1*deg2rad + pi/(multiRotor.nbRotor/2)*(i-1);    
        Rz = Zrotation(beta(i));
        OT(:, i) = Rz*[ multiRotor.l + multiRotor.L*cos(multiRotor.alpha);...% - multiRotor.hM*sin(multiRotor.alpha);...
                               0        ;...
                       multiRotor.hG + multiRotor.L*sin(multiRotor.alpha)]; % + multiRotor.hM*cos(multiRotor.alpha)];
                   
        if ( multiRotor.rotorDirection(i) > 0 )
            T(:, i) = Rz*Tp_CCW;
            D(:, i) = Rphi_CCW*Rtheta*Rz*[0; 0; -multiRotor.propellers.cQ];
        else
            T(:, i) = Rz*Tp_CW;
            D(:, i) = Rphi_CW*Rtheta*Rz*[0; 0; multiRotor.propellers.cQ];
        end
        
        M_Gamma(:, i) = cross(OT(:, i), T(:, i)) + D(:, i);
    end
    M_tm = [ T(3, :) ;...
             M_Gamma ];
    
end
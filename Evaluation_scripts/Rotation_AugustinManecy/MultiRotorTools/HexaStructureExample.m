clc;
clear all;
close all;

% universal constants
g = 9.81;           % [m.s^-2] gravition constant
rhoAir = 1.2041;    % [kg.m^-3] air density

% structure multiRotor (HEXA)

% hexa:
% theta0 = 23 deg, angle of attack at the rotor shaft
% thetaT = 5.7 deg, angle of attack at the blade tip

Hexa.nbRotor    = 6;        % [-]       Number of rotor of the multirotor
Hexa.m          = 5.0;      % [kg]      Mass of the multirotor
Hexa.hG         = 0.05;     % [m]       orthogonal distance between rotors' plane and the center of gravity 
Hexa.l          = 0.165;    % [m]       half span of the central frame
Hexa.L          = 0.25;     % [m]       span of an arm,
Hexa.hM         = 0.04;     % [m]       height of a motor,
Hexa.alpha      = -9;       % [deg]     tilt angle of an arm
Hexa.gamma      = 1;        % [deg]     tilt angle (absolute value) of the motor due to the wedge (for rotor 1)
Hexa.beta1      = -30;      % [deg]     yaw angle of the arm 1 in the body fixed frame [rad]
Hexa.rotorDirection = [1, -1, 1, -1, 1, -1];    % [-1, 1]   direction of rotation of the rotor 1, ( -1: ClockWise (CW), 1: CounterClockWise (CCW) )
Hexa.Kdrag      = [0.5, 0.5, 0.5];

Hexa.propellers.cT      = 2.5e-6;   % [kg.s^2.rad^2]    thrust coefficient (T = cT*omega_r^2)
Hexa.propellers.cQ      = 2.1e-7;   % [kg.s^2.rad^2]    drag coefficient   (D = cQ*omega_r^2)
Hexa.propellers.theta0  = 23;       % [deg]             angle of attack of the propellers at the rotor shaft
Hexa.propellers.theta1  = 17.3;     % [deg]             equivalent twist angle
Hexa.propellers.c       = 0.033;    % [m]               mean chord of the propeller   
Hexa.propellers.r       = 0.191;    % [m]               radius of the propeller
Hexa.propellers.a       = 6;        %                   coefficient
Hexa.propellers.I       = slabInertia(Hexa.propellers.r, Hexa.propellers.c, 0.002, 0.017);  % inertia matrix of the propeller
Hexa.propellers.Izz     = Hexa.propellers.I(3,3);                                           % inertia around z axis
Hexa.propellers.lock    = rhoAir*Hexa.propellers.a*Hexa.propellers.c*Hexa.propellers.r^4;   % Lock number

Hexa.motors.omega_max   = 2*pi*60;  % [rad.s^-1]    maximum pulsation speed of motors (2*pi*fmax)
Hexa.motors.omega_min   = 2*pi*6;   % [rad.s^-1]    minimum pulsation speed of motors (2*pi*fmax)
Hexa.motors.m           = 10e-3;    % [kg]          motor mass
Hexa.motors.r           = 0.01;     % [m]           motor radius
Hexa.motors.h           = 0.02;     % [m]           motor height
Hexa.motors.I           = cylinderInertia(Hexa.motors.r, Hexa.motors.h, Hexa.motors.m);

Hexa.Mixer = computeMultiRotorMixer(Hexa);
% create the corresponding Simulink bus object
BusInfo = struct2BusObject(Hexa, 'hexaBus');


mode = 'fixed'; % 'fixed', 'seq'
switch mode
    case 'fixed'
        P0 = [0; 0; 0];
        R0 = eye(3,3);
        % R0 = Xrotation(pi/6);
        
        figure(1); clf(1);
        plotMultiRotor(Hexa, P0, R0);
        view(3)
        xlim([-2 2])
        ylim([-2 2])
        zlim([-2 2])
        axis square
        
    case 'seq'
        step = 0.1;
        % disp1
        Z1 = [0:step:2];
        X1 = [zeros(1, length(Z1))];
        Y1 = [zeros(1, length(Z1))];
        % disp2
        X2 = [X1(end):step:2];
        Y2 = [Y1(end)*ones(1, length(X2))];
        Z2 = [Z1(end)*ones(1, length(X2))];
        % disp3
        Y3 = [Y2(end):step:2];
        X3 = [X2(end)*ones(1, length(Y3))];
        Z3 = [Z2(end)*ones(1, length(Y3))];
        % disp4
        X4 = [X3(end):-step:-2];
        Y4 = [Y3(end)*ones(1, length(X4))];
        Z4 = [Z3(end)*ones(1, length(X4))];
        % disp3
        Y5 = [Y4(end):-step:-2];
        X5 = [X4(end)*ones(1, length(Y5))];
        Z5 = [Z4(end)*ones(1, length(Y5))];

        X = [X1, X2, X3, X4, X5];
        Y = [Y1, Y2, Y3, Y4, Y5];
        Z = [Z1, Z2, Z3, Z4, Z5];
        
        R = Xrotation(pi/6);
        tic;
        for i=1:1:length(X)
            figure(1); clf(1);
            P = [X(i); Y(i); Z(i)];
            plotMultiRotor(Hexa, P, R);
            %view([0 0])
            view(3)
            xlim([-4 4])
            ylim([-4 4])
            zlim([-4 4])
            xlabel('x')
            ylabel('y')
            zlabel('z')
        end
        dt = toc;
        ['Mean refresh rate: ' num2str(length(X)/dt) ' Hz']
end




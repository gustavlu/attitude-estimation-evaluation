function [a1s, b1s, Rflap] = computeFlapping(Uav, k, omega_r, q, V, Omega)

    [~, ~, psi] = Quaternion2Euler(q);
    Vb = QuaternionVectorProduct(InvQuaternion(q),V) + cross(Omega, Uav.D(:,k)); % R^T*V + Omega x D; apprarent wind in body frame
    omega_r = omega_r(k)*Uav.rotorDirection(k);
    mu = sqrt(Vb(1)^2+Vb(2)^2)/(omega_r*Uav.propellers.r);
    beta = atan2(Vb(2), Vb(1))-psi;  % determine the flight direction in the body frame
        % flapping angle and rotation matrix for each rotor
    a1s = ((8/3*Uav.propellers.theta0_rad*mu + 2*Uav.propellers.theta1_rad*mu - Uav.propellers.cT)/(1-mu^2/2) + (-16*Omega(2)/(Uav.propellers.lock*omega_r)+ Omega(1)/(omega_r))/(1-mu^2/2));
    b1s = -((4/3)/(1+mu^2/2)*(2/3*Uav.propellers.lock*mu/Uav.propellers.a*Uav.propellers.cT/Uav.propellers.sigma + mu)+((-16*Omega(1)/(Uav.propellers.lock*omega_r)+Omega(2)/(omega_r))/(1+mu^2/2)));
    Rflap = Zrotation(beta)*Yrotation(a1s)*Xrotation(b1s);

end
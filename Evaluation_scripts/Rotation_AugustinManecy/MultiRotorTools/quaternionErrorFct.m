% QUATERNIONERRORFCT computes the angular error between two attitude quaternions, i.e., quaternion version of Q2-Q1
%
% [e, Q_tilde] = QUATERNIONERRORFCT(Q1, Q2)
% Q1: a unit quaternion
% Q2: a unit quaternion
% e: [3x1] error corresponding to the angular error: "Q2-Q1" [rad]
% Q_tilde: error quaternion
% 
% see also: QuaternionProduct, InvQuaternion, Quaternion

function [e, Q_tilde] = quaternionErrorFct(Q1, Q2)

    Q_tilde = QuaternionProduct(InvQuaternion(Q1), Q2);
    s_tilde = Q_tilde(1);
    s_tilde = acos(Q_tilde(1))*2;
    v_tilde = Q_tilde(2:4)/(sin(s_tilde/2)+eps);
    e = s_tilde*v_tilde;

end

%RotMatSeq Euler angles to rotation matrix conversion
%
%   R = RotMatSeq(SEQ) computes the symbolic rotation matrix
%   corresponding to the Euler angles PHI, THETA and PSI for thze sequence
%   SEQ
%
%       SEQ:   'ZYX', 'ZXY', 'YXZ', 'YZX', 'XYZ', or 'XZY'.
%
%   For example the Sequence SEQ 'XYZ' corresponds to the intrinsec rotation order 
%   R1 = Z Axis Rotation, R2 = Y Axis Rotation, and R3 = X Axis Rotation. 
%   The convention used here is:
%       - X axis forward  (roll rotation's axis)
%       - Y axis leftward (pitch rotation's axis)
%       - Z axis Upward   (yaw rotation's axis)
%
%   Return the symbolic rotation matrix corresponding to the sequence SEQ.
%
%   See also: Euler2RotMat, Xrotation, Yrotation, Zrotation, Euler2Quaternion, 
%   Quaternion2AngleAxis, Quaternion2RotMat, RotMat2Quaternion

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Augustin Manecy
%
% Copyright (C) 2011-2014 Augustin Manecy
%
% augustin.manecy@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This file is part of RT-MaG Toolbox.
%
%   RT-MaG Toolbox is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   RT-MaG Toolbox is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with RT-MaG Toolbox.  If not, see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This version of GPL is at https://www.gnu.org/licenses/gpl-3.0.txt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function R = RotMatSeq(SEQ)

    syms Rx Ry Rz PHI THETA PSI R

    Rx = [1    0         0    ;...
          0 cos(PHI) -sin(PHI);...
          0 sin(PHI)  cos(PHI)];
      
    Ry = [ cos(THETA) 0 sin(THETA);...
                0     1     0     ;...
          -sin(THETA) 0 cos(THETA)];
      
    Rz = [cos(PSI) -sin(PSI) 0;...  
          sin(PSI)  cos(PSI) 0;...
             0         0     1];
    
    switch SEQ
        case 'ZYX'
            R = Rx*Ry*Rz;
        case 'ZXY'
            R = Ry*Rx*Rz;
        case 'YXZ'
            R = Rz*Rx*Ry;
        case 'YZX'
            R = Rx*Rz*Ry;
        case 'XYZ'
            R = Rz*Ry*Rx;
        case 'XZY'
            R = Ry*Rz*Rx;
    end
end
%RotMat2Euler2 Quaternion to Euler angles conversion
%
%   [PHI, THETA, PSI] = RotMat2Euler2(R) computes the Euler angles
%   corresponding to the rotation matrix R.
%
%   The convention used here is:
%       - X axis forward  (roll rotation's axis)
%       - Y axis leftward (pitch rotation's axis)
%       - Z axis Upwrad   (yaw rotation's axis)
%   The Combination of rotation is assumed to be the yaw rotation first, the pitch 
%   and to finish the roll rotation: R = Rz.Ry.Rx
%   So, R is assumed to be:
%   
%   R = [ c(PSI)*c(THETA), c(PSI)*s(PHI)*s(THETA) - c(PHI)*s(PSI), s(PHI)*s(PSI) + c(PHI)*c(PSI)*s(THETA);...
%         c(THETA)*s(PSI), c(PHI)*c(PSI) + s(PHI)*s(PSI)*s(THETA), c(PHI)*s(PSI)*s(THETA) - c(PSI)*s(PHI);...
%            -s(THETA)   ,              c(THETA)*s(PHI)          ,            c(PHI)*c(THETA)            ]
%
%   Return the Euler angles in radians corresponding to the rotation matrix
%   R:
%       PHI:   the roll angle in radians
%       THETA: the pitch angle in radians
%       PSI:   the yaw angle in radians
%
%   See also: Euler2RotMat, Xrotation, Yrotation, Zrotation, Euler2Quaternion, 
%   Quaternion2AngleAxis, Quaternion2RotMat, RotMat2Quaternion

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Augustin Manecy
%
% Copyright (C) 2011-2014 Augustin Manecy
%
% augustin.manecy@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This file is part of RT-MaG Toolbox.
%
%   RT-MaG Toolbox is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   RT-MaG Toolbox is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with RT-MaG Toolbox.  If not, see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This version of GPL is at https://www.gnu.org/licenses/gpl-3.0.txt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [phi, theta, psi] = RotMat2Euler(R)
    
    %#codegen

    phi   = atan2(R(3,2), R(3,3));
    theta = -asin(R(3,1));
    psi   = atan2(R(2,1), R(1,1)); 

end
% EULERCOV2EULERSTD computes the standard deviation from the euler
% covariance matrix
% se = EULERCOV2EULERSTD(Qe)
% Qe: Covariance matrix of euler angles
% se: standard deviation of euler angles: se = [phi_std;tehta_std;psi_std]
%
% see also QUATERNIONCOV2EULERSTD

function se = EulerCov2EulerSTD(Qe)

%     [~, D] = eig(Qe);
%     se = [sqrt(D(1,1));...
%           sqrt(D(2,2));...
%           sqrt(D(3,3))];
    % according to cov2corr of matlab not needed to diagonalize the matrix before  
    se = [sqrt(Qe(1,1));...
          sqrt(Qe(2,2));...
          sqrt(Qe(3,3))];

end
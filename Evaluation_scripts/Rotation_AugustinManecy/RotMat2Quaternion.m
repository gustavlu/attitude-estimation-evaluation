%RotMat2Quaternion Rotation matrix to quaternion conversion
%
%   Q = RotMat2Quaternion(R) computes the unit-quaternion corresponding to
%   the rotation matrix R
%
%   R: is a [3x3] rotation matrix
%
%   Return a unit-quaternion Q
%
%   See also: Quaternion2RotMat, Euler2Quaternion, Quaternion2Euler,
%   RotMat2Euler, Euler2RotMat

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Augustin Manecy
%
% Copyright (C) 2011-2014 Augustin Manecy
%
% augustin.manecy@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This file is part of RT-MaG Toolbox.
%
%   RT-MaG Toolbox is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   RT-MaG Toolbox is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with RT-MaG Toolbox.  If not, see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This version of GPL is at https://www.gnu.org/licenses/gpl-3.0.txt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Q = RotMat2Quaternion(R)
    % Default
    q0 = 1; q1 = 0; q2 = 0; q3 = 3;
    %#codegen

    T = R(1,1) + R(2,2) + R(3,3) + 1;
    if (T > 0)
        q0 = sqrt(T)/2;
        q1 = (R(3,2)-R(2,3))/(4*q0);
        q2 = (R(1,3)-R(3,1))/(4*q0);
        q3 = (R(2,1)-R(1,2))/(4*q0);
    else
        [~, index] = max([R(1,1) R(2,2) R(3,3)]);
        switch index
            case 1
                T = R(1,1) - R(2,2) - R(3,3) + 1;
                q1 = sqrt(T)/2;
                q0 = (R(2,3)-R(3,2))/(4*q1);
                q2 = (R(1,3)-R(3,1))/(4*q1);
                q3 = (R(2,1)-R(1,2))/(4*q1);
            case 2
                T = - R(1,1) + R(2,2) - R(3,3) + 1;
                q2 = sqrt(T)/2;
                q0 = (R(1,3)-R(3,1))/(4*q2);
                q1 = (R(1,2)+R(2,1))/(4*q2);
                q3 = (R(3,2)+R(2,3))/(4*q2);
            case 3
                T = - R(1,1) - R(2,2) + R(3,3) + 1;
                q3 = sqrt(T)/2;
                q0 = (R(2,1)-R(1,2))/(4*q3);
                q1 = (R(1,3)+R(3,1))/(4*q3);
                q2 = (R(2,3)+R(3,2))/(4*q3);
        end
    end
    Q = [q0; q1; q2; q3];
end

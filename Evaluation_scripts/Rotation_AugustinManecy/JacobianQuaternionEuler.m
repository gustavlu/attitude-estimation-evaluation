% JACOBIANQUATERNIONEULER compute the linearized jacobian of quaternion 
% with respect to Euler angles (Euler Sequence: ZYX)
% Jq = JACOBIANQUATERNIONEULER(E0)
% E0: Euler angles [phi; theta; psi] in ZYX sequence [rad]
% Je: the corresponding 4x3 Jacobain matrix. i.e., d(q)/d(Euler) evaluated at
% Euler = E0
% see also JACOBIANQUATERNIONEULER

function Jq = JacobianQuaternionEuler(E0)

phi = E0(1); the = E0(2); psi = E0(3);

cphi = cos(phi/2); sphi = sin(phi/2);
cthe = cos(the/2); sthe = sin(the/2);
cpsi = cos(psi/2); spsi = sin(psi/2);

dq1dphi = .5*(cphi*sthe*spsi-sphi*cthe*cpsi); 
dq1dthe = .5*(sphi*cthe*spsi-cphi*sthe*cpsi);
dq1dpsi = .5*(sphi*sthe*cpsi-cphi*cthe*spsi);

dq2dphi = .5*(cphi*cthe*cpsi+sphi*sthe*spsi); 
dq2dthe = .5*(-sphi*sthe*cpsi-cphi*cthe*spsi);
dq2dpsi = .5*(-sphi*cthe*spsi-cphi*sthe*cpsi);

dq3dphi = .5*(-sphi*sthe*cpsi+cphi*cthe*spsi); 
dq3dthe = .5*(cphi*cthe*cpsi-sphi*sthe*spsi);
dq3dpsi = .5*(-cphi*sthe*spsi+sphi*cthe*cpsi);

dq4dphi = .5*(-sphi*cthe*spsi-cphi*sthe*cpsi); 
dq4dthe = .5*(-cphi*sthe*spsi-sphi*cthe*cpsi);
dq4dpsi = .5*(cphi*cthe*cpsi+sphi*sthe*spsi);

Jq = [dq1dphi, dq1dthe, dq1dpsi;...
      dq2dphi, dq2dthe, dq2dpsi
      dq3dphi, dq3dthe, dq3dpsi
      dq4dphi, dq4dthe, dq4dpsi];

end
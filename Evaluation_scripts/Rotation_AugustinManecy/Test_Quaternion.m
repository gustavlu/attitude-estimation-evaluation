%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Augustin Manecy
%
% Copyright (C) 2011-2014 Augustin Manecy
%
% augustin.manecy@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This file is part of RT-MaG Toolbox.
%
%   RT-MaG Toolbox is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   RT-MaG Toolbox is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with RT-MaG Toolbox.  If not, see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This version of GPL is at https://www.gnu.org/licenses/gpl-3.0.txt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
close all;
clear all;

phi = 10;
theta = 89;
psi = 25;

deg2rad = pi/180;
rad2deg = 180/pi;

phi_rad = deg2rad * phi;
theta_rad = deg2rad * theta;
psi_rad = deg2rad * psi;

% constuct corresponding rotation matrix
R = Zrotation(phi_rad)*Yrotation(theta_rad)*Xrotation(psi_rad)
[phiR_res, thetaR_res, psiR_res] = RotMat2Euler(R);
phiR_res = phiR_res*rad2deg
thetaR_res = thetaR_res*rad2deg
psiR_res = psiR_res*rad2deg

% convert R to Q
Q = RotMat2Quaternion(R)
[phi_res, theta_res, psi_res] = Quaternion2Euler(Q);
phi_res = phi_res*rad2deg
theta_res = theta_res*rad2deg
psi_res = psi_res*rad2deg

% constrcut the quaternion for each rotation
Qx = Quaternion(phi_rad, [1;0;0]);
Qy = Quaternion(theta_rad, [0;1;0]);
Qz = Quaternion(psi_rad, [0;0;1]);

% recover theses angles and axis
[phiQ_res, Axis_X] = Quaternion2AngleAxis(Qx);
[thetaQ_res, Axis_Y] = Quaternion2AngleAxis(Qy);
[psiQ_res, Axis_Z] = Quaternion2AngleAxis(Qz);
phiQ_res = phiQ_res*rad2deg
Axis_X
thetaQ_res = thetaQ_res*rad2deg
Axis_Y
psiQ_res = psiQ_res*rad2deg
Axis_Z

% construct the global rotation
Qxyz = Euler2Quaternion(phi_rad, theta_rad, psi_rad);

Rq = Quaternion2RotMat(Qxyz)

% extract the euler angles
[phi_res, theta_res, psi_res] = Quaternion2Euler(Qxyz);
phi_res = phi_res*rad2deg
theta_res = theta_res*rad2deg
psi_res = psi_res*rad2deg

%% Jacobain and covariance
clc;
deg2rad = pi/180; rad2deg = 180/pi;
phi0 = 10*pi/180;
theta0 = 20*pi/180;
psi0 = 30*pi/180;
Euler0 = [phi0, theta0, psi0]';
Q0 = Euler2Quaternion(phi0, theta0, psi0);
[phi, theta, psi] = Quaternion2Euler(Q0);
phi = phi*rad2deg, theta = theta*rad2deg, psi = psi*rad2deg

sigma_Euler0 = 1/3*[2, 2, 4]'*deg2rad;
Pe = diag(sigma_Euler0.^2);
dqde = JacobianQuaternionEuler(Euler0);
Pq = dqde*diag(sigma_Euler0.^2)*dqde';
dedq = JacobianEulerQuaternion(Q0);
Pe2 = dedq*Pq*dedq';

Pe
Pq
Pe2
        

%% test efficiency
V = [1;2;3]/norm([1;2;3]);
tic
for i=1:1:10000
    Vr = QuaternionProduct(QuaternionProduct(Qxyz,[0; V(:)]),InvQuaternion(Qxyz));
end
toc

tic
for i=1:1:10000
    Vr = Quaternion2RotMat(Qxyz)*V;
end
toc

Q = Qxyz;
tic
for i=1:1:100000
    s = Q(1); 
    v1 = Q(2); v2 = Q(3); v3 = Q(4);

    R = [ s^2+v1^2-v2^2-v3^2 ,    2*(v1*v2-s*v3)  ,   2*(v1*v3+s*v2)  ;...
             2*(v1*v2+s*v3)  , s^2-v1^2+v2^2-v3^2 ,   2*(v2*v3-s*v1)  ;...
             2*(v1*v3-s*v2)  ,    2*(v2*v3+s*v1)  , s^2-v1^2-v2^2+v3^2];
end
toc
R

tic
for i=1:1:100000
    s = Q(1); 
    v1 = Q(2); v2 = Q(3); v3 = Q(4);

    R = [ 1-2*(v2^2+v3^2) , 2*(v1*v2-s*v3)  , 2*(v1*v3+s*v2) ;...
          2*(v1*v2+s*v3)  , 1-2*(v1^2+v3^2) , 2*(v2*v3-s*v1) ;...
          2*(v1*v3-s*v2)  , 2*(v2*v3+s*v1)  , 1-2*(v1^2-v2^2)];
end
toc
R

% QUATERNIONCOV2EULERCOV computes the euler covariance matrix,
% corresponding to a given quaternion covariance matrix at a given attitude q0
% QUATERNIONCOV2EULERCOV(q0, Qq)
% q0: linearization point (current attitude quaternion)
% Qq: Quaternion covariance matrix
%
% see also EULERCOV2QUATERNIONCOV

function Qe = QuaternionCov2EulerCov(q0, Qq)

    dedq = JacobianEulerQuaternion(q0);
    Qe = dedq*Qq*dedq';

end
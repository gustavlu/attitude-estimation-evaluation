% QUATERNIONCOV2EULERSTD computes the standard deviation from the
% quaternion covariance matrix
% se = QUATERNIONCOV2EULERSTD(Qq)
% Qq: Quaternion covariance matrix
% q0: attitude quaternion to use for linearization
% se: standard deviation of euler angles: se = [phi_std;tehta_std;psi_std]
%
% see also QUATERNIONCOV2EULERSTD

function se = QuaternionCov2EulerSTD(Qq, q0)

    Qe = QuaternionCov2EulerCov(q0, Qq);
    se = EulerCov2EulerSTD(Qe);

end
%Quaternion2RotMat Quaternion to Rotation matrix conversion
%
%   R = Quaternion2RotaMat(Q) computes the rotation matrix corresponding to
%   the unit-quaternion Q
%
%   Q: is a unit-quaternion (you can normalize a quaternion with
%      UnitQuaternion(Q)
%
%   Return a [3x3] rotation matrix 
%
%   See also: RotMat2Quaternion, Euler2Quaternion, Quaternion2Euler,
%   RotMat2Euler, Euler2RotMat

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Augustin Manecy
%
% Copyright (C) 2011-2014 Augustin Manecy
%
% augustin.manecy@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This file is part of RT-MaG Toolbox.
%
%   RT-MaG Toolbox is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   RT-MaG Toolbox is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with RT-MaG Toolbox.  If not, see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This version of GPL is at https://www.gnu.org/licenses/gpl-3.0.txt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function R = Quaternion2RotMat(Q)

    %#codegen

    s = Q(1); 
    v1 = Q(2); v2 = Q(3); v3 = Q(4);

%     R = [ s^2+v1^2-v2^2-v3^2 ,    2*(v1*v2-s*v3)  ,   2*(v1*v3+s*v2)  ;...
%              2*(v1*v2+s*v3)  , s^2-v1^2+v2^2-v3^2 ,   2*(v2*v3-s*v1)  ;...
%              2*(v1*v3-s*v2)  ,    2*(v2*v3+s*v1)  , s^2-v1^2-v2^2+v3^2];

    % more efficient     
    R = [ 1-2*(v2^2+v3^2) , 2*(v1*v2-s*v3)  , 2*(v1*v3+s*v2) ;...
          2*(v1*v2+s*v3)  , 1-2*(v1^2+v3^2) , 2*(v2*v3-s*v1) ;...
          2*(v1*v3-s*v2)  , 2*(v2*v3+s*v1)  , 1-2*(v1^2+v2^2)];
end

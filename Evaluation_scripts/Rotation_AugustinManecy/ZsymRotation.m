%Zrotation matrix representation of a Z-axis rotation
%
%   Rz = ZsymRotation computes the [3x3] symbolic rotation matrix corresponding to
%   a rotation of angle PSI around the Z-axis
%
%   Name: a string corresponding to the index for the angle THETA
%
%   Return a rotation [3x3] matrix 
%
%   See also: Xrotation, Yrotation, Euler2RotMat, RotMat2Euler,
%   RotMat2Quaternion, Quaternion2RotMat

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Augustin Manecy
%
% Copyright (C) 2011-2014 Augustin Manecy
%
% augustin.manecy@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This file is part of RT-MaG Toolbox.
%
%   RT-MaG Toolbox is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   RT-MaG Toolbox is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with RT-MaG Toolbox.  If not, see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This version of GPL is at https://www.gnu.org/licenses/gpl-3.0.txt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function Rz = ZsymRotation(Name)

    %#codegen
    
    VariableName = ['PSI_' Name];
    eval(['syms ' VariableName])

    eval(['Rz = [cos(' VariableName '), -sin(' VariableName '), 0; sin(' VariableName '), cos(' VariableName ') , 0; 0, 0, 1];']);

end
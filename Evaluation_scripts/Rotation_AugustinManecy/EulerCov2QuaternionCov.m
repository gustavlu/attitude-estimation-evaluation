% EULERCOV2QUATERNIONCOV computes the quaternion covariance matrix,
% corresponding to a given euler covariance matrix at the given euler point
% EULERCOV2QUATERNIONCOV(E0, Qe)
% E0: linearization point (current euler angles, 
%       e.g., E0 = [phi0, theta0, psi0]')
% Qe: Euler covariance matrix
%
% see also QUATERNIONCOV2EULERCOV

function Qq = EulerCov2QuaternionCov(E0, Qe)

    dqde = JacobianQuaternionEuler(E0);
    Qq = dqde*Qe*dqde';

end
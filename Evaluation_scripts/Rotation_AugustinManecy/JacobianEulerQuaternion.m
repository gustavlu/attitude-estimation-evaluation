% JACOBIANEULERQUATERNION compute the linearized jacobian of Euler angles 
% with respect to a quaternion (Euler Sequence: ZYX)
% Je = JACOBIANEULERQUATERNION(Q0)
% Q0: A unit-quaternion representing the attitude
% Je: the corresponding 3x4 Jacobain matrix. i.e., d(Euler)/d(q) evaluated at
% q = Q0
% see also JACOBIANQUATERNIONEULER

function Je = JacobianEulerQuaternion(Q0)

q1=Q0(1); q2=Q0(2); q3=Q0(3); q4=Q0(4);

n1 = q2+q4; n2 = q2-q4; n3 = q1-q3; n4 = q1+q3;
d1 = (q1-q3)^2+(q2+q4)^2; d2 = (q1+q3)^2+(q2-q4)^2; 
d3 = sqrt(1-4*(q4*q2-q1*q3)^2);

dx1dq1 = -n1/d1-n2/d2;
dx1dq2 = n3/d1+n4/d2;
dx1dq3 = n1/d1-n2/d2;
dx1dq4 = n3/d1-n4/d2;

dx2dq1 = -2*q3/d3;
dx2dq2 = 2*q4/d3;
dx2dq3 = -2*q1/d3;
dx2dq4 = 2*q2/d3;

dx3dq1 = n2/d2-n1/d1;
dx3dq2 = n3/d1-n4/d2;
dx3dq3 = n1/d1+n2/d2;
dx3dq4 = n3/d1+n4/d2;

Je = [dx1dq1, dx1dq2, dx1dq3, dx1dq4;...
      dx2dq1, dx2dq2, dx2dq3, dx2dq4;...
      dx3dq1, dx3dq2, dx3dq3, dx3dq4];

end
%Euler2RotMat Euler angles to rotation matrix conversion
%
%   R = Euler2RotMat(R1, R2, R3, SEQ) computes the rotation matrix
%   corresponding to the Euler angles PHI, THETA and PSI
%
%       R1:   the roll angle in radians
%       R2: the pitch angle in radians
%       R3:   the yaw angle in radians
%       SEQ:   'ZYX', 'ZYZ', 'ZXY', 'ZXZ', 'YXZ', 'YXY', 'YZX', 'YZY', 'XYZ', 'XYX', 'XZY', or 'XZX'.
%
%   For example the Sequence SEQ 'XYZ' corresponds to the intrinsec rotation order 
%   R1 = Z Axis Rotation, R2 = Y Axis Rotation, and R3 = X Axis Rotation. 
%   The convention used here is:
%       - X axis forward  (roll rotation's axis)
%       - Y axis leftward (pitch rotation's axis)
%       - Z axis Upward   (yaw rotation's axis)
%
%   Return the rotation matrix corresponding to the Euler Angles R1,
%   R2, R3 for the corresponding rotation sequence SEQ.
%
%   See also: Euler2RotMat, Xrotation, Yrotation, Zrotation, Euler2Quaternion, 
%   Quaternion2AngleAxis, Quaternion2RotMat, RotMat2Quaternion

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Augustin Manecy
%
% Copyright (C) 2011-2014 Augustin Manecy
%
% augustin.manecy@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This file is part of RT-MaG Toolbox.
%
%   RT-MaG Toolbox is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   RT-MaG Toolbox is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with RT-MaG Toolbox.  If not, see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This version of GPL is at https://www.gnu.org/licenses/gpl-3.0.txt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function R = Euler2RotMat(R1, R2, R3, varargin)

    %#codegen

    if (nargin == 4)
        SEQ = varargin{1};
    else
        SEQ = 'XYZ';
    end
    
    switch SEQ
        case 'ZYX'
            Rm1 = Zrotation(R1);
            Rm2 = Yrotation(R2);
            Rm3 = Xrotation(R3);
        case 'ZYZ'
            Rm1 = Zrotation(R1);
            Rm2 = Yrotation(R2);
            Rm3 = Zrotation(R3);
        case 'ZXY'
            Rm1 = Zrotation(R1);
            Rm2 = Xrotation(R2);
            Rm3 = Yrotation(R3);
        case 'ZXZ'
            Rm1 = Zrotation(R1);
            Rm2 = Xrotation(R2);
            Rm3 = Zrotation(R3);
        case 'YXZ'
            Rm1 = Yrotation(R1);
            Rm2 = Xrotation(R2);
            Rm3 = Zrotation(R3);
        case 'YXY'
            Rm1 = Yrotation(R1);
            Rm2 = Xrotation(R2);
            Rm3 = Yrotation(R3);
        case 'YZX'
            Rm1 = Yrotation(R1);
            Rm2 = Zrotation(R2);
            Rm3 = Xrotation(R3);
        case 'YZY'
            Rm1 = Yrotation(R1);
            Rm2 = Zrotation(R2);
            Rm3 = Yrotation(R3);
        case 'XYZ'
            Rm1 = Xrotation(R1);
            Rm2 = Yrotation(R2);
            Rm3 = Zrotation(R3);
        case 'XYX'
            Rm1 = Xrotation(R1);
            Rm2 = Yrotation(R2);
            Rm3 = Xrotation(R3);
        case 'XZY'
            Rm1 = Xrotation(R1);
            Rm2 = Zrotation(R2);
            Rm3 = Yrotation(R3);
        case 'XZX'
            Rm1 = Xrotation(R1);
            Rm2 = Zrotation(R2);
            Rm3 = Xrotation(R3);
        otherwise
            warning('ROTATION:invalidSequence', 'Invalid sequence "%s", "XYZ" taken by default!', SEQ);
            Rm1 = Xrotation(R1);
            Rm2 = Yrotation(R2);
            Rm3 = Zrotation(R3);
    end
    R = Rm3*Rm2*Rm1;
end
%Euler2Quaternion Euler angles to quaternion conversion
%
%   Q = Euler2Quaternion(PHI, THETA, PSI) computes the unit-quaternion
%   corresponding to the Euler angles PHI, THETA and PSI.
%
%   PHI:   the roll angle in radians
%   THETA: the pitch angle in radians
%   PSI:   the yaw angle in radians
%
%   The convention used here is:
%       - X axis forward  (roll rotation's axis)
%       - Y axis leftward (pitch rotation's axis)
%       - Z axis Upwrad   (yaw rotation's axis)
%   The Combination of intrinsec rotations is given by the yaw rotation first, the pitch 
%   and to finish the roll rotation: Q = Qz.Qy.Qx
%
%   Return the unit-quaternion Q corresponding to the Euler angles PHI,
%   THETA and PSI.
%
%   See also: Quaternion2Euler, Quaternion2AngleAxis, Quaternion2RotMat,
%   RotMat2Quaternion, Euler2RotMat, RotMat2Euler

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: Augustin Manecy
%
% Copyright (C) 2011-2014 Augustin Manecy
%
% augustin.manecy@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This file is part of RT-MaG Toolbox.
%
%   RT-MaG Toolbox is free software: you can redistribute it and/or modify
%   it under the terms of the GNU General Public License as published by
%   the Free Software Foundation, either version 3 of the License, or
%   (at your option) any later version.
%
%   RT-MaG Toolbox is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%
%   You should have received a copy of the GNU General Public License
%   along with RT-MaG Toolbox.  If not, see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This version of GPL is at https://www.gnu.org/licenses/gpl-3.0.txt
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Q = Euler2Quaternion(PHI, THETA, PSI)

    %#codegen

    ex = [1;0;0];
    ey = [0;1;0];
    ez = [0;0;1];
    
    qx = Quaternion(PHI, ex);
    qy = Quaternion(THETA, ey);
    qz = Quaternion(PSI, ez);
    
    Q = QuaternionProduct(qz, QuaternionProduct(qy, qx));
    
%     sphi = sin(PHI/2); cphi = cos(PHI/2);
%     sthe = sin(THETA/2); cthe = cos(THETA/2);
%     spsi = sin(PSI/2); cpsi = cos(PSI/2);
%     q = [ sphi*sthe*spsi + cphi*cthe*cpsi;...
%           sphi*cpsi*cthe - cphi*sthe*spsi;...
%           cphi*sthe*cpsi + sphi*cthe*spsi;...
%           cphi*cthe*spsi - sphi*sthe*cpsi]

end


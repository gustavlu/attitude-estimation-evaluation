function [q] = chr2qua(M)

% [q] = chr2qua(M);
%--------------------------------------------------------------------------
% Passage de la matrice de changement de repere (rotation)
% au quaternion
%--------------------------------------------------------------------------
% M     : matrice de rotation Rm/Ro = Matrice de passage de Ro � Rm
% q     : quaternion unitaire 
%--------------------------------------------------------------------------
% ATTENTION :  M doit etre une matrice de rotation
%--------------------------------------------------------------------------

% References:
%   [1] http://fr.wikipedia.org/wiki/Quaternions_et_rotation_dans_l'espace
% 
%   [2] Robust Sensor Fusion for Robot Attitude Estimation
%       P. Allgeuer, S. Behnke
%       Proc. 14th IEEE-RAS Int Conf on Humanoid Robots
%       Madrid, Spain, Niv. 2014
%       Appendix


tr = M(1,1) + M(2,2) + M(3,3) ;
if tr>=0
    s    = 0.5 * sqrt(1+tr) ;
    q(1) = s ;
    q(2) = 0.25 * (M(3,2)-M(2,3)) / s ;
    q(3) = 0.25 * (M(1,3)-M(3,1)) / s ;
    q(4) = 0.25 * (M(2,1)-M(1,2)) / s ;
else
    tr = M(1,1) - M(2,2) - M(3,3) ;
    if tr>=0
        x    = 0.5 * sqrt(1+tr) ;
        q(1) = 0.25 * (M(3,2)-M(2,3)) / x ;
        q(2) = x ;
        q(3) = 0.25 * (M(1,2)+M(2,1)) / x ;
        q(4) = 0.25 * (M(3,1)+M(1,3)) / x ;
    else
        tr = -M(1,1) + M(2,2) - M(3,3) ;
        if tr>=0
            y    = 0.5 * sqrt(1+tr) ;
            q(1) = 0.25 * (M(1,3)-M(3,1)) / y ;
            q(2) = 0.25 * (M(1,2)+M(2,1)) / y ;
            q(3) = y ;
            q(4) = 0.25 * (M(2,3)+M(3,2)) / y ;
        else
            tr = -M(1,1) - M(2,2) + M(3,3) ;
            if tr>=0
                z    = 0.5 * sqrt(1+tr) ;
                q(1) = 0.25 * (M(2,1)-M(1,2)) / z ;
                q(2) = 0.25 * (M(3,1)+M(1,3)) / z ;
                q(3) = 0.25 * (M(2,3)+M(3,2)) / z ;
                q(4) = z ;
            else
            error('R n''est pas une matrice de rotation');
            end;
        end;
    end;
end;

% %--------------------------------------------------------------------------
% % Un code qui fonctionne, mais dont je n'ai plus la r�f�rence
% %--------------------------------------------------------------------------
% 
% % Permutation paire amenant le plus grand terme (en valeur absolue) de la 
% % diagonale de M en premier element
% [~,imax] = max(abs(diag(M))) ;
% shift = 1 + mod([-1 0 1] + imax,3) ;
% 
% % Quaternion
% N = M(shift,shift);
% r = sqrt( 1+N(1,1)-N(2,2)-N(3,3) ); 
% q(1) = ( N(3,2)-N(2,3) ) /(2*r);
% q(2) = r/2;
% q(3) = ( N(1,2)+N(2,1) ) /(2*r);
% q(4) = ( N(3,1)+N(1,3) ) /(2*r);
% 
% % Permutation inverse
% q(1+shift) = q(2:4);



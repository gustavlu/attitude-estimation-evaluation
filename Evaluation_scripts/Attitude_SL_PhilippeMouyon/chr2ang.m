function [alpha,u]=chr2ang(M)

%  [alpha,u] = chr2ang(M);
%--------------------------------------------------------------------------
% Calcul l'angle et la direction d'une rotation
% a partir de la matrice de passage entre deux reperes
%--------------------------------------------------------------------------
% M     :  matrice de passage
% alpha :  angle de la rotation
% u     :  vecteur unitaire de l'axe de rotation
%--------------------------------------------------------------------------
% ATTENTION :  M doit �tre une matrice de rotation
%--------------------------------------------------------------------------

% Valeur a priori de la uection
persistent u0
if isempty(u0)
    u0 = [1;0;0];
end;

% Angle
cosa	= (trace(M)-1)/2;
alpha	= acos( cosa );     % sur [0,pi]
sina	= sin( alpha );     % >0, donc au signe pr�s...

% uection
if abs(alpha)<7e-04
    %----------------------------------------------------------------------
    % Angle tr�s petit: la direction est ind�termin�e. 
    % On utilise la direction pr�c�dente comme r�f�rence.
    %----------------------------------------------------------------------
    M = M * manti(u0);
    u = [ M(3,2)-M(2,3) ; M(1,3)-M(3,1) ; M(2,1)-M(1,2) ] / (2*cosa);
    
elseif abs(alpha-pi)<7e-04 
    %----------------------------------------------------------------------
    % Angle voisin de pi: le sens de la direction est ind�termin�.
    % On utilise celui de la direction pr�c�dente comme r�f�rence.
    %----------------------------------------------------------------------
    
    % direction sans choisir le sens
    u = (diag(M)-cosa)/(1-cosa);	% carr�s des composantes
    [u,iu] = max(u); di = sqrt(u);
    for ku = 1:3
        if ku==iu 
            u(ku) = di;
        else
            u(ku) = (M(ku,iu)+M(iu,ku)) / (2*di*(1-cosa));
        end;
    end;
    
    % Recherche du sens
%     M = M * manti(u0);
%     eu = [ M(3,2)-M(2,3) ; M(1,3)-M(3,1) ; M(2,1)-M(1,2) ] / (2*cosa);
%     sens = sign(eu'*u); 
%     if sens==0, sens=sign(u0'*u); end;
%     if sens==0, sens=1; end;
    sens = sign(u0'*u);
    if sens==0, sens=1; end;
    
    % Correction du sens de la direction
    u = sens * u;
    
else
    %----------------------------------------------------------------------
    % Cas nominal
    %----------------------------------------------------------------------
    u = [ M(3,2)-M(2,3) ; M(1,3)-M(3,1) ; M(2,1)-M(1,2) ] / (2*sina);
    
end;

% Memorisation
u0 = u;

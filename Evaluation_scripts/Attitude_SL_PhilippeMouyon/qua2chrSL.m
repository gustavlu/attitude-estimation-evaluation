function [M] = qua2chrSL(q)

% [M] = qua2chr(q);
%--------------------------------------------------------------------------
% Calcul la matrice de passage entre deux reperes a partir du quaternion 
%--------------------------------------------------------------------------
% q :  quaternion unitaire 
% M :  matrice de passage (rotation)
%--------------------------------------------------------------------------
% ATTENTION :  q doit etre unitaire
%--------------------------------------------------------------------------
M = zeros(3);

s = q(1);
x = q(2);
y = q(3);
z = q(4);

M(1,1) = 1 - 2 * ( y*y + z*z ) ;
M(2,2) = 1 - 2 * ( x*x + z*z ) ;
M(3,3) = 1 - 2 * ( x*x + y*y ) ;

M(1,2) = 2 * ( x*y - s*z ) ;
M(1,3) = 2 * ( x*z + s*y )  ;

M(2,1) = 2 * ( y*x + s*z )  ;
M(2,3) = 2 * ( y*z - s*x )  ;

M(3,1) = 2 * ( z*x - s*y )  ;
M(3,2) = 2 * ( z*y + s*x )  ;


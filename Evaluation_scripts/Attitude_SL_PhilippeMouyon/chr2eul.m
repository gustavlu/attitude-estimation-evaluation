function [eul]=chr2eul(M)

% [eul] = chr2eul(M);
%--------------------------------------------------------------------------
% Passage de la matrice de changement de repere (rotation)
% aux 3 angles d'Euler (convention aeronautique) 
%--------------------------------------------------------------------------
% M     : matrice de rotation Rm/Ro = Matrice de passage de Ro � Rm
% eul	: vecteur des angles d'Euler [psi;theta;phi]
%--------------------------------------------------------------------------
% ATTENTION:
%   Si l'angle de tangage (theta) est proche de +-pi/2 on utilise la 
%   continuit� par rapport � la valeur pr�c�dente.
%--------------------------------------------------------------------------

% References:
% [1] Repr�sentation des attitudes
% Michel Llibre
% version 1.1 - f�vrier 2009
% Note DCSD-2009_008-NOT-003-1.1.pdf
% michel.llibre.pagesperso-orange.fr/docs/DCSD-2009_008-NOT-003-1.1.pdf
% page 40, 44 et 45

%--------------------------------------------------------------------------
% Valeurs a priori de S = psi + sin(theta) * phi
%--------------------------------------------------------------------------
persistent S 
if isempty(S)
    S = 0;  
end;

%--------------------------------------------------------------------------
% Assiette = theta
%--------------------------------------------------------------------------
% M(3,1) = -sin(theta);
theta   = -asin(min([1,max([-1,M(3,1)])]));
pm      = sign(theta); 

%--------------------------------------------------------------------------
% Roulis (phi) et Cap (psi)
%--------------------------------------------------------------------------
if abs(abs(theta)-pi/2)<7e-04 
    % Cas d'un angle de tangage proche de pm*pi/2
    % -M(1,2) + pm * M(2,3) = 2 * sin(psi-pm*phi) ;
    %  M(2,2) + pm * M(1,3) = 2 * cos(psi-pm*phi) ;
    D = atan2(-M(1,2)+pm*M(2,3),M(2,2)+pm*M(1,3));
    psi = (S+D)/2;
    phi = pm*(S-D)/2;
else 
    % Cas d'un angle de tangage tel que |cos(theta)|>>0
    % M(3,2) = cos(theta)*sin(phi);
    % M(3,3) = cos(theta)*cos(phi);
    phi = atan2(M(3,2),M(3,3));
    % M(1,1) = cos(theta)*cos(psi);
    % M(2,1) = cos(theta)*sin(psi);
    psi = atan2(M(2,1),M(1,1));
    D = psi - pm * phi;
end;

%--------------------------------------------------------------------------
% Preparation des arguments de sortie
%--------------------------------------------------------------------------

% Mise a jour de la valeur a priori 
S   = psi + pm * phi;  


% vecteur des angles
eul(1,1) = psi;
eul(2,1) = theta;
eul(3,1) = phi;

end

function [M] = ang2chr(alpha,u)

% [M] = ang2chr(alpha,u);
%--------------------------------------------------------------------------
% Calcul la matrice de passage entre deux reperes
% a partir de l'angle et de la uection 
%--------------------------------------------------------------------------
% alpha :  angle de la rotation
% u     :  vecteur unitaire de l'axe de rotation 
% M     :  matrice de passage
%--------------------------------------------------------------------------
% ATTENTION :  u doit �tre unitaire
%--------------------------------------------------------------------------

% % http://fr.wikipedia.org/wiki/Matrice_de_rotation
% c = cos(alpha); s = sin(alpha); C = 1-c
% xs = u(1,1)*s;   ys = u(2,1)*s;   zs = alpha*u(3,1)*s
% xC = u(1,1)*C;   yC = u(2,1)*C;   zC = alpha*u(3,1)*C
% xyC = x*yC; yzC = y*zC; zxC = z*xC
% [ x*xC+c   xyC-zs   zxC+ys ]
% [ xyC+zs   y*yC+c   yzC-xs ]
% [ zxC-ys   yzC+xs   z*zC+c ]
% %

thx = alpha * u(1,1);
thy = alpha * u(2,1);
thz = alpha * u(3,1);

matia = [ 0   -thz  thy ;
          thz  0   -thx ;
         -thy  thx  0   ];

% cas des petits angles
%-----------------------
if abs(alpha)<1.0e-05
  alpha2 = alpha*alpha;
  Sinf = 1-alpha2/6;    % Approximation de sin(x)/x
  Cinf = 0.5-alpha2/24; % Approximation de (1-cos(x))/x^2

% autres cas
%------------
else
  Sinf = sin(alpha)/alpha;
  Cinf = (1-cos(alpha))/(alpha*alpha);
end;

M = eye(3,3) + ( Sinf * eye(3,3) + Cinf * matia ) * matia ;


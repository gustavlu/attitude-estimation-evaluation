function [M]=eul2chr(eul)

% [M] = eul2chr(eul)
%--------------------------------------------------------------------------
% Passage des 3 angles d'Euler (convention aeronautique) 
% � la matrice de changement de repere (rotation)
%--------------------------------------------------------------------------
% eul	: vecteur des angles d'Euler [psi;theta;phi]
% M     : matrice de rotation Rm/Ro = Matrice de passage de Ro � Rm
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% M est obtenue par composition de 3 rotations successives.
%
% Rotation d'angle psi autour de l'axe Z: R_Z(psi)
% M = ...
%     [ cpsi  -spsi   0 ;
%       spsi   cpsi   0 ;
%       0      0      1 ];
% Rotation d'angle theta autour de l'axe Y: R_Y(theta)
% M = M * ...
%     [  cthe   0  sthe ;
%        0      1  0    ;
%       -sthe   0  cthe ];
% Rotation d'angle phi autour de l'axe X: R_X(phi)
% M = M * ...
%     [ 1   0      0    ;
%       0   cphi  -sphi ;
%       0   sphi   cphi ];
%--------------------------------------------------------------------------

% References:
% [1] Repr�sentation des attitudes
% Michel Llibre
% version 1.1 - f�vrier 2009
% Note DCSD-2009_008-NOT-003-1.1.pdf
% michel.llibre.pagesperso-orange.fr/docs/DCSD-2009_008-NOT-003-1.1.pdf
% page 40

psi = eul(1); cpsi = cos(psi); spsi = sin(psi); 
the = eul(2); cthe = cos(the); sthe = sin(the);
phi = eul(3); cphi = cos(phi); sphi = sin(phi);

M = [ ...
    cpsi*cthe , cpsi*sthe*sphi-spsi*cphi , cpsi*sthe*cphi+spsi*sphi ;
    spsi*cthe , spsi*sthe*sphi+cpsi*cphi , spsi*sthe*cphi-cpsi*sphi ;
    -sthe     , cthe*sphi                , cthe*cphi                ];

end
    
function [eul] = qua2eul(q)

% [eul] = qua2eul(q);
%--------------------------------------------------------------------------
% Passage du quaternion 
% aux 3 angles d'Euler (convention aeronautique) 
%--------------------------------------------------------------------------
% q     : quaternion unitaire
% eul	: vecteur des angles d'Euler [psi;theta;phi]
%--------------------------------------------------------------------------
% ATTENTION:
%   Si l'angle de tangage (theta) est proche de +-pi/2 on utilise la 
%   continuit� par rapport � la valeur pr�c�dente.
%--------------------------------------------------------------------------

% R�f�rences:
% [1] Repr�sentation des attitudes
% Michel Llibre
% version 1.1 - f�vrier 2009
% Note DCSD-2009_008-NOT-003-1.1.pdf
% michel.llibre.pagesperso-orange.fr/docs/DCSD-2009_008-NOT-003-1.1.pdf
% page ???

eul = zeros(3,1);

%--------------------------------------------------------------------------
% Valeurs a priori de S = psi + sin(theta) * phi
%--------------------------------------------------------------------------
persistent S 
if isempty(S)
    S = 0;  
end;

s = q(1);
x = q(2);
y = q(3);
z = q(4);

%--------------------------------------------------------------------------
% Assiette = theta
%--------------------------------------------------------------------------
theta   = asin( 2*(s*y-x*z) );
pm      = sign(theta); 

%--------------------------------------------------------------------------
% Roulis (phi) et Cap (psi)
%--------------------------------------------------------------------------
if abs(abs(theta)-pi/2)<7e-04 
    % Cas d'un angle de tangage proche de pm*pi/2
    D = 2 * atan2(z-pm*x , s+pm*y);
    psi = (S+D)/2;
    phi = pm*(S-D)/2;    
else 
    % Cas d'un angle de tangage tel que |cos(theta)|>>0
    psi = atan2( 2*(s*z+x*y) , s*s+x*x-y*y-z*z );
    phi = atan2( 2*(s*x+y*z) , s*s+z*z-x*x-y*y );
    D = psi - pm * phi;
end;

%--------------------------------------------------------------------------
% Preparation des arguments de sortie
%--------------------------------------------------------------------------

% Mise a jour de la valeur a priori 
S   = psi + pm * phi;  

% vecteur des angles
eul(1,1) = phi;
eul(2,1) = theta;
eul(3,1) = psi;

end

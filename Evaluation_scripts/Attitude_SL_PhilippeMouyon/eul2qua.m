function [q] = eul2qua(eul)

% [q] = eul2qua(eul);
%--------------------------------------------------------------------------
% Passage des 3 angles d'Euler (convention aeronautique) 
% au quaternion
%--------------------------------------------------------------------------
% eul	: vecteur des angles d'Euler [psi;theta;phi]
% q     : quaternion unitaire
%--------------------------------------------------------------------------

% References:
% [1] Représentation des attitudes
% Michel Llibre
% version 1.1 - février 2009
% Note DCSD-2009_008-NOT-003-1.1.pdf
% michel.llibre.pagesperso-orange.fr/docs/DCSD-2009_008-NOT-003-1.1.pdf
% page 79

cpsi   = cos(eul(1)/2);
spsi   = sin(eul(1)/2);
cthe   = cos(eul(2)/2);
sthe   = sin(eul(2)/2);
cphi   = cos(eul(3)/2);
sphi   = sin(eul(3)/2);

q0 =  cpsi*cthe*cphi + spsi*sthe*sphi;
q1 = -spsi*sthe*cphi + cpsi*cthe*sphi;
q2 =  cpsi*sthe*cphi + spsi*cthe*sphi;
q3 =  spsi*cthe*cphi - cpsi*sthe*sphi;

q = [q0 ; q1 ; q2 ; q3];

end


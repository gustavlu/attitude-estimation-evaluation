clear
warning('off','all')

% FT_EKF_IO_Main_Mk7
InitUavParameters_mod_X16_LinAcc_Sim

resdir =  pwd; % Results directory

% FORMAT: [1]: filter, [2-4]: b_glr, b_lpgyro, b_att_reset, [5]: Use FT arch.
% sim_mat = [8 0 0 0 0;  % IEKF w. norm rejection
%            4 0 0 0 1;  % FT-KF
%            8 0 1 0 1;  % FT-IEKF-X2
%            4 1 1 0 1;  % FT-KF-GLR
%            8 1 1 0 1]; % FT-IEKF-GLR
sim_mat = [8 0 0 0 0;  % IEKF w. norm rejection
           8 0 0 0 1;  % FT-KF
           8 0 1 0 1;  % FT-IEKF-X2
           8 1 1 0 1]; % FT-IEKF-GLR
% sim_mat = [8 0 0 1 0;  % IEKF
%            8 0 0 0 0;  % IEKF w. norm rejection
%            8 0 0 0 1;  % FT-IEKF-X2 no LP-gyro
%            8 0 1 0 1;  % FT-IEKF-X2
%            8 1 1 0 1]; % FT-IEKF-GLR
% sim_mat = [7 0 1 0 1; % FT-MEKF-X2
%            7 1 1 0 1]; % FT-MEKF-GLR
% sim_mat = [8 0 1 0 1; % FT-IEKF-X2
%            8 1 1 0 1]; % FT-IEKF-GLR
% sim_mat = [8 1 1 0 1]; % FT-IEKF-GLR
sim_mat = [8 0 0 0 1]; % FT-IEKF
sim_mat = [4 1 1 0 1]; % FT-NLKF-GLR
% sim_mat = [4 0 0 0 0    % NL-KF
%            4 1 1 0 1];  % FT-KF-GLR
% sim_mat = [8 0 1 0 1]; % FT-IEKF-X2
     
testcase_vec = [20]; % See INSTRUCTIONS for Thesis <> testcase equivalence


% ============= FILTER PARAMETERS ===============
% GLR Parameters
b_useglr_vec = [1, 0, 0, 1, 1, 0]; % [1]: Norm, [2]: Outlier, [3]: PM
L = 25;
% PFA_GLR = 1e-5;
PFA_GLR = 1-0.9967;   % 3 sigma
% PFA_GLR = 1-0.99;
PFA_chi2 = PFA_GLR;
% PFA_chi2 = 1e-5;
ndt_glr = 1; % sample divisor for GLR test
% ==============================================

numsims = numel(testcase_vec)*size(sim_mat,1);

time_sim = zeros(numsims,1);

cnt_multi = 1;

%% Mkdir etc.
clk = fix(clock);
time_str_short = ['-', num2str(clk(1)), '-' ,num2str(clk(2)), '-', ...
    num2str(clk(3)), '-', num2str(clk(4)), '-', num2str(clk(5))];

saver = lower(input([num2str(numsims),' sims. demanded. Save results (y/N)? '],'s'));
flag_save = strcmp(saver,'y');
if flag_save
    savename = input('Save name: ','s');
    savedir = uigetdir(resdir);
    if isempty(savedir)
        savedir = fullfile(resdir,'Sim');
    end
    if isempty(savename)
        savename = 'Sim';
    end
    savedir = fullfile(savedir,[savename, time_str_short]);
    mkdir(savedir)
end

hmulti = waitbar(0);
for testcase = testcase_vec
    FT_EKF_IO_Main_Mk7
    for klm = 1:size(sim_mat,1)
        ke_sim = sim_mat(klm,1);
        b_glr = sim_mat(klm,2);
        b_lpgyro = sim_mat(klm,3);
%         b_glr_attreset = sim_mat(klm,4);
        b_glr_attreset = 0;
        b_force_ok = sim_mat(klm,4);
        b_useftarch = sim_mat(klm,5);

        savename = ['Sim_',num2str(testcase),...
                    '_',num2str(ke_sim),'_',num2str(b_glr),'_',num2str(b_force_ok),...
                    '_',num2str(b_lpgyro),'_',num2str(b_useftarch)];
        waitbar(cnt_multi/numsims, hmulti, ['Multi Simulation: ', num2str(cnt_multi),'/',num2str(numsims)]);
        
        close all
        
        % Covariance bloating 
        if ~b_useftarch % && ~b_glr
            EKF.kcov = 10;
            EKF.kcov_mag = 5;
        else
            EKF.kcov = 10;
            EKF.kcov_mag = 5;
%             EKF.kcov = 1;
%             EKF.kcov_mag = 1;
        end
        kcov = EKF.kcov;
        kcov_mag = EKF.kcov_mag;
        
        if ~(b_glr_attreset && ~b_glr)
            t1 = tic;
            Simloop_ft_arch_GLR_FD_Mk7_MultiSim
            texec = toc(t1);
            time_sim(cnt_multi) = texec;
            save(fullfile(savedir,savename),'t','px4_rotated','eulerout',...
                'eulerhatout','sigma_euler_out','norm_error_euler_out',...
                'bias_out', 'sigma_bias_out','savename','rpy_in','testcase',...
                'ke_sim','tstop', 'accm_out','acc_out','magm_out','mag_out',...
                'norm_acc_out','norm_mag_out','corr_status_out','texec','L','EKF',...
                'acc_ok','mag_ok','sim_mat')
            %save(fullfile(savedir,savename))
        end
        
        cnt_multi = cnt_multi+1;
        
    end
end
close(hmulti);


    %% Plotty
    indplot = (1:numel(t));
    tplot = t(indplot);
    ndown = 1;
    % Euler plot
    figure,
    subplot(411),title(filt_title)
    for k = 1:3
        subplot(4,1,k),hold on, axis([0 t(end) -180 180])
        plot(tplot, px4_rotated(indplot,k),'b','linewidth',1)
        plot(tplot,eulerout(k,indplot)*r2d,'r',tplot,eulerhatout(k,indplot)*r2d,'k--','linewidth',1)
        plotShaded(downsample(tplot,ndown),...
            [downsample(eulerout(k,indplot)+3*sigma_euler_out(k,indplot),ndown);...
            downsample(eulerout(k,indplot)-3*sigma_euler_out(k,indplot),ndown)]*r2d,'r',0.5)
        xlim([0 tplot(end)])
    end
    subplot(4,1,4),hold on, axis([0 t(end) 0 max(norm_error_euler_out(indplot)*r2d*1.1)])
    plot(tplot, norm_error_euler_out(indplot)*r2d)
    
    %%
        figure,
        subplot(411),title(filt_title)
        for k = 1:3
            subplot(4,1,k),hold on, axis([0 tplot(end) -EKF.bsat.bmax*2*r2d EKF.bsat.bmax*2*r2d])
            plot(tplot,xhat_out(k+4,indplot)*r2d,tplot,bias_out(k,indplot)*r2d)
            plotShaded(downsample(tplot,ndown),...
                [downsample(bias_out(k,indplot)+3*sigma_bias_out(k,indplot),ndown);...
                downsample(bias_out(k,indplot)-3*sigma_bias_out(k,indplot),ndown)]*r2d,'r',0.5)
        end
        subplot(4,1,4), hold on, axis([0 tplot(end) 0 EKF.bsat.bmax*2*r2d])
        plot(tplot, norm_error_bhat_out(indplot)*r2d)
    
    % subplot(3,1,1),hold on, axis tight
    % plot(tplot,eulerout(2,indplot)*r2d,tplot,eulerhatout(2,indplot)*r2d)
    % plotShaded(tplot,[eulerhatout(2,indplot)+3*sigma_euler(2,indplot); eulerhatout(2,indplot)-3*sigma_euler(2,indplot)]*r2d,'r',0.5)
    % subplot(3,1,1),hold on, axis tight
    % plot(tplot,eulerout(1,indplot)*r2d,tplot,eulerhatout(1,indplot)*r2d)
    % plotShaded(tplot,[eulerhatout(1,indplot)+3*sigma_euler(1,indplot); eulerhatout(3,indplot)-3*sigma_euler(1,indplot)]*r2d,'r',0.5)
    
    %% Mag measurements
    figure,
    subplot(311), plot( t, magm_out(1,:), t, mag_out(1,:), t, mhat_out(1,:), t, mc_out(1,:));
    subplot(312), plot( t, magm_out(2,:), t, mag_out(2,:), t, mhat_out(2,:), t, mc_out(2,:));
    subplot(313), plot( t, magm_out(3,:), t, mag_out(3,:), t, mhat_out(3,:), t, mc_out(3,:));
    legend('mag_m', 'mag_r_e_a_l', 'mag_e_s_t', 'mag_c')
    
    %% Acc measurements
%     figure
%     subplot(311), plot( t, accm_out(1,:), t, acc_out(1,:), t, ahat_out(1,:), t, ac_out(1,:));
%     subplot(312), plot( t, accm_out(2,:), t, acc_out(2,:), t, ahat_out(2,:), t, ac_out(2,:));
%     subplot(313), plot( t, accm_out(3,:), t, acc_out(3,:), t, ahat_out(3,:), t, ac_out(3,:))
%     
    %% Calculate norm difference
    
    %% Mag and Acc estimates for Martin/Sarras
    if ke == 6
        figure,
        subplot(311),
        plot(t,xhat_ms_out(1,:), t, accm_out(1,:)),
        subplot(312),
        plot(t,xhat_ms_out(2,:), t, accm_out(2,:)),
        subplot(313),
        plot(t,xhat_ms_out(3,:), t, accm_out(3,:))
        
        figure,
        subplot(311),
        plot(t,xhat_ms_out(4,:), t, magm_out(1,:)),
        subplot(312),
        plot(t,xhat_ms_out(5,:), t, magm_out(2,:)),
        subplot(313),
        plot(t,xhat_ms_out(6,:), t, magm_out(3,:))
    end
   
 %% Plotty
% GLR statistic
% figure, 
% subplot(211)
% plot(t, GLRvec)
% title('GLR statistic')
% hold on
% plot(t, chi2vec)
% plot(t, ones(size(t))*GLRparams.GLRthr)
% legend({'GLR','\chi^2'},'Location', 'Best'),
% subplot(212)
% plot(t, qvec)
% title('Change detection date')

%%
% Compare GLR and X2 statistics
figstat = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
title('GLR statistic')
cnt = 1;
for kl = [1 3 5]    
    subplot(3,2,kl), hold on
    plot(t, GLRvec(cnt,:))
    plot(t, chi2vec(cnt,:))
    legend('l_{GLR}','l_{\chi^2}')
    title(titlevec_acc(cnt))
    subplot(3,2,kl+1), hold on
    plot(t, GLRvec(cnt+3,:))
    plot(t, chi2vec(cnt+3,:))
    legend('l_{GLR}','l_{\chi^2}')
    title(titlevec_mag(cnt))
    cnt = cnt+1;
end
    %%
%        figstat = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
%     title('GLR Fault date')
%     cnt = 1;
%     for k = [1 3 5]
%         subplot(3,2,k), hold on
%         plot(t, qdetvec(cnt,:))
%         legend('q_{GLR}','l_{\chi^2}')
%         title(titlevec_acc(cnt))
%         subplot(3,2,k+1), hold on
%         plot(t, qdetvec(cnt+3,:))
%         legend('q_{GLR}','l_{\chi^2}')
%         title(titlevec_mag(cnt))
%         cnt = cnt+1;
%     end
%% Fault detection booleans
figbool = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
cnt = 1;
for kl=[1 3 5]
    subplot(3,2,kl), 
    plot(t, acc_status_out(cnt,:),'*')
    title(titlevec_acc(cnt))
    subplot(3,2,kl+1), 
    plot(t, mag_status_out(cnt,:),'*')
    title(titlevec_mag(cnt))
    cnt = cnt+1;
end

%% Bias estimation plot
figure,
for klm = 1:3
subplot(3,1,klm), hold on,
plot(t, bhat_out(klm,:)*r2d)
plot(t, bias_lp_out(klm,:)*r2d)
plot(t, ones(size(t))*EKF.bsat.bmax*r2d,'k--')
plot(t, -ones(size(t))*EKF.bsat.bmax*r2d,'k--')
legend({'bhat_{KF}','bhat_{gyro}'})
end
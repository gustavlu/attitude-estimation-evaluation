% clear
warning('off','all')

if exist('testcase','var')
    tc_tmp = testcase;
else
    tc_tmp = 0;
end

testcase = 20; % Test scenario
% if testcase ~= tc_tmp % Load trajectory only if not already loaded
FT_EKF_IO_Main_Mk7
% end
% InitUavParameters_mod_X16_LinAcc_Sim

%%
nx = 7; % state dimension
r2d = 180/pi;
d2r = pi/180;

dt = mean(diff(tvec)); % time step
dt_acc = dt;
dt_mag = dt;
dt_vec = [dt; diff(tvec)];
tend = tvec_imu(end);
t = tvec_imu'; % time vector
if tstop == 0; tstop = tend; end

% Simulation flags
ke_vec = [8]; % Filters to be simulated
b_glr = 0;  % Enable fault detection by GLR
b_glr_attreset = 0; % Enable attitude reset on fault detection
b_lpgyro = 1; % Enable low pass filter of gyro for bias estimation
b_useftarch = 1;

if ~b_useftarch && ~b_glr
    EKF.kcov = 10;
    EKF.kcov_mag = 10;
else
    EKF.kcov = 10;
    EKF.kcov_mag = 10;
end
% EKF.kcov = 1;
% EKF.kcov_mag = 1;
    
% GLR Parameters
b_useglr_vec = [1, 0, 0, 1, 1, 0]; % [1/4]: Norm, [2/5]: Outlier, [3/6]: PM
L = 25;
% PFA_GLR = 1e-5;
PFA_GLR = 1-0.9967;   % 3 sigma
% PFA_GLR = 1-0.99;
% PFA_chi2 = PFA_GLR;
% PFA_chi2 = 1e-5;
PFA_chi2 = 1-0.9967;   % 3 sigma
ndt_glr = 1;
dt_glr = dt*ndt_glr;

% Measurement availability
acc_ok = [1;abs(diff(acc_in(:,2)))>0];
mag_ok = [1;abs(diff(mag_in(:,2)))>0];

e1 = [1;0;0];
e3 = [0;0;1];

% References
gvec = e3*-9.81;
mag_ref = mean(mag_in(1:100,2:4))';

EKF.mag_ref = mag_ref;
EKF.gvec = gvec;

cov_mag = diag([0.0067, 0.0509, 0.0441]);
% cov_mag = eye(3)*(norm(EKF.mag_ref)/3)^2;
cov_acc = eye(3)*(norm(gvec)/3)^2;%diag((std(acc_in(1:100,2:4))).^2);
cov_gyro = ([25; 12; 4]/3*d2r).^2;

% First order perf. model
tau_fb_acc = 3;
EKF.accest.tau = tau_fb_acc;
EKF.accest.k_fb = 1/tau_fb_acc;

tau_fb_mag = 3;
EKF.magest.tau = tau_fb_mag;
EKF.magest.k_fb = 1/tau_fb_mag;

EKF.partq.taupsi = 10/3;
EKF.partq.taualpha = 5/3;
EKF.partq.taupsi_bias = 100/3;
EKF.partq.taualpha_bias = 100/3;
EKF.partq.dt_gain_mag = dt;
EKF.partq.dt_gain_acc = dt;

EKF.bsat.bmax = 3*d2r;
EKF.bsat.tau = dt;
EKF.bsat.kaw = 1-exp(-dt/(EKF.bsat.tau));
EKF.bsat.kaw_sto = EKF.bsat.kaw;
EKF.magest.k_fb = 0;
EKF.accest.k_fb = 0;

% Measurement covariances
Ra = cov_acc;
Rm = cov_mag;
EKF.Ra = Ra;
EKF.Rm = Rm;
EKF.cov.gyro = cov_gyro;
EKF.cov.bgyro = diag(EKF.bsat.Qd);

% EKF.kcov = 1;
% EKF.kcov_mag = 1;

Scenario.t = t;
Scenario.Ts = dt;
Scenario.tsim = tend;

euler0 = pi/180*[45;45;45]*0;
% euler0 = pi/180*[10;10;10]*4.5;
q0 = eul2qua(euler0);
q = q0;
R = qua2chrSL(q);

% Init EKF
% eulerhat0 = 1*[-0, 0, 45]*d2r;
eulerhat0 = 0*[0, 45, 45]*d2r;
% eulerhat0 = euler0 + [25;25;45];
qhat0 = eul2qua(eulerhat0);
% qhat0=[1;0;0;0];
% bhat0 = [0;0;0];
bhat0 = [7.10; -23.30; 42.66]*d2r/183; % bhat0 for test 14
% bhat0(3) = 0.554*d2r;
mrhat_cov = eye(3)*(norm(mag_ref)*0.1/3/sqrt(EKF.magest.tau))^2;
arhat_cov = eye(3)*(norm(gvec)*0.1/3/sqrt(EKF.magest.tau))^2;
bhat_cov = eye(3)*(0.5/3/r2d)^2;
euler_cov = diag(([5/3, 5/3, 5/3]*d2r).^2);
Phat0 = blkdiag(EulerCov2QuaternionCov(eulerhat0,euler_cov), bhat_cov);

xhat_ex = qhat0;
Phat_ex = Phat0(1:4,1:4);

xhat_ms = [EKF.gvec;
    EKF.mag_ref;
    zeros(9,1)];

% Init MEKF
xhat_mekf0 = [qhat0; bhat0];
Phat_mekf0 = blkdiag(euler_cov, bhat_cov);

% Init MEKF-PM Mag
xhat_mekf_pm_mag0 = [qhat0; bhat0; EKF.mag_ref];
Phat_mekf_pm_mag0 = blkdiag(euler_cov, bhat_cov, mrhat_cov);

% Init MEKF-PM Acc Mag
xhat_mekf_pm_acc_mag0 = [qhat0; bhat0; EKF.gvec; EKF.mag_ref];
Phat_mekf_pm_acc_mag0 = blkdiag(euler_cov, bhat_cov, arhat_cov, mrhat_cov);

% Init Sensor EKF
ahat0 = qua2chrSL(qhat0)'*gvec;
Phat_acc0 = Ra;
ahat_p = ahat0; Phat_acc_p = Phat_acc0;
mhat0 = qua2chrSL(qhat0)'*mag_ref;
Phat_mag0 = Rm;
mhat_p = mhat0; Phat_mag_p = Phat_mag0;

% Martin & Sarras init
ahat0 = qua2chrSL(qhat0)'*gvec;
ahat = ahat0; Phat_acc_p = Phat_acc0;
mhat0 = qua2chrSL(qhat0)'*mag_ref;
mhat = mhat0; Phat_mag_p = Phat_mag0;
bhat = zeros(3,1);

% Init comp
ka = 0.2; km = 0.8; kp = 0.1; ki = 0.1; kb = 1;
gain = [ka km kp ki kb];

k_a = 2; m_a = 10; k_b = 1; l_b = 10; m_b = 10;
gain_ms = [k_a, k_b, l_b, m_a, m_b];

tau_b = 100;
kc = 10;
ka = kc;
km = kc;

% lc = kc/2*(3*omegasignoise+1/tau_b);
lc = 0.01;
la = lc;
lm = lc;

gain_m = [ka km la lm];

% cov_in.acc = Ra;
% cov_in.mag = Rm;
% cov_in.gyro = diag(cov_gyro);

xhat_m = [ahat; mhat; zeros(3,1)];

% init
eulerhatout = zeros(3,size(t,2));
eulerhatexout = zeros(3,size(t,2));
eulerout = zeros(3,size(t,2));
sigma_euler_out = zeros(3,size(t,2));
sigma_euler_ex_out = zeros(3,size(t,2));
sigma_bias_out = zeros(3,size(t,2));

ahat_out = zeros(3,size(t,2));
sigma_ahat_out = zeros(3,size(t,2));
mhat_out = zeros(3,size(t,2));
sigma_mhat_out = zeros(3,size(t,2));

acc_status_out = zeros(3,size(t,2));
mag_status_out = zeros(3,size(t,2));
corr_status_out = zeros(2,size(t,2));

acc_out = zeros(3,size(t,2));
mag_out = zeros(3,size(t,2));

ac_out = zeros(3,size(t,2));
mc_out = zeros(3,size(t,2));

norm_acc_out = zeros(size(t));
norm_mag_out = zeros(size(t));

accm_out = zeros(3,size(t,2));
magm_out = zeros(3,size(t,2));
gyro_out = zeros(3,size(t,2));
bias_out = zeros(3,size(t,2));
bias_lp_out = zeros(3,size(t,2));

xhat_out = zeros(7,size(t,2));
Phat_out = zeros(7,7,size(t,2));

xhat_m_out = zeros(9,size(t,2));

xhat_ms_out = zeros(15,size(t,2));


norm_error_qhat_ex_out = zeros(1,size(t,2));
norm_error_euler_out = zeros(1,size(t,2));
norm_error_qhat_out = zeros(1,size(t,2));
norm_error_bhat_out = zeros(1,size(t,2));

btracep_out = zeros(1,size(t,2));
ptrace_pre_out = zeros(1,size(t,2));
ptrace_post_out = zeros(1,size(t,2));

q_out = zeros(4,size(t,2));
qsig_out = zeros(4,size(t,2));
qhat_out = zeros(4,size(t,2));
bhat_out = zeros(3,size(t,2));
mi_out = zeros(3,size(t,2));

tmp1_out = zeros(1,size(t,2));
tmp2_out =zeros(1,size(t,2));
tmp3_out =zeros(1,size(t,2));
tmp4_out =zeros(1,size(t,2));
tmp_dec_out = zeros(3,size(t,2));

% GLR Parameters
% b_lpgyro = 1; % Open loop estimate of gyroscope bias
% b_glr = 1;  % Enable fault detection by GLR
% b_glr_attreset = 0; % Enable attitude reset on fault detection
% b_useglr_vec = [0, 0, 0, 1, 1, 0]; % [1]: Norm, [2]: Outlier, [3]: PM
% L = 50;
% % PFA_GLR = 1e-5;
% PFA_GLR = 1-0.9967;   % 3 sigma
% % PFA_GLR = 1-0.99;
% % PFA_chi2 = PFA_GLR;
% PFA_chi2 = 1-0.9967;   % 3 sigma
% ndt_glr = 1;
% dt_glr = dt*ndt_glr;

% glr_params
[ GLRparams_3, GLRinit_3 ] = setGLRparams( eye(3),eye(3),eye(3),L,PFA_GLR,PFA_chi2, dt_glr);
[ GLRparams_1, GLRinit_1 ] = setGLRparams( 1,1,1,L,PFA_GLR,PFA_chi2, dt_glr);

PHIxn_magnorm  = GLRinit_1.PHIxn;
bhatn_magnorm  = GLRinit_1.bhatn;   % filter bank estimation [nb x L]
LAMbn_magnorm  = GLRinit_1.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_magnorm     = GLRinit_1.qn;      % Filter bank age [1 x L]
GLRn_magnorm   = GLRinit_1.GLRn;    % GLR statistic storage          

PHIxn_magpm  = GLRinit_3.PHIxn;
bhatn_magpm  = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_magpm  = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_magpm     = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_magpm   = GLRinit_1.GLRn;    % GLR statistic storage          

PHIxn_magout = GLRinit_3.PHIxn;
bhatn_magout = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_magout = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_magout    = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_magout  = GLRinit_1.GLRn;    % GLR statistic storage          

PHIxn_accnorm  = GLRinit_1.PHIxn;
bhatn_accnorm  = GLRinit_1.bhatn;   % filter bank estimation [nb x L]
LAMbn_accnorm  = GLRinit_1.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_accnorm     = GLRinit_1.qn;      % Filter bank age [1 x L]
GLRn_accnorm   = GLRinit_1.GLRn;    % GLR statistic storage          

PHIxn_accpm  = GLRinit_3.PHIxn;
bhatn_accpm  = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_accpm  = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_accpm     = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_accpm   = GLRinit_1.GLRn;    % GLR statistic storage          

PHIxn_accout = GLRinit_3.PHIxn;
bhatn_accout = GLRinit_3.bhatn;   % filter bank estimation [nb x L]
LAMbn_accout = GLRinit_3.LAMbn;	% filter bank estimation information [nb x nb*L]
qn_accout    = GLRinit_3.qn;      % Filter bank age [1 x L]
GLRn_accout  = GLRinit_1.GLRn;    % GLR statistic storage          

% Storage
GLRvec = zeros(6,numel(t));
qdetvec = zeros(6,numel(t));
chi2vec = zeros(6,numel(t));

alpha_error = zeros(1,numel(t));
psi_error = zeros(1,numel(t));

att_reset = zeros(2,numel(t));
l_gyro_out = zeros(1,numel(t));
l_gyro_iso_out = zeros(3,numel(t));

clear eout;
qbias0 = eul2qua(rpy_in(1,2:4)); 
qbias = qbias0; 
for k = 1:numel(t), 
    qbias = quatintegrate(qbias, gyro_in(k,2:4)', dt_vec(k)); 
    eout(:,k) = qua2eul(qbias); 
end
bhat_ol = (qua2eul(qbias)-qua2eul(qbias0))/(gyro_in(end,1));
ext_bias_out  = zeros(3, size(t,2));

% Rotated Px4
px4_rotated = (px4_in(:,2:4)-[ones(size(t'))*px4_in(1,2), ones(size(t'))*px4_in(1,3), ones(size(t'))*px4_in(1,4)])*r2d;

% Gyro LP filter for bias estimation
tau = EKF.partq.taupsi_bias;
alpha = 1 - exp(-dt/tau);
alpha_fuse = 1 - exp(-dt/0.00001);
bhat_lp_0 = bhat0;

biascase = 1;
biasfactor = 1;
resetcase = 1;

%%
% for ke = [ 1 2 3 4]
% for ke = [1 2 3 4 5]
for ke  = ke_vec
    Phat = Phat0;
    xhat = [qhat0; bhat0];
    Phat_mekf = Phat_mekf0;
    xhat_mekf = [qhat0; bhat0];
    Phat_mekf_pm_mag = Phat_mekf_pm_mag0;
    xhat_mekf_pm_mag = xhat_mekf_pm_mag0;
    Phat_mekf_pm_acc_mag = Phat_mekf_pm_acc_mag0;
    xhat_mekf_pm_acc_mag = xhat_mekf_pm_acc_mag0;
    xhat_m = [ahat0; mhat0; zeros(3,1)];
    q = q0;
    ahat_p = ahat0; Phat_acc_p = Phat_acc0;
    mhat_p = mhat0; Phat_mag_p = Phat_mag0;
    % Bias initialisation at ss value
    bhat_lp = bhat_ol;
    xhat(5:7) = bhat_ol;
    xhat_mekf(5:7) = bhat_ol;
    xhat_mekf_pm_mag(5:7) = bhat_ol;
    xhat_mekf_pm_acc_mag(5:7) = bhat_ol;
    ext_bias = bhat_ol;
%     bhat_lp = zeros(3,1);
%     xhat(5:7) = zeros(3,1);
%     bhat_lp = 20*ones(3,1)*d2r;
%     xhat(5:7) = 20*ones(3,1)*d2r;   
    
    h1 = waitbar(0,'Simulating...');
    for k = 1:floor(numel(t))
%         tic1 = tic;
        tk = t(k);
        dt = dt_vec(k);
        
%         fprintf('======= %3.4f ======\n',tk);
        % gyro bias
        %
        om_bias = [0;0;0];
        
        % Attitude update
        q = eul2qua(rpy_in(k,2:4));
        
        R = qua2chrSL(q);
        [PHI, THETA, PSI] = Quaternion2Euler(q);
        euler = [PHI; THETA; PSI];
        eulerout(:,k) = rpy_in(k,2:4);
        
        acc_k = R'*gvec;
        mag_k = R'*mag_ref;
        
        % Measurements
        %accm = acc_k + bnoise*randn(3,1)*meassignoise + R'*accfault(:,k);
        %magm = mag_k + bnoise*randn(3,1)*meassignoise + R'*magfault(:,k);
        accm = acc_in(k,2:4)';
        magm = mag_in(k,2:4)';
        
        acc_out(:,k) = acc_k;
        mag_out(:,k) = mag_k;
        
        accm_out(:,k) = accm;
        magm_out(:,k) = magm;
        gyro_out(:,k) = gyro_in(k,2:4)';
        bias_out(:,k) = om_bias;
        
        inputs = gyro_out(:,k);
        measurements = [accm; magm];
        %         xhat_pm = [q; om_bias];
        xhat_pm = xhat;
%         tic2 = tic;
        
        l_gyro_out(:,k) = gyro_in(k,2:4)*pinv(diag(EKF.cov.gyro))*gyro_in(k,2:4)';
                l_gyro = zeros(3,1);
        for klmn = 1:3
            l_gyro(klmn) = gyro_out(klmn,k)^2/EKF.cov.gyro(klmn);
            l_gyro_iso_out(klmn,k) = l_gyro(klmn);
        end
        b_update_lpgyro = l_gyro < GLRparams_1.chi2thr;
        % LP filtering of gyro to estimate gyro bias in open loop
%         if l_gyro_out(:,k) < GLRparams_3.chi2thr
%             bhat_lp = sat((ones(3,1)-b_update_lpgyro.*ones(3,1)*alpha).*bhat_lp + b_update_lpgyro.*ones(3,1)*alpha.*gyro_out(1:3,k),-EKF.bsat.bmax,EKF.bsat.bmax);
%         end
%         if l_gyro_out(:,k) < GLRparams_3.chi2thr
        bhat_lp = sat((ones(3,1)-b_update_lpgyro.*ones(3,1)*alpha).*bhat_lp + b_update_lpgyro.*ones(3,1)*alpha.*gyro_out(1:3,k),-EKF.bsat.bmax,EKF.bsat.bmax);
%         end
        % LP filtering of gyro to estimate gyro bias in open loop
%         if l_gyro_out(:,k) < GLRparams_3.GLRthr
%             bhat_lp = sat((1-alpha)*bhat_lp + alpha*gyro_in(k,2:4)',-EKF.bsat.bmax,EKF.bsat.bmax);
%         end
        bias_lp_out(:,k) = bhat_lp;
        
        
        % ============= Sensor estimation =======================
        [b_norm_acc, b_norm_mag, l_acc, l_mag,...
        S_norm_acc, S_norm_mag, delta_norm_acc, delta_norm_mag] ...
        = measnormcheck(EKF,measurements);
    
        
        [ mhat_p, Phat_mag_p, status_vec_mag, S_pm_mag, delta_pm_mag,...
        S_out_mag, delta_out_mag] = mag_perf_model(EKF, mhat_p, Phat_mag_p,...
            inputs, magm, xhat_pm, Phat, b_norm_mag, dt );
        [ ahat_p, Phat_acc_p, status_vec_acc, S_pm_acc, delta_pm_acc,...
        S_out_acc, delta_out_acc ] = acc_perf_model(EKF, ahat_p, Phat_acc_p,...
            inputs, accm, xhat_pm, Phat, b_norm_acc, dt );
        
        status_vec_acc_a = [b_norm_acc; status_vec_acc];
        status_vec_mag_a = [b_norm_mag; status_vec_mag];
        
        % =================================================================
        %               GLR fault detection
        % =================================================================
        %         idp_mag = 1 - double(rem(t(k),dt_glr) < 0.001*dt);
        idp_mag = 1-mag_ok(k);
        idp_acc = 1-acc_ok(k);
        kresetglr = 0;
        lamff = 1;
        
        if b_useftarch
            % GLR test for acc norm
            nunp1 = delta_norm_acc;
            Knp1 = zeros(1)/2;
            Snp1 = S_norm_acc;
            if b_glr && b_useglr_vec(1)
                [qdetnp1,GLRmax,qn_accnorm,bhatn_accnorm,LAMbn_accnorm,PHIxn_accnorm, GLRn_accnorm] ...
                    = glrtest(idp_acc, kresetglr,lamff,Knp1,Snp1,nunp1,qn_accnorm,bhatn_accnorm,LAMbn_accnorm,PHIxn_accnorm,GLRn_accnorm,GLRparams_1);
                b_norm_acc_glr = double(GLRmax > GLRparams_1.GLRthr);
                b_norm_acc_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_1.chi2thr);
                b_norm_acc = double(b_norm_acc_glr + b_norm_acc_chi2 > 0);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_norm_acc = double(GLRmax > GLRparams_1.chi2thr);
            end
            
            
            qdetvec(1,k) = qdetnp1;
            GLRvec(1,k) = GLRmax;
            chi2vec(1,k) = nunp1'*pinv(Snp1)*nunp1;
            
            % GLR test for acc outliers
            nunp1 = delta_out_acc;
            Knp1 = zeros(3)/2;
            Snp1 = S_out_acc;
            if b_glr && b_useglr_vec(2)
                [qdetnp1,GLRmax,qn_accout,bhatn_accout,LAMbn_accout,PHIxn_accout,GLRn_accout] ...
                    = glrtest(idp_acc, kresetglr,lamff,Knp1,Snp1,nunp1,qn_accout,bhatn_accout,LAMbn_accout,PHIxn_accout,GLRn_accout,GLRparams_3);
                b_out_acc_glr = double(GLRmax > GLRparams_3.GLRthr);
                b_out_acc_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_3.chi2thr);
                b_out_acc = double(b_out_acc_glr + b_out_acc_chi2 > 0);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_out_acc = double(GLRmax > GLRparams_3.chi2thr);
            end
            
            
            
            qdetvec(2,k) = qdetnp1;
            GLRvec(2,k) = GLRmax;
            chi2vec(2,k) = nunp1'*pinv(Snp1)*nunp1;
            
            % GLR test for acc perf model
            nunp1 = delta_pm_acc;
            Knp1 = zeros(3)/2;
            Snp1 = S_pm_acc;
            if b_glr && b_useglr_vec(3)
                [qdetnp1,GLRmax,qn_accpm,bhatn_accpm,LAMbn_accpm,PHIxn_accpm,GLRn_accpm] ...
                    = glrtest(idp_acc, kresetglr,lamff,Knp1,Snp1,nunp1,qn_accpm,bhatn_accpm,LAMbn_accpm,PHIxn_accpm,GLRn_accpm,GLRparams_3);
                b_pm_acc = double(GLRmax > GLRparams_3.GLRthr);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_pm_acc = double(GLRmax > GLRparams_3.chi2thr);
            end
            
            
            qdetvec(3,k) = qdetnp1;
            GLRvec(3,k) = GLRmax;
            chi2vec(3,k) = nunp1'*pinv(Snp1)*nunp1;
            
            % GLR test for mag norm
            nunp1 = delta_norm_mag;
            Knp1 = zeros(1)/2;
            Snp1 = S_norm_mag;
            if b_glr && b_useglr_vec(4)
                [qdetnp1,GLRmax,qn_magnorm,bhatn_magnorm,LAMbn_magnorm,PHIxn_magnorm,GLRn_magnorm] ...
                    = glrtest(idp_mag, kresetglr,lamff,Knp1,Snp1,nunp1,qn_magnorm,bhatn_magnorm,LAMbn_magnorm,PHIxn_magnorm,GLRn_magnorm,GLRparams_1);
                b_norm_mag_glr = double(GLRmax > GLRparams_1.GLRthr);
                b_norm_mag_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_1.chi2thr);
                b_norm_mag = double(b_norm_mag_glr + b_norm_mag_chi2 > 0);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_norm_mag = double(GLRmax > GLRparams_1.chi2thr);
            end
            
            
            qdetvec(4,k) = qdetnp1;
            GLRvec(4,k) = GLRmax;
            chi2vec(4,k) = nunp1'*pinv(Snp1)*nunp1;
            
            % GLR test for mag outliers
            nunp1 = delta_out_mag;
            Knp1 = zeros(3)/2;
            Snp1 = S_out_mag;
            if b_glr && b_useglr_vec(5)
                [qdetnp1,GLRmax,qn_magout,bhatn_magout,LAMbn_magout,PHIxn_magout,GLRn_magout] ...
                    = glrtest(idp_mag, kresetglr,lamff,Knp1,Snp1,nunp1,qn_magout,bhatn_magout,LAMbn_magout,PHIxn_magout,GLRn_magout,GLRparams_3);
                b_out_mag_glr = double(GLRmax > GLRparams_3.GLRthr);
                b_out_mag_chi2 = double(nunp1'/Snp1*nunp1 > GLRparams_3.chi2thr);
                b_out_mag = double(b_out_mag_glr + b_out_mag_chi2 > 0);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_out_mag = double(GLRmax > GLRparams_3.chi2thr);
            end
            
            qdetvec(5,k) = qdetnp1;
            GLRvec(5,k) = GLRmax;
            chi2vec(5,k) = nunp1'*pinv(Snp1)*nunp1;
            
            % GLR test for mag perf model
            nunp1 = delta_pm_mag;
            Knp1 = zeros(3)/2;
            Snp1 = S_pm_mag;
            if b_glr && b_useglr_vec(6)
                [qdetnp1,GLRmax,qn_magpm,bhatn_magpm,LAMbn_magpm,PHIxn_magpm,GLRn_magpm] ...
                    = glrtest(idp_mag, kresetglr,lamff,Knp1,Snp1,nunp1,qn_magpm,bhatn_magpm,LAMbn_magpm,PHIxn_magpm,GLRn_magpm,GLRparams_3);
                b_pm_mag = double(GLRmax > GLRparams_3.GLRthr);
            else
                GLRmax = nunp1'*pinv(Snp1)*nunp1;
                qdetnp1 = 0;
                b_pm_mag = double(GLRmax > GLRparams_3.chi2thr);
            end
            
            
            qdetvec(6,k) = qdetnp1;
            GLRvec(6,k) = GLRmax;
            chi2vec(6,k) = nunp1'*pinv(Snp1)*nunp1;
            
            status_vec_mag_a = [b_norm_mag; b_out_mag; b_pm_mag];
            
            status_vec_acc_a = [b_norm_acc; b_out_acc; b_pm_acc];
        
            % Measurement consolidation
            [ac, Phat_ac, mc, Phat_mc, corr_status] = input_consolidation(...
             EKF, ahat_p, accm, mhat_p, magm, Phat_acc_p, Phat_mag_p,...
             status_vec_acc_a, status_vec_mag_a);
        else
            ac = accm; Phat_ac = EKF.Ra;
            mc = magm; Phat_mc = EKF.Rm;
            corr_status = [1-status_vec_acc_a(1); 1-status_vec_mag_a(1)];
        end
        
        % =================================================================
        mhat_out(:,k) = mhat_p;
        sigma_mhat_out(:,k) = diag(Phat_mag_p);
        ahat_out(:,k) = ahat_p;
        sigma_ahat_out(:,k) = diag(Phat_acc_p);
        
        acc_status_out(:,k) = status_vec_acc_a;
        mag_status_out(:,k) = status_vec_mag_a;
        
        %==================================================================
        % Attitude reset for GLR fault rejection
        %==================================================================
        if k > 1 && b_glr_attreset && b_glr
            % Reset on norm detection
            if (mag_status_out(1,k) > 0 &&  mag_status_out(1,k-1) < 1) && b_useglr_vec(4)
                att_reset(1,k) = 1;
                b_omega_tmp = xhat_out(5:7,k-qdetvec(4,k)*ndt_glr);
                qhat_tmp = xhat_out(1:4,k-qdetvec(4,k)*ndt_glr);
                omega_mat_tmp = gyro_out(:,k-qdetvec(4,k)*ndt_glr:k);
                for kl = 1:qdetvec(4,k)*ndt_glr
                    qhat_tmp = quatint( qhat_tmp, omega_mat_tmp(:,kl)-b_omega_tmp, dt);
                end
%                 xhat = [qhat_tmp; b_omega_tmp];
                xhat = [qhat_tmp; xhat(5:7)];
            % Reset on outlier detection
            elseif (mag_status_out(2,k) > 0 &&  mag_status_out(2,k-1) < 1) && b_useglr_vec(5)
                att_reset(2,k) = 1;
                b_omega_tmp = xhat_out(5:7,k-qdetvec(5,k)*ndt_glr);
                qhat_tmp = xhat_out(1:4,k-qdetvec(5,k)*ndt_glr);
                omega_mat_tmp = gyro_out(:,k-qdetvec(5,k)*ndt_glr:k);
                for kl = 1:qdetvec(5,k)*ndt_glr
                    qhat_tmp = quatint( qhat_tmp, omega_mat_tmp(:,kl)-b_omega_tmp, dt);
                end
%                 xhat = [qhat_tmp; b_omega_tmp];
                xhat = [qhat_tmp; xhat(5:7)];
            else
                att_reset(:,k) = 0;
            end
        end
        %==================================================================
        
        
        b_nobias = 0;
%         ac = accm; mc = magm;
%                 ac = ahat_p; 
%         mc = mhat_p;
        % if ke == 3
%         corr_status = [1;0];
%         corr_status = [1;1];
        % end
        
        measurements = [ac; mc];
        
        corr_status = corr_status.*[acc_ok(k); mag_ok(k)];
        norm_acc_out(k) = norm(accm);
        norm_mag_out(k) = norm(magm);
        ac_out(:,k) = measurements(1:3);
        mc_out(:,k) = measurements(4:6);
        corr_status_out(:,k) = corr_status;
        
        % Use LP gyro estimation in case of disturbance
        ext_bias = xhat(5:7);
        if b_lpgyro
            if sum(status_vec_mag_a(1:2)) > 0
                xhat(7) = bhat_lp(3);
                xhat_mekf(7) = bhat_lp(3);
                ext_bias(3) =  bhat_lp(3);
            end
            if sum(status_vec_acc_a(1:2)) > 0
                xhat(5:6) = bhat_lp(1:2);
                xhat_mekf(5:6) = bhat_lp(1:2);
                ext_bias(1:2) =  bhat_lp(1:2);
            end
        end
        ext_bias_out(:,k) = ext_bias;
%         if b_lpgyro
% %             if sum(status_vec_mag_a(1:2)) > 0
%             if k > 2 && (sum(mag_status_out(1,k-2:k)) > 2 || sum(mag_status_out(1,k-2:k)) > 2)
%                 ext_bias(3) =  (1-alpha_fuse)*ext_bias(3) + alpha_fuse*bhat_lp(3);
%                 
% %                 xhat(7) = ext_bias(3);
% %                 xhat_mekf(7) = ext_bias(3);
% %                 xhat_mekf_pm_mag(7) = ext_bias(3);
% %                 xhat_mekf_pm_acc_mag(7) = ext_bias(3);
%                 %ext_bias(3) =  bhat_lp(3);
%             else
%                 ext_bias(3) =  (1-alpha_fuse)*ext_bias(3) + alpha_fuse*xhat(7);
%             end
% %             if sum(status_vec_acc_a(1:2)) > 0
%             if k > 2 && (sum(acc_status_out(1,k-2:k)) > 2 || sum(acc_status_out(1,k-2:k)) > 2)
%                 ext_bias(1:2) =  (1-alpha_fuse)*ext_bias(1:2) + alpha_fuse*bhat_lp(1:2);
% %                 xhat(5:6) = bhat_lp(1:2);
% %                 xhat_mekf(5:6) = bhat_lp(1:2);
% %                 xhat_mekf_pm_mag(5:6) = bhat_lp(1:2);
% %                 xhat_mekf_pm_acc_mag(5:6) = bhat_lp(1:2);
%                 %ext_bias(1:2) =  bhat_lp(1:2);
%             else
%                 ext_bias(1:2) =  (1-alpha_fuse)*ext_bias(1:2) + alpha_fuse*xhat(5:6);
%             end
%             
%         end
%         ext_bias_out(:,k) = ext_bias;
        
%         toc(tic2)
        % ============= Attitude estimation =======================
%         tic3 = tic;
        switch ke
            case 1
                % ---- Comp. filter ----
                filt_title = 'Hua';
                [ xhat] = hua_2011_decoupled(EKF, xhat, inputs,  measurements, dt, gain, corr_status);
            case 2 % ---- EKF ----
                filt_title = 'EKF';
                [xhat,Phat] = ekf_update(EKF,inputs, measurements, xhat,Phat, dt, corr_status );
            case 3 % ---- DEC - EKF ----
                filt_title = 'Measurement decoupled KF';
                [tmp_dec,xhat,Phat] = deckf_update(EKF,inputs, measurements, xhat,Phat, dt, corr_status );
            case 4 % ---- NL - KF ----
                filt_title = 'NL-KF';
                [tmpstruct2, tmpstruct, tmp, xhat,Phat] = ftkf_update(EKF,inputs,measurements,xhat,Phat, dt, corr_status, b_nobias, ext_bias);
                %                 filt_title = 'eXogenous Kalman Filter KF';
                %                 uhat_ex = xhat; cov_uhat_ex = Phat;
                %                 [xhat_ex,Phat_ex] = qhat_xkf_update(EKF,inputs,uhat_ex, cov_uhat_ex, xhat_ex,Phat_ex, dt);
            case 5 % Comp filter global decoupling
                filt_title = 'Hua, Global';
                [ xhat] = hua_2011_decoupled_global(EKF, xhat, inputs,  measurements, dt, gain, corr_status);
            case 6 % Sarras / Martin attitude observer with measurement biases
                filt_title = 'Martin + Sarras, 2018, Bias observer';
%                 [ xhat_ms, qhat] = martin_sarras_bias(EKF, xhat_ms, inputs,  measurements, dt, gain_ms, corr_status);
                [ xhat_ms, qhat] = martin_sarras_2018_bias(xhat_ms, inputs,  measurements, EKF,  dt, gain_ms);
                %                 measurements = xhat_ms(1:6);
                %                 [ xhat] = hua_2011_decoupled_global(EKF, xhat, inputs,  measurements, dt, gain, corr_status);
                
                xhat = [qhat; xhat_ms(7:9)];
                xhat_ms_out(:,k) = xhat_ms;
            case 7 % ---- DEC MEKF ----
                filt_title = 'MEKF';
%                 [ xhat,Phat ] = smekf_update(EKF, inputs, measurements, xhat,Phat, dt, corr_status);
                [ Phat_mekf, xhat_mekf, Knorm ] = mekf( Phat_mekf, xhat_mekf, inputs, measurements, corr_status, EKF, dt, ext_bias );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf(1:3,1:3)*ddqda', Phat_mekf(4:6,4:6));
                xhat = xhat_mekf;
                Knorm_out(:,k) = Knorm;
            case 8 % ---- DEC GMEKF ----
                filt_title = 'Dec. IEKF';
%                 [ xhat,Phat ] = smekf_update(EKF, inputs, measurements, xhat,Phat, dt, corr_status);
                [ Phat_mekf, xhat_mekf, Knorm ] = invariant_mekf( Phat_mekf, xhat_mekf, inputs, measurements, corr_status, EKF, dt, ext_bias );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf(1:3,1:3)*ddqda', Phat_mekf(4:6,4:6));
                xhat = xhat_mekf;
                Knorm_out(:,k) = Knorm;
            case 9 % ---- DEC IEKF ----
                filt_title = 'Decoupled IEKF, Mag. PM';
%                 [ Phat_mekf_pm_mag, xhat_mekf_pm_mag ] = iekf( Phat_mekf_pm_mag, xhat_mekf_pm_mag, inputs, measurements, corr_status, EKF, dt );
                [ Phat_mekf_pm_mag, xhat_mekf_pm_mag, Knorm ] = iekf_dec_mag( Phat_mekf_pm_mag, xhat_mekf_pm_mag, inputs, measurements, corr_status, EKF, dt );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf_pm_mag(1:3,1:3)*ddqda', Phat_mekf_pm_mag(4:end,4:end));
                xhat = xhat_mekf_pm_mag;
                mag_ekf_out(:,k) = xhat_mekf_pm_mag(8:10);
                Knorm_out(:,k) = Knorm;
            case 10 % ---- DEC IEKF ----
                filt_title = 'Decoupled IEKF, Full PM';
%                 [ Phat_mekf_pm_mag, xhat_mekf_pm_mag ] = iekf( Phat_mekf_pm_mag, xhat_mekf_pm_mag, inputs, measurements, corr_status, EKF, dt );
                [ Phat_mekf_pm_acc_mag, xhat_mekf_pm_acc_mag, Knorm ] = iekf_dec( Phat_mekf_pm_acc_mag, xhat_mekf_pm_acc_mag, inputs, measurements, corr_status, EKF, dt );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf_pm_acc_mag(1:3,1:3)*ddqda', Phat_mekf_pm_acc_mag(4:end,4:end));
                xhat = xhat_mekf_pm_acc_mag;
                mag_ekf_out(:,k) = xhat_mekf_pm_acc_mag(11:13);
                acc_ekf_out(:,k) = xhat_mekf_pm_acc_mag(8:10);
                Knorm_out(:,k) = Knorm;
            case 11 % ---- Valenti ----
                filt_title = 'Valenti';
                [ xhat ] = valenti_2016_fast(EKF, inputs, measurements, xhat, dt );
            case 12 % ---- FT-KF with measured uhat ----
                filt_title = 'NL-KF - umeas';
                [tmpstruct2, tmpstruct, tmp, xhat,Phat] = ftkf_update_2(EKF,inputs,measurements,xhat,Phat, dt, corr_status, b_nobias, ext_bias);
            case 13 % ---- DEC GMEKF ----
                filt_title = ['Dec. IEKF, ncov=',num2str(ncov)];
%                 [ xhat,Phat ] = smekf_update(EKF, inputs, measurements, xhat,Phat, dt, corr_status);
                [ Phat_mekf, xhat_mekf, Knorm , l_error] = invariant_mekf_covmax( Phat_mekf, xhat_mekf, inputs, measurements, corr_status, EKF, dt, ext_bias,ncov );
                ddqda = [zeros(1,3); eye(3)*0.5];
                Phat = blkdiag(ddqda*Phat_mekf(1:3,1:3)*ddqda', Phat_mekf(4:6,4:6));
                xhat = xhat_mekf;
                l_mekf_out(k) = l_error;
            case 14 % ---- EKF ----
                filt_title = 'FT_EKF';
                [xhat,Phat] = ft_ekf_update(EKF,inputs, measurements, xhat,Phat, dt, corr_status );
            
        end
            
%         toc(tic3)
        
%         ac_n = ac/norm(ac);
%         mc_n = mc/norm(mc);
        
        %         Rot_meas = [ac_n, cross(ac_n, mc_n), cross(ac_n, cross(ac_n, mc_n))];
%         Rot_meas = [cross(ac_n,cross(ac_n, mc_n)), cross(ac_n, mc_n), ac_n];
%         
%         eul_meas(:,k) = chr2eul(Rot_meas');
        
%         if ke ==  3
%             tmp_dec_out(1:3,k) = 4*tmp_dec(2:4)/(tmp_dec(1)+1); % Modified Rodrigues parameter
%             %             aacc = acos(tmp_dec(1));
%             %             tmp_dec_out(1:3,k) = tmp_dec(2:4)/sin(aacc);
%             tmp_dec_out(1:3,k) = tmp_dec_out(1:3,k) / norm(tmp_dec_out(1:3,k));
%         end
        
%         if floor(tk) == floor(tend/2)
%             qreset = eul2qua([45, 45, 90]*pi/180);
%             xhat = [qreset; xhat(5:7)];
%             xhat_mekf(1:4) = qreset;
%             xhat_mekf_pm_mag(1:4) = qreset;
%             xhat_mekf_pm_acc_mag(1:4) = qreset;
%             q = [1;0;0;0];
%         end
        
        qhat = xhat(1:4);
        e3b = R'*e3;
        e1b = R'*e1;
        
        e3bhat = Quaternion2RotMat(qhat)'*e3;
        e1bhat = Quaternion2RotMat(qhat)'*e1;
        
        alpha_error(1,k) = norm(cross(e3b,e3bhat));
        psi_error(1,k) = norm(cross(e1b,e1bhat));
        
        
%         Ptrace_post = trace(Phat);
        
        [PHI, THETA, PSI] = Quaternion2Euler(qhat);
        eulerhat = [PHI; THETA; PSI];
        eulerhatout(:,k) = eulerhat;
        eulerhatexout(:,k) = qua2eulSL(xhat_ex);
        
        sigma_euler_ex_out(:,k) = QuaternionCov2EulerSTD(Phat_ex(1:4,1:4),xhat_ex);
        sigma_euler_out(:,k) = QuaternionCov2EulerSTD(Phat(1:4,1:4),qhat);
        sigma_bias_out(:,k) = sqrt(diag(Phat(5:7,5:7)));
        [PHI, THETA, PSI] = Quaternion2Euler(q);
        euler = [PHI; THETA; PSI];
        error_euler = rpy_in(k,2:4)' - eulerhat;
        
        norm_error_euler_out(:,k) = norm(error_euler);
        norm_error_qhat_out(:,k) = norm(quaternionErrorFct(q,qhat));
        norm_error_qhat_ex_out(:,k) = norm(quaternionErrorFct(q,xhat_ex));
        norm_error_bhat_out(:,k) = norm( xhat(5:7)-om_bias);
        
        qsig_out(:,k) = sqrt(diag(Phat(1:4,1:4)));
        Phat_out(:,:,k) = Phat;
        qhat_out(:,k) = qhat;
        bhat_out(:,k) =  xhat(5:7);
        xhat_out(:,k) = xhat;
        
        q_out(:,k) = q;
        
        if abs(mod(k,100)) < 0.0001
            waitbar((t(k)/tend),h1)
        end
%         toc(tic1)
    end
    close(h1);
   
    %% Edit plot parameters
    if b_glr
            filt_title  = [filt_title, [', GLR detection: L=',num2str(L),', P_{FA} = ',num2str(PFA_GLR)]];
        else
            filt_title  = [filt_title, [', \chi^2 detection: P_{FA} = ',num2str(PFA_GLR)]];
    end
    titlevec_acc = {'Acc Norm','Acc Out','Acc PM'};
    titlevec_mag = {'Mag Norm','Mag Out','Mag PM'};
    
    
    %% Plotty
    indplot = 1:find(t>=min(tstop,t(end)));
    tplot = t(indplot);
    ndown = 1;
    % Euler plot
    figure,
    subplot(411),title(filt_title)
    for k = 1:3
        subplot(4,1,k),hold on, axis([0 t(end) -180 180])
        plot(tplot, px4_rotated(indplot,k),'b','linewidth',1)
        plot(tplot,eulerout(k,indplot)*r2d,'r',tplot,eulerhatout(k,indplot)*r2d,'k--','linewidth',1)
        plotShaded(downsample(tplot,ndown),...
            [downsample(eulerout(k,indplot)+3*sigma_euler_out(k,indplot),ndown);...
            downsample(eulerout(k,indplot)-3*sigma_euler_out(k,indplot),ndown)]*r2d,'r',0.5)
        xlim([0 tplot(end)])
    end
    subplot(4,1,4),hold on, axis([0 t(end) 0 max(norm_error_euler_out(indplot)*r2d*1.1)])
    plot(tplot, norm_error_euler_out(indplot)*r2d)
    xlim([0 tplot(end)])
    %% Quaternion
%         indplot = (1:numel(t));
%         tplot = t(indplot);
%         ndown = 1;
%         % Euler plot
%         figure,
%         subplot(511),title([filt_title,' Quaternion'])
%         for k = 1:4
%             subplot(5,1,k),hold on, axis auto %axis([0 t(end) -2 2])
%             plot(tplot,xhat_out(k,indplot),tplot,xhat_out(k,indplot))
%             plotShaded(downsample(tplot,ndown),...
%                 [downsample(q_out(k,indplot)+3*qsig_out(k,indplot),ndown);...
%                 downsample(q_out(k,indplot)-3*qsig_out(k,indplot),ndown)],'r',0.5)
%         end
%         subplot(5,1,5)
%         plot(tplot, norm_error_qhat_out(indplot))
    
    %%
        figure,
        subplot(411),title(['Bias estimation, ',filt_title])
        for k = 1:3
            subplot(4,1,k),hold on, axis([0 tplot(end) -EKF.bsat.bmax*2*r2d EKF.bsat.bmax*2*r2d])
            plot(tplot,xhat_out(k+4,indplot)*r2d,tplot,bias_out(k,indplot)*r2d)
            plotShaded(downsample(tplot,ndown),...
                [downsample(bias_out(k,indplot)+3*sigma_bias_out(k,indplot),ndown);...
                downsample(bias_out(k,indplot)-3*sigma_bias_out(k,indplot),ndown)]*r2d,'r',0.5)
            xlim([0 tplot(end)])
        end
        subplot(4,1,4), hold on, axis([0 tplot(end) 0 EKF.bsat.bmax*2*r2d])
        plot(tplot, norm_error_bhat_out(indplot)*r2d)
        xlim([0 tplot(end)])
    % subplot(3,1,1),hold on, axis tight
    % plot(tplot,eulerout(2,indplot)*r2d,tplot,eulerhatout(2,indplot)*r2d)
    % plotShaded(tplot,[eulerhatout(2,indplot)+3*sigma_euler(2,indplot); eulerhatout(2,indplot)-3*sigma_euler(2,indplot)]*r2d,'r',0.5)
    % subplot(3,1,1),hold on, axis tight
    % plot(tplot,eulerout(1,indplot)*r2d,tplot,eulerhatout(1,indplot)*r2d)
    % plotShaded(tplot,[eulerhatout(1,indplot)+3*sigma_euler(1,indplot); eulerhatout(3,indplot)-3*sigma_euler(1,indplot)]*r2d,'r',0.5)
    
    %% Mag measurements
%     figure,
%     subplot(311), plot( tplot, magm_out(1,indplot), tplot, mag_out(1,indplot), tplot, mhat_out(1,indplot), tplot, mc_out(1,indplot)),xlim([0 tplot(end)]);
%     subplot(312), plot( tplot, magm_out(2,indplot), tplot, mag_out(2,indplot), tplot, mhat_out(2,indplot), tplot, mc_out(2,indplot)),xlim([0 tplot(end)]);
%     subplot(313), plot( tplot, magm_out(3,indplot), tplot, mag_out(3,indplot), tplot, mhat_out(3,indplot), tplot, mc_out(3,indplot)),xlim([0 tplot(end)]);
%     legend('mag_m', 'mag_r_e_a_l', 'mag_e_s_t', 'mag_c')
%     
    
    %% Acc measurements
%     figure
%     subplot(311), plot( tplot, accm_out(1,indplot), tplot, acc_out(1,indplot), tplot, ahat_out(1,indplot), tplot, ac_out(1,indplot));
%     subplot(312), plot( tplot, accm_out(2,indplot), tplot, acc_out(2,indplot), tplot, ahat_out(2,indplot), tplot, ac_out(2,indplot));
%     subplot(313), plot( tplot, accm_out(3,indplot), tplot, acc_out(3,indplot), tplot, ahat_out(3,indplot), tplot, ac_out(3,indplot))
%     
    %% Calculate norm difference
    
    %% Mag and Acc estimates for Martin/Sarras
%     if ke == 6
%         figure,
%         subplot(311),
%         plot(tplot,xhat_ms_out(1,indplot), tplot, accm_out(1,indplot)),xlim([0 tplot(end)])
%         subplot(312),
%         plot(tplot,xhat_ms_out(2,indplot), tplot, accm_out(2,indplot)),xlim([0 tplot(end)])
%         subplot(313),
%         plot(tplot,xhat_ms_out(3,indplot), tplot, accm_out(3,indplot)),xlim([0 tplot(end)])
%         
%         figure,
%         subplot(311),
%         plot(tplot,xhat_ms_out(4,indplot), tplot, magm_out(1,indplot)),xlim([0 tplot(end)])
%         subplot(312),
%         plot(tplot,xhat_ms_out(5,indplot), tplot, magm_out(2,indplot)),xlim([0 tplot(end)])
%         subplot(313),
%         plot(tplot,xhat_ms_out(6,indplot), tplot, magm_out(3,indplot)),xlim([0 tplot(end)])
%     end
   
 %% Plotty
% GLR statistic
% figure, 
% subplot(211)
% plot(tplot, GLRvec)
% title('GLR statistic')
% hold on
% plot(tplot, chi2vec)
% plot(tplot, ones(size(t))*GLRparams.GLRthr)
% legend({'GLR','\chi^2'},'Location', 'Best'),
% subplot(212)
% plot(tplot, qvec)
% title('Change detection date')

%%
% Compare GLR and X2 statistics
% figstat = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
% title('GLR statistic')
% cnt = 1;
% lw = 0.5;
% for kl = [1 3 5]    
%     subplot(3,2,kl), hold on
%     plot(tplot, GLRvec(cnt,indplot))
%     plot(tplot, chi2vec(cnt,indplot))
%     if kl == 1; 
%         plot(tplot, ones(size(tplot))*GLRparams_1.GLRthr,'k--','linewidth',lw);
%         plot(tplot, ones(size(tplot))*GLRparams_1.chi2thr,'k-.','linewidth',lw);
%     else
%         plot(tplot, ones(size(tplot))*GLRparams_3.GLRthr,'k--','linewidth',lw);
%         plot(tplot, ones(size(tplot))*GLRparams_3.chi2thr,'k-.','linewidth',lw);
%     end
%     legend('l_{GLR}','l_{\chi^2}','l_{GLR,thr}','l_{\chi^2,thr}')
%     xlim([0 tplot(end)])
%     title(titlevec_acc(cnt))
%     subplot(3,2,kl+1), hold on
%     plot(tplot, GLRvec(cnt+3,indplot))
%     plot(tplot, chi2vec(cnt+3,indplot))
%     if kl == 1; 
%         plot(tplot, ones(size(tplot))*GLRparams_1.GLRthr,'k--','linewidth',lw);
%         plot(tplot, ones(size(tplot))*GLRparams_1.chi2thr,'k-.','linewidth',lw);
%     else
%         plot(tplot, ones(size(tplot))*GLRparams_3.GLRthr,'k--','linewidth',lw);
%         plot(tplot, ones(size(tplot))*GLRparams_3.chi2thr,'k-.','linewidth',lw);
%     end
%     legend('l_{GLR}','l_{\chi^2}','l_{GLR,thr}','l_{\chi^2,thr}')
%     xlim([0 tplot(end)])
%     title(titlevec_mag(cnt))
%     cnt = cnt+1;
% end
    %%
%        figstat = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
%     title('GLR Fault date')
%     cnt = 1;
%     for k = [1 3 5]
%         subplot(3,2,k), hold on
%         plot(tplot, qdetvec(cnt,indplot))
%         legend('q_{GLR}','l_{\chi^2}')
%         title(titlevec_acc(cnt))
%         subplot(3,2,k+1), hold on
%         plot(tplot, qdetvec(cnt+3,indplot))
%         legend('q_{GLR}','l_{\chi^2}')
%         title(titlevec_mag(cnt))
%         cnt = cnt+1;
%     end
%% Fault detection booleans
% figbool = figure('units','normalized','outerposition',[0 0 1 1]); set(gcf, 'Color', 'w');
% title(['Detection booleans, ',filt_title])
% cnt = 1;
% for kl=[1 3 5]
%     subplot(3,2,kl), 
%     plot(tplot, acc_status_out(cnt,indplot),'*')
%     xlim([0 tplot(end)])
%     title(titlevec_acc(cnt))
%     subplot(3,2,kl+1), 
%     plot(tplot, mag_status_out(cnt,indplot),'*')
%     xlim([0 tplot(end)])
%     title(titlevec_mag(cnt))
%     cnt = cnt+1;
% end
%% IMU plot for consolidation example
% figure('units','normalized','outerposition',[0 0 1 1]);
% s(1) = subplot(3,1,1);
% plot(tplot, gyro_out(:,indplot)*r2d), xlim(s(1),[0 tplot(end)])
% legend({'$\omega_{m,x}$','$\omega_{m,y}$','$\omega_{m,z}$'},'interpreter','latex')
% ylabel('$\omega_m\,[^{\circ}/s]$','interpreter','latex')
% funFigureProperty
% s(2) = subplot(3,1,2);
% plot(tplot, norm_acc_out(:,indplot)), xlim(s(2),[0 tplot(end)])
% ylabel('$||a_m||\,[m/s^2]$','interpreter','latex')
% funFigureProperty
% s(3) = subplot(3,1,3);
% plot(tplot, norm_mag_out(:,indplot)), xlim(s(3),[0 tplot(end)])
% ylabel('$||m_m||\,[G]$','interpreter','latex')
% xlabel('Time $[s]$','interpreter','latex')
% funFigureProperty
% set(gcf, 'Color', 'w');
% % export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',[num2str(testcase),'_IMU_for_meas_con']),'-pdf',gcf)

%% Bias estimation plot
% figure('units','normalized','outerposition',[0 0 1 1]);
% title(['Bias estimation, ',filt_title])
% ylabel_list = {'$\hat{b}_x \,[^{\circ}]$','$\hat{b}_y \,[^{\circ}]$','$\hat{b}_z \,[^{\circ}]$'};
% for klm = 1:3
% s(klm) = subplot(3,1,klm); hold on,
% plot(tplot, bhat_out(klm,indplot)*r2d,'linewidth',1)
% plot(tplot, bias_lp_out(klm,indplot)*r2d,'linewidth',1)
% 
% % plot(tplot, ones(size(tplot))*EKF.bsat.bmax*r2d,'k--')
% % plot(tplot, -ones(size(tplot))*EKF.bsat.bmax*r2d,'k--')
% xlim(s(klm),[0 tplot(end)])
% ylabel(s(klm),ylabel_list{klm},'interpreter','latex')
% legend({'$\hat{b}_{c}$','$\hat{b}_{LP}$'},'interpreter','latex')
% funFigureProperty
% end
% set(gcf, 'Color', 'w');
% export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',[num2str(testcase),'_bias_est_cons']),'-pdf',gcf)

%% Chi^2 vs GLR plot for norm residual
% fonty = 20;
% figure('units','normalized','outerposition',[0 0 1 1]);
% s1 = subplot(211); plot(tplot, norm_mag_out(indplot), 'linewidth',1),
% xlim([0 tplot(end)])
% ylabel(s1,'Norm [G]','interpreter', 'latex','fontsize', fonty);
% l1 = legend(s1,{'$||m_m||$'},'interpreter','latex', 'fontsize',fonty,'location','best'); funFigureProperty(l1)
% s2 = subplot(212); hold on, plot(tplot, GLRvec(4,indplot), 'linewidth',1), plot(tplot, chi2vec(4,indplot), 'linewidth',1), plot(tplot, ones(size(indplot))*GLRparams_1.GLRthr,'k--', 'linewidth',2)
% % set(gca,'xticklabel','off')
% % set(gca,'xtick',[])
% xlim([0 tplot(end)])
% xlabel(s2,'Time [s]','interpreter', 'latex','fontsize', fonty);
% ylabel(s2,'Test statistic [-]','interpreter', 'latex','fontsize', fonty);
% l2 = legend(s2,{'$l_{GLR}$','$l_{\chi^2}$','$l_{det}$'},'interpreter','latex', 'fontsize',fonty,'location','best'); funFigureProperty(l2)
% axzoom = axes('Position',[0.35 0.38 0.2 0.1]);
% hold on, plot(tplot((tplot>30).*(tplot<70)>0), GLRvec(4,indplot((tplot>30).*(tplot<70)>0)), 'linewidth',1),
% plot(tplot((tplot>30).*(tplot<70)>0), chi2vec(4,indplot((tplot>30).*(tplot<70)>0)), 'linewidth',1),
% plot(tplot((tplot>30).*(tplot<70)>0), ones(size(indplot((tplot>30).*(tplot<70)>0)))*GLRparams_1.GLRthr,'k--', 'linewidth',2),
% 
% funFigureProperty
% ylim([0,50])
% xlim([30 70])
% set(gcf, 'Color', 'w');
% s2.Position = s2.Position + [0 0.05 0 0];
% % export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',[num2str(testcase),'_chi2_vs_glr_norm']),'-pdf',gcf)

%% Mag measurements vs Mag estimation
%     fonty = 20;
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     s1 = subplot(311); plot( tplot, magm_out(1,indplot), tplot, mag_out(1,indplot), tplot, mhat_out(1,indplot), 'linewidth',2),xlim([0 tplot(end)]);
%     ylabel('$m_x\,[G]$','interpreter', 'latex','fontsize', fonty)
%     funFigureProperty
%     s2 = subplot(312); plot( tplot, magm_out(2,indplot), tplot, mag_out(2,indplot), tplot, mhat_out(2,indplot), 'linewidth',2),xlim([0 tplot(end)]);
%     ylabel('$m_y\,[G]$','interpreter', 'latex','fontsize', fonty)
%     funFigureProperty
%     s3 = subplot(313); plot( tplot, magm_out(3,indplot), tplot, mag_out(3,indplot), tplot, mhat_out(3,indplot), 'linewidth',2),xlim([0 tplot(end)]);
%     ylabel('$m_z\,[G]$','interpreter', 'latex','fontsize', fonty)
%     legend({'$m_m$', '$m_B$', '$\hat{m}$'},'interpreter','latex', 'fontsize',20, 'location','best')
%     funFigureProperty
%     set(gcf, 'Color', 'w');
%     linkaxes([s1 s2 s3],'x')
%     export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',[num2str(testcase),'_mag_m_vs_mag_est']),'-pdf',gcf)

%% Mag measurements vs Mag estimation
%     fonty = 20;
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     subplot(311), hold on, plot( tplot, magm_out(1,indplot), tplot, mag_out(1,indplot), 'linewidth',2)
%     for k = 1:4, plot(tplot, squeeze(m_save(k,1,indplot)),'linewidth',1); end
%     xlim([0 tplot(end)]);
%     ylabel('$m_x\,[G]$','interpreter', 'latex','fontsize', fonty)
%     funFigureProperty
%     subplot(312),hold on, plot( tplot, magm_out(2,indplot), tplot, mag_out(2,indplot), 'linewidth',2),
%     for k = 1:4, plot(tplot, squeeze(m_save(k,2,indplot)),'linewidth',1); end
%     xlim([0 tplot(end)]);
%     ylabel('$m_y\,[G]$','interpreter', 'latex','fontsize', fonty)
%     funFigureProperty
%     subplot(313),hold on, plot( tplot, magm_out(3,indplot), tplot, mag_out(3,indplot), 'linewidth',2),
%     for k = 1:4, plot(tplot, squeeze(m_save(k,3,indplot)),'linewidth',1); end
%     xlim([0 tplot(end)]);
%     ylabel('$m_z\,[G]$','interpreter', 'latex','fontsize', fonty)
%     legend({'$m_m$', '$m_B$', '$\hat{m}_{\tau_m = 3}$', '$\hat{m}_{\tau_m = 10}$', '$\hat{m}_{\tau_m = 30}$', '$\hat{m}_{\tau_m = 100}$'},'interpreter','latex', 'fontsize',20, 'location','best')
%     funFigureProperty
%     set(gcf, 'Color', 'w');
%     export_fig(fullfile('E:\Hem_190503\Thesis\Figures\figures_FTKF',[num2str(testcase),'_mag_m_vs_mag_est_tausweep']),'-pdf',gcf)
end
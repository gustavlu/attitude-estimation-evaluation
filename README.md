# Attitude estimation evaluation

IMU data and evaluation scripts for testing attitude estimation algorithms on realistic sensor data collected on a Mikrokopter Mk7 quadrotor. Optitrack reference available for ground truth data.